## Helpful links

- https://distrowatch.com/dwres.php?resource=compare-packages&firstlist=arch&secondlist=debian&firstversions=0&secondversions=0&showall=yes#allpackages
- https://packages.gentoo.org/packages/search?q=psurface
- https://openbuildservice.org/help/manuals/obs-user-guide/art.obs.bg.html
- https://en.opensuse.org/openSUSE:Specfile_guidelines
- https://openbuildservice.org/files/manuals/obs-user-guide.pdf
- https://documentation.suse.com/sbp/all/pdf/SBP-RPM-Packaging_color_en.pdf
- https://www.debian.org/doc/manuals/packaging-tutorial/packaging-tutorial.en.pdf
- https://www.debian.org/doc/manuals/maint-guide/maint-guide.en.pdf
- https://indico.cern.ch/event/163759/sessions/19562/attachments/194723/273146/DebTutorial.pdf
- https://manpages.debian.org/testing/debhelper/index.html
- https://www.debian.org/doc/manuals/developers-reference/tools.html

- [](https://lists.dune-project.org/pipermail/dune/2021-May/thread.html)
- [](https://lists.dune-project.org/pipermail/dune-devel/2021-July/thread.html)

### Position independent code

- [`DCMAKE_POSITION_INDEPENDENT_CODE`](https://cmake.org/cmake/help/latest/prop_tgt/POSITION_INDEPENDENT_CODE.html)
- [What is the idiomatic way in CMAKE to add the `-fPIC` compiler option?](https://stackoverflow.com/a/38297422/9302545)

[dune-mirrors](https://github.com/dune-mirrors)

https://www.mpi-magdeburg.mpg.de/822857/software
[](https://cmake.org/cmake/help/latest/manual/cmake.1.html)
