#!/usr/bin/bash

set -e -u

echo "Bash version: ${BASH_VERSION}"

# ls -d */ --format=single-column
lista=$(ls -d PKGBUILDS/*/ | xargs -L1 basename | tr '\n' ' ')
echo $lista
# cd PKGBUILDS
# pwd
# for d in */; do
#   aur-out-of-date -local "$d".SRCINFO
# done
# echo ${lista[@]} | xargs ls --format=single-column {}
# cd {} && ls | xargs --arg-file=/tmp/text
