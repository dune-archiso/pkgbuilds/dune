```
`configure' configures this package to adapt to many kinds of systems.

Usage: ./configure [OPTION]... [VAR=VALUE]...

To assign environment variables (e.g., CC, CFLAGS...), specify them as
VAR=VALUE.  See below for descriptions of some of the useful variables.

Defaults for the options are specified in brackets.

Configuration:
  -h, --help              display this help and exit
      --help=short        display options specific to this package
      --help=recursive    display the short help of all the included packages
  -V, --version           display version information and exit
  -q, --quiet, --silent   do not print `checking...' messages
      --cache-file=FILE   cache test results in FILE [disabled]
  -C, --config-cache      alias for `--cache-file=config.cache'
  -n, --no-create         do not create output files
      --srcdir=DIR        find the sources in DIR [configure dir or `..']

Installation directories:
  --prefix=PREFIX         install architecture-independent files in PREFIX
                          [/usr/local]
  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
                          [PREFIX]

By default, `make install' will install all the files in
`/usr/local/bin', `/usr/local/lib' etc.  You can specify
an installation prefix other than `/usr/local' using `--prefix',
for instance `--prefix=$HOME'.

For better control, use the options below.

Fine tuning of the installation directories:
  --bindir=DIR            user executables [EPREFIX/bin]
  --sbindir=DIR           system admin executables [EPREFIX/sbin]
  --libexecdir=DIR        program executables [EPREFIX/libexec]
  --sysconfdir=DIR        read-only single-machine data [PREFIX/etc]
  --sharedstatedir=DIR    modifiable architecture-independent data [PREFIX/com]
  --localstatedir=DIR     modifiable single-machine data [PREFIX/var]
  --libdir=DIR            object code libraries [EPREFIX/lib]
  --includedir=DIR        C header files [PREFIX/include]
  --oldincludedir=DIR     C header files for non-gcc [/usr/include]
  --datarootdir=DIR       read-only arch.-independent data root [PREFIX/share]
  --datadir=DIR           read-only architecture-independent data [DATAROOTDIR]
  --infodir=DIR           info documentation [DATAROOTDIR/info]
  --localedir=DIR         locale-dependent data [DATAROOTDIR/locale]
  --mandir=DIR            man documentation [DATAROOTDIR/man]
  --docdir=DIR            documentation root [DATAROOTDIR/doc/PACKAGE]
  --htmldir=DIR           html documentation [DOCDIR]
  --dvidir=DIR            dvi documentation [DOCDIR]
  --pdfdir=DIR            pdf documentation [DOCDIR]
  --psdir=DIR             ps documentation [DOCDIR]

Optional Packages:
  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
  --without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
  --with-gdal-config=GDAL_CONFIG
                          the location of gdal-config
  --without-curses        exclude user interface anyway (default: detect)
  --with-gd               include gd png library
  --with-extdbase         include ext-dbase headers
  --with-netcdf           include netcdf library
  --with-gdal             include gdal library
  --with-debug            use debug option (-g)
  --with-libgstat         compile to libgstat.a
  --with-meschach         use local meschach lib (default: include)
  --with-sparse           include sparse matrix routines
  --with-csf              link to csf library (default: detect)
  --with-grass6=path       define path to grass (look up GISBASE)
  --with-grass            link to grass library
  --with-grass=path       define path to grass (look up GISBASE)
  --with-gsl              include gnu scientific library (gsl)
  --with-gsl=PATH         define path to gsl library subdirs (e.g., /usr/local)

Some influential environment variables:
  CC          C compiler command
  CFLAGS      C compiler flags
  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
              nonstandard directory <lib dir>
  LIBS        libraries to pass to the linker, e.g. -l<library>
  CPPFLAGS    C/C++/Objective C preprocessor flags, e.g. -I<include dir> if
              you have headers in a nonstandard directory <include dir>
  CPP         C preprocessor
  YACC        The `Yet Another C Compiler' implementation to use. Defaults to
              the first program found out of: `bison -y', `byacc', `yacc'.
  YFLAGS      The list of arguments that will be passed by default to $YACC.
              This script will default YFLAGS to the empty string to avoid a
              default value of `-d' given by some make applications.

Use these variables to override the choices made by `configure' or to help
it to find libraries and programs with nonstandard names/locations.

Report bugs to the package provider.
```

```
Packages (41) cairo-1.17.6-2  cfitsio-1:4.1.0-1  fontconfig-2:2.14.0-1  freetype2-2.12.0-2  geos-3.9.1-1  giflib-5.2.1-2  graphite-1:1.3.14-1  harfbuzz-4.2.1-1
              hdf5-1.12.1-1  lcms2-2.13.1-1  libaec-1.0.6-1  libaio-0.3.113-1  libfreexl-1.0.6-2  libgeotiff-1.6.0-4  libjpeg-turbo-2.1.3-1  libnsl-2.0.0-2
              libpng-1.6.37-3  librttopo-1.1.0-4  libspatialite-5.0.1-2  libtiff-4.3.0-2  libx11-1.7.5-1  libxau-1.0.9-3  libxcb-1.14-1  libxdmcp-1.1.3-3
              libxext-1.3.4-3  libxrender-0.9.10-4  lzo-2.10-3  mariadb-libs-10.7.3-1  minizip-1:1.2.12-2  netcdf-4.8.1-2  nspr-4.33-2  nss-3.77-1  openjpeg2-2.4.0-1
              pixman-0.40.0-1  poppler-22.03.0-1  postgresql-libs-14.2-1  proj-8.2.0-1  xcb-proto-1.14.1-5  xerces-c-3.2.3-6  xorgproto-2022.1-1  gdal-3.4.0-5
```

```
[aur@b371cf810c9c standalone]$ make --help
Usage: make [options] [target] ...
Options:
  -b, -m                      Ignored for compatibility.
  -B, --always-make           Unconditionally make all targets.
  -C DIRECTORY, --directory=DIRECTORY
                              Change to DIRECTORY before doing anything.
  -d                          Print lots of debugging information.
  --debug[=FLAGS]             Print various types of debugging information.
  -e, --environment-overrides
                              Environment variables override makefiles.
  -E STRING, --eval=STRING    Evaluate STRING as a makefile statement.
  -f FILE, --file=FILE, --makefile=FILE
                              Read FILE as a makefile.
  -h, --help                  Print this message and exit.
  -i, --ignore-errors         Ignore errors from recipes.
  -I DIRECTORY, --include-dir=DIRECTORY
                              Search DIRECTORY for included makefiles.
  -j [N], --jobs[=N]          Allow N jobs at once; infinite jobs with no arg.
  -k, --keep-going            Keep going when some targets can't be made.
  -l [N], --load-average[=N], --max-load[=N]
                              Don't start multiple jobs unless load is below N.
  -L, --check-symlink-times   Use the latest mtime between symlinks and target.
  -n, --just-print, --dry-run, --recon
                              Don't actually run any recipe; just print them.
  -o FILE, --old-file=FILE, --assume-old=FILE
                              Consider FILE to be very old and don't remake it.
  -O[TYPE], --output-sync[=TYPE]
                              Synchronize output of parallel jobs by TYPE.
  -p, --print-data-base       Print make's internal database.
  -q, --question              Run no recipe; exit status says if up to date.
  -r, --no-builtin-rules      Disable the built-in implicit rules.
  -R, --no-builtin-variables  Disable the built-in variable settings.
  -s, --silent, --quiet       Don't echo recipes.
  --no-silent                 Echo recipes (disable --silent mode).
  -S, --no-keep-going, --stop
                              Turns off -k.
  -t, --touch                 Touch targets instead of remaking them.
  --trace                     Print tracing information.
  -v, --version               Print the version number of make and exit.
  -w, --print-directory       Print the current directory.
  --no-print-directory        Turn off -w, even if it was turned on implicitly.
  -W FILE, --what-if=FILE, --new-file=FILE, --assume-new=FILE
                              Consider FILE to be infinitely new.
  --warn-undefined-variables  Warn when an undefined variable is referenced.

This program built for x86_64-pc-linux-gnu
Report bugs to <bug-make@gnu.org>
```

```
Packages (15) aom-3.3.0-1  dav1d-0.9.2-1  libavif-0.9.3-1  libde265-1.0.8-2  libheif-1.12.0-3  libice-1.0.10-3  libsm-1.2.3-2  libwebp-1.2.2-1  libxpm-3.5.13-2
              libxt-1.2.1-1  libyuv-r2266+eb6e7bb6-1  rav1e-0.4.1-2  svt-av1-0.9.0-2  x265-3.5-3  gd-2.3.3-3
```

[](https://git.pld-linux.org/?p=packages/libcsf.git;a=blob;f=libcsf.spec;h=cf5c4f95e150562c69369c414d005af43a822c63;hb=HEAD)

[](https://stackoverflow.com/questions/66967644/makefile-error-lib-scrt1-o-undefined-reference-to-main)
[](https://stackoverflow.com/questions/34227808/how-to-link-meschach-library-to-a-c-program-using-windows)
[](https://salsa.debian.org/science-team/meschach/-/blob/debian/1.2b-17/debian/rules)
[](https://github.com/LMMS/lmms/issues/4645)
[](https://stackoverflow.com/questions/20673370/why-do-we-write-d-reentrant-while-compiling-c-code-using-threads)
[](https://stackoverflow.com/questions/6329887/compiling-problems-cannot-find-crt1-o)
[](https://stackoverflow.com/questions/39046160/valgrind-memcheck-error-allocating-a-string-memory)
