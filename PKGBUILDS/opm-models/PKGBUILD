# Maintainer: anon at sansorgan.es
pkgname=opm-models
_dunever=2.9.0
pkgver=2023.10
pkgrel=1
#epoch=
pkgdesc="The models module for the Open Porous Media Simulation (OPM) framework"
arch=(x86_64)
url="https://github.com/OPM/${pkgname}"
license=(GPL3)
# groups=('opm')
depends=("opm-grid>=${pkgver}" "dune-alugrid>=${_dunever}" "dune-localfunctions>=${_dunever}") # "dune-grid>=${_dunever}"
makedepends=(cppcheck superlu suitesparse)
# git zoltan "opm-grid>=${pkgver}"
# makedepends=('openmpi' 'texlive-core' 'biber' 'imagemagick' 'python-sphinx' 'doxygen' 'gnu-free-fonts' 'inkscape' 'vc')
checkdepends=()
# 'onetbb: High level abstract threading library'
optdepends=('dune-fem: for dofmanager support')
# DiscreteFunctionSpace
#   'vc: C++ Vectorization library')
#provides=()
#conflicts=()
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=(${pkgname}-release-${pkgver}-final.tar.gz::${url}/archive/release/${pkgver}/final.tar.gz)
# source=("git+${url}.git?signed#tag=release/${pkgver}/final1")
#noextract=()
sha512sums=('ff9fef6a918528c3e8f5c6595cd088fbc52475c6ec4729d19c1ac607bab8357637ef6918862b8356b0ea9fcf02365ef598e87ba5874ee282a1de2676a7e569fa')
# validpgpkeys=('ABE52C516431013C5874107C3F71FE0770D47FFB') # Markus Blatt (applied mathematician and DUNE core developer) <markus@dr-blatt.de>

# prepare() {
#   sed -i 's/^Version: '"${pkgver}"'-final/Version: '"${pkgver}"'/' ${pkgname}-release-${pkgver}-final/dune.module
# }

build() {
  export PATH="${srcdir}/${pkgname}-release-${pkgver}-final/bin:${PATH}"
  cmake \
    -S ${pkgname}-release-${pkgver}-final \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=1 \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DUSE_MPI=1 \
    -Wno-dev
  cmake --build build-cmake --target all #help
}

check() {
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L93
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
  if [ -z "$(ldconfig -p | grep libcuda.so.1)" ]; then
    export OMPI_MCA_opal_warn_on_missing_libcuda=0
  fi
  cmake --build build-cmake --target test-suite tests examples
  ctest --verbose --output-on-failure --test-dir build-cmake # -E "( *parallel|quadrature)"
}

package() {
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install # EmbeddedPython
  install -Dm644 ${pkgname}-release-${pkgver}-final/COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}
