[](https://github.com/OPM/opm-models/blob/master/cmake/OPM-CMake.md)

```
-- The C compiler identification is GNU 11.2.0
-- The CXX compiler identification is GNU 11.2.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/sbin/gcc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/sbin/g++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Performing Test HAVE_C99
-- Performing Test HAVE_C99 - Success
-- Found C99: -std=c99  
-- Found Boost: /usr/lib64/cmake/Boost-1.78.0/BoostConfig.cmake (found suitable version "1.78.0", minimum required is "1.44.0") found components: system unit_test_framework 
-- Performing Test HAVE_CJSON
-- Performing Test HAVE_CJSON - Success
-- Found cjson: /usr/include/cjson  
-- Looking for sgemm_
-- Looking for sgemm_ - not found
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Success
-- Found Threads: TRUE  
-- Looking for sgemm_
-- Looking for sgemm_ - found
-- Found BLAS: /usr/lib/libblas.so  
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - found
-- Found LAPACK: /usr/lib/liblapack.so;/usr/lib/libblas.so  
-- Performing Test HAVE_CXA_DEMANGLE
-- Performing Test HAVE_CXA_DEMANGLE - Success
-- Found MPI_C: /usr/lib/openmpi/libmpi.so (found version "3.1") 
-- Found MPI_CXX: /usr/lib/openmpi/libmpi_cxx.so (found version "3.1") 
-- Found MPI: TRUE (found version "3.1")  
-- Found GMP: /usr/lib/libgmpxx.so  
-- Found dune-common: /usr/include  
-- Looking for MPI_Finalized
-- Looking for MPI_Finalized - found
-- Version 2.8.0 of dune-common from /usr/lib64/cmake/dune-common
-- Found dune-geometry: /usr/include  
-- Version 2.8.0 of dune-geometry from /usr/lib64/cmake/dune-geometry
-- Found dune-uggrid: /usr/include  
-- Version 2.8.0 of dune-uggrid from /usr/lib64/cmake/dune-uggrid
-- Found dune-grid: /usr/include  
-- Version 2.8.0 of dune-grid from /usr/lib64/cmake/dune-grid
-- Found METIS: /usr/lib/libmetis.so  
-- Looking for parmetis.h
-- Looking for parmetis.h - found
-- Found ParMETIS: /usr/include  
-- Performing Test HAVE_UMFPACK_WITHOUT_CHOLMOD
-- Performing Test HAVE_UMFPACK_WITHOUT_CHOLMOD - Success
-- Creating target SuitSparse::umfpack
-- Found SuiteSparse: /usr/lib/libumfpack.so;/usr/lib/libamd.so  
-- Found dune-istl: /usr/include  
-- Version 2.8.0 of dune-istl from /usr/lib64/cmake/dune-istl
-- Could NOT find VALGRIND (missing: VALGRIND_INCLUDE_DIR VALGRIND_PROGRAM) 
-- Found opm-material: /usr/include;/usr/include/cjson  
-- Found dune-localfunctions: /usr/include  
-- Version 2.8.0 of dune-localfunctions from /usr/lib64/cmake/dune-localfunctions
-- Found ZLIB: /usr/lib/libz.so (found version "1.2.12") 
-- Found PTScotch: /usr/include/scotch  
-- Found ZOLTAN: /usr/lib/libzoltan.so  
-- Found dune-alugrid: /usr/include;/usr/include/scotch  
-- Could NOT find dune-fem (missing: dune-fem_DIR)
-- Could NOT find Quadmath (missing: QUADMATH_LIBRARIES HAVE_QUAD) 
-- Could NOT find dune-fem (missing: dune-fem_FOUND HAVE_DUNE_FEM) 
-- Found opm-grid: /usr/lib/libopmgrid.so  
-- Performing Test HAVE_ATTRIBUTE_ALWAYS_INLINE
-- Performing Test HAVE_ATTRIBUTE_ALWAYS_INLINE - Success
-- CMake version: 3.23.0
-- Linux distribution: Arch Linux
-- Target architecture: x86_64
-- Found Git: /usr/sbin/git (found version "2.35.2") 
-- Source code repository: not found!
-- GNU C++ compiler version: 11.2.0
-- Linker: ld 2.38
Processing opm_defaults opm-models
-- Checking to see if CXX compiler accepts flag -Wl,--enable-new-dtags
-- Checking to see if CXX compiler accepts flag -Wl,--enable-new-dtags - yes
-- Precompiled headers: disabled
-- Build type: None
-- Checking to see if CXX compiler accepts flag -mtune=native
-- Checking to see if CXX compiler accepts flag -mtune=native - yes
-- Found OpenMP_C: -fopenmp (found version "4.5") 
-- Found OpenMP_CXX: -fopenmp (found version "4.5") 
-- Found OpenMP: TRUE (found version "4.5")  
-- Checking to see if CXX compiler accepts flag -pthread
-- Checking to see if CXX compiler accepts flag -pthread - yes
-- Found CppCheck: /usr/sbin/cppcheck  
-- Disabling clang-check as CMAKE_EXPORT_COMPILE_COMMANDS is not enabled
-- Generating debug symbols: -ggdb3
-- Looking for strip utility
-- Looking for strip utility - found
-- Performing Test HAVE_DYNAMIC_BOOST_TEST
-- Performing Test HAVE_DYNAMIC_BOOST_TEST - Success
-- Writing config file "/tmp/makepkg/opm-models/src/build-cmake/config.h"...
-- This build defaults to installing in /usr
-- Could NOT find Doxygen (missing: DOXYGEN_EXECUTABLE) 
-- Writing version information to local header project-version.h
-- Configuring done
-- Generating done
-- Build files have been written to: /tmp/makepkg/opm-models/src/build-cmake
The following are some of the valid targets for this Makefile:
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
additionals
attic
check
check-commits
datafiles
distclean
examples
opm-models_prepare
test-suite
tests
art2dgf
co2injection_flash_ecfv
co2injection_flash_ni_ecfv
co2injection_flash_ni_vcfv
co2injection_flash_vcfv
co2injection_immiscible_ecfv
co2injection_immiscible_ni_ecfv
co2injection_immiscible_ni_vcfv
co2injection_immiscible_vcfv
co2injection_ncp_ecfv
co2injection_ncp_ni_ecfv
co2injection_ncp_ni_vcfv
co2injection_ncp_vcfv
co2injection_pvs_ecfv
co2injection_pvs_ni_ecfv
co2injection_pvs_ni_vcfv
co2injection_pvs_vcfv
cuvette_pvs
diffusion_flash
diffusion_ncp
diffusion_pvs
finger_immiscible_ecfv
finger_immiscible_vcfv
fracture_discretefracture
groundwater_immiscible
infiltration_pvs
lens_immiscible_ecfv_ad
lens_immiscible_ecfv_ad_23
lens_immiscible_ecfv_ad_mcu
lens_immiscible_ecfv_ad_trans
lens_immiscible_vcfv_ad
lens_immiscible_vcfv_fd
lens_richards_ecfv
lens_richards_vcfv
obstacle_immiscible
obstacle_ncp
obstacle_pvs
outflow_pvs
powerinjection_darcy_ad
powerinjection_darcy_fd
powerinjection_forchheimer_ad
powerinjection_forchheimer_fd
reservoir_blackoil_ecfv
reservoir_blackoil_vcfv
reservoir_ncp_ecfv
reservoir_ncp_vcfv
test_mpiutil
test_propertysystem
test_quadrature
test_tasklets
tutorial1
waterair_pvs_ni
art2dgf/art2dgf.o
art2dgf/art2dgf.i
art2dgf/art2dgf.s
tests/co2injection_flash_ecfv.o
tests/co2injection_flash_ecfv.i
tests/co2injection_flash_ecfv.s
tests/co2injection_flash_ni_ecfv.o
tests/co2injection_flash_ni_ecfv.i
tests/co2injection_flash_ni_ecfv.s
tests/co2injection_flash_ni_vcfv.o
tests/co2injection_flash_ni_vcfv.i
tests/co2injection_flash_ni_vcfv.s
tests/co2injection_flash_vcfv.o
tests/co2injection_flash_vcfv.i
tests/co2injection_flash_vcfv.s
tests/co2injection_immiscible_ecfv.o
tests/co2injection_immiscible_ecfv.i
tests/co2injection_immiscible_ecfv.s
tests/co2injection_immiscible_ni_ecfv.o
tests/co2injection_immiscible_ni_ecfv.i
tests/co2injection_immiscible_ni_ecfv.s
tests/co2injection_immiscible_ni_vcfv.o
tests/co2injection_immiscible_ni_vcfv.i
tests/co2injection_immiscible_ni_vcfv.s
tests/co2injection_immiscible_vcfv.o
tests/co2injection_immiscible_vcfv.i
tests/co2injection_immiscible_vcfv.s
tests/co2injection_ncp_ecfv.o
tests/co2injection_ncp_ecfv.i
tests/co2injection_ncp_ecfv.s
tests/co2injection_ncp_ni_ecfv.o
tests/co2injection_ncp_ni_ecfv.i
tests/co2injection_ncp_ni_ecfv.s
tests/co2injection_ncp_ni_vcfv.o
tests/co2injection_ncp_ni_vcfv.i
tests/co2injection_ncp_ni_vcfv.s
tests/co2injection_ncp_vcfv.o
tests/co2injection_ncp_vcfv.i
tests/co2injection_ncp_vcfv.s
tests/co2injection_pvs_ecfv.o
tests/co2injection_pvs_ecfv.i
tests/co2injection_pvs_ecfv.s
tests/co2injection_pvs_ni_ecfv.o
tests/co2injection_pvs_ni_ecfv.i
tests/co2injection_pvs_ni_ecfv.s
tests/co2injection_pvs_ni_vcfv.o
tests/co2injection_pvs_ni_vcfv.i
tests/co2injection_pvs_ni_vcfv.s
tests/co2injection_pvs_vcfv.o
tests/co2injection_pvs_vcfv.i
tests/co2injection_pvs_vcfv.s
tests/cuvette_pvs.o
tests/cuvette_pvs.i
tests/cuvette_pvs.s
tests/diffusion_flash.o
tests/diffusion_flash.i
tests/diffusion_flash.s
tests/diffusion_ncp.o
tests/diffusion_ncp.i
tests/diffusion_ncp.s
tests/diffusion_pvs.o
tests/diffusion_pvs.i
tests/diffusion_pvs.s
tests/finger_immiscible_ecfv.o
tests/finger_immiscible_ecfv.i
tests/finger_immiscible_ecfv.s
tests/finger_immiscible_vcfv.o
tests/finger_immiscible_vcfv.i
tests/finger_immiscible_vcfv.s
tests/fracture_discretefracture.o
tests/fracture_discretefracture.i
tests/fracture_discretefracture.s
tests/groundwater_immiscible.o
tests/groundwater_immiscible.i
tests/groundwater_immiscible.s
tests/infiltration_pvs.o
tests/infiltration_pvs.i
tests/infiltration_pvs.s
tests/lens_immiscible_ecfv_ad.o
tests/lens_immiscible_ecfv_ad.i
tests/lens_immiscible_ecfv_ad.s
tests/lens_immiscible_ecfv_ad_23.o
tests/lens_immiscible_ecfv_ad_23.i
tests/lens_immiscible_ecfv_ad_23.s
tests/lens_immiscible_ecfv_ad_cu1.o
tests/lens_immiscible_ecfv_ad_cu1.i
tests/lens_immiscible_ecfv_ad_cu1.s
tests/lens_immiscible_ecfv_ad_cu2.o
tests/lens_immiscible_ecfv_ad_cu2.i
tests/lens_immiscible_ecfv_ad_cu2.s
tests/lens_immiscible_ecfv_ad_main.o
tests/lens_immiscible_ecfv_ad_main.i
tests/lens_immiscible_ecfv_ad_main.s
tests/lens_immiscible_ecfv_ad_trans.o
tests/lens_immiscible_ecfv_ad_trans.i
tests/lens_immiscible_ecfv_ad_trans.s
tests/lens_immiscible_vcfv_ad.o
tests/lens_immiscible_vcfv_ad.i
tests/lens_immiscible_vcfv_ad.s
tests/lens_immiscible_vcfv_fd.o
tests/lens_immiscible_vcfv_fd.i
tests/lens_immiscible_vcfv_fd.s
tests/lens_richards_ecfv.o
tests/lens_richards_ecfv.i
tests/lens_richards_ecfv.s
tests/lens_richards_vcfv.o
tests/lens_richards_vcfv.i
tests/lens_richards_vcfv.s
tests/obstacle_immiscible.o
tests/obstacle_immiscible.i
tests/obstacle_immiscible.s
tests/obstacle_ncp.o
tests/obstacle_ncp.i
tests/obstacle_ncp.s
tests/obstacle_pvs.o
tests/obstacle_pvs.i
tests/obstacle_pvs.s
tests/outflow_pvs.o
tests/outflow_pvs.i
tests/outflow_pvs.s
tests/powerinjection_darcy_ad.o
tests/powerinjection_darcy_ad.i
tests/powerinjection_darcy_ad.s
tests/powerinjection_darcy_fd.o
tests/powerinjection_darcy_fd.i
tests/powerinjection_darcy_fd.s
tests/powerinjection_forchheimer_ad.o
tests/powerinjection_forchheimer_ad.i
tests/powerinjection_forchheimer_ad.s
tests/powerinjection_forchheimer_fd.o
tests/powerinjection_forchheimer_fd.i
tests/powerinjection_forchheimer_fd.s
tests/reservoir_blackoil_ecfv.o
tests/reservoir_blackoil_ecfv.i
tests/reservoir_blackoil_ecfv.s
tests/reservoir_blackoil_vcfv.o
tests/reservoir_blackoil_vcfv.i
tests/reservoir_blackoil_vcfv.s
tests/reservoir_ncp_ecfv.o
tests/reservoir_ncp_ecfv.i
tests/reservoir_ncp_ecfv.s
tests/reservoir_ncp_vcfv.o
tests/reservoir_ncp_vcfv.i
tests/reservoir_ncp_vcfv.s
tests/test_mpiutil.o
tests/test_mpiutil.i
tests/test_mpiutil.s
tests/test_propertysystem.o
tests/test_propertysystem.i
tests/test_propertysystem.s
tests/test_quadrature.o
tests/test_quadrature.i
tests/test_quadrature.s
tests/test_tasklets.o
tests/test_tasklets.i
tests/test_tasklets.s
tests/waterair_pvs_ni.o
tests/waterair_pvs_ni.i
tests/waterair_pvs_ni.s
tutorial/tutorial1.o
tutorial/tutorial1.i
tutorial/tutorial1.s
```

```
test 48
      Start 48: quadrature
48: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--plain" "-e" "test_quadrature" "--"
48: Test timeout computed to be: 1500
48: ######################
48: # Running test 'test_quadrature'
48: ######################
48: executing "./bin/test_quadrature "
48: testing identity mapping...
48: WARNING (ignored): Could not open file 'alugrid.cfg', using default values 0 < [balance] < 1.2, partitioning method 'ALUGRID_SpaceFillingCurve(9)'.
48: 
48: You are using DUNE-ALUGrid, please don't forget to cite the paper:
48: Alkaemper, Dedner, Kloefkorn, Nolte. The DUNE-ALUGrid Module, 2016.
48: 
48: Created parallel ALUGrid<3,3,simplex,nonconforming> from input stream. 
48: 
48: test_quadrature: /usr/include/dune/alugrid/3d/grid.hh:1204: const Dune::ALU3dGrid<dim, dimworld, <anonymous>, <template-parameter-1-4> >::Communications& Dune::ALU3dGrid<dim, dimworld, <anonymous>, <template-parameter-1-4> >::communications() const [with int dim = 3; int dimworld = 3; Dune::ALU3dGridElementType elType = Dune::tetra; Comm = Dune::ALUGridMPIComm; Communications = Dune::ALU3dGridCommunications<3, 3, Dune::tetra, Dune::ALUGridMPIComm>]: Assertion `communications_' failed.
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] *** Process received signal ***
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] Signal: Aborted (6)
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] Signal code:  (-6)
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f62558738e0]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f62558c320c]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f6255873838]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f625585d535]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f625585d45c]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f625586c366]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 6] ./bin/test_quadrature(+0x10b5ca)[0x556e484155ca]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 7] ./bin/test_quadrature(+0x11d5fe)[0x556e484275fe]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 8] ./bin/test_quadrature(+0x11e3b1)[0x556e484283b1]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 9] ./bin/test_quadrature(+0x10c2f2)[0x556e484162f2]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [10] ./bin/test_quadrature(+0x1328a4)[0x556e4843c8a4]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [11] ./bin/test_quadrature(+0x11def6)[0x556e48427ef6]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [12] ./bin/test_quadrature(+0x10bd4c)[0x556e48415d4c]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [13] ./bin/test_quadrature(+0xf7a5a)[0x556e48401a5a]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [14] ./bin/test_quadrature(+0xe4a3c)[0x556e483eea3c]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [15] ./bin/test_quadrature(+0xd81ee)[0x556e483e21ee]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [16] ./bin/test_quadrature(+0xb813b)[0x556e483c213b]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [17] ./bin/test_quadrature(+0xb8b7a)[0x556e483c2b7a]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [18] /usr/lib/libc.so.6(+0x2d290)[0x7f625585e290]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [19] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f625585e34a]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] [20] ./bin/test_quadrature(+0xb7675)[0x556e483c1675]
48: [runner-j2nyww-s-project-26315187-concurrent-0:04510] *** End of error message ***
48: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh: line 82:  4510 Aborted                 (core dumped) "$TEST_BINARY" $TEST_ARGS
48/58 Test #48: quadrature ..........................***Failed    2.45 sec
######################
# Running test 'test_quadrature'
######################
executing "./bin/test_quadrature "
testing identity mapping...
WARNING (ignored): Could not open file 'alugrid.cfg', using default values 0 < [balance] < 1.2, partitioning method 'ALUGRID_SpaceFillingCurve(9)'.
You are using DUNE-ALUGrid, please don't forget to cite the paper:
Alkaemper, Dedner, Kloefkorn, Nolte. The DUNE-ALUGrid Module, 2016.
Created parallel ALUGrid<3,3,simplex,nonconforming> from input stream. 
test_quadrature: /usr/include/dune/alugrid/3d/grid.hh:1204: const Dune::ALU3dGrid<dim, dimworld, <anonymous>, <template-parameter-1-4> >::Communications& Dune::ALU3dGrid<dim, dimworld, <anonymous>, <template-parameter-1-4> >::communications() const [with int dim = 3; int dimworld = 3; Dune::ALU3dGridElementType elType = Dune::tetra; Comm = Dune::ALUGridMPIComm; Communications = Dune::ALU3dGridCommunications<3, 3, Dune::tetra, Dune::ALUGridMPIComm>]: Assertion `communications_' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04510] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04510] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04510] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f62558738e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f62558c320c]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f6255873838]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f625585d535]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f625585d45c]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f625586c366]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 6] ./bin/test_quadrature(+0x10b5ca)[0x556e484155ca]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 7] ./bin/test_quadrature(+0x11d5fe)[0x556e484275fe]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 8] ./bin/test_quadrature(+0x11e3b1)[0x556e484283b1]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [ 9] ./bin/test_quadrature(+0x10c2f2)[0x556e484162f2]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [10] ./bin/test_quadrature(+0x1328a4)[0x556e4843c8a4]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [11] ./bin/test_quadrature(+0x11def6)[0x556e48427ef6]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [12] ./bin/test_quadrature(+0x10bd4c)[0x556e48415d4c]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [13] ./bin/test_quadrature(+0xf7a5a)[0x556e48401a5a]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [14] ./bin/test_quadrature(+0xe4a3c)[0x556e483eea3c]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [15] ./bin/test_quadrature(+0xd81ee)[0x556e483e21ee]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [16] ./bin/test_quadrature(+0xb813b)[0x556e483c213b]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [17] ./bin/test_quadrature(+0xb8b7a)[0x556e483c2b7a]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [18] /usr/lib/libc.so.6(+0x2d290)[0x7f625585e290]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [19] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f625585e34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] [20] ./bin/test_quadrature(+0xb7675)[0x556e483c1675]
[runner-j2nyww-s-project-26315187-concurrent-0:04510] *** End of error message ***
/tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh: line 82:  4510 Aborted                 (core dumped) "$TEST_BINARY" $TEST_ARGS
test 49
      Start 49: co2injection_ncp_ni_ecfv_parallel
49: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--parallel-simulation=4" "-e" "co2injection_ncp_ni_ecfv" "--"
49: Test timeout computed to be: 1500
49: ######################
49: # Running test 'co2injection_ncp_ni_ecfv'
49: ######################
49: executing "mpirun -np "4" ./bin/co2injection_ncp_ni_ecfv "
49: eWoms  will now start the trip. Please sit back, relax and enjoy the ride.
49: Allocating the simulation vanguard
49: Distributing the vanguard's data
49: Allocating the model
49: Allocating the problem
49: Initializing the model
49: Initializing the problem
49: Simulator successfully set up
49: Applying the initial solution of the "co2injection_ncp_ni_ecfv" problem
49: Writing visualization results for the current time step.
49: Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
co2injection_ncp_ni_ecfv: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
49: co2injection_ncp_ni_ecfv: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = Opm::Linear::BorderIndex]: Assertion `!mpiStatus_.MPI_ERROR' failed.
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] *** Process received signal ***
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] Signal: Aborted (6)
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] Signal code:  (-6)
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] co2injection_ncp_ni_ecfv: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] *** Process received signal ***
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] *** Process received signal ***
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] Signal: Aborted (6)
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] Signal code:  (-6)
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] Signal: Aborted (6)
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] Signal code:  (-6)
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f6a3b6528e0]
49: [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7fdc302f08e0]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f6a3b6a220c]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fdc3034020c]
49: [ 0] [runner-j2nyww-s-project-26315187-concurrent-0:04536] /usr/lib/libc.so.6(+0x428e0)[0x7f558cf7d8e0]
49: [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f6a3b652838]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 2] [runner-j2nyww-s-project-26315187-concurrent-0:04533] /usr/lib/libc.so.6(raise+0x18)[0x7fdc302f0838]
49: [ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04536] /usr/lib/libc.so.6(+0x9220c)[0x7f558cfcd20c]
49: [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f6a3b63c535]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04533] /usr/lib/libc.so.6(abort+0xcf)[0x7fdc302da535]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f558cf7d838]
49: [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f6a3b63c45c]
49: co2injection_ncp_ni_ecfv: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] *** Process received signal ***
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] Signal: Aborted (6)
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] Signal code:  (-6)
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f406be748e0]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f406bec420c]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f406be74838]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f406be5e535]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f406be5e45c]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f406be6d366]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 6] ./bin/co2injection_ncp_ni_ecfv(+0xb9e1a)[0x55da9adb1e1a]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 7] ./bin/co2injection_ncp_ni_ecfv(+0x1fef9c)[0x55da9aef6f9c]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 8] ./bin/co2injection_ncp_ni_ecfv(+0x1f7b26)[0x55da9aeefb26]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 9] ./bin/co2injection_ncp_ni_ecfv(+0x1f174f)[0x55da9aee974f]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [10] ./bin/co2injection_ncp_ni_ecfv(+0x1ea838)[0x55da9aee2838]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [11] ./bin/co2injection_ncp_ni_ecfv(+0x1ddfe8)[0x55da9aed5fe8]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [12] ./bin/co2injection_ncp_ni_ecfv(+0x1cfaea)[0x55da9aec7aea]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [13] ./bin/co2injection_ncp_ni_ecfv(+0x1bb0be)[0x55da9aeb30be]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04533] /usr/lib/libc.so.6(+0x2c45c)[0x7fdc302da45c]
49: [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04536] /usr/lib/libc.so.6(abort+0xcf)[0x7f558cf67535]
49: [ 5] [runner-j2nyww-s-project-26315187-concurrent-0:04535] [14] /usr/lib/libc.so.6(+0x3b366)[0x7f6a3b64b366]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fdc302e9366]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] ./bin/co2injection_ncp_ni_ecfv(+0x1a27e1)[0x55da9ae9a7e1]
49: [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 6] /usr/lib/libc.so.6(+0x2c45c)[0x7f558cf6745c]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 6] ./bin/co2injection_ncp_ni_ecfv(+0x207b86)[0x563c4e237b86]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] ./bin/co2injection_ncp_ni_ecfv(+0xb9e1a)[0x55990db7ee1a]
49: [ 5] [runner-j2nyww-s-project-26315187-concurrent-0:04535] /usr/lib/libc.so.6(+0x3b366)[0x7f558cf76366]
49: [15] ./bin/co2injection_ncp_ni_ecfv(+0x180e74)[0x55da9ae78e74]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [ 6] [runner-j2nyww-s-project-26315187-concurrent-0:04534] [runner-j2nyww-s-project-26315187-concurrent-0:04536] ./bin/co2injection_ncp_ni_ecfv(+0xb9e1a)[0x5641f3e18e1a]
49: [ 7] [ 7] ./bin/co2injection_ncp_ni_ecfv(+0x1feff6)[0x563c4e22eff6]
49: ./bin/co2injection_ncp_ni_ecfv(+0x1fef9c)[0x55990dcc3f9c]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [runner-j2nyww-s-project-26315187-concurrent-0:04533] [16] [ 7] ./bin/co2injection_ncp_ni_ecfv(+0x160d07)[0x55da9ae58d07]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 8] [ 8] ./bin/co2injection_ncp_ni_ecfv(+0x1f7b26)[0x55990dcbcb26]
49: ./bin/co2injection_ncp_ni_ecfv(+0x1f7b26)[0x563c4e227b26]
49: ./bin/co2injection_ncp_ni_ecfv(+0x1fef9c)[0x5641f3f5df9c]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [ 8] ./bin/co2injection_ncp_ni_ecfv(+0x1f7b26)[0x5641f3f56b26]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [ 9] ./bin/co2injection_ncp_ni_ecfv(+0x1f174f)[0x5641f3f5074f]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [10] ./bin/co2injection_ncp_ni_ecfv(+0x1ea838)[0x5641f3f49838]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [11] [runner-j2nyww-s-project-26315187-concurrent-0:04535] [17] [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 9] ./bin/co2injection_ncp_ni_ecfv(+0x1f174f)[0x55990dcb674f]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [10] ./bin/co2injection_ncp_ni_ecfv(+0x1ea838)[0x55990dcaf838]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [11] ./bin/co2injection_ncp_ni_ecfv(+0x1ddfe8)[0x55990dca2fe8]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [12] ./bin/co2injection_ncp_ni_ecfv(+0x1cfaea)[0x55990dc94aea]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [13] ./bin/co2injection_ncp_ni_ecfv(+0x1bb0be)[0x55990dc800be]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [14] ./bin/co2injection_ncp_ni_ecfv(+0x1a27e1)[0x55990dc677e1]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [15] ./bin/co2injection_ncp_ni_ecfv(+0x180e74)[0x55990dc45e74]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [16] ./bin/co2injection_ncp_ni_ecfv(+0x160d07)[0x55990dc25d07]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [17] ./bin/co2injection_ncp_ni_ecfv(+0x140649)[0x55990dc05649]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [18] ./bin/co2injection_ncp_ni_ecfv(+0x1250b9)[0x55990dbea0b9]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [19] ./bin/co2injection_ncp_ni_ecfv(+0x10a55d)[0x55990dbcf55d]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [20] ./bin/co2injection_ncp_ni_ecfv(+0xf45f0)[0x55990dbb95f0]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [21] ./bin/co2injection_ncp_ni_ecfv(+0xe4556)[0x55990dba9556]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [22] ./bin/co2injection_ncp_ni_ecfv(+0xd5a57)[0x55990db9aa57]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [23] ./bin/co2injection_ncp_ni_ecfv(+0xc5a36)[0x55990db8aa36]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [24] ./bin/co2injection_ncp_ni_ecfv(+0x8d717)[0x55990db52717]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [25] ./bin/co2injection_ncp_ni_ecfv(+0x140649)[0x55da9ae38649]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [18] ./bin/co2injection_ncp_ni_ecfv(+0x1250b9)[0x55da9ae1d0b9]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [19] ./bin/co2injection_ncp_ni_ecfv(+0x10a55d)[0x55da9ae0255d]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [20] ./bin/co2injection_ncp_ni_ecfv(+0xf45f0)[0x55da9adec5f0]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [21] ./bin/co2injection_ncp_ni_ecfv(+0xe4556)[0x55da9addc556]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [22] ./bin/co2injection_ncp_ni_ecfv(+0xd5a57)[0x55da9adcda57]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [23] ./bin/co2injection_ncp_ni_ecfv(+0xc5a36)[0x55da9adbda36]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [24] ./bin/co2injection_ncp_ni_ecfv(+0x8d717)[0x55da9ad85717]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [25] ./bin/co2injection_ncp_ni_ecfv(+0x8d014)[0x55da9ad85014]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f406be5f290]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f406be5f34a]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] [28] ./bin/co2injection_ncp_ni_ecfv(+0x8cb85)[0x55da9ad84b85]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04535] *** End of error message ***
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 9] ./bin/co2injection_ncp_ni_ecfv(+0x1f174f)[0x563c4e22174f]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [10] ./bin/co2injection_ncp_ni_ecfv(+0x1ea838)[0x563c4e21a838]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [11] ./bin/co2injection_ncp_ni_ecfv(+0x1ddfe8)[0x563c4e20dfe8]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [12] ./bin/co2injection_ncp_ni_ecfv(+0x1cfaea)[0x563c4e1ffaea]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [13] ./bin/co2injection_ncp_ni_ecfv(+0x1bb0be)[0x563c4e1eb0be]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [14] ./bin/co2injection_ncp_ni_ecfv(+0x1a27e1)[0x563c4e1d27e1]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [15] ./bin/co2injection_ncp_ni_ecfv(+0x180e74)[0x563c4e1b0e74]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [16] ./bin/co2injection_ncp_ni_ecfv(+0x160d07)[0x563c4e190d07]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [17] ./bin/co2injection_ncp_ni_ecfv(+0x140649)[0x563c4e170649]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [18] ./bin/co2injection_ncp_ni_ecfv(+0x1250b9)[0x563c4e1550b9]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [19] ./bin/co2injection_ncp_ni_ecfv(+0x10a55d)[0x563c4e13a55d]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [20] ./bin/co2injection_ncp_ni_ecfv(+0xf45f0)[0x563c4e1245f0]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [21] ./bin/co2injection_ncp_ni_ecfv(+0xe4556)[0x563c4e114556]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [22] ./bin/co2injection_ncp_ni_ecfv(+0xd5a57)[0x563c4e105a57]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [23] ./bin/co2injection_ncp_ni_ecfv(+0xc5a36)[0x563c4e0f5a36]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [24] ./bin/co2injection_ncp_ni_ecfv(+0x8d717)[0x563c4e0bd717]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [25] ./bin/co2injection_ncp_ni_ecfv(+0x8d014)[0x563c4e0bd014]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f6a3b63d290]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f6a3b63d34a]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] [28] ./bin/co2injection_ncp_ni_ecfv(+0x8cb85)[0x563c4e0bcb85]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04536] *** End of error message ***
49: ./bin/co2injection_ncp_ni_ecfv(+0x1ddfe8)[0x5641f3f3cfe8]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [12] ./bin/co2injection_ncp_ni_ecfv(+0x1cfaea)[0x5641f3f2eaea]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [13] ./bin/co2injection_ncp_ni_ecfv(+0x1bb0be)[0x5641f3f1a0be]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [14] ./bin/co2injection_ncp_ni_ecfv(+0x1a27e1)[0x5641f3f017e1]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [15] ./bin/co2injection_ncp_ni_ecfv(+0x180e74)[0x5641f3edfe74]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [16] ./bin/co2injection_ncp_ni_ecfv(+0x160d07)[0x5641f3ebfd07]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [17] ./bin/co2injection_ncp_ni_ecfv(+0x140649)[0x5641f3e9f649]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [18] ./bin/co2injection_ncp_ni_ecfv(+0x1250b9)[0x5641f3e840b9]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [19] ./bin/co2injection_ncp_ni_ecfv(+0x10a55d)[0x5641f3e6955d]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [20] ./bin/co2injection_ncp_ni_ecfv(+0xf45f0)[0x5641f3e535f0]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [21] ./bin/co2injection_ncp_ni_ecfv(+0xe4556)[0x5641f3e43556]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [22] ./bin/co2injection_ncp_ni_ecfv(+0xd5a57)[0x5641f3e34a57]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [23] ./bin/co2injection_ncp_ni_ecfv(+0xc5a36)[0x5641f3e24a36]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [24] ./bin/co2injection_ncp_ni_ecfv(+0x8d717)[0x5641f3dec717]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [25] ./bin/co2injection_ncp_ni_ecfv(+0x8d014)[0x5641f3dec014]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f558cf68290]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f558cf6834a]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] [28] ./bin/co2injection_ncp_ni_ecfv(+0x8cb85)[0x5641f3debb85]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04533] *** End of error message ***
49: ./bin/co2injection_ncp_ni_ecfv(+0x8d014)[0x55990db52014]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fdc302db290]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fdc302db34a]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] [28] ./bin/co2injection_ncp_ni_ecfv(+0x8cb85)[0x55990db51b85]
49: [runner-j2nyww-s-project-26315187-concurrent-0:04534] *** End of error message ***
49: --------------------------------------------------------------------------
49: Primary job  terminated normally, but 1 process returned
49: a non-zero exit code. Per user-direction, the job has been aborted.
49: --------------------------------------------------------------------------
49: --------------------------------------------------------------------------
49: mpirun noticed that process rank 3 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
49: --------------------------------------------------------------------------
49: Executing the binary failed!
49/58 Test #49: co2injection_ncp_ni_ecfv_parallel ...***Failed   11.37 sec
######################
# Running test 'co2injection_ncp_ni_ecfv'
######################
executing "mpirun -np "4" ./bin/co2injection_ncp_ni_ecfv "
eWoms  will now start the trip. Please sit back, relax and enjoy the ride.
Allocating the simulation vanguard
Distributing the vanguard's data
Allocating the model
Allocating the problem
Initializing the model
Initializing the problem
Simulator successfully set up
Applying the initial solution of the "co2injection_ncp_ni_ecfv" problem
Writing visualization results for the current time step.
Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
co2injection_ncp_ni_ecfv: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
co2injection_ncp_ni_ecfv: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = Opm::Linear::BorderIndex]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04533] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04533] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04533] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04533] co2injection_ncp_ni_ecfv: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04534] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04536] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04534] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04534] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04536] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04536] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f6a3b6528e0]
[ 0] /usr/lib/libc.so.6(+0x428e0)[0x7fdc302f08e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f6a3b6a220c]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fdc3034020c]
[ 0] [runner-j2nyww-s-project-26315187-concurrent-0:04536] /usr/lib/libc.so.6(+0x428e0)[0x7f558cf7d8e0]
[ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f6a3b652838]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 2] [runner-j2nyww-s-project-26315187-concurrent-0:04533] /usr/lib/libc.so.6(raise+0x18)[0x7fdc302f0838]
[ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04536] /usr/lib/libc.so.6(+0x9220c)[0x7f558cfcd20c]
[ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f6a3b63c535]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04533] /usr/lib/libc.so.6(abort+0xcf)[0x7fdc302da535]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f558cf7d838]
[ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f6a3b63c45c]
co2injection_ncp_ni_ecfv: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04535] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04535] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04535] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f406be748e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f406bec420c]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f406be74838]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f406be5e535]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f406be5e45c]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f406be6d366]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 6] ./bin/co2injection_ncp_ni_ecfv(+0xb9e1a)[0x55da9adb1e1a]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 7] ./bin/co2injection_ncp_ni_ecfv(+0x1fef9c)[0x55da9aef6f9c]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 8] ./bin/co2injection_ncp_ni_ecfv(+0x1f7b26)[0x55da9aeefb26]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [ 9] ./bin/co2injection_ncp_ni_ecfv(+0x1f174f)[0x55da9aee974f]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [10] ./bin/co2injection_ncp_ni_ecfv(+0x1ea838)[0x55da9aee2838]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [11] ./bin/co2injection_ncp_ni_ecfv(+0x1ddfe8)[0x55da9aed5fe8]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [12] ./bin/co2injection_ncp_ni_ecfv(+0x1cfaea)[0x55da9aec7aea]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [13] ./bin/co2injection_ncp_ni_ecfv(+0x1bb0be)[0x55da9aeb30be]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04533] /usr/lib/libc.so.6(+0x2c45c)[0x7fdc302da45c]
[ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04536] /usr/lib/libc.so.6(abort+0xcf)[0x7f558cf67535]
[ 5] [runner-j2nyww-s-project-26315187-concurrent-0:04535] [14] /usr/lib/libc.so.6(+0x3b366)[0x7f6a3b64b366]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fdc302e9366]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] ./bin/co2injection_ncp_ni_ecfv(+0x1a27e1)[0x55da9ae9a7e1]
[ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 6] /usr/lib/libc.so.6(+0x2c45c)[0x7f558cf6745c]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 6] ./bin/co2injection_ncp_ni_ecfv(+0x207b86)[0x563c4e237b86]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] ./bin/co2injection_ncp_ni_ecfv(+0xb9e1a)[0x55990db7ee1a]
[ 5] [runner-j2nyww-s-project-26315187-concurrent-0:04535] /usr/lib/libc.so.6(+0x3b366)[0x7f558cf76366]
[15] ./bin/co2injection_ncp_ni_ecfv(+0x180e74)[0x55da9ae78e74]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [ 6] [runner-j2nyww-s-project-26315187-concurrent-0:04534] [runner-j2nyww-s-project-26315187-concurrent-0:04536] ./bin/co2injection_ncp_ni_ecfv(+0xb9e1a)[0x5641f3e18e1a]
[ 7] [ 7] ./bin/co2injection_ncp_ni_ecfv(+0x1feff6)[0x563c4e22eff6]
./bin/co2injection_ncp_ni_ecfv(+0x1fef9c)[0x55990dcc3f9c]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [runner-j2nyww-s-project-26315187-concurrent-0:04533] [16] [ 7] ./bin/co2injection_ncp_ni_ecfv(+0x160d07)[0x55da9ae58d07]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 8] [ 8] ./bin/co2injection_ncp_ni_ecfv(+0x1f7b26)[0x55990dcbcb26]
./bin/co2injection_ncp_ni_ecfv(+0x1f7b26)[0x563c4e227b26]
./bin/co2injection_ncp_ni_ecfv(+0x1fef9c)[0x5641f3f5df9c]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [ 8] ./bin/co2injection_ncp_ni_ecfv(+0x1f7b26)[0x5641f3f56b26]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [ 9] ./bin/co2injection_ncp_ni_ecfv(+0x1f174f)[0x5641f3f5074f]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [10] ./bin/co2injection_ncp_ni_ecfv(+0x1ea838)[0x5641f3f49838]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [11] [runner-j2nyww-s-project-26315187-concurrent-0:04535] [17] [runner-j2nyww-s-project-26315187-concurrent-0:04534] [ 9] ./bin/co2injection_ncp_ni_ecfv(+0x1f174f)[0x55990dcb674f]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [10] ./bin/co2injection_ncp_ni_ecfv(+0x1ea838)[0x55990dcaf838]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [11] ./bin/co2injection_ncp_ni_ecfv(+0x1ddfe8)[0x55990dca2fe8]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [12] ./bin/co2injection_ncp_ni_ecfv(+0x1cfaea)[0x55990dc94aea]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [13] ./bin/co2injection_ncp_ni_ecfv(+0x1bb0be)[0x55990dc800be]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [14] ./bin/co2injection_ncp_ni_ecfv(+0x1a27e1)[0x55990dc677e1]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [15] ./bin/co2injection_ncp_ni_ecfv(+0x180e74)[0x55990dc45e74]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [16] ./bin/co2injection_ncp_ni_ecfv(+0x160d07)[0x55990dc25d07]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [17] ./bin/co2injection_ncp_ni_ecfv(+0x140649)[0x55990dc05649]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [18] ./bin/co2injection_ncp_ni_ecfv(+0x1250b9)[0x55990dbea0b9]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [19] ./bin/co2injection_ncp_ni_ecfv(+0x10a55d)[0x55990dbcf55d]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [20] ./bin/co2injection_ncp_ni_ecfv(+0xf45f0)[0x55990dbb95f0]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [21] ./bin/co2injection_ncp_ni_ecfv(+0xe4556)[0x55990dba9556]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [22] ./bin/co2injection_ncp_ni_ecfv(+0xd5a57)[0x55990db9aa57]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [23] ./bin/co2injection_ncp_ni_ecfv(+0xc5a36)[0x55990db8aa36]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [24] ./bin/co2injection_ncp_ni_ecfv(+0x8d717)[0x55990db52717]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [25] ./bin/co2injection_ncp_ni_ecfv(+0x140649)[0x55da9ae38649]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [18] ./bin/co2injection_ncp_ni_ecfv(+0x1250b9)[0x55da9ae1d0b9]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [19] ./bin/co2injection_ncp_ni_ecfv(+0x10a55d)[0x55da9ae0255d]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [20] ./bin/co2injection_ncp_ni_ecfv(+0xf45f0)[0x55da9adec5f0]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [21] ./bin/co2injection_ncp_ni_ecfv(+0xe4556)[0x55da9addc556]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [22] ./bin/co2injection_ncp_ni_ecfv(+0xd5a57)[0x55da9adcda57]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [23] ./bin/co2injection_ncp_ni_ecfv(+0xc5a36)[0x55da9adbda36]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [24] ./bin/co2injection_ncp_ni_ecfv(+0x8d717)[0x55da9ad85717]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [25] ./bin/co2injection_ncp_ni_ecfv(+0x8d014)[0x55da9ad85014]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f406be5f290]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f406be5f34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] [28] ./bin/co2injection_ncp_ni_ecfv(+0x8cb85)[0x55da9ad84b85]
[runner-j2nyww-s-project-26315187-concurrent-0:04535] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [ 9] ./bin/co2injection_ncp_ni_ecfv(+0x1f174f)[0x563c4e22174f]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [10] ./bin/co2injection_ncp_ni_ecfv(+0x1ea838)[0x563c4e21a838]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [11] ./bin/co2injection_ncp_ni_ecfv(+0x1ddfe8)[0x563c4e20dfe8]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [12] ./bin/co2injection_ncp_ni_ecfv(+0x1cfaea)[0x563c4e1ffaea]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [13] ./bin/co2injection_ncp_ni_ecfv(+0x1bb0be)[0x563c4e1eb0be]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [14] ./bin/co2injection_ncp_ni_ecfv(+0x1a27e1)[0x563c4e1d27e1]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [15] ./bin/co2injection_ncp_ni_ecfv(+0x180e74)[0x563c4e1b0e74]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [16] ./bin/co2injection_ncp_ni_ecfv(+0x160d07)[0x563c4e190d07]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [17] ./bin/co2injection_ncp_ni_ecfv(+0x140649)[0x563c4e170649]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [18] ./bin/co2injection_ncp_ni_ecfv(+0x1250b9)[0x563c4e1550b9]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [19] ./bin/co2injection_ncp_ni_ecfv(+0x10a55d)[0x563c4e13a55d]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [20] ./bin/co2injection_ncp_ni_ecfv(+0xf45f0)[0x563c4e1245f0]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [21] ./bin/co2injection_ncp_ni_ecfv(+0xe4556)[0x563c4e114556]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [22] ./bin/co2injection_ncp_ni_ecfv(+0xd5a57)[0x563c4e105a57]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [23] ./bin/co2injection_ncp_ni_ecfv(+0xc5a36)[0x563c4e0f5a36]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [24] ./bin/co2injection_ncp_ni_ecfv(+0x8d717)[0x563c4e0bd717]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [25] ./bin/co2injection_ncp_ni_ecfv(+0x8d014)[0x563c4e0bd014]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f6a3b63d290]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f6a3b63d34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] [28] ./bin/co2injection_ncp_ni_ecfv(+0x8cb85)[0x563c4e0bcb85]
[runner-j2nyww-s-project-26315187-concurrent-0:04536] *** End of error message ***
./bin/co2injection_ncp_ni_ecfv(+0x1ddfe8)[0x5641f3f3cfe8]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [12] ./bin/co2injection_ncp_ni_ecfv(+0x1cfaea)[0x5641f3f2eaea]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [13] ./bin/co2injection_ncp_ni_ecfv(+0x1bb0be)[0x5641f3f1a0be]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [14] ./bin/co2injection_ncp_ni_ecfv(+0x1a27e1)[0x5641f3f017e1]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [15] ./bin/co2injection_ncp_ni_ecfv(+0x180e74)[0x5641f3edfe74]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [16] ./bin/co2injection_ncp_ni_ecfv(+0x160d07)[0x5641f3ebfd07]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [17] ./bin/co2injection_ncp_ni_ecfv(+0x140649)[0x5641f3e9f649]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [18] ./bin/co2injection_ncp_ni_ecfv(+0x1250b9)[0x5641f3e840b9]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [19] ./bin/co2injection_ncp_ni_ecfv(+0x10a55d)[0x5641f3e6955d]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [20] ./bin/co2injection_ncp_ni_ecfv(+0xf45f0)[0x5641f3e535f0]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [21] ./bin/co2injection_ncp_ni_ecfv(+0xe4556)[0x5641f3e43556]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [22] ./bin/co2injection_ncp_ni_ecfv(+0xd5a57)[0x5641f3e34a57]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [23] ./bin/co2injection_ncp_ni_ecfv(+0xc5a36)[0x5641f3e24a36]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [24] ./bin/co2injection_ncp_ni_ecfv(+0x8d717)[0x5641f3dec717]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [25] ./bin/co2injection_ncp_ni_ecfv(+0x8d014)[0x5641f3dec014]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f558cf68290]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f558cf6834a]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] [28] ./bin/co2injection_ncp_ni_ecfv(+0x8cb85)[0x5641f3debb85]
[runner-j2nyww-s-project-26315187-concurrent-0:04533] *** End of error message ***
./bin/co2injection_ncp_ni_ecfv(+0x8d014)[0x55990db52014]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fdc302db290]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fdc302db34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] [28] ./bin/co2injection_ncp_ni_ecfv(+0x8cb85)[0x55990db51b85]
[runner-j2nyww-s-project-26315187-concurrent-0:04534] *** End of error message ***
--------------------------------------------------------------------------
Primary job  terminated normally, but 1 process returned
a non-zero exit code. Per user-direction, the job has been aborted.
--------------------------------------------------------------------------
--------------------------------------------------------------------------
mpirun noticed that process rank 3 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
--------------------------------------------------------------------------
Executing the binary failed!
test 50
      Start 50: obstacle_immiscible_parallel
50: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--parallel-simulation=4" "-e" "obstacle_immiscible" "--" "--end-time=1" "--initial-time-step-size=1"
50: Test timeout computed to be: 1500
50: ######################
50: # Running test 'obstacle_immiscible'
50: ######################
50: executing "mpirun -np "4" ./bin/obstacle_immiscible --end-time=1 --initial-time-step-size=1"
50: eWoms  will now start the trip. Please sit back, relax and enjoy the ride.
50: Allocating the simulation vanguard
50: Distributing the vanguard's data
50: Allocating the model
50: Allocating the problem
50: Initializing the model
50: Initializing the problem
50: Simulator successfully set up
50: Applying the initial solution of the "obstacle_immiscible" problem
50: Writing visualization results for the current time step.
50: Begin time step 1. Start time: 0 seconds, step size: 1 seconds
obstacle_immiscible: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
50: obstacle_immiscible: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = Opm::Linear::BorderIndex]: Assertion `!mpiStatus_.MPI_ERROR' failed.
50: obstacle_immiscible: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] *** Process received signal ***
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] *** Process received signal ***
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] Signal: Aborted (6)
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] Signal code:  (-6)
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [runner-j2nyww-s-project-26315187-concurrent-0:04563] Signal: Aborted (6)
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] Signal code:  (-6)
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [runner-j2nyww-s-project-26315187-concurrent-0:04562] *** Process received signal ***
50: [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7fdd7124f8e0]
50: [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7fc6ec1708e0]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] Signal: Aborted (6)
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] Signal code:  (-6)
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fdd7129f20c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fc6ec1c020c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7fdd7124f838]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7fc6ec170838]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7fdd71239535]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7fc6ec15a535]
50: [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7fdd7123945c]
50: [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f6eb352e8e0]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04563] /usr/lib/libc.so.6(+0x2c45c)[0x7fc6ec15a45c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [ 5] [ 1] /usr/lib/libc.so.6(+0x3b366)[0x7fdd71248366]
50: /usr/lib/libc.so.6(+0x9220c)[0x7f6eb357e20c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fc6ec169366]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 6] [runner-j2nyww-s-project-26315187-concurrent-0:04562] ./bin/obstacle_immiscible(+0x190970)[0x55eb52f31970]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 2] [ 6] /usr/lib/libc.so.6(raise+0x18)[0x7f6eb352e838]
50: ./bin/obstacle_immiscible(+0x87cc8)[0x55e30a520cc8]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 7] [runner-j2nyww-s-project-26315187-concurrent-0:04565] ./bin/obstacle_immiscible(+0x18bf4e)[0x55eb52f2cf4e]
50: [ 3] [ 7] /usr/lib/libc.so.6(abort+0xcf)[0x7f6eb3518535]
50: ./bin/obstacle_immiscible(+0x18bef4)[0x55e30a624ef4]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 8] [runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04565] ./bin/obstacle_immiscible(+0x186874)[0x55eb52f27874]
50: [ 4] [ 8] ./bin/obstacle_immiscible(+0x186874)[0x55e30a61f874]
50: /usr/lib/libc.so.6(+0x2c45c)[0x7f6eb351845c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 9] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [runner-j2nyww-s-project-26315187-concurrent-0:04562] ./bin/obstacle_immiscible(+0x181f75)[0x55eb52f22f75]
50: [ 9] [ 5] ./bin/obstacle_immiscible(+0x181f75)[0x55e30a61af75]
50: /usr/lib/libc.so.6(+0x3b366)[0x7f6eb3527366]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04563] [10] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 6] ./bin/obstacle_immiscible(+0x17d4cc)[0x55eb52f1e4cc]
50: [10] ./bin/obstacle_immiscible(+0x17d4cc)[0x55e30a6164cc]
50: ./bin/obstacle_immiscible(+0x87cc8)[0x5602bcd5ccc8]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [11] ./bin/obstacle_immiscible(+0x176910)[0x55eb52f17910]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 7] [11] ./bin/obstacle_immiscible(+0x176910)[0x55e30a60f910]
50: ./bin/obstacle_immiscible(+0x18bef4)[0x5602bce60ef4]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [12] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [12] ./bin/obstacle_immiscible(+0x16e08c)[0x55eb52f0f08c]
50: ./bin/obstacle_immiscible(+0x16e08c)[0x55e30a60708c]
50: obstacle_immiscible: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] *** Process received signal ***
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] Signal: Aborted (6)
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] Signal code:  (-6)
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f82e7ac18e0]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f82e7b1120c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f82e7ac1838]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f82e7aab535]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f82e7aab45c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f82e7aba366]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 6] ./bin/obstacle_immiscible(+0x87cc8)[0x564f76024cc8]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 7] ./bin/obstacle_immiscible(+0x18bef4)[0x564f76128ef4]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 8] ./bin/obstacle_immiscible(+0x186874)[0x564f76123874]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 9] ./bin/obstacle_immiscible(+0x181f75)[0x564f7611ef75]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [10] ./bin/obstacle_immiscible(+0x17d4cc)[0x564f7611a4cc]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [11] ./bin/obstacle_immiscible(+0x176910)[0x564f76113910]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [12] ./bin/obstacle_immiscible(+0x16e08c)[0x564f7610b08c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [13] ./bin/obstacle_immiscible(+0x161548)[0x564f760fe548]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [14] ./bin/obstacle_immiscible(+0x14e4ab)[0x564f760eb4ab]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [15] ./bin/obstacle_immiscible(+0x138eb0)[0x564f760d5eb0]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [16] ./bin/obstacle_immiscible(+0x1224ab)[0x564f760bf4ab]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [17] ./bin/obstacle_immiscible(+0x106671)[0x564f760a3671]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [18] ./bin/obstacle_immiscible(+0xedb03)[0x564f7608ab03]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [19] ./bin/obstacle_immiscible(+0xd3725)[0x564f76070725]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [20] ./bin/obstacle_immiscible(+0xbef62)[0x564f7605bf62]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [21] ./bin/obstacle_immiscible(+0xaef16)[0x564f7604bf16]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [22] ./bin/obstacle_immiscible(+0xa10bb)[0x564f7603e0bb]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [23] [runner-j2nyww-s-project-26315187-concurrent-0:04562] [ 8] [runner-j2nyww-s-project-26315187-concurrent-0:04563] ./bin/obstacle_immiscible(+0x186874)[0x5602bce5b874]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [13] [13] ./bin/obstacle_immiscible(+0x161548)[0x55eb52f02548]
50: ./bin/obstacle_immiscible(+0x161548)[0x55e30a5fa548]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [ 9] ./bin/obstacle_immiscible(+0x181f75)[0x5602bce56f75]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [14] [runner-j2nyww-s-project-26315187-concurrent-0:04565] ./bin/obstacle_immiscible(+0x14e4ab)[0x55eb52eef4ab]
50: [14] ./bin/obstacle_immiscible(+0x14e4ab)[0x55e30a5e74ab]
50: ./bin/obstacle_immiscible(+0x920b2)[0x564f7602f0b2]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [10] ./bin/obstacle_immiscible(+0x17d4cc)[0x5602bce524cc]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [11] ./bin/obstacle_immiscible(+0x176910)[0x5602bce4b910]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [12] ./bin/obstacle_immiscible(+0x16e08c)[0x5602bce4308c]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [13] ./bin/obstacle_immiscible(+0x161548)[0x5602bce36548]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [14] ./bin/obstacle_immiscible(+0x14e4ab)[0x5602bce234ab]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [15] ./bin/obstacle_immiscible(+0x138eb0)[0x5602bce0deb0]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [16] ./bin/obstacle_immiscible(+0x1224ab)[0x5602bcdf74ab]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [17] ./bin/obstacle_immiscible(+0x106671)[0x5602bcddb671]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [18] ./bin/obstacle_immiscible(+0xedb03)[0x5602bcdc2b03]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [19] ./bin/obstacle_immiscible(+0xd3725)[0x5602bcda8725]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [20] ./bin/obstacle_immiscible(+0xbef62)[0x5602bcd93f62]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [21] ./bin/obstacle_immiscible(+0xaef16)[0x5602bcd83f16]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [22] ./bin/obstacle_immiscible(+0xa10bb)[0x5602bcd760bb]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [23] ./bin/obstacle_immiscible(+0x920b2)[0x5602bcd670b2]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [24] ./bin/obstacle_immiscible(+0x6f5aa)[0x5602bcd445aa]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [25] ./bin/obstacle_immiscible(+0x6eea7)[0x5602bcd43ea7]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f6eb3519290]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f6eb351934a]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] [28] ./bin/obstacle_immiscible(+0x6ea55)[0x5602bcd43a55]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04562] *** End of error message ***
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [24] ./bin/obstacle_immiscible(+0x6f5aa)[0x564f7600c5aa]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [25] ./bin/obstacle_immiscible(+0x6eea7)[0x564f7600bea7]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f82e7aac290]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f82e7aac34a]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] [28] ./bin/obstacle_immiscible(+0x6ea55)[0x564f7600ba55]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04564] *** End of error message ***
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [15] ./bin/obstacle_immiscible(+0x138eb0)[0x55eb52ed9eb0]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [16] ./bin/obstacle_immiscible(+0x1224ab)[0x55eb52ec34ab]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [17] ./bin/obstacle_immiscible(+0x106671)[0x55eb52ea7671]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [18] ./bin/obstacle_immiscible(+0xedb03)[0x55eb52e8eb03]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [19] ./bin/obstacle_immiscible(+0xd3725)[0x55eb52e74725]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [20] ./bin/obstacle_immiscible(+0xbef62)[0x55eb52e5ff62]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [21] ./bin/obstacle_immiscible(+0xaef16)[0x55eb52e4ff16]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [22] ./bin/obstacle_immiscible(+0xa10bb)[0x55eb52e420bb]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [23] ./bin/obstacle_immiscible(+0x920b2)[0x55eb52e330b2]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [24] ./bin/obstacle_immiscible(+0x6f5aa)[0x55eb52e105aa]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [25] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [15] ./bin/obstacle_immiscible(+0x138eb0)[0x55e30a5d1eb0]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [16] ./bin/obstacle_immiscible(+0x1224ab)[0x55e30a5bb4ab]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [17] ./bin/obstacle_immiscible(+0x106671)[0x55e30a59f671]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [18] ./bin/obstacle_immiscible(+0xedb03)[0x55e30a586b03]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [19] ./bin/obstacle_immiscible(+0xd3725)[0x55e30a56c725]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [20] ./bin/obstacle_immiscible(+0xbef62)[0x55e30a557f62]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [21] ./bin/obstacle_immiscible(+0xaef16)[0x55e30a547f16]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [22] ./bin/obstacle_immiscible(+0xa10bb)[0x55e30a53a0bb]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [23] ./bin/obstacle_immiscible(+0x920b2)[0x55e30a52b0b2]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [24] ./bin/obstacle_immiscible(+0x6f5aa)[0x55e30a5085aa]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [25] ./bin/obstacle_immiscible(+0x6eea7)[0x55e30a507ea7]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fc6ec15b290]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fc6ec15b34a]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] [28] ./bin/obstacle_immiscible(+0x6ea55)[0x55e30a507a55]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04565] *** End of error message ***
50: ./bin/obstacle_immiscible(+0x6eea7)[0x55eb52e0fea7]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fdd7123a290]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fdd7123a34a]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] [28] ./bin/obstacle_immiscible(+0x6ea55)[0x55eb52e0fa55]
50: [runner-j2nyww-s-project-26315187-concurrent-0:04563] *** End of error message ***
50: --------------------------------------------------------------------------
50: Primary job  terminated normally, but 1 process returned
50: a non-zero exit code. Per user-direction, the job has been aborted.
50: --------------------------------------------------------------------------
50: --------------------------------------------------------------------------
50: mpirun noticed that process rank 3 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
50: --------------------------------------------------------------------------
50: Executing the binary failed!
50/58 Test #50: obstacle_immiscible_parallel ........***Failed    2.09 sec
######################
# Running test 'obstacle_immiscible'
######################
executing "mpirun -np "4" ./bin/obstacle_immiscible --end-time=1 --initial-time-step-size=1"
eWoms  will now start the trip. Please sit back, relax and enjoy the ride.
Allocating the simulation vanguard
Distributing the vanguard's data
Allocating the model
Allocating the problem
Initializing the model
Initializing the problem
Simulator successfully set up
Applying the initial solution of the "obstacle_immiscible" problem
Writing visualization results for the current time step.
Begin time step 1. Start time: 0 seconds, step size: 1 seconds
obstacle_immiscible: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
obstacle_immiscible: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = Opm::Linear::BorderIndex]: Assertion `!mpiStatus_.MPI_ERROR' failed.
obstacle_immiscible: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04563] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04565] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04565] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04565] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [runner-j2nyww-s-project-26315187-concurrent-0:04563] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04563] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [runner-j2nyww-s-project-26315187-concurrent-0:04562] *** Process received signal ***
[ 0] /usr/lib/libc.so.6(+0x428e0)[0x7fdd7124f8e0]
[ 0] /usr/lib/libc.so.6(+0x428e0)[0x7fc6ec1708e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04562] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fdd7129f20c]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fc6ec1c020c]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7fdd7124f838]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7fc6ec170838]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7fdd71239535]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7fc6ec15a535]
[ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7fdd7123945c]
[ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f6eb352e8e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04563] /usr/lib/libc.so.6(+0x2c45c)[0x7fc6ec15a45c]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [ 5] [ 1] /usr/lib/libc.so.6(+0x3b366)[0x7fdd71248366]
/usr/lib/libc.so.6(+0x9220c)[0x7f6eb357e20c]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fc6ec169366]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 6] [runner-j2nyww-s-project-26315187-concurrent-0:04562] ./bin/obstacle_immiscible(+0x190970)[0x55eb52f31970]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 2] [ 6] /usr/lib/libc.so.6(raise+0x18)[0x7f6eb352e838]
./bin/obstacle_immiscible(+0x87cc8)[0x55e30a520cc8]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 7] [runner-j2nyww-s-project-26315187-concurrent-0:04565] ./bin/obstacle_immiscible(+0x18bf4e)[0x55eb52f2cf4e]
[ 3] [ 7] /usr/lib/libc.so.6(abort+0xcf)[0x7f6eb3518535]
./bin/obstacle_immiscible(+0x18bef4)[0x55e30a624ef4]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 8] [runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04565] ./bin/obstacle_immiscible(+0x186874)[0x55eb52f27874]
[ 4] [ 8] ./bin/obstacle_immiscible(+0x186874)[0x55e30a61f874]
/usr/lib/libc.so.6(+0x2c45c)[0x7f6eb351845c]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [ 9] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [runner-j2nyww-s-project-26315187-concurrent-0:04562] ./bin/obstacle_immiscible(+0x181f75)[0x55eb52f22f75]
[ 9] [ 5] ./bin/obstacle_immiscible(+0x181f75)[0x55e30a61af75]
/usr/lib/libc.so.6(+0x3b366)[0x7f6eb3527366]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04563] [10] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 6] ./bin/obstacle_immiscible(+0x17d4cc)[0x55eb52f1e4cc]
[10] ./bin/obstacle_immiscible(+0x17d4cc)[0x55e30a6164cc]
./bin/obstacle_immiscible(+0x87cc8)[0x5602bcd5ccc8]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [11] ./bin/obstacle_immiscible(+0x176910)[0x55eb52f17910]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [ 7] [11] ./bin/obstacle_immiscible(+0x176910)[0x55e30a60f910]
./bin/obstacle_immiscible(+0x18bef4)[0x5602bce60ef4]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [12] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [12] ./bin/obstacle_immiscible(+0x16e08c)[0x55eb52f0f08c]
./bin/obstacle_immiscible(+0x16e08c)[0x55e30a60708c]
obstacle_immiscible: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04564] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04564] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04564] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f82e7ac18e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f82e7b1120c]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f82e7ac1838]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f82e7aab535]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f82e7aab45c]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f82e7aba366]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 6] ./bin/obstacle_immiscible(+0x87cc8)[0x564f76024cc8]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 7] ./bin/obstacle_immiscible(+0x18bef4)[0x564f76128ef4]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 8] ./bin/obstacle_immiscible(+0x186874)[0x564f76123874]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [ 9] ./bin/obstacle_immiscible(+0x181f75)[0x564f7611ef75]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [10] ./bin/obstacle_immiscible(+0x17d4cc)[0x564f7611a4cc]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [11] ./bin/obstacle_immiscible(+0x176910)[0x564f76113910]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [12] ./bin/obstacle_immiscible(+0x16e08c)[0x564f7610b08c]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [13] ./bin/obstacle_immiscible(+0x161548)[0x564f760fe548]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [14] ./bin/obstacle_immiscible(+0x14e4ab)[0x564f760eb4ab]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [15] ./bin/obstacle_immiscible(+0x138eb0)[0x564f760d5eb0]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [16] ./bin/obstacle_immiscible(+0x1224ab)[0x564f760bf4ab]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [17] ./bin/obstacle_immiscible(+0x106671)[0x564f760a3671]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [18] ./bin/obstacle_immiscible(+0xedb03)[0x564f7608ab03]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [19] ./bin/obstacle_immiscible(+0xd3725)[0x564f76070725]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [20] ./bin/obstacle_immiscible(+0xbef62)[0x564f7605bf62]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [21] ./bin/obstacle_immiscible(+0xaef16)[0x564f7604bf16]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [22] ./bin/obstacle_immiscible(+0xa10bb)[0x564f7603e0bb]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [23] [runner-j2nyww-s-project-26315187-concurrent-0:04562] [ 8] [runner-j2nyww-s-project-26315187-concurrent-0:04563] ./bin/obstacle_immiscible(+0x186874)[0x5602bce5b874]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [13] [13] ./bin/obstacle_immiscible(+0x161548)[0x55eb52f02548]
./bin/obstacle_immiscible(+0x161548)[0x55e30a5fa548]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [ 9] ./bin/obstacle_immiscible(+0x181f75)[0x5602bce56f75]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [14] [runner-j2nyww-s-project-26315187-concurrent-0:04565] ./bin/obstacle_immiscible(+0x14e4ab)[0x55eb52eef4ab]
[14] ./bin/obstacle_immiscible(+0x14e4ab)[0x55e30a5e74ab]
./bin/obstacle_immiscible(+0x920b2)[0x564f7602f0b2]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [10] ./bin/obstacle_immiscible(+0x17d4cc)[0x5602bce524cc]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [11] ./bin/obstacle_immiscible(+0x176910)[0x5602bce4b910]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [12] ./bin/obstacle_immiscible(+0x16e08c)[0x5602bce4308c]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [13] ./bin/obstacle_immiscible(+0x161548)[0x5602bce36548]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [14] ./bin/obstacle_immiscible(+0x14e4ab)[0x5602bce234ab]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [15] ./bin/obstacle_immiscible(+0x138eb0)[0x5602bce0deb0]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [16] ./bin/obstacle_immiscible(+0x1224ab)[0x5602bcdf74ab]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [17] ./bin/obstacle_immiscible(+0x106671)[0x5602bcddb671]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [18] ./bin/obstacle_immiscible(+0xedb03)[0x5602bcdc2b03]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [19] ./bin/obstacle_immiscible(+0xd3725)[0x5602bcda8725]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [20] ./bin/obstacle_immiscible(+0xbef62)[0x5602bcd93f62]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [21] ./bin/obstacle_immiscible(+0xaef16)[0x5602bcd83f16]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [22] ./bin/obstacle_immiscible(+0xa10bb)[0x5602bcd760bb]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [23] ./bin/obstacle_immiscible(+0x920b2)[0x5602bcd670b2]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [24] ./bin/obstacle_immiscible(+0x6f5aa)[0x5602bcd445aa]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [25] ./bin/obstacle_immiscible(+0x6eea7)[0x5602bcd43ea7]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f6eb3519290]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f6eb351934a]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] [28] ./bin/obstacle_immiscible(+0x6ea55)[0x5602bcd43a55]
[runner-j2nyww-s-project-26315187-concurrent-0:04562] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [24] ./bin/obstacle_immiscible(+0x6f5aa)[0x564f7600c5aa]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [25] ./bin/obstacle_immiscible(+0x6eea7)[0x564f7600bea7]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f82e7aac290]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f82e7aac34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] [28] ./bin/obstacle_immiscible(+0x6ea55)[0x564f7600ba55]
[runner-j2nyww-s-project-26315187-concurrent-0:04564] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [15] ./bin/obstacle_immiscible(+0x138eb0)[0x55eb52ed9eb0]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [16] ./bin/obstacle_immiscible(+0x1224ab)[0x55eb52ec34ab]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [17] ./bin/obstacle_immiscible(+0x106671)[0x55eb52ea7671]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [18] ./bin/obstacle_immiscible(+0xedb03)[0x55eb52e8eb03]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [19] ./bin/obstacle_immiscible(+0xd3725)[0x55eb52e74725]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [20] ./bin/obstacle_immiscible(+0xbef62)[0x55eb52e5ff62]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [21] ./bin/obstacle_immiscible(+0xaef16)[0x55eb52e4ff16]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [22] ./bin/obstacle_immiscible(+0xa10bb)[0x55eb52e420bb]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [23] ./bin/obstacle_immiscible(+0x920b2)[0x55eb52e330b2]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [24] ./bin/obstacle_immiscible(+0x6f5aa)[0x55eb52e105aa]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [25] [runner-j2nyww-s-project-26315187-concurrent-0:04565] [15] ./bin/obstacle_immiscible(+0x138eb0)[0x55e30a5d1eb0]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [16] ./bin/obstacle_immiscible(+0x1224ab)[0x55e30a5bb4ab]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [17] ./bin/obstacle_immiscible(+0x106671)[0x55e30a59f671]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [18] ./bin/obstacle_immiscible(+0xedb03)[0x55e30a586b03]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [19] ./bin/obstacle_immiscible(+0xd3725)[0x55e30a56c725]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [20] ./bin/obstacle_immiscible(+0xbef62)[0x55e30a557f62]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [21] ./bin/obstacle_immiscible(+0xaef16)[0x55e30a547f16]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [22] ./bin/obstacle_immiscible(+0xa10bb)[0x55e30a53a0bb]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [23] ./bin/obstacle_immiscible(+0x920b2)[0x55e30a52b0b2]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [24] ./bin/obstacle_immiscible(+0x6f5aa)[0x55e30a5085aa]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [25] ./bin/obstacle_immiscible(+0x6eea7)[0x55e30a507ea7]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fc6ec15b290]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fc6ec15b34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] [28] ./bin/obstacle_immiscible(+0x6ea55)[0x55e30a507a55]
[runner-j2nyww-s-project-26315187-concurrent-0:04565] *** End of error message ***
./bin/obstacle_immiscible(+0x6eea7)[0x55eb52e0fea7]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fdd7123a290]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fdd7123a34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] [28] ./bin/obstacle_immiscible(+0x6ea55)[0x55eb52e0fa55]
[runner-j2nyww-s-project-26315187-concurrent-0:04563] *** End of error message ***
--------------------------------------------------------------------------
Primary job  terminated normally, but 1 process returned
a non-zero exit code. Per user-direction, the job has been aborted.
--------------------------------------------------------------------------
--------------------------------------------------------------------------
mpirun noticed that process rank 3 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
--------------------------------------------------------------------------
Executing the binary failed!
test 51
      Start 51: lens_immiscible_vcfv_fd_parallel
51: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--parallel-simulation=4" "-e" "lens_immiscible_vcfv_fd" "--" "--end-time=250" "--initial-time-step-size=250"
51: Test timeout computed to be: 1500
51: ######################
51: # Running test 'lens_immiscible_vcfv_fd'
51: ######################
51: executing "mpirun -np "4" ./bin/lens_immiscible_vcfv_fd --end-time=250 --initial-time-step-size=250"
51: Ground remediation problem where a dense oil infiltrates an aquifer with an
51: embedded low-permability lens. This is the binary for the isothermal variant
51: using finite differenceand the vertex centered finite volume discretization
51: 
51: Allocating the simulation vanguard
51: Distributing the vanguard's data
51: Allocating the model
51: Allocating the problem
51: Initializing the model
51: Initializing the problem
51: Simulator successfully set up
51: Applying the initial solution of the "lens_immiscible_vcfv_fd" problem
51: Writing visualization results for the current time step.
51: Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
lens_immiscible_vcfv_fd: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = Opm::Linear::BorderIndex]: Assertion `!mpiStatus_.MPI_ERROR' failed.
51: lens_immiscible_vcfv_fd: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] *** Process received signal ***
51: lens_immiscible_vcfv_fd: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] Signal: Aborted (6)
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] Signal code:  (-6)
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [runner-j2nyww-s-project-26315187-concurrent-0:04593] *** Process received signal ***
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] Signal: Aborted (6)
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] Signal code:  (-6)
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7ff3a7d508e0]
51: [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f3eada7f8e0]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [runner-j2nyww-s-project-26315187-concurrent-0:04592] *** Process received signal ***
51: [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7ff3a7da020c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04592] Signal: Aborted (6)
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] Signal code:  (-6)
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 2] /usr/lib/libc.so.6(+0x9220c)[0x7f3eadacf20c]
51: /usr/lib/libc.so.6(raise+0x18)[0x7ff3a7d50838]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 2] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 3] /usr/lib/libc.so.6(raise+0x18)[0x7f3eada7f838]
51: /usr/lib/libc.so.6(abort+0xcf)[0x7ff3a7d3a535]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f3eada69535]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7ff3a7d3a45c]
51: [ 0] [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f3eada6945c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 5] /usr/lib/libc.so.6(+0x428e0)[0x7f3e00ae88e0]
51: /usr/lib/libc.so.6(+0x3b366)[0x7ff3a7d49366]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f3eada78366]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 6] /usr/lib/libc.so.6(+0x9220c)[0x7f3e00b3820c]
51: ./bin/lens_immiscible_vcfv_fd(+0xb139c)[0x55b1ae42e39c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 6] ./bin/lens_immiscible_vcfv_fd(+0xb139c)[0x563b2a1a139c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f3e00ae8838]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 7] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 3] ./bin/lens_immiscible_vcfv_fd(+0x1a0ad4)[0x55b1ae51dad4]
51: /usr/lib/libc.so.6(abort+0xcf)[0x7f3e00ad2535]
51: [ 7] ./bin/lens_immiscible_vcfv_fd(+0x1a0ad4)[0x563b2a290ad4]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f3e00ad245c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [runner-j2nyww-s-project-26315187-concurrent-0:04593] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 8] [ 8] ./bin/lens_immiscible_vcfv_fd(+0x19aa1c)[0x55b1ae517a1c]
51: ./bin/lens_immiscible_vcfv_fd(+0x19aa1c)[0x563b2a28aa1c]
51: [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f3e00ae1366]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 9] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 9] ./bin/lens_immiscible_vcfv_fd(+0x196443)[0x563b2a286443]
51: [ 6] ./bin/lens_immiscible_vcfv_fd(+0x196443)[0x55b1ae513443]
51: ./bin/lens_immiscible_vcfv_fd(+0x1a5370)[0x555a82af3370]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [10] [10] ./bin/lens_immiscible_vcfv_fd(+0x191c2c)[0x563b2a281c2c]
51: ./bin/lens_immiscible_vcfv_fd(+0x191c2c)[0x55b1ae50ec2c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 7] ./bin/lens_immiscible_vcfv_fd(+0x1a0b2e)[0x555a82aeeb2e]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [11] [runner-j2nyww-s-project-26315187-concurrent-0:04594] ./bin/lens_immiscible_vcfv_fd(+0x18b45c)[0x563b2a27b45c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [11] [ 8] ./bin/lens_immiscible_vcfv_fd(+0x18b45c)[0x55b1ae50845c]
51: ./bin/lens_immiscible_vcfv_fd(+0x19aa1c)[0x555a82ae8a1c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [12] [runner-j2nyww-s-project-26315187-concurrent-0:04594] ./bin/lens_immiscible_vcfv_fd(+0x18337a)[0x563b2a27337a]
51: [12] ./bin/lens_immiscible_vcfv_fd(+0x18337a)[0x55b1ae50037a]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 9] ./bin/lens_immiscible_vcfv_fd(+0x196443)[0x555a82ae4443]
51: lens_immiscible_vcfv_fd: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] *** Process received signal ***
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] Signal: Aborted (6)
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] Signal code:  (-6)
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7efcbeddb8e0]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7efcbee2b20c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7efcbeddb838]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7efcbedc5535]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7efcbedc545c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7efcbedd4366]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 6] ./bin/lens_immiscible_vcfv_fd(+0xb139c)[0x55ff424d939c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 7] ./bin/lens_immiscible_vcfv_fd(+0x1a0ad4)[0x55ff425c8ad4]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 8] ./bin/lens_immiscible_vcfv_fd(+0x19aa1c)[0x55ff425c2a1c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 9] ./bin/lens_immiscible_vcfv_fd(+0x196443)[0x55ff425be443]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [10] ./bin/lens_immiscible_vcfv_fd(+0x191c2c)[0x55ff425b9c2c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [11] ./bin/lens_immiscible_vcfv_fd(+0x18b45c)[0x55ff425b345c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [12] ./bin/lens_immiscible_vcfv_fd(+0x18337a)[0x55ff425ab37a]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [13] ./bin/lens_immiscible_vcfv_fd(+0x1776ea)[0x55ff4259f6ea]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [14] ./bin/lens_immiscible_vcfv_fd(+0x165b15)[0x55ff4258db15]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [15] ./bin/lens_immiscible_vcfv_fd(+0x152238)[0x55ff4257a238]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [16] ./bin/lens_immiscible_vcfv_fd(+0x13fe2b)[0x55ff42567e2b]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [17] ./bin/lens_immiscible_vcfv_fd(+0x129e7d)[0x55ff42551e7d]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [18] ./bin/lens_immiscible_vcfv_fd(+0x116ff7)[0x55ff4253eff7]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [19] ./bin/lens_immiscible_vcfv_fd(+0x1014af)[0x55ff425294af]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [20] ./bin/lens_immiscible_vcfv_fd(+0xefcc4)[0x55ff42517cc4]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [21] ./bin/lens_immiscible_vcfv_fd(+0xdfcea)[0x55ff42507cea]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [22] ./bin/lens_immiscible_vcfv_fd(+0xd122b)[0x55ff424f922b]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [23] ./bin/lens_immiscible_vcfv_fd(+0xc054c)[0x55ff424e854c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [24] ./bin/lens_immiscible_vcfv_fd(+0x915ae)[0x55ff424b95ae]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [25] ./bin/lens_immiscible_vcfv_fd(+0x90eab)[0x55ff424b8eab]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [26] /usr/lib/libc.so.6(+0x2d290)[0x7efcbedc6290]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7efcbedc634a]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] [28] ./bin/lens_immiscible_vcfv_fd(+0x90a15)[0x55ff424b8a15]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04591] *** End of error message ***
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [13] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [13] ./bin/lens_immiscible_vcfv_fd(+0x1776ea)[0x563b2a2676ea]
51: [10] ./bin/lens_immiscible_vcfv_fd(+0x1776ea)[0x55b1ae4f46ea]
51: ./bin/lens_immiscible_vcfv_fd(+0x191c2c)[0x555a82adfc2c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [14] [runner-j2nyww-s-project-26315187-concurrent-0:04594] ./bin/lens_immiscible_vcfv_fd(+0x165b15)[0x563b2a255b15]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [14] [11] ./bin/lens_immiscible_vcfv_fd(+0x18b45c)[0x555a82ad945c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [12] ./bin/lens_immiscible_vcfv_fd(+0x18337a)[0x555a82ad137a]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [13] ./bin/lens_immiscible_vcfv_fd(+0x1776ea)[0x555a82ac56ea]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [14] ./bin/lens_immiscible_vcfv_fd(+0x165b15)[0x555a82ab3b15]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [15] ./bin/lens_immiscible_vcfv_fd(+0x152238)[0x555a82aa0238]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [16] ./bin/lens_immiscible_vcfv_fd(+0x13fe2b)[0x555a82a8de2b]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [17] ./bin/lens_immiscible_vcfv_fd(+0x129e7d)[0x555a82a77e7d]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [18] ./bin/lens_immiscible_vcfv_fd(+0x116ff7)[0x555a82a64ff7]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [19] ./bin/lens_immiscible_vcfv_fd(+0x1014af)[0x555a82a4f4af]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [20] ./bin/lens_immiscible_vcfv_fd(+0xefcc4)[0x555a82a3dcc4]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [21] ./bin/lens_immiscible_vcfv_fd(+0xdfcea)[0x555a82a2dcea]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [22] ./bin/lens_immiscible_vcfv_fd(+0xd122b)[0x555a82a1f22b]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [23] ./bin/lens_immiscible_vcfv_fd(+0xc054c)[0x555a82a0e54c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [24] ./bin/lens_immiscible_vcfv_fd(+0x915ae)[0x555a829df5ae]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [25] ./bin/lens_immiscible_vcfv_fd(+0x90eab)[0x555a829deeab]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f3e00ad3290]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f3e00ad334a]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] [28] ./bin/lens_immiscible_vcfv_fd(+0x90a15)[0x555a829dea15]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04592] *** End of error message ***
51: ./bin/lens_immiscible_vcfv_fd(+0x165b15)[0x55b1ae4e2b15]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [15] ./bin/lens_immiscible_vcfv_fd(+0x152238)[0x563b2a242238]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [16] ./bin/lens_immiscible_vcfv_fd(+0x13fe2b)[0x563b2a22fe2b]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [17] ./bin/lens_immiscible_vcfv_fd(+0x129e7d)[0x563b2a219e7d]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [18] ./bin/lens_immiscible_vcfv_fd(+0x116ff7)[0x563b2a206ff7]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [19] ./bin/lens_immiscible_vcfv_fd(+0x1014af)[0x563b2a1f14af]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [20] ./bin/lens_immiscible_vcfv_fd(+0xefcc4)[0x563b2a1dfcc4]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [21] ./bin/lens_immiscible_vcfv_fd(+0xdfcea)[0x563b2a1cfcea]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [22] ./bin/lens_immiscible_vcfv_fd(+0xd122b)[0x563b2a1c122b]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [23] ./bin/lens_immiscible_vcfv_fd(+0xc054c)[0x563b2a1b054c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [24] ./bin/lens_immiscible_vcfv_fd(+0x915ae)[0x563b2a1815ae]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [25] ./bin/lens_immiscible_vcfv_fd(+0x90eab)[0x563b2a180eab]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f3eada6a290]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f3eada6a34a]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] [28] ./bin/lens_immiscible_vcfv_fd(+0x90a15)[0x563b2a180a15]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04593] *** End of error message ***
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [15] ./bin/lens_immiscible_vcfv_fd(+0x152238)[0x55b1ae4cf238]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [16] ./bin/lens_immiscible_vcfv_fd(+0x13fe2b)[0x55b1ae4bce2b]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [17] ./bin/lens_immiscible_vcfv_fd(+0x129e7d)[0x55b1ae4a6e7d]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [18] ./bin/lens_immiscible_vcfv_fd(+0x116ff7)[0x55b1ae493ff7]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [19] ./bin/lens_immiscible_vcfv_fd(+0x1014af)[0x55b1ae47e4af]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [20] ./bin/lens_immiscible_vcfv_fd(+0xefcc4)[0x55b1ae46ccc4]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [21] ./bin/lens_immiscible_vcfv_fd(+0xdfcea)[0x55b1ae45ccea]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [22] ./bin/lens_immiscible_vcfv_fd(+0xd122b)[0x55b1ae44e22b]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [23] ./bin/lens_immiscible_vcfv_fd(+0xc054c)[0x55b1ae43d54c]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [24] ./bin/lens_immiscible_vcfv_fd(+0x915ae)[0x55b1ae40e5ae]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [25] ./bin/lens_immiscible_vcfv_fd(+0x90eab)[0x55b1ae40deab]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [26] /usr/lib/libc.so.6(+0x2d290)[0x7ff3a7d3b290]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7ff3a7d3b34a]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] [28] ./bin/lens_immiscible_vcfv_fd(+0x90a15)[0x55b1ae40da15]
51: [runner-j2nyww-s-project-26315187-concurrent-0:04594] *** End of error message ***
51: --------------------------------------------------------------------------
51: Primary job  terminated normally, but 1 process returned
51: a non-zero exit code. Per user-direction, the job has been aborted.
51: --------------------------------------------------------------------------
51: --------------------------------------------------------------------------
51: mpirun noticed that process rank 1 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
51: --------------------------------------------------------------------------
51: Executing the binary failed!
51/58 Test #51: lens_immiscible_vcfv_fd_parallel ....***Failed    1.79 sec
######################
# Running test 'lens_immiscible_vcfv_fd'
######################
executing "mpirun -np "4" ./bin/lens_immiscible_vcfv_fd --end-time=250 --initial-time-step-size=250"
Ground remediation problem where a dense oil infiltrates an aquifer with an
embedded low-permability lens. This is the binary for the isothermal variant
using finite differenceand the vertex centered finite volume discretization
Allocating the simulation vanguard
Distributing the vanguard's data
Allocating the model
Allocating the problem
Initializing the model
Initializing the problem
Simulator successfully set up
Applying the initial solution of the "lens_immiscible_vcfv_fd" problem
Writing visualization results for the current time step.
Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
lens_immiscible_vcfv_fd: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = Opm::Linear::BorderIndex]: Assertion `!mpiStatus_.MPI_ERROR' failed.
lens_immiscible_vcfv_fd: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04594] *** Process received signal ***
lens_immiscible_vcfv_fd: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04594] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04594] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [runner-j2nyww-s-project-26315187-concurrent-0:04593] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04593] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04593] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7ff3a7d508e0]
[ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f3eada7f8e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [runner-j2nyww-s-project-26315187-concurrent-0:04592] *** Process received signal ***
[ 1] /usr/lib/libc.so.6(+0x9220c)[0x7ff3a7da020c]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04592] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04592] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 2] /usr/lib/libc.so.6(+0x9220c)[0x7f3eadacf20c]
/usr/lib/libc.so.6(raise+0x18)[0x7ff3a7d50838]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 2] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 3] /usr/lib/libc.so.6(raise+0x18)[0x7f3eada7f838]
/usr/lib/libc.so.6(abort+0xcf)[0x7ff3a7d3a535]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f3eada69535]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7ff3a7d3a45c]
[ 0] [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f3eada6945c]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 5] /usr/lib/libc.so.6(+0x428e0)[0x7f3e00ae88e0]
/usr/lib/libc.so.6(+0x3b366)[0x7ff3a7d49366]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f3eada78366]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 6] /usr/lib/libc.so.6(+0x9220c)[0x7f3e00b3820c]
./bin/lens_immiscible_vcfv_fd(+0xb139c)[0x55b1ae42e39c]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 6] ./bin/lens_immiscible_vcfv_fd(+0xb139c)[0x563b2a1a139c]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f3e00ae8838]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 7] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [runner-j2nyww-s-project-26315187-concurrent-0:04593] [ 3] ./bin/lens_immiscible_vcfv_fd(+0x1a0ad4)[0x55b1ae51dad4]
/usr/lib/libc.so.6(abort+0xcf)[0x7f3e00ad2535]
[ 7] ./bin/lens_immiscible_vcfv_fd(+0x1a0ad4)[0x563b2a290ad4]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f3e00ad245c]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [runner-j2nyww-s-project-26315187-concurrent-0:04593] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 8] [ 8] ./bin/lens_immiscible_vcfv_fd(+0x19aa1c)[0x55b1ae517a1c]
./bin/lens_immiscible_vcfv_fd(+0x19aa1c)[0x563b2a28aa1c]
[ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f3e00ae1366]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [ 9] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 9] ./bin/lens_immiscible_vcfv_fd(+0x196443)[0x563b2a286443]
[ 6] ./bin/lens_immiscible_vcfv_fd(+0x196443)[0x55b1ae513443]
./bin/lens_immiscible_vcfv_fd(+0x1a5370)[0x555a82af3370]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [10] [10] ./bin/lens_immiscible_vcfv_fd(+0x191c2c)[0x563b2a281c2c]
./bin/lens_immiscible_vcfv_fd(+0x191c2c)[0x55b1ae50ec2c]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 7] ./bin/lens_immiscible_vcfv_fd(+0x1a0b2e)[0x555a82aeeb2e]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [11] [runner-j2nyww-s-project-26315187-concurrent-0:04594] ./bin/lens_immiscible_vcfv_fd(+0x18b45c)[0x563b2a27b45c]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [11] [ 8] ./bin/lens_immiscible_vcfv_fd(+0x18b45c)[0x55b1ae50845c]
./bin/lens_immiscible_vcfv_fd(+0x19aa1c)[0x555a82ae8a1c]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [12] [runner-j2nyww-s-project-26315187-concurrent-0:04594] ./bin/lens_immiscible_vcfv_fd(+0x18337a)[0x563b2a27337a]
[12] ./bin/lens_immiscible_vcfv_fd(+0x18337a)[0x55b1ae50037a]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [ 9] ./bin/lens_immiscible_vcfv_fd(+0x196443)[0x555a82ae4443]
lens_immiscible_vcfv_fd: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04591] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04591] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04591] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7efcbeddb8e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7efcbee2b20c]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7efcbeddb838]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7efcbedc5535]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7efcbedc545c]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7efcbedd4366]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 6] ./bin/lens_immiscible_vcfv_fd(+0xb139c)[0x55ff424d939c]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 7] ./bin/lens_immiscible_vcfv_fd(+0x1a0ad4)[0x55ff425c8ad4]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 8] ./bin/lens_immiscible_vcfv_fd(+0x19aa1c)[0x55ff425c2a1c]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [ 9] ./bin/lens_immiscible_vcfv_fd(+0x196443)[0x55ff425be443]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [10] ./bin/lens_immiscible_vcfv_fd(+0x191c2c)[0x55ff425b9c2c]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [11] ./bin/lens_immiscible_vcfv_fd(+0x18b45c)[0x55ff425b345c]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [12] ./bin/lens_immiscible_vcfv_fd(+0x18337a)[0x55ff425ab37a]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [13] ./bin/lens_immiscible_vcfv_fd(+0x1776ea)[0x55ff4259f6ea]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [14] ./bin/lens_immiscible_vcfv_fd(+0x165b15)[0x55ff4258db15]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [15] ./bin/lens_immiscible_vcfv_fd(+0x152238)[0x55ff4257a238]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [16] ./bin/lens_immiscible_vcfv_fd(+0x13fe2b)[0x55ff42567e2b]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [17] ./bin/lens_immiscible_vcfv_fd(+0x129e7d)[0x55ff42551e7d]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [18] ./bin/lens_immiscible_vcfv_fd(+0x116ff7)[0x55ff4253eff7]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [19] ./bin/lens_immiscible_vcfv_fd(+0x1014af)[0x55ff425294af]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [20] ./bin/lens_immiscible_vcfv_fd(+0xefcc4)[0x55ff42517cc4]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [21] ./bin/lens_immiscible_vcfv_fd(+0xdfcea)[0x55ff42507cea]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [22] ./bin/lens_immiscible_vcfv_fd(+0xd122b)[0x55ff424f922b]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [23] ./bin/lens_immiscible_vcfv_fd(+0xc054c)[0x55ff424e854c]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [24] ./bin/lens_immiscible_vcfv_fd(+0x915ae)[0x55ff424b95ae]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [25] ./bin/lens_immiscible_vcfv_fd(+0x90eab)[0x55ff424b8eab]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [26] /usr/lib/libc.so.6(+0x2d290)[0x7efcbedc6290]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7efcbedc634a]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] [28] ./bin/lens_immiscible_vcfv_fd(+0x90a15)[0x55ff424b8a15]
[runner-j2nyww-s-project-26315187-concurrent-0:04591] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [runner-j2nyww-s-project-26315187-concurrent-0:04594] [13] [runner-j2nyww-s-project-26315187-concurrent-0:04592] [13] ./bin/lens_immiscible_vcfv_fd(+0x1776ea)[0x563b2a2676ea]
[10] ./bin/lens_immiscible_vcfv_fd(+0x1776ea)[0x55b1ae4f46ea]
./bin/lens_immiscible_vcfv_fd(+0x191c2c)[0x555a82adfc2c]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [14] [runner-j2nyww-s-project-26315187-concurrent-0:04594] ./bin/lens_immiscible_vcfv_fd(+0x165b15)[0x563b2a255b15]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [14] [11] ./bin/lens_immiscible_vcfv_fd(+0x18b45c)[0x555a82ad945c]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [12] ./bin/lens_immiscible_vcfv_fd(+0x18337a)[0x555a82ad137a]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [13] ./bin/lens_immiscible_vcfv_fd(+0x1776ea)[0x555a82ac56ea]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [14] ./bin/lens_immiscible_vcfv_fd(+0x165b15)[0x555a82ab3b15]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [15] ./bin/lens_immiscible_vcfv_fd(+0x152238)[0x555a82aa0238]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [16] ./bin/lens_immiscible_vcfv_fd(+0x13fe2b)[0x555a82a8de2b]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [17] ./bin/lens_immiscible_vcfv_fd(+0x129e7d)[0x555a82a77e7d]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [18] ./bin/lens_immiscible_vcfv_fd(+0x116ff7)[0x555a82a64ff7]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [19] ./bin/lens_immiscible_vcfv_fd(+0x1014af)[0x555a82a4f4af]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [20] ./bin/lens_immiscible_vcfv_fd(+0xefcc4)[0x555a82a3dcc4]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [21] ./bin/lens_immiscible_vcfv_fd(+0xdfcea)[0x555a82a2dcea]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [22] ./bin/lens_immiscible_vcfv_fd(+0xd122b)[0x555a82a1f22b]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [23] ./bin/lens_immiscible_vcfv_fd(+0xc054c)[0x555a82a0e54c]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [24] ./bin/lens_immiscible_vcfv_fd(+0x915ae)[0x555a829df5ae]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [25] ./bin/lens_immiscible_vcfv_fd(+0x90eab)[0x555a829deeab]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f3e00ad3290]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f3e00ad334a]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] [28] ./bin/lens_immiscible_vcfv_fd(+0x90a15)[0x555a829dea15]
[runner-j2nyww-s-project-26315187-concurrent-0:04592] *** End of error message ***
./bin/lens_immiscible_vcfv_fd(+0x165b15)[0x55b1ae4e2b15]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [15] ./bin/lens_immiscible_vcfv_fd(+0x152238)[0x563b2a242238]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [16] ./bin/lens_immiscible_vcfv_fd(+0x13fe2b)[0x563b2a22fe2b]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [17] ./bin/lens_immiscible_vcfv_fd(+0x129e7d)[0x563b2a219e7d]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [18] ./bin/lens_immiscible_vcfv_fd(+0x116ff7)[0x563b2a206ff7]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [19] ./bin/lens_immiscible_vcfv_fd(+0x1014af)[0x563b2a1f14af]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [20] ./bin/lens_immiscible_vcfv_fd(+0xefcc4)[0x563b2a1dfcc4]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [21] ./bin/lens_immiscible_vcfv_fd(+0xdfcea)[0x563b2a1cfcea]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [22] ./bin/lens_immiscible_vcfv_fd(+0xd122b)[0x563b2a1c122b]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [23] ./bin/lens_immiscible_vcfv_fd(+0xc054c)[0x563b2a1b054c]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [24] ./bin/lens_immiscible_vcfv_fd(+0x915ae)[0x563b2a1815ae]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [25] ./bin/lens_immiscible_vcfv_fd(+0x90eab)[0x563b2a180eab]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f3eada6a290]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f3eada6a34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] [28] ./bin/lens_immiscible_vcfv_fd(+0x90a15)[0x563b2a180a15]
[runner-j2nyww-s-project-26315187-concurrent-0:04593] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [15] ./bin/lens_immiscible_vcfv_fd(+0x152238)[0x55b1ae4cf238]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [16] ./bin/lens_immiscible_vcfv_fd(+0x13fe2b)[0x55b1ae4bce2b]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [17] ./bin/lens_immiscible_vcfv_fd(+0x129e7d)[0x55b1ae4a6e7d]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [18] ./bin/lens_immiscible_vcfv_fd(+0x116ff7)[0x55b1ae493ff7]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [19] ./bin/lens_immiscible_vcfv_fd(+0x1014af)[0x55b1ae47e4af]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [20] ./bin/lens_immiscible_vcfv_fd(+0xefcc4)[0x55b1ae46ccc4]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [21] ./bin/lens_immiscible_vcfv_fd(+0xdfcea)[0x55b1ae45ccea]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [22] ./bin/lens_immiscible_vcfv_fd(+0xd122b)[0x55b1ae44e22b]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [23] ./bin/lens_immiscible_vcfv_fd(+0xc054c)[0x55b1ae43d54c]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [24] ./bin/lens_immiscible_vcfv_fd(+0x915ae)[0x55b1ae40e5ae]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [25] ./bin/lens_immiscible_vcfv_fd(+0x90eab)[0x55b1ae40deab]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [26] /usr/lib/libc.so.6(+0x2d290)[0x7ff3a7d3b290]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7ff3a7d3b34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] [28] ./bin/lens_immiscible_vcfv_fd(+0x90a15)[0x55b1ae40da15]
[runner-j2nyww-s-project-26315187-concurrent-0:04594] *** End of error message ***
--------------------------------------------------------------------------
Primary job  terminated normally, but 1 process returned
a non-zero exit code. Per user-direction, the job has been aborted.
--------------------------------------------------------------------------
--------------------------------------------------------------------------
mpirun noticed that process rank 1 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
--------------------------------------------------------------------------
Executing the binary failed!
test 52
      Start 52: lens_immiscible_vcfv_ad_parallel
52: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--parallel-simulation=4" "-e" "lens_immiscible_vcfv_ad" "--" "--end-time=250" "--initial-time-step-size=250"
52: Test timeout computed to be: 1500
52: ######################
52: # Running test 'lens_immiscible_vcfv_ad'
52: ######################
52: executing "mpirun -np "4" ./bin/lens_immiscible_vcfv_ad --end-time=250 --initial-time-step-size=250"
52: Ground remediation problem where a dense oil infiltrates an aquifer with an
52: embedded low-permability lens. This is the binary for the isothermal variant
52: using automatic differentiationand the vertex centered finite volume
52: discretization
52: 
52: Allocating the simulation vanguard
52: Distributing the vanguard's data
52: Allocating the model
52: Allocating the problem
52: Initializing the model
52: Initializing the problem
52: Simulator successfully set up
52: Applying the initial solution of the "lens_immiscible_vcfv_ad" problem
52: Writing visualization results for the current time step.
52: Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
lens_immiscible_vcfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
52: lens_immiscible_vcfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
52: lens_immiscible_vcfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] *** Process received signal ***
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] Signal: Aborted (6)
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] Signal code:  (-6)
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [runner-j2nyww-s-project-26315187-concurrent-0:04622] *** Process received signal ***
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] Signal: Aborted (6)
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] Signal code:  (-6)
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7fc00a1ab8e0]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [runner-j2nyww-s-project-26315187-concurrent-0:04621] *** Process received signal ***
52: [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fc00a1fb20c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] Signal: Aborted (6)
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] Signal code:  (-6)
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 0] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7fc00a1ab838]
52: /usr/lib/libc.so.6(+0x428e0)[0x7fb1c99658e0]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04622] /usr/lib/libc.so.6(abort+0xcf)[0x7fc00a195535]
52: [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fb1c99b520c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 2] /usr/lib/libc.so.6(+0x2c45c)[0x7fc00a19545c]
52: [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f029215f8e0]
52: /usr/lib/libc.so.6(raise+0x18)[0x7fb1c9965838]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fc00a1a4366]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7fb1c994f535]
52: [ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04623] /usr/lib/libc.so.6(+0x9220c)[0x7f02921af20c]
52: [ 6] ./bin/lens_immiscible_vcfv_ad(+0xb13a8)[0x55a6cb7493a8]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7fb1c994f45c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f029215f838]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fb1c995e366]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 7] ./bin/lens_immiscible_vcfv_ad(+0x1a327c)[0x55a6cb83b27c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04622] /usr/lib/libc.so.6(abort+0xcf)[0x7f0292149535]
52: [ 6] ./bin/lens_immiscible_vcfv_ad(+0xb13a8)[0x55dacbbbc3a8]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f029214945c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 8] ./bin/lens_immiscible_vcfv_ad(+0x19dd3c)[0x55a6cb835d3c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f0292158366]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 7] ./bin/lens_immiscible_vcfv_ad(+0x1a327c)[0x55dacbcae27c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 9] ./bin/lens_immiscible_vcfv_ad(+0x1992bf)[0x55a6cb8312bf]
52: [ 6] ./bin/lens_immiscible_vcfv_ad(+0xb13a8)[0x55c0476863a8]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 8] [runner-j2nyww-s-project-26315187-concurrent-0:04623] [10] ./bin/lens_immiscible_vcfv_ad(+0x19dd3c)[0x55dacbca8d3c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] ./bin/lens_immiscible_vcfv_ad(+0x1941ee)[0x55a6cb82c1ee]
52: [ 7] ./bin/lens_immiscible_vcfv_ad(+0x1a327c)[0x55c04777827c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 9] ./bin/lens_immiscible_vcfv_ad(+0x1992bf)[0x55dacbca42bf]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [11] ./bin/lens_immiscible_vcfv_ad(+0x18d460)[0x55a6cb825460]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 8] [runner-j2nyww-s-project-26315187-concurrent-0:04622] ./bin/lens_immiscible_vcfv_ad(+0x19dd3c)[0x55c047772d3c]
52: [10] ./bin/lens_immiscible_vcfv_ad(+0x1941ee)[0x55dacbc9f1ee]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [12] ./bin/lens_immiscible_vcfv_ad(+0x185262)[0x55a6cb81d262]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [runner-j2nyww-s-project-26315187-concurrent-0:04621] [11] [ 9] ./bin/lens_immiscible_vcfv_ad(+0x18d460)[0x55dacbc98460]
52: ./bin/lens_immiscible_vcfv_ad(+0x1992bf)[0x55c04776e2bf]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [13] ./bin/lens_immiscible_vcfv_ad(+0x1784d2)[0x55a6cb8104d2]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [runner-j2nyww-s-project-26315187-concurrent-0:04621] [12] [10] ./bin/lens_immiscible_vcfv_ad(+0x185262)[0x55dacbc90262]
52: ./bin/lens_immiscible_vcfv_ad(+0x1941ee)[0x55c0477691ee]
52: lens_immiscible_vcfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] *** Process received signal ***
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] Signal: Aborted (6)
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] Signal code:  (-6)
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f4410d1a8e0]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f4410d6a20c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f4410d1a838]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f4410d04535]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f4410d0445c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f4410d13366]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 6] ./bin/lens_immiscible_vcfv_ad(+0xb13a8)[0x5623acb543a8]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 7] ./bin/lens_immiscible_vcfv_ad(+0x1a327c)[0x5623acc4627c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 8] ./bin/lens_immiscible_vcfv_ad(+0x19dd3c)[0x5623acc40d3c]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 9] ./bin/lens_immiscible_vcfv_ad(+0x1992bf)[0x5623acc3c2bf]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [10] ./bin/lens_immiscible_vcfv_ad(+0x1941ee)[0x5623acc371ee]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [11] ./bin/lens_immiscible_vcfv_ad(+0x18d460)[0x5623acc30460]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [12] ./bin/lens_immiscible_vcfv_ad(+0x185262)[0x5623acc28262]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [13] ./bin/lens_immiscible_vcfv_ad(+0x1784d2)[0x5623acc1b4d2]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [14] ./bin/lens_immiscible_vcfv_ad(+0x1674fb)[0x5623acc0a4fb]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [15] ./bin/lens_immiscible_vcfv_ad(+0x153cbc)[0x5623acbf6cbc]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [16] ./bin/lens_immiscible_vcfv_ad(+0x140d23)[0x5623acbe3d23]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [17] ./bin/lens_immiscible_vcfv_ad(+0x12a77b)[0x5623acbcd77b]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [18] ./bin/lens_immiscible_vcfv_ad(+0x1175bb)[0x5623acbba5bb]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [19] ./bin/lens_immiscible_vcfv_ad(+0x1016a3)[0x5623acba46a3]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [20] ./bin/lens_immiscible_vcfv_ad(+0xefd7a)[0x5623acb92d7a]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [21] ./bin/lens_immiscible_vcfv_ad(+0xdfd04)[0x5623acb82d04]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [22] ./bin/lens_immiscible_vcfv_ad(+0xd1237)[0x5623acb74237]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [23] ./bin/lens_immiscible_vcfv_ad(+0xc0558)[0x5623acb63558]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [24] ./bin/lens_immiscible_vcfv_ad(+0x915ae)[0x5623acb345ae]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [25] ./bin/lens_immiscible_vcfv_ad(+0x90eab)[0x5623acb33eab]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f4410d05290]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f4410d0534a]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] [28] ./bin/lens_immiscible_vcfv_ad(+0x90a15)[0x5623acb33a15]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04620] *** End of error message ***
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [runner-j2nyww-s-project-26315187-concurrent-0:04621] [14] [11] ./bin/lens_immiscible_vcfv_ad(+0x1674fb)[0x55a6cb7ff4fb]
52: ./bin/lens_immiscible_vcfv_ad(+0x18d460)[0x55c047762460]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [13] ./bin/lens_immiscible_vcfv_ad(+0x1784d2)[0x55dacbc834d2]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [runner-j2nyww-s-project-26315187-concurrent-0:04621] [15] [12] ./bin/lens_immiscible_vcfv_ad(+0x153cbc)[0x55a6cb7ebcbc]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [14] ./bin/lens_immiscible_vcfv_ad(+0x1674fb)[0x55dacbc724fb]
52: ./bin/lens_immiscible_vcfv_ad(+0x185262)[0x55c04775a262]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [16] ./bin/lens_immiscible_vcfv_ad(+0x140d23)[0x55a6cb7d8d23]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [15] ./bin/lens_immiscible_vcfv_ad(+0x153cbc)[0x55dacbc5ecbc]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [16] ./bin/lens_immiscible_vcfv_ad(+0x140d23)[0x55dacbc4bd23]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [17] ./bin/lens_immiscible_vcfv_ad(+0x12a77b)[0x55dacbc3577b]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [18] ./bin/lens_immiscible_vcfv_ad(+0x1175bb)[0x55dacbc225bb]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [19] ./bin/lens_immiscible_vcfv_ad(+0x1016a3)[0x55dacbc0c6a3]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [20] ./bin/lens_immiscible_vcfv_ad(+0xefd7a)[0x55dacbbfad7a]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [21] ./bin/lens_immiscible_vcfv_ad(+0xdfd04)[0x55dacbbead04]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [22] ./bin/lens_immiscible_vcfv_ad(+0xd1237)[0x55dacbbdc237]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [23] ./bin/lens_immiscible_vcfv_ad(+0xc0558)[0x55dacbbcb558]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [24] ./bin/lens_immiscible_vcfv_ad(+0x915ae)[0x55dacbb9c5ae]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [25] ./bin/lens_immiscible_vcfv_ad(+0x90eab)[0x55dacbb9beab]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fb1c9950290]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fb1c995034a]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] [28] ./bin/lens_immiscible_vcfv_ad(+0x90a15)[0x55dacbb9ba15]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04622] *** End of error message ***
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [13] ./bin/lens_immiscible_vcfv_ad(+0x1784d2)[0x55c04774d4d2]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [14] ./bin/lens_immiscible_vcfv_ad(+0x1674fb)[0x55c04773c4fb]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [15] ./bin/lens_immiscible_vcfv_ad(+0x153cbc)[0x55c047728cbc]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [16] ./bin/lens_immiscible_vcfv_ad(+0x140d23)[0x55c047715d23]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [17] ./bin/lens_immiscible_vcfv_ad(+0x12a77b)[0x55c0476ff77b]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [18] ./bin/lens_immiscible_vcfv_ad(+0x1175bb)[0x55c0476ec5bb]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [19] ./bin/lens_immiscible_vcfv_ad(+0x1016a3)[0x55c0476d66a3]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [20] ./bin/lens_immiscible_vcfv_ad(+0xefd7a)[0x55c0476c4d7a]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [21] ./bin/lens_immiscible_vcfv_ad(+0xdfd04)[0x55c0476b4d04]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [22] ./bin/lens_immiscible_vcfv_ad(+0xd1237)[0x55c0476a6237]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [23] ./bin/lens_immiscible_vcfv_ad(+0xc0558)[0x55c047695558]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [24] ./bin/lens_immiscible_vcfv_ad(+0x915ae)[0x55c0476665ae]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [25] ./bin/lens_immiscible_vcfv_ad(+0x90eab)[0x55c047665eab]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f029214a290]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f029214a34a]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] [28] ./bin/lens_immiscible_vcfv_ad(+0x90a15)[0x55c047665a15]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04621] *** End of error message ***
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [17] ./bin/lens_immiscible_vcfv_ad(+0x12a77b)[0x55a6cb7c277b]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [18] ./bin/lens_immiscible_vcfv_ad(+0x1175bb)[0x55a6cb7af5bb]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [19] ./bin/lens_immiscible_vcfv_ad(+0x1016a3)[0x55a6cb7996a3]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [20] ./bin/lens_immiscible_vcfv_ad(+0xefd7a)[0x55a6cb787d7a]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [21] ./bin/lens_immiscible_vcfv_ad(+0xdfd04)[0x55a6cb777d04]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [22] ./bin/lens_immiscible_vcfv_ad(+0xd1237)[0x55a6cb769237]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [23] ./bin/lens_immiscible_vcfv_ad(+0xc0558)[0x55a6cb758558]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [24] ./bin/lens_immiscible_vcfv_ad(+0x915ae)[0x55a6cb7295ae]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [25] ./bin/lens_immiscible_vcfv_ad(+0x90eab)[0x55a6cb728eab]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fc00a196290]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fc00a19634a]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] [28] ./bin/lens_immiscible_vcfv_ad(+0x90a15)[0x55a6cb728a15]
52: [runner-j2nyww-s-project-26315187-concurrent-0:04623] *** End of error message ***
52: --------------------------------------------------------------------------
52: Primary job  terminated normally, but 1 process returned
52: a non-zero exit code. Per user-direction, the job has been aborted.
52: --------------------------------------------------------------------------
52: --------------------------------------------------------------------------
52: mpirun noticed that process rank 2 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
52: --------------------------------------------------------------------------
52: Executing the binary failed!
52/58 Test #52: lens_immiscible_vcfv_ad_parallel ....***Failed    1.45 sec
######################
# Running test 'lens_immiscible_vcfv_ad'
######################
executing "mpirun -np "4" ./bin/lens_immiscible_vcfv_ad --end-time=250 --initial-time-step-size=250"
Ground remediation problem where a dense oil infiltrates an aquifer with an
embedded low-permability lens. This is the binary for the isothermal variant
using automatic differentiationand the vertex centered finite volume
discretization
Allocating the simulation vanguard
Distributing the vanguard's data
Allocating the model
Allocating the problem
Initializing the model
Initializing the problem
Simulator successfully set up
Applying the initial solution of the "lens_immiscible_vcfv_ad" problem
Writing visualization results for the current time step.
Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
lens_immiscible_vcfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
lens_immiscible_vcfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
lens_immiscible_vcfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04623] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04623] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04623] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [runner-j2nyww-s-project-26315187-concurrent-0:04622] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04622] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04622] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7fc00a1ab8e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [runner-j2nyww-s-project-26315187-concurrent-0:04621] *** Process received signal ***
[ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fc00a1fb20c]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04621] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 0] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7fc00a1ab838]
/usr/lib/libc.so.6(+0x428e0)[0x7fb1c99658e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04622] /usr/lib/libc.so.6(abort+0xcf)[0x7fc00a195535]
[ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fb1c99b520c]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 2] /usr/lib/libc.so.6(+0x2c45c)[0x7fc00a19545c]
[ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f029215f8e0]
/usr/lib/libc.so.6(raise+0x18)[0x7fb1c9965838]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fc00a1a4366]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7fb1c994f535]
[ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04623] /usr/lib/libc.so.6(+0x9220c)[0x7f02921af20c]
[ 6] ./bin/lens_immiscible_vcfv_ad(+0xb13a8)[0x55a6cb7493a8]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7fb1c994f45c]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f029215f838]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fb1c995e366]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 7] ./bin/lens_immiscible_vcfv_ad(+0x1a327c)[0x55a6cb83b27c]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04622] /usr/lib/libc.so.6(abort+0xcf)[0x7f0292149535]
[ 6] ./bin/lens_immiscible_vcfv_ad(+0xb13a8)[0x55dacbbbc3a8]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f029214945c]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 8] ./bin/lens_immiscible_vcfv_ad(+0x19dd3c)[0x55a6cb835d3c]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f0292158366]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 7] ./bin/lens_immiscible_vcfv_ad(+0x1a327c)[0x55dacbcae27c]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [runner-j2nyww-s-project-26315187-concurrent-0:04623] [ 9] ./bin/lens_immiscible_vcfv_ad(+0x1992bf)[0x55a6cb8312bf]
[ 6] ./bin/lens_immiscible_vcfv_ad(+0xb13a8)[0x55c0476863a8]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 8] [runner-j2nyww-s-project-26315187-concurrent-0:04623] [10] ./bin/lens_immiscible_vcfv_ad(+0x19dd3c)[0x55dacbca8d3c]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] ./bin/lens_immiscible_vcfv_ad(+0x1941ee)[0x55a6cb82c1ee]
[ 7] ./bin/lens_immiscible_vcfv_ad(+0x1a327c)[0x55c04777827c]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [ 9] ./bin/lens_immiscible_vcfv_ad(+0x1992bf)[0x55dacbca42bf]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [11] ./bin/lens_immiscible_vcfv_ad(+0x18d460)[0x55a6cb825460]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [ 8] [runner-j2nyww-s-project-26315187-concurrent-0:04622] ./bin/lens_immiscible_vcfv_ad(+0x19dd3c)[0x55c047772d3c]
[10] ./bin/lens_immiscible_vcfv_ad(+0x1941ee)[0x55dacbc9f1ee]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [12] ./bin/lens_immiscible_vcfv_ad(+0x185262)[0x55a6cb81d262]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [runner-j2nyww-s-project-26315187-concurrent-0:04621] [11] [ 9] ./bin/lens_immiscible_vcfv_ad(+0x18d460)[0x55dacbc98460]
./bin/lens_immiscible_vcfv_ad(+0x1992bf)[0x55c04776e2bf]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [13] ./bin/lens_immiscible_vcfv_ad(+0x1784d2)[0x55a6cb8104d2]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [runner-j2nyww-s-project-26315187-concurrent-0:04621] [12] [10] ./bin/lens_immiscible_vcfv_ad(+0x185262)[0x55dacbc90262]
./bin/lens_immiscible_vcfv_ad(+0x1941ee)[0x55c0477691ee]
lens_immiscible_vcfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04620] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04620] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04620] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f4410d1a8e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f4410d6a20c]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f4410d1a838]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f4410d04535]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f4410d0445c]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f4410d13366]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 6] ./bin/lens_immiscible_vcfv_ad(+0xb13a8)[0x5623acb543a8]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 7] ./bin/lens_immiscible_vcfv_ad(+0x1a327c)[0x5623acc4627c]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 8] ./bin/lens_immiscible_vcfv_ad(+0x19dd3c)[0x5623acc40d3c]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [ 9] ./bin/lens_immiscible_vcfv_ad(+0x1992bf)[0x5623acc3c2bf]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [10] ./bin/lens_immiscible_vcfv_ad(+0x1941ee)[0x5623acc371ee]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [11] ./bin/lens_immiscible_vcfv_ad(+0x18d460)[0x5623acc30460]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [12] ./bin/lens_immiscible_vcfv_ad(+0x185262)[0x5623acc28262]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [13] ./bin/lens_immiscible_vcfv_ad(+0x1784d2)[0x5623acc1b4d2]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [14] ./bin/lens_immiscible_vcfv_ad(+0x1674fb)[0x5623acc0a4fb]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [15] ./bin/lens_immiscible_vcfv_ad(+0x153cbc)[0x5623acbf6cbc]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [16] ./bin/lens_immiscible_vcfv_ad(+0x140d23)[0x5623acbe3d23]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [17] ./bin/lens_immiscible_vcfv_ad(+0x12a77b)[0x5623acbcd77b]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [18] ./bin/lens_immiscible_vcfv_ad(+0x1175bb)[0x5623acbba5bb]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [19] ./bin/lens_immiscible_vcfv_ad(+0x1016a3)[0x5623acba46a3]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [20] ./bin/lens_immiscible_vcfv_ad(+0xefd7a)[0x5623acb92d7a]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [21] ./bin/lens_immiscible_vcfv_ad(+0xdfd04)[0x5623acb82d04]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [22] ./bin/lens_immiscible_vcfv_ad(+0xd1237)[0x5623acb74237]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [23] ./bin/lens_immiscible_vcfv_ad(+0xc0558)[0x5623acb63558]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [24] ./bin/lens_immiscible_vcfv_ad(+0x915ae)[0x5623acb345ae]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [25] ./bin/lens_immiscible_vcfv_ad(+0x90eab)[0x5623acb33eab]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f4410d05290]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f4410d0534a]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] [28] ./bin/lens_immiscible_vcfv_ad(+0x90a15)[0x5623acb33a15]
[runner-j2nyww-s-project-26315187-concurrent-0:04620] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [runner-j2nyww-s-project-26315187-concurrent-0:04621] [14] [11] ./bin/lens_immiscible_vcfv_ad(+0x1674fb)[0x55a6cb7ff4fb]
./bin/lens_immiscible_vcfv_ad(+0x18d460)[0x55c047762460]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [13] ./bin/lens_immiscible_vcfv_ad(+0x1784d2)[0x55dacbc834d2]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [runner-j2nyww-s-project-26315187-concurrent-0:04621] [15] [12] ./bin/lens_immiscible_vcfv_ad(+0x153cbc)[0x55a6cb7ebcbc]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [14] ./bin/lens_immiscible_vcfv_ad(+0x1674fb)[0x55dacbc724fb]
./bin/lens_immiscible_vcfv_ad(+0x185262)[0x55c04775a262]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [16] ./bin/lens_immiscible_vcfv_ad(+0x140d23)[0x55a6cb7d8d23]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [15] ./bin/lens_immiscible_vcfv_ad(+0x153cbc)[0x55dacbc5ecbc]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [16] ./bin/lens_immiscible_vcfv_ad(+0x140d23)[0x55dacbc4bd23]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [17] ./bin/lens_immiscible_vcfv_ad(+0x12a77b)[0x55dacbc3577b]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [18] ./bin/lens_immiscible_vcfv_ad(+0x1175bb)[0x55dacbc225bb]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [19] ./bin/lens_immiscible_vcfv_ad(+0x1016a3)[0x55dacbc0c6a3]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [20] ./bin/lens_immiscible_vcfv_ad(+0xefd7a)[0x55dacbbfad7a]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [21] ./bin/lens_immiscible_vcfv_ad(+0xdfd04)[0x55dacbbead04]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [22] ./bin/lens_immiscible_vcfv_ad(+0xd1237)[0x55dacbbdc237]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [23] ./bin/lens_immiscible_vcfv_ad(+0xc0558)[0x55dacbbcb558]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [24] ./bin/lens_immiscible_vcfv_ad(+0x915ae)[0x55dacbb9c5ae]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [25] ./bin/lens_immiscible_vcfv_ad(+0x90eab)[0x55dacbb9beab]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fb1c9950290]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fb1c995034a]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] [28] ./bin/lens_immiscible_vcfv_ad(+0x90a15)[0x55dacbb9ba15]
[runner-j2nyww-s-project-26315187-concurrent-0:04622] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [13] ./bin/lens_immiscible_vcfv_ad(+0x1784d2)[0x55c04774d4d2]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [14] ./bin/lens_immiscible_vcfv_ad(+0x1674fb)[0x55c04773c4fb]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [15] ./bin/lens_immiscible_vcfv_ad(+0x153cbc)[0x55c047728cbc]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [16] ./bin/lens_immiscible_vcfv_ad(+0x140d23)[0x55c047715d23]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [17] ./bin/lens_immiscible_vcfv_ad(+0x12a77b)[0x55c0476ff77b]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [18] ./bin/lens_immiscible_vcfv_ad(+0x1175bb)[0x55c0476ec5bb]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [19] ./bin/lens_immiscible_vcfv_ad(+0x1016a3)[0x55c0476d66a3]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [20] ./bin/lens_immiscible_vcfv_ad(+0xefd7a)[0x55c0476c4d7a]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [21] ./bin/lens_immiscible_vcfv_ad(+0xdfd04)[0x55c0476b4d04]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [22] ./bin/lens_immiscible_vcfv_ad(+0xd1237)[0x55c0476a6237]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [23] ./bin/lens_immiscible_vcfv_ad(+0xc0558)[0x55c047695558]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [24] ./bin/lens_immiscible_vcfv_ad(+0x915ae)[0x55c0476665ae]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [25] ./bin/lens_immiscible_vcfv_ad(+0x90eab)[0x55c047665eab]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f029214a290]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f029214a34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] [28] ./bin/lens_immiscible_vcfv_ad(+0x90a15)[0x55c047665a15]
[runner-j2nyww-s-project-26315187-concurrent-0:04621] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [17] ./bin/lens_immiscible_vcfv_ad(+0x12a77b)[0x55a6cb7c277b]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [18] ./bin/lens_immiscible_vcfv_ad(+0x1175bb)[0x55a6cb7af5bb]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [19] ./bin/lens_immiscible_vcfv_ad(+0x1016a3)[0x55a6cb7996a3]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [20] ./bin/lens_immiscible_vcfv_ad(+0xefd7a)[0x55a6cb787d7a]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [21] ./bin/lens_immiscible_vcfv_ad(+0xdfd04)[0x55a6cb777d04]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [22] ./bin/lens_immiscible_vcfv_ad(+0xd1237)[0x55a6cb769237]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [23] ./bin/lens_immiscible_vcfv_ad(+0xc0558)[0x55a6cb758558]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [24] ./bin/lens_immiscible_vcfv_ad(+0x915ae)[0x55a6cb7295ae]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [25] ./bin/lens_immiscible_vcfv_ad(+0x90eab)[0x55a6cb728eab]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fc00a196290]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fc00a19634a]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] [28] ./bin/lens_immiscible_vcfv_ad(+0x90a15)[0x55a6cb728a15]
[runner-j2nyww-s-project-26315187-concurrent-0:04623] *** End of error message ***
--------------------------------------------------------------------------
Primary job  terminated normally, but 1 process returned
a non-zero exit code. Per user-direction, the job has been aborted.
--------------------------------------------------------------------------
--------------------------------------------------------------------------
mpirun noticed that process rank 2 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
--------------------------------------------------------------------------
Executing the binary failed!
test 53
      Start 53: lens_immiscible_ecfv_ad_parallel
53: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--parallel-simulation=4" "-e" "lens_immiscible_ecfv_ad" "--" "--end-time=250" "--initial-time-step-size=250"
53: Test timeout computed to be: 1500
53: ######################
53: # Running test 'lens_immiscible_ecfv_ad'
53: ######################
53: executing "mpirun -np "4" ./bin/lens_immiscible_ecfv_ad --end-time=250 --initial-time-step-size=250"
53: Ground remediation problem where a dense oil infiltrates an aquifer with an
53: embedded low-permability lens. This is the binary for the isothermal variant
53: using automatic differentiationand the element centered finite volume
53: discretization
53: 
53: Allocating the simulation vanguard
53: Distributing the vanguard's data
53: Allocating the model
53: Allocating the problem
53: Initializing the model
53: Initializing the problem
53: Simulator successfully set up
53: Applying the initial solution of the "lens_immiscible_ecfv_ad" problem
53: Writing visualization results for the current time step.
53: Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
lens_immiscible_ecfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
53: lens_immiscible_ecfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
53: lens_immiscible_ecfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] *** Process received signal ***
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] Signal: Aborted (6)
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] Signal code:  (-6)
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [runner-j2nyww-s-project-26315187-concurrent-0:04650] *** Process received signal ***
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] Signal: Aborted (6)
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] Signal code:  (-6)
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [runner-j2nyww-s-project-26315187-concurrent-0:04651] *** Process received signal ***
53: [ 0] [runner-j2nyww-s-project-26315187-concurrent-0:04651] Signal: Aborted (6)
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] Signal code:  (-6)
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] /usr/lib/libc.so.6(+0x428e0)[0x7f93349a08e0]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 1] [ 0] /usr/lib/libc.so.6(+0x9220c)[0x7f93349f020c]
53: /usr/lib/libc.so.6(+0x428e0)[0x7fde11a288e0]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 2] [runner-j2nyww-s-project-26315187-concurrent-0:04650] /usr/lib/libc.so.6(raise+0x18)[0x7f93349a0838]
53: [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fde11a7820c]
53: [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f9bc2af98e0]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04650] /usr/lib/libc.so.6(abort+0xcf)[0x7f933498a535]
53: [ 2] [runner-j2nyww-s-project-26315187-concurrent-0:04651] /usr/lib/libc.so.6(raise+0x18)[0x7fde11a28838]
53: [ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f933498a45c]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 3] /usr/lib/libc.so.6(+0x9220c)[0x7f9bc2b4920c]
53: /usr/lib/libc.so.6(abort+0xcf)[0x7fde11a12535]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 5] [ 2] /usr/lib/libc.so.6(+0x3b366)[0x7f9334999366]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] /usr/lib/libc.so.6(raise+0x18)[0x7f9bc2af9838]
53: [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 6] /usr/lib/libc.so.6(+0x2c45c)[0x7fde11a1245c]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f9bc2ae3535]
53: ./bin/lens_immiscible_ecfv_ad(+0xb0db2)[0x55f01b026db2]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fde11a21366]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04650] /usr/lib/libc.so.6(+0x2c45c)[0x7f9bc2ae345c]
53: [ 6] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 7] ./bin/lens_immiscible_ecfv_ad(+0x1b464e)[0x55f01b12a64e]
53: ./bin/lens_immiscible_ecfv_ad(+0xb0db2)[0x55e22363edb2]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f9bc2af2366]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 8] [ 6] [runner-j2nyww-s-project-26315187-concurrent-0:04650] ./bin/lens_immiscible_ecfv_ad(+0xb0db2)[0x55b1ebbbfdb2]
53: [ 7] ./bin/lens_immiscible_ecfv_ad(+0x1aed54)[0x55f01b124d54]
53: ./bin/lens_immiscible_ecfv_ad(+0x1b464e)[0x55e22374264e]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 7] [runner-j2nyww-s-project-26315187-concurrent-0:04652] ./bin/lens_immiscible_ecfv_ad(+0x1b464e)[0x55b1ebcc364e]
53: [ 9] [runner-j2nyww-s-project-26315187-concurrent-0:04650] [ 8] ./bin/lens_immiscible_ecfv_ad(+0x1a9f51)[0x55f01b11ff51]
53: ./bin/lens_immiscible_ecfv_ad(+0x1aed54)[0x55e22373cd54]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 8] ./bin/lens_immiscible_ecfv_ad(+0x1aed54)[0x55b1ebcbdd54]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [10] [runner-j2nyww-s-project-26315187-concurrent-0:04650] [ 9] ./bin/lens_immiscible_ecfv_ad(+0x1a4bc2)[0x55f01b11abc2]
53: ./bin/lens_immiscible_ecfv_ad(+0x1a9f51)[0x55e223737f51]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 9] ./bin/lens_immiscible_ecfv_ad(+0x1a9f51)[0x55b1ebcb8f51]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [runner-j2nyww-s-project-26315187-concurrent-0:04650] [10] [11] ./bin/lens_immiscible_ecfv_ad(+0x1a4bc2)[0x55e223732bc2]
53: ./bin/lens_immiscible_ecfv_ad(+0x19d678)[0x55f01b113678]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [10] ./bin/lens_immiscible_ecfv_ad(+0x1a4bc2)[0x55b1ebcb3bc2]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [11] ./bin/lens_immiscible_ecfv_ad(+0x19d678)[0x55e22372b678]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [12] ./bin/lens_immiscible_ecfv_ad(+0x194aba)[0x55f01b10aaba]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [11] ./bin/lens_immiscible_ecfv_ad(+0x19d678)[0x55b1ebcac678]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [12] ./bin/lens_immiscible_ecfv_ad(+0x194aba)[0x55e223722aba]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [13] ./bin/lens_immiscible_ecfv_ad(+0x185b16)[0x55f01b0fbb16]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [12] lens_immiscible_ecfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] *** Process received signal ***
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] Signal: Aborted (6)
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] Signal code:  (-6)
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f2f2377a8e0]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f2f237ca20c]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f2f2377a838]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f2f23764535]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f2f2376445c]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f2f23773366]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 6] ./bin/lens_immiscible_ecfv_ad(+0xb0db2)[0x563cb0e09db2]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 7] ./bin/lens_immiscible_ecfv_ad(+0x1b464e)[0x563cb0f0d64e]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 8] ./bin/lens_immiscible_ecfv_ad(+0x1aed54)[0x563cb0f07d54]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 9] ./bin/lens_immiscible_ecfv_ad(+0x1a9f51)[0x563cb0f02f51]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [10] ./bin/lens_immiscible_ecfv_ad(+0x1a4bc2)[0x563cb0efdbc2]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [11] ./bin/lens_immiscible_ecfv_ad(+0x19d678)[0x563cb0ef6678]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [12] ./bin/lens_immiscible_ecfv_ad(+0x194aba)[0x563cb0eedaba]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [13] ./bin/lens_immiscible_ecfv_ad(+0x185b16)[0x563cb0edeb16]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [14] ./bin/lens_immiscible_ecfv_ad(+0x17244d)[0x563cb0ecb44d]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [15] ./bin/lens_immiscible_ecfv_ad(+0x159846)[0x563cb0eb2846]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [16] ./bin/lens_immiscible_ecfv_ad(+0x143b45)[0x563cb0e9cb45]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [17] ./bin/lens_immiscible_ecfv_ad(+0x12ab01)[0x563cb0e83b01]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [18] ./bin/lens_immiscible_ecfv_ad(+0x115261)[0x563cb0e6e261]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [19] ./bin/lens_immiscible_ecfv_ad(+0xffb7d)[0x563cb0e58b7d]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [20] ./bin/lens_immiscible_ecfv_ad(+0xef3a8)[0x563cb0e483a8]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [21] ./bin/lens_immiscible_ecfv_ad(+0xdfd3e)[0x563cb0e38d3e]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [22] ./bin/lens_immiscible_ecfv_ad(+0xd0b4f)[0x563cb0e29b4f]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [23] ./bin/lens_immiscible_ecfv_ad(+0xbf740)[0x563cb0e18740]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [24] ./bin/lens_immiscible_ecfv_ad(+0x905be)[0x563cb0de95be]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [25] ./bin/lens_immiscible_ecfv_ad(+0x8febb)[0x563cb0de8ebb]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f2f23765290]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f2f2376534a]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] [28] ./bin/lens_immiscible_ecfv_ad(+0x8fa25)[0x563cb0de8a25]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04649] *** End of error message ***
53: ./bin/lens_immiscible_ecfv_ad(+0x194aba)[0x55b1ebca3aba]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [runner-j2nyww-s-project-26315187-concurrent-0:04650] [13] [14] ./bin/lens_immiscible_ecfv_ad(+0x185b16)[0x55e223713b16]
53: ./bin/lens_immiscible_ecfv_ad(+0x17244d)[0x55f01b0e844d]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [13] ./bin/lens_immiscible_ecfv_ad(+0x185b16)[0x55b1ebc94b16]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [14] ./bin/lens_immiscible_ecfv_ad(+0x17244d)[0x55e22370044d]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [15] [runner-j2nyww-s-project-26315187-concurrent-0:04651] [14] ./bin/lens_immiscible_ecfv_ad(+0x17244d)[0x55b1ebc8144d]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [15] ./bin/lens_immiscible_ecfv_ad(+0x159846)[0x55b1ebc68846]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [16] ./bin/lens_immiscible_ecfv_ad(+0x143b45)[0x55b1ebc52b45]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [17] ./bin/lens_immiscible_ecfv_ad(+0x12ab01)[0x55b1ebc39b01]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [18] ./bin/lens_immiscible_ecfv_ad(+0x115261)[0x55b1ebc24261]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [19] ./bin/lens_immiscible_ecfv_ad(+0xffb7d)[0x55b1ebc0eb7d]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [20] ./bin/lens_immiscible_ecfv_ad(+0xef3a8)[0x55b1ebbfe3a8]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [21] ./bin/lens_immiscible_ecfv_ad(+0xdfd3e)[0x55b1ebbeed3e]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [22] ./bin/lens_immiscible_ecfv_ad(+0xd0b4f)[0x55b1ebbdfb4f]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [23] ./bin/lens_immiscible_ecfv_ad(+0xbf740)[0x55b1ebbce740]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [24] ./bin/lens_immiscible_ecfv_ad(+0x905be)[0x55b1ebb9f5be]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [25] ./bin/lens_immiscible_ecfv_ad(+0x8febb)[0x55b1ebb9eebb]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f9bc2ae4290]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f9bc2ae434a]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] [28] ./bin/lens_immiscible_ecfv_ad(+0x8fa25)[0x55b1ebb9ea25]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04651] *** End of error message ***
53: ./bin/lens_immiscible_ecfv_ad(+0x159846)[0x55f01b0cf846]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [15] ./bin/lens_immiscible_ecfv_ad(+0x159846)[0x55e2236e7846]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [16] ./bin/lens_immiscible_ecfv_ad(+0x143b45)[0x55e2236d1b45]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [17] ./bin/lens_immiscible_ecfv_ad(+0x12ab01)[0x55e2236b8b01]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [18] ./bin/lens_immiscible_ecfv_ad(+0x115261)[0x55e2236a3261]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [19] ./bin/lens_immiscible_ecfv_ad(+0xffb7d)[0x55e22368db7d]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [20] ./bin/lens_immiscible_ecfv_ad(+0xef3a8)[0x55e22367d3a8]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [21] ./bin/lens_immiscible_ecfv_ad(+0xdfd3e)[0x55e22366dd3e]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [22] ./bin/lens_immiscible_ecfv_ad(+0xd0b4f)[0x55e22365eb4f]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [23] ./bin/lens_immiscible_ecfv_ad(+0xbf740)[0x55e22364d740]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [24] ./bin/lens_immiscible_ecfv_ad(+0x905be)[0x55e22361e5be]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [25] ./bin/lens_immiscible_ecfv_ad(+0x8febb)[0x55e22361debb]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fde11a13290]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fde11a1334a]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] [28] ./bin/lens_immiscible_ecfv_ad(+0x8fa25)[0x55e22361da25]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04650] *** End of error message ***
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [16] ./bin/lens_immiscible_ecfv_ad(+0x143b45)[0x55f01b0b9b45]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [17] ./bin/lens_immiscible_ecfv_ad(+0x12ab01)[0x55f01b0a0b01]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [18] ./bin/lens_immiscible_ecfv_ad(+0x115261)[0x55f01b08b261]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [19] ./bin/lens_immiscible_ecfv_ad(+0xffb7d)[0x55f01b075b7d]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [20] ./bin/lens_immiscible_ecfv_ad(+0xef3a8)[0x55f01b0653a8]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [21] ./bin/lens_immiscible_ecfv_ad(+0xdfd3e)[0x55f01b055d3e]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [22] ./bin/lens_immiscible_ecfv_ad(+0xd0b4f)[0x55f01b046b4f]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [23] ./bin/lens_immiscible_ecfv_ad(+0xbf740)[0x55f01b035740]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [24] ./bin/lens_immiscible_ecfv_ad(+0x905be)[0x55f01b0065be]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [25] ./bin/lens_immiscible_ecfv_ad(+0x8febb)[0x55f01b005ebb]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f933498b290]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f933498b34a]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] [28] ./bin/lens_immiscible_ecfv_ad(+0x8fa25)[0x55f01b005a25]
53: [runner-j2nyww-s-project-26315187-concurrent-0:04652] *** End of error message ***
53: --------------------------------------------------------------------------
53: Primary job  terminated normally, but 1 process returned
53: a non-zero exit code. Per user-direction, the job has been aborted.
53: --------------------------------------------------------------------------
53: --------------------------------------------------------------------------
53: mpirun noticed that process rank 3 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
53: --------------------------------------------------------------------------
53: Executing the binary failed!
53/58 Test #53: lens_immiscible_ecfv_ad_parallel ....***Failed    0.75 sec
######################
# Running test 'lens_immiscible_ecfv_ad'
######################
executing "mpirun -np "4" ./bin/lens_immiscible_ecfv_ad --end-time=250 --initial-time-step-size=250"
Ground remediation problem where a dense oil infiltrates an aquifer with an
embedded low-permability lens. This is the binary for the isothermal variant
using automatic differentiationand the element centered finite volume
discretization
Allocating the simulation vanguard
Distributing the vanguard's data
Allocating the model
Allocating the problem
Initializing the model
Initializing the problem
Simulator successfully set up
Applying the initial solution of the "lens_immiscible_ecfv_ad" problem
Writing visualization results for the current time step.
Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
lens_immiscible_ecfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
lens_immiscible_ecfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
lens_immiscible_ecfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04652] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04652] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04652] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [runner-j2nyww-s-project-26315187-concurrent-0:04650] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04650] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04650] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [runner-j2nyww-s-project-26315187-concurrent-0:04651] *** Process received signal ***
[ 0] [runner-j2nyww-s-project-26315187-concurrent-0:04651] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04651] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04651] /usr/lib/libc.so.6(+0x428e0)[0x7f93349a08e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 1] [ 0] /usr/lib/libc.so.6(+0x9220c)[0x7f93349f020c]
/usr/lib/libc.so.6(+0x428e0)[0x7fde11a288e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 2] [runner-j2nyww-s-project-26315187-concurrent-0:04650] /usr/lib/libc.so.6(raise+0x18)[0x7f93349a0838]
[ 1] /usr/lib/libc.so.6(+0x9220c)[0x7fde11a7820c]
[ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f9bc2af98e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 3] [runner-j2nyww-s-project-26315187-concurrent-0:04650] /usr/lib/libc.so.6(abort+0xcf)[0x7f933498a535]
[ 2] [runner-j2nyww-s-project-26315187-concurrent-0:04651] /usr/lib/libc.so.6(raise+0x18)[0x7fde11a28838]
[ 1] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f933498a45c]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 3] /usr/lib/libc.so.6(+0x9220c)[0x7f9bc2b4920c]
/usr/lib/libc.so.6(abort+0xcf)[0x7fde11a12535]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 5] [ 2] /usr/lib/libc.so.6(+0x3b366)[0x7f9334999366]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] /usr/lib/libc.so.6(raise+0x18)[0x7f9bc2af9838]
[ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 6] /usr/lib/libc.so.6(+0x2c45c)[0x7fde11a1245c]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f9bc2ae3535]
./bin/lens_immiscible_ecfv_ad(+0xb0db2)[0x55f01b026db2]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7fde11a21366]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 4] [runner-j2nyww-s-project-26315187-concurrent-0:04650] /usr/lib/libc.so.6(+0x2c45c)[0x7f9bc2ae345c]
[ 6] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 7] ./bin/lens_immiscible_ecfv_ad(+0x1b464e)[0x55f01b12a64e]
./bin/lens_immiscible_ecfv_ad(+0xb0db2)[0x55e22363edb2]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f9bc2af2366]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [runner-j2nyww-s-project-26315187-concurrent-0:04652] [ 8] [ 6] [runner-j2nyww-s-project-26315187-concurrent-0:04650] ./bin/lens_immiscible_ecfv_ad(+0xb0db2)[0x55b1ebbbfdb2]
[ 7] ./bin/lens_immiscible_ecfv_ad(+0x1aed54)[0x55f01b124d54]
./bin/lens_immiscible_ecfv_ad(+0x1b464e)[0x55e22374264e]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 7] [runner-j2nyww-s-project-26315187-concurrent-0:04652] ./bin/lens_immiscible_ecfv_ad(+0x1b464e)[0x55b1ebcc364e]
[ 9] [runner-j2nyww-s-project-26315187-concurrent-0:04650] [ 8] ./bin/lens_immiscible_ecfv_ad(+0x1a9f51)[0x55f01b11ff51]
./bin/lens_immiscible_ecfv_ad(+0x1aed54)[0x55e22373cd54]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 8] ./bin/lens_immiscible_ecfv_ad(+0x1aed54)[0x55b1ebcbdd54]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [10] [runner-j2nyww-s-project-26315187-concurrent-0:04650] [ 9] ./bin/lens_immiscible_ecfv_ad(+0x1a4bc2)[0x55f01b11abc2]
./bin/lens_immiscible_ecfv_ad(+0x1a9f51)[0x55e223737f51]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [ 9] ./bin/lens_immiscible_ecfv_ad(+0x1a9f51)[0x55b1ebcb8f51]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [runner-j2nyww-s-project-26315187-concurrent-0:04650] [10] [11] ./bin/lens_immiscible_ecfv_ad(+0x1a4bc2)[0x55e223732bc2]
./bin/lens_immiscible_ecfv_ad(+0x19d678)[0x55f01b113678]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [10] ./bin/lens_immiscible_ecfv_ad(+0x1a4bc2)[0x55b1ebcb3bc2]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [11] ./bin/lens_immiscible_ecfv_ad(+0x19d678)[0x55e22372b678]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [12] ./bin/lens_immiscible_ecfv_ad(+0x194aba)[0x55f01b10aaba]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [11] ./bin/lens_immiscible_ecfv_ad(+0x19d678)[0x55b1ebcac678]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [12] ./bin/lens_immiscible_ecfv_ad(+0x194aba)[0x55e223722aba]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [13] ./bin/lens_immiscible_ecfv_ad(+0x185b16)[0x55f01b0fbb16]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [12] lens_immiscible_ecfv_ad: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/opm/models/parallel/mpibuffer.hh:121: void Opm::MpiBuffer<DataType>::receive(unsigned int) [with DataType = unsigned int]: Assertion `!mpiStatus_.MPI_ERROR' failed.
[runner-j2nyww-s-project-26315187-concurrent-0:04649] *** Process received signal ***
[runner-j2nyww-s-project-26315187-concurrent-0:04649] Signal: Aborted (6)
[runner-j2nyww-s-project-26315187-concurrent-0:04649] Signal code:  (-6)
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 0] /usr/lib/libc.so.6(+0x428e0)[0x7f2f2377a8e0]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 1] /usr/lib/libc.so.6(+0x9220c)[0x7f2f237ca20c]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 2] /usr/lib/libc.so.6(raise+0x18)[0x7f2f2377a838]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 3] /usr/lib/libc.so.6(abort+0xcf)[0x7f2f23764535]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 4] /usr/lib/libc.so.6(+0x2c45c)[0x7f2f2376445c]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 5] /usr/lib/libc.so.6(+0x3b366)[0x7f2f23773366]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 6] ./bin/lens_immiscible_ecfv_ad(+0xb0db2)[0x563cb0e09db2]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 7] ./bin/lens_immiscible_ecfv_ad(+0x1b464e)[0x563cb0f0d64e]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 8] ./bin/lens_immiscible_ecfv_ad(+0x1aed54)[0x563cb0f07d54]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [ 9] ./bin/lens_immiscible_ecfv_ad(+0x1a9f51)[0x563cb0f02f51]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [10] ./bin/lens_immiscible_ecfv_ad(+0x1a4bc2)[0x563cb0efdbc2]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [11] ./bin/lens_immiscible_ecfv_ad(+0x19d678)[0x563cb0ef6678]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [12] ./bin/lens_immiscible_ecfv_ad(+0x194aba)[0x563cb0eedaba]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [13] ./bin/lens_immiscible_ecfv_ad(+0x185b16)[0x563cb0edeb16]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [14] ./bin/lens_immiscible_ecfv_ad(+0x17244d)[0x563cb0ecb44d]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [15] ./bin/lens_immiscible_ecfv_ad(+0x159846)[0x563cb0eb2846]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [16] ./bin/lens_immiscible_ecfv_ad(+0x143b45)[0x563cb0e9cb45]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [17] ./bin/lens_immiscible_ecfv_ad(+0x12ab01)[0x563cb0e83b01]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [18] ./bin/lens_immiscible_ecfv_ad(+0x115261)[0x563cb0e6e261]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [19] ./bin/lens_immiscible_ecfv_ad(+0xffb7d)[0x563cb0e58b7d]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [20] ./bin/lens_immiscible_ecfv_ad(+0xef3a8)[0x563cb0e483a8]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [21] ./bin/lens_immiscible_ecfv_ad(+0xdfd3e)[0x563cb0e38d3e]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [22] ./bin/lens_immiscible_ecfv_ad(+0xd0b4f)[0x563cb0e29b4f]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [23] ./bin/lens_immiscible_ecfv_ad(+0xbf740)[0x563cb0e18740]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [24] ./bin/lens_immiscible_ecfv_ad(+0x905be)[0x563cb0de95be]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [25] ./bin/lens_immiscible_ecfv_ad(+0x8febb)[0x563cb0de8ebb]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f2f23765290]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f2f2376534a]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] [28] ./bin/lens_immiscible_ecfv_ad(+0x8fa25)[0x563cb0de8a25]
[runner-j2nyww-s-project-26315187-concurrent-0:04649] *** End of error message ***
./bin/lens_immiscible_ecfv_ad(+0x194aba)[0x55b1ebca3aba]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [runner-j2nyww-s-project-26315187-concurrent-0:04650] [13] [14] ./bin/lens_immiscible_ecfv_ad(+0x185b16)[0x55e223713b16]
./bin/lens_immiscible_ecfv_ad(+0x17244d)[0x55f01b0e844d]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [13] ./bin/lens_immiscible_ecfv_ad(+0x185b16)[0x55b1ebc94b16]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [14] ./bin/lens_immiscible_ecfv_ad(+0x17244d)[0x55e22370044d]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [15] [runner-j2nyww-s-project-26315187-concurrent-0:04651] [14] ./bin/lens_immiscible_ecfv_ad(+0x17244d)[0x55b1ebc8144d]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [15] ./bin/lens_immiscible_ecfv_ad(+0x159846)[0x55b1ebc68846]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [16] ./bin/lens_immiscible_ecfv_ad(+0x143b45)[0x55b1ebc52b45]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [17] ./bin/lens_immiscible_ecfv_ad(+0x12ab01)[0x55b1ebc39b01]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [18] ./bin/lens_immiscible_ecfv_ad(+0x115261)[0x55b1ebc24261]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [19] ./bin/lens_immiscible_ecfv_ad(+0xffb7d)[0x55b1ebc0eb7d]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [20] ./bin/lens_immiscible_ecfv_ad(+0xef3a8)[0x55b1ebbfe3a8]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [21] ./bin/lens_immiscible_ecfv_ad(+0xdfd3e)[0x55b1ebbeed3e]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [22] ./bin/lens_immiscible_ecfv_ad(+0xd0b4f)[0x55b1ebbdfb4f]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [23] ./bin/lens_immiscible_ecfv_ad(+0xbf740)[0x55b1ebbce740]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [24] ./bin/lens_immiscible_ecfv_ad(+0x905be)[0x55b1ebb9f5be]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [25] ./bin/lens_immiscible_ecfv_ad(+0x8febb)[0x55b1ebb9eebb]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f9bc2ae4290]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f9bc2ae434a]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] [28] ./bin/lens_immiscible_ecfv_ad(+0x8fa25)[0x55b1ebb9ea25]
[runner-j2nyww-s-project-26315187-concurrent-0:04651] *** End of error message ***
./bin/lens_immiscible_ecfv_ad(+0x159846)[0x55f01b0cf846]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [15] ./bin/lens_immiscible_ecfv_ad(+0x159846)[0x55e2236e7846]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [16] ./bin/lens_immiscible_ecfv_ad(+0x143b45)[0x55e2236d1b45]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [17] ./bin/lens_immiscible_ecfv_ad(+0x12ab01)[0x55e2236b8b01]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [18] ./bin/lens_immiscible_ecfv_ad(+0x115261)[0x55e2236a3261]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [19] ./bin/lens_immiscible_ecfv_ad(+0xffb7d)[0x55e22368db7d]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [20] ./bin/lens_immiscible_ecfv_ad(+0xef3a8)[0x55e22367d3a8]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [21] ./bin/lens_immiscible_ecfv_ad(+0xdfd3e)[0x55e22366dd3e]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [22] ./bin/lens_immiscible_ecfv_ad(+0xd0b4f)[0x55e22365eb4f]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [23] ./bin/lens_immiscible_ecfv_ad(+0xbf740)[0x55e22364d740]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [24] ./bin/lens_immiscible_ecfv_ad(+0x905be)[0x55e22361e5be]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [25] ./bin/lens_immiscible_ecfv_ad(+0x8febb)[0x55e22361debb]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [26] /usr/lib/libc.so.6(+0x2d290)[0x7fde11a13290]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7fde11a1334a]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] [28] ./bin/lens_immiscible_ecfv_ad(+0x8fa25)[0x55e22361da25]
[runner-j2nyww-s-project-26315187-concurrent-0:04650] *** End of error message ***
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [16] ./bin/lens_immiscible_ecfv_ad(+0x143b45)[0x55f01b0b9b45]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [17] ./bin/lens_immiscible_ecfv_ad(+0x12ab01)[0x55f01b0a0b01]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [18] ./bin/lens_immiscible_ecfv_ad(+0x115261)[0x55f01b08b261]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [19] ./bin/lens_immiscible_ecfv_ad(+0xffb7d)[0x55f01b075b7d]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [20] ./bin/lens_immiscible_ecfv_ad(+0xef3a8)[0x55f01b0653a8]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [21] ./bin/lens_immiscible_ecfv_ad(+0xdfd3e)[0x55f01b055d3e]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [22] ./bin/lens_immiscible_ecfv_ad(+0xd0b4f)[0x55f01b046b4f]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [23] ./bin/lens_immiscible_ecfv_ad(+0xbf740)[0x55f01b035740]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [24] ./bin/lens_immiscible_ecfv_ad(+0x905be)[0x55f01b0065be]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [25] ./bin/lens_immiscible_ecfv_ad(+0x8febb)[0x55f01b005ebb]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [26] /usr/lib/libc.so.6(+0x2d290)[0x7f933498b290]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [27] /usr/lib/libc.so.6(__libc_start_main+0x8a)[0x7f933498b34a]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] [28] ./bin/lens_immiscible_ecfv_ad(+0x8fa25)[0x55f01b005a25]
[runner-j2nyww-s-project-26315187-concurrent-0:04652] *** End of error message ***
--------------------------------------------------------------------------
Primary job  terminated normally, but 1 process returned
a non-zero exit code. Per user-direction, the job has been aborted.
--------------------------------------------------------------------------
--------------------------------------------------------------------------
mpirun noticed that process rank 3 with PID 0 on node runner-j2nyww-s-project-26315187-concurrent-0 exited on signal 6 (Aborted).
--------------------------------------------------------------------------
Executing the binary failed!
test 54
      Start 54: obstacle_immiscible_parameters
54: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--parameters" "-e" "obstacle_immiscible" "--"
54: Test timeout computed to be: 1500
54: ######################
54: # Running test 'obstacle_immiscible'
54: ######################
54: Parameter file "foobar.ini" does not exist or is not readable.
54: 
54: ./bin/obstacle_immiscible
54: Recognized options:
54:     -h,--help                                     Print this help message and exit
54:     --help-all                                    Print all parameters, including obsolete, hidden and deprecated ones.
54:     --continue-on-convergence-error=BOOLEAN       Continue with a non-converged solution instead of giving up if we encounter a time step size smaller than the minimum time step size. Default: false
54:     --enable-async-vtk-output=BOOLEAN             Dispatch a separate thread to write the VTK output. Default: true
54:     --enable-gravity=BOOLEAN                      Use the gravity correction for the pressure gradients. Default: true
54:     --enable-grid-adaptation=BOOLEAN              Enable adaptive grid refinement/coarsening. Default: false
54:     --enable-intensive-quantity-cache=BOOLEAN     Turn on caching of intensive quantities. Default: false
54:     --enable-storage-cache=BOOLEAN                Store previous storage terms and avoid re-calculating them. Default: false
54:     --enable-thermodynamic-hints=BOOLEAN          Enable thermodynamic hints. Default: false
54:     --enable-vtk-output=BOOLEAN                   Global switch for turning on writing VTK files. Default: true
54:     --end-time=SCALAR                             The simulation time at which the simulation is finished [s]. Default: 10000
54:     --grid-file=STRING                            The file name of the DGF file to load. Default: "./data/obstacle_24x16.dgf"
54:     --grid-global-refinements=INTEGER             The number of global refinements of the grid executed after it was loaded. Default: 0
54:     --initial-time-step-size=SCALAR               The size of the initial time step [s]. Default: 250
54:     --linear-solver-abs-tolerance=SCALAR          The maximum accepted error of the norm of the residual. Default: -1
54:     --linear-solver-max-error=SCALAR              The maximum residual error which the linear solver tolerates without giving up. Default: 1e+07
54:     --linear-solver-max-iterations=INTEGER        The maximum number of iterations of the linear solver. Default: 1000
54:     --linear-solver-overlap-size=INTEGER          The size of the algebraic overlap for the linear solver. Default: 2
54:     --linear-solver-tolerance=SCALAR              The maximum allowed error between of the linear solver. Default: 0.001
54:     --linear-solver-verbosity=INTEGER             The verbosity level of the linear solver. Default: 0
54:     --max-time-step-divisions=INTEGER             The maximum number of divisions by two of the timestep size before the simulation bails out. Default: 10
54:     --max-time-step-size=SCALAR                   The maximum size to which all time steps are limited to [s]. Default: inf
54:     --min-time-step-size=SCALAR                   The minimum size to which all time steps are limited to [s]. Default: 0
54:     --newton-max-error=SCALAR                     The maximum error tolerated by the Newton method to which does not cause an abort. Default: 1e+100
54:     --newton-max-iterations=INTEGER               The maximum number of Newton iterations per time step. Default: 18
54:     --newton-target-iterations=INTEGER            The 'optimum' number of Newton iterations per time step. Default: 10
54:     --newton-tolerance=SCALAR                     The maximum raw error tolerated by the Newtonmethod for considering a solution to be converged. Default: 1e-08
54:     --newton-verbose=BOOLEAN                      Specify whether the Newton method should inform the user about its progress or not. Default: true
54:     --newton-write-convergence=BOOLEAN            Write the convergence behaviour of the Newton method to a VTK file. Default: false
54:     --numeric-difference-method=INTEGER           The method used for numeric differentiation (-1: backward differences, 0: central differences, 1: forward differences). Default: 1
54:     --output-dir=STRING                           The directory to which result files are written. Default: "."
54:     --parameter-file=STRING                       An .ini file which contains a set of run-time parameters. Default: ""
54:     --preconditioner-relaxation=SCALAR            The relaxation factor of the preconditioner. Default: 1
54:     --predetermined-time-steps-file=STRING        A file with a list of predetermined time step sizes (one time step per line). Default: ""
54:     --print-parameters=INTEGER                    Print the values of the run-time parameters at the start of the simulation. Default: 2
54:     --print-properties=INTEGER                    Print the values of the compile time properties at the start of the simulation. Default: 2
54:     --restart-time=SCALAR                         The simulation time at which a restart should be attempted [s]. Default: -1e+35
54:     --threads-per-process=INTEGER                 The maximum number of threads to be instantiated per process ('-1' means 'automatic'). Default: 1
54:     --vtk-write-average-molar-masses=BOOLEAN      Include the average phase mass in the VTK output files. Default: false
54:     --vtk-write-densities=BOOLEAN                 Include the phase densities in the VTK output files. Default: true
54:     --vtk-write-dof-index=BOOLEAN                 Include the index of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-extrusion-factor=BOOLEAN          Include the extrusion factor of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-filter-velocities=BOOLEAN         Include in the filter velocities of the phases the VTK output files. Default: false
54:     --vtk-write-intrinsic-permeabilities=BOOLEAN  Include the intrinsic permeability in the VTK output files. Default: false
54:     --vtk-write-mobilities=BOOLEAN                Include the phase mobilities in the VTK output files. Default: false
54:     --vtk-write-porosity=BOOLEAN                  Include the porosity in the VTK output files. Default: true
54:     --vtk-write-potential-gradients=BOOLEAN       Include the phase pressure potential gradients in the VTK output files. Default: false
54:     --vtk-write-pressures=BOOLEAN                 Include the phase pressures in the VTK output files. Default: true
54:     --vtk-write-primary-vars=BOOLEAN              Include the primary variables into the VTK output files. Default: false
54:     --vtk-write-process-rank=BOOLEAN              Include the MPI process rank into the VTK output files. Default: false
54:     --vtk-write-relative-permeabilities=BOOLEAN   Include the phase relative permeabilities in the VTK output files. Default: true
54:     --vtk-write-saturations=BOOLEAN               Include the phase saturations in the VTK output files. Default: true
54:     --vtk-write-temperature=BOOLEAN               Include the temperature in the VTK output files. Default: true
54:     --vtk-write-viscosities=BOOLEAN               Include component phase viscosities in the VTK output files. Default: false
54: The following explicitly specified parameter is unknown:
54: 
54:    UndefinedParam="bla"
54: 
54: Use
54: 
54:   ./bin/obstacle_immiscible --help
54: 
54: to obtain the list of recognized command line parameters.
54: 
54: Parameter 'Foo' is missing a value.  Please use --foo=value.
54: 
54: Usage: ./bin/obstacle_immiscible [OPTIONS]
54: 
54: Recognized options:
54:     -h,--help                                     Print this help message and exit
54:     --help-all                                    Print all parameters, including obsolete, hidden and deprecated ones.
54:     --continue-on-convergence-error=BOOLEAN       Continue with a non-converged solution instead of giving up if we encounter a time step size smaller than the minimum time step size. Default: false
54:     --enable-async-vtk-output=BOOLEAN             Dispatch a separate thread to write the VTK output. Default: true
54:     --enable-gravity=BOOLEAN                      Use the gravity correction for the pressure gradients. Default: true
54:     --enable-grid-adaptation=BOOLEAN              Enable adaptive grid refinement/coarsening. Default: false
54:     --enable-intensive-quantity-cache=BOOLEAN     Turn on caching of intensive quantities. Default: false
54:     --enable-storage-cache=BOOLEAN                Store previous storage terms and avoid re-calculating them. Default: false
54:     --enable-thermodynamic-hints=BOOLEAN          Enable thermodynamic hints. Default: false
54:     --enable-vtk-output=BOOLEAN                   Global switch for turning on writing VTK files. Default: true
54:     --end-time=SCALAR                             The simulation time at which the simulation is finished [s]. Default: 10000
54:     --grid-file=STRING                            The file name of the DGF file to load. Default: "./data/obstacle_24x16.dgf"
54:     --grid-global-refinements=INTEGER             The number of global refinements of the grid executed after it was loaded. Default: 0
54:     --initial-time-step-size=SCALAR               The size of the initial time step [s]. Default: 250
54:     --linear-solver-abs-tolerance=SCALAR          The maximum accepted error of the norm of the residual. Default: -1
54:     --linear-solver-max-error=SCALAR              The maximum residual error which the linear solver tolerates without giving up. Default: 1e+07
54:     --linear-solver-max-iterations=INTEGER        The maximum number of iterations of the linear solver. Default: 1000
54:     --linear-solver-overlap-size=INTEGER          The size of the algebraic overlap for the linear solver. Default: 2
54:     --linear-solver-tolerance=SCALAR              The maximum allowed error between of the linear solver. Default: 0.001
54:     --linear-solver-verbosity=INTEGER             The verbosity level of the linear solver. Default: 0
54:     --max-time-step-divisions=INTEGER             The maximum number of divisions by two of the timestep size before the simulation bails out. Default: 10
54:     --max-time-step-size=SCALAR                   The maximum size to which all time steps are limited to [s]. Default: inf
54:     --min-time-step-size=SCALAR                   The minimum size to which all time steps are limited to [s]. Default: 0
54:     --newton-max-error=SCALAR                     The maximum error tolerated by the Newton method to which does not cause an abort. Default: 1e+100
54:     --newton-max-iterations=INTEGER               The maximum number of Newton iterations per time step. Default: 18
54:     --newton-target-iterations=INTEGER            The 'optimum' number of Newton iterations per time step. Default: 10
54:     --newton-tolerance=SCALAR                     The maximum raw error tolerated by the Newtonmethod for considering a solution to be converged. Default: 1e-08
54:     --newton-verbose=BOOLEAN                      Specify whether the Newton method should inform the user about its progress or not. Default: true
54:     --newton-write-convergence=BOOLEAN            Write the convergence behaviour of the Newton method to a VTK file. Default: false
54:     --numeric-difference-method=INTEGER           The method used for numeric differentiation (-1: backward differences, 0: central differences, 1: forward differences). Default: 1
54:     --output-dir=STRING                           The directory to which result files are written. Default: "."
54:     --parameter-file=STRING                       An .ini file which contains a set of run-time parameters. Default: ""
54:     --preconditioner-relaxation=SCALAR            The relaxation factor of the preconditioner. Default: 1
54:     --predetermined-time-steps-file=STRING        A file with a list of predetermined time step sizes (one time step per line). Default: ""
54:     --print-parameters=INTEGER                    Print the values of the run-time parameters at the start of the simulation. Default: 2
54:     --print-properties=INTEGER                    Print the values of the compile time properties at the start of the simulation. Default: 2
54:     --restart-time=SCALAR                         The simulation time at which a restart should be attempted [s]. Default: -1e+35
54:     --threads-per-process=INTEGER                 The maximum number of threads to be instantiated per process ('-1' means 'automatic'). Default: 1
54:     --vtk-write-average-molar-masses=BOOLEAN      Include the average phase mass in the VTK output files. Default: false
54:     --vtk-write-densities=BOOLEAN                 Include the phase densities in the VTK output files. Default: true
54:     --vtk-write-dof-index=BOOLEAN                 Include the index of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-extrusion-factor=BOOLEAN          Include the extrusion factor of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-filter-velocities=BOOLEAN         Include in the filter velocities of the phases the VTK output files. Default: false
54:     --vtk-write-intrinsic-permeabilities=BOOLEAN  Include the intrinsic permeability in the VTK output files. Default: false
54:     --vtk-write-mobilities=BOOLEAN                Include the phase mobilities in the VTK output files. Default: false
54:     --vtk-write-porosity=BOOLEAN                  Include the porosity in the VTK output files. Default: true
54:     --vtk-write-potential-gradients=BOOLEAN       Include the phase pressure potential gradients in the VTK output files. Default: false
54:     --vtk-write-pressures=BOOLEAN                 Include the phase pressures in the VTK output files. Default: true
54:     --vtk-write-primary-vars=BOOLEAN              Include the primary variables into the VTK output files. Default: false
54:     --vtk-write-process-rank=BOOLEAN              Include the MPI process rank into the VTK output files. Default: false
54:     --vtk-write-relative-permeabilities=BOOLEAN   Include the phase relative permeabilities in the VTK output files. Default: true
54:     --vtk-write-saturations=BOOLEAN               Include the phase saturations in the VTK output files. Default: true
54:     --vtk-write-temperature=BOOLEAN               Include the temperature in the VTK output files. Default: true
54:     --vtk-write-viscosities=BOOLEAN               Include component phase viscosities in the VTK output files. Default: false
54: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh: line 82:  4961 Segmentation fault      (core dumped) $TEST_BINARY --foo 2>&1 > /dev/null
54: Illegal parameter "foo".
54: 
54: Usage: ./bin/obstacle_immiscible [OPTIONS]
54: 
54: Recognized options:
54:     -h,--help                                     Print this help message and exit
54:     --help-all                                    Print all parameters, including obsolete, hidden and deprecated ones.
54:     --continue-on-convergence-error=BOOLEAN       Continue with a non-converged solution instead of giving up if we encounter a time step size smaller than the minimum time step size. Default: false
54:     --enable-async-vtk-output=BOOLEAN             Dispatch a separate thread to write the VTK output. Default: true
54:     --enable-gravity=BOOLEAN                      Use the gravity correction for the pressure gradients. Default: true
54:     --enable-grid-adaptation=BOOLEAN              Enable adaptive grid refinement/coarsening. Default: false
54:     --enable-intensive-quantity-cache=BOOLEAN     Turn on caching of intensive quantities. Default: false
54:     --enable-storage-cache=BOOLEAN                Store previous storage terms and avoid re-calculating them. Default: false
54:     --enable-thermodynamic-hints=BOOLEAN          Enable thermodynamic hints. Default: false
54:     --enable-vtk-output=BOOLEAN                   Global switch for turning on writing VTK files. Default: true
54:     --end-time=SCALAR                             The simulation time at which the simulation is finished [s]. Default: 10000
54:     --grid-file=STRING                            The file name of the DGF file to load. Default: "./data/obstacle_24x16.dgf"
54:     --grid-global-refinements=INTEGER             The number of global refinements of the grid executed after it was loaded. Default: 0
54:     --initial-time-step-size=SCALAR               The size of the initial time step [s]. Default: 250
54:     --linear-solver-abs-tolerance=SCALAR          The maximum accepted error of the norm of the residual. Default: -1
54:     --linear-solver-max-error=SCALAR              The maximum residual error which the linear solver tolerates without giving up. Default: 1e+07
54:     --linear-solver-max-iterations=INTEGER        The maximum number of iterations of the linear solver. Default: 1000
54:     --linear-solver-overlap-size=INTEGER          The size of the algebraic overlap for the linear solver. Default: 2
54:     --linear-solver-tolerance=SCALAR              The maximum allowed error between of the linear solver. Default: 0.001
54:     --linear-solver-verbosity=INTEGER             The verbosity level of the linear solver. Default: 0
54:     --max-time-step-divisions=INTEGER             The maximum number of divisions by two of the timestep size before the simulation bails out. Default: 10
54:     --max-time-step-size=SCALAR                   The maximum size to which all time steps are limited to [s]. Default: inf
54:     --min-time-step-size=SCALAR                   The minimum size to which all time steps are limited to [s]. Default: 0
54:     --newton-max-error=SCALAR                     The maximum error tolerated by the Newton method to which does not cause an abort. Default: 1e+100
54:     --newton-max-iterations=INTEGER               The maximum number of Newton iterations per time step. Default: 18
54:     --newton-target-iterations=INTEGER            The 'optimum' number of Newton iterations per time step. Default: 10
54:     --newton-tolerance=SCALAR                     The maximum raw error tolerated by the Newtonmethod for considering a solution to be converged. Default: 1e-08
54:     --newton-verbose=BOOLEAN                      Specify whether the Newton method should inform the user about its progress or not. Default: true
54:     --newton-write-convergence=BOOLEAN            Write the convergence behaviour of the Newton method to a VTK file. Default: false
54:     --numeric-difference-method=INTEGER           The method used for numeric differentiation (-1: backward differences, 0: central differences, 1: forward differences). Default: 1
54:     --output-dir=STRING                           The directory to which result files are written. Default: "."
54:     --parameter-file=STRING                       An .ini file which contains a set of run-time parameters. Default: ""
54:     --preconditioner-relaxation=SCALAR            The relaxation factor of the preconditioner. Default: 1
54:     --predetermined-time-steps-file=STRING        A file with a list of predetermined time step sizes (one time step per line). Default: ""
54:     --print-parameters=INTEGER                    Print the values of the run-time parameters at the start of the simulation. Default: 2
54:     --print-properties=INTEGER                    Print the values of the compile time properties at the start of the simulation. Default: 2
54:     --restart-time=SCALAR                         The simulation time at which a restart should be attempted [s]. Default: -1e+35
54:     --threads-per-process=INTEGER                 The maximum number of threads to be instantiated per process ('-1' means 'automatic'). Default: 1
54:     --vtk-write-average-molar-masses=BOOLEAN      Include the average phase mass in the VTK output files. Default: false
54:     --vtk-write-densities=BOOLEAN                 Include the phase densities in the VTK output files. Default: true
54:     --vtk-write-dof-index=BOOLEAN                 Include the index of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-extrusion-factor=BOOLEAN          Include the extrusion factor of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-filter-velocities=BOOLEAN         Include in the filter velocities of the phases the VTK output files. Default: false
54:     --vtk-write-intrinsic-permeabilities=BOOLEAN  Include the intrinsic permeability in the VTK output files. Default: false
54:     --vtk-write-mobilities=BOOLEAN                Include the phase mobilities in the VTK output files. Default: false
54:     --vtk-write-porosity=BOOLEAN                  Include the porosity in the VTK output files. Default: true
54:     --vtk-write-potential-gradients=BOOLEAN       Include the phase pressure potential gradients in the VTK output files. Default: false
54:     --vtk-write-pressures=BOOLEAN                 Include the phase pressures in the VTK output files. Default: true
54:     --vtk-write-primary-vars=BOOLEAN              Include the primary variables into the VTK output files. Default: false
54:     --vtk-write-process-rank=BOOLEAN              Include the MPI process rank into the VTK output files. Default: false
54:     --vtk-write-relative-permeabilities=BOOLEAN   Include the phase relative permeabilities in the VTK output files. Default: true
54:     --vtk-write-saturations=BOOLEAN               Include the phase saturations in the VTK output files. Default: true
54:     --vtk-write-temperature=BOOLEAN               Include the temperature in the VTK output files. Default: true
54:     --vtk-write-viscosities=BOOLEAN               Include component phase viscosities in the VTK output files. Default: false
54: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh: line 245:  4962 Segmentation fault      (core dumped) $TEST_BINARY "$PARAM" --end-time=100 2>&1 > /dev/null
54: Illegal parameter "--".
54: 
54: Usage: ./bin/obstacle_immiscible [OPTIONS]
54: 
54: Recognized options:
54:     -h,--help                                     Print this help message and exit
54:     --help-all                                    Print all parameters, including obsolete, hidden and deprecated ones.
54:     --continue-on-convergence-error=BOOLEAN       Continue with a non-converged solution instead of giving up if we encounter a time step size smaller than the minimum time step size. Default: false
54:     --enable-async-vtk-output=BOOLEAN             Dispatch a separate thread to write the VTK output. Default: true
54:     --enable-gravity=BOOLEAN                      Use the gravity correction for the pressure gradients. Default: true
54:     --enable-grid-adaptation=BOOLEAN              Enable adaptive grid refinement/coarsening. Default: false
54:     --enable-intensive-quantity-cache=BOOLEAN     Turn on caching of intensive quantities. Default: false
54:     --enable-storage-cache=BOOLEAN                Store previous storage terms and avoid re-calculating them. Default: false
54:     --enable-thermodynamic-hints=BOOLEAN          Enable thermodynamic hints. Default: false
54:     --enable-vtk-output=BOOLEAN                   Global switch for turning on writing VTK files. Default: true
54:     --end-time=SCALAR                             The simulation time at which the simulation is finished [s]. Default: 10000
54:     --grid-file=STRING                            The file name of the DGF file to load. Default: "./data/obstacle_24x16.dgf"
54:     --grid-global-refinements=INTEGER             The number of global refinements of the grid executed after it was loaded. Default: 0
54:     --initial-time-step-size=SCALAR               The size of the initial time step [s]. Default: 250
54:     --linear-solver-abs-tolerance=SCALAR          The maximum accepted error of the norm of the residual. Default: -1
54:     --linear-solver-max-error=SCALAR              The maximum residual error which the linear solver tolerates without giving up. Default: 1e+07
54:     --linear-solver-max-iterations=INTEGER        The maximum number of iterations of the linear solver. Default: 1000
54:     --linear-solver-overlap-size=INTEGER          The size of the algebraic overlap for the linear solver. Default: 2
54:     --linear-solver-tolerance=SCALAR              The maximum allowed error between of the linear solver. Default: 0.001
54:     --linear-solver-verbosity=INTEGER             The verbosity level of the linear solver. Default: 0
54:     --max-time-step-divisions=INTEGER             The maximum number of divisions by two of the timestep size before the simulation bails out. Default: 10
54:     --max-time-step-size=SCALAR                   The maximum size to which all time steps are limited to [s]. Default: inf
54:     --min-time-step-size=SCALAR                   The minimum size to which all time steps are limited to [s]. Default: 0
54:     --newton-max-error=SCALAR                     The maximum error tolerated by the Newton method to which does not cause an abort. Default: 1e+100
54:     --newton-max-iterations=INTEGER               The maximum number of Newton iterations per time step. Default: 18
54:     --newton-target-iterations=INTEGER            The 'optimum' number of Newton iterations per time step. Default: 10
54:     --newton-tolerance=SCALAR                     The maximum raw error tolerated by the Newtonmethod for considering a solution to be converged. Default: 1e-08
54:     --newton-verbose=BOOLEAN                      Specify whether the Newton method should inform the user about its progress or not. Default: true
54:     --newton-write-convergence=BOOLEAN            Write the convergence behaviour of the Newton method to a VTK file. Default: false
54:     --numeric-difference-method=INTEGER           The method used for numeric differentiation (-1: backward differences, 0: central differences, 1: forward differences). Default: 1
54:     --output-dir=STRING                           The directory to which result files are written. Default: "."
54:     --parameter-file=STRING                       An .ini file which contains a set of run-time parameters. Default: ""
54:     --preconditioner-relaxation=SCALAR            The relaxation factor of the preconditioner. Default: 1
54:     --predetermined-time-steps-file=STRING        A file with a list of predetermined time step sizes (one time step per line). Default: ""
54:     --print-parameters=INTEGER                    Print the values of the run-time parameters at the start of the simulation. Default: 2
54:     --print-properties=INTEGER                    Print the values of the compile time properties at the start of the simulation. Default: 2
54:     --restart-time=SCALAR                         The simulation time at which a restart should be attempted [s]. Default: -1e+35
54:     --threads-per-process=INTEGER                 The maximum number of threads to be instantiated per process ('-1' means 'automatic'). Default: 1
54:     --vtk-write-average-molar-masses=BOOLEAN      Include the average phase mass in the VTK output files. Default: false
54:     --vtk-write-densities=BOOLEAN                 Include the phase densities in the VTK output files. Default: true
54:     --vtk-write-dof-index=BOOLEAN                 Include the index of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-extrusion-factor=BOOLEAN          Include the extrusion factor of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-filter-velocities=BOOLEAN         Include in the filter velocities of the phases the VTK output files. Default: false
54:     --vtk-write-intrinsic-permeabilities=BOOLEAN  Include the intrinsic permeability in the VTK output files. Default: false
54:     --vtk-write-mobilities=BOOLEAN                Include the phase mobilities in the VTK output files. Default: false
54:     --vtk-write-porosity=BOOLEAN                  Include the porosity in the VTK output files. Default: true
54:     --vtk-write-potential-gradients=BOOLEAN       Include the phase pressure potential gradients in the VTK output files. Default: false
54:     --vtk-write-pressures=BOOLEAN                 Include the phase pressures in the VTK output files. Default: true
54:     --vtk-write-primary-vars=BOOLEAN              Include the primary variables into the VTK output files. Default: false
54:     --vtk-write-process-rank=BOOLEAN              Include the MPI process rank into the VTK output files. Default: false
54:     --vtk-write-relative-permeabilities=BOOLEAN   Include the phase relative permeabilities in the VTK output files. Default: true
54:     --vtk-write-saturations=BOOLEAN               Include the phase saturations in the VTK output files. Default: true
54:     --vtk-write-temperature=BOOLEAN               Include the temperature in the VTK output files. Default: true
54:     --vtk-write-viscosities=BOOLEAN               Include component phase viscosities in the VTK output files. Default: false
54: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh: line 245:  4963 Segmentation fault      (core dumped) $TEST_BINARY "$PARAM" --end-time=100 2>&1 > /dev/null
54: Illegal parameter "-0foo".
54: 
54: Usage: ./bin/obstacle_immiscible [OPTIONS]
54: 
54: Recognized options:
54:     -h,--help                                     Print this help message and exit
54:     --help-all                                    Print all parameters, including obsolete, hidden and deprecated ones.
54:     --continue-on-convergence-error=BOOLEAN       Continue with a non-converged solution instead of giving up if we encounter a time step size smaller than the minimum time step size. Default: false
54:     --enable-async-vtk-output=BOOLEAN             Dispatch a separate thread to write the VTK output. Default: true
54:     --enable-gravity=BOOLEAN                      Use the gravity correction for the pressure gradients. Default: true
54:     --enable-grid-adaptation=BOOLEAN              Enable adaptive grid refinement/coarsening. Default: false
54:     --enable-intensive-quantity-cache=BOOLEAN     Turn on caching of intensive quantities. Default: false
54:     --enable-storage-cache=BOOLEAN                Store previous storage terms and avoid re-calculating them. Default: false
54:     --enable-thermodynamic-hints=BOOLEAN          Enable thermodynamic hints. Default: false
54:     --enable-vtk-output=BOOLEAN                   Global switch for turning on writing VTK files. Default: true
54:     --end-time=SCALAR                             The simulation time at which the simulation is finished [s]. Default: 10000
54:     --grid-file=STRING                            The file name of the DGF file to load. Default: "./data/obstacle_24x16.dgf"
54:     --grid-global-refinements=INTEGER             The number of global refinements of the grid executed after it was loaded. Default: 0
54:     --initial-time-step-size=SCALAR               The size of the initial time step [s]. Default: 250
54:     --linear-solver-abs-tolerance=SCALAR          The maximum accepted error of the norm of the residual. Default: -1
54:     --linear-solver-max-error=SCALAR              The maximum residual error which the linear solver tolerates without giving up. Default: 1e+07
54:     --linear-solver-max-iterations=INTEGER        The maximum number of iterations of the linear solver. Default: 1000
54:     --linear-solver-overlap-size=INTEGER          The size of the algebraic overlap for the linear solver. Default: 2
54:     --linear-solver-tolerance=SCALAR              The maximum allowed error between of the linear solver. Default: 0.001
54:     --linear-solver-verbosity=INTEGER             The verbosity level of the linear solver. Default: 0
54:     --max-time-step-divisions=INTEGER             The maximum number of divisions by two of the timestep size before the simulation bails out. Default: 10
54:     --max-time-step-size=SCALAR                   The maximum size to which all time steps are limited to [s]. Default: inf
54:     --min-time-step-size=SCALAR                   The minimum size to which all time steps are limited to [s]. Default: 0
54:     --newton-max-error=SCALAR                     The maximum error tolerated by the Newton method to which does not cause an abort. Default: 1e+100
54:     --newton-max-iterations=INTEGER               The maximum number of Newton iterations per time step. Default: 18
54:     --newton-target-iterations=INTEGER            The 'optimum' number of Newton iterations per time step. Default: 10
54:     --newton-tolerance=SCALAR                     The maximum raw error tolerated by the Newtonmethod for considering a solution to be converged. Default: 1e-08
54:     --newton-verbose=BOOLEAN                      Specify whether the Newton method should inform the user about its progress or not. Default: true
54:     --newton-write-convergence=BOOLEAN            Write the convergence behaviour of the Newton method to a VTK file. Default: false
54:     --numeric-difference-method=INTEGER           The method used for numeric differentiation (-1: backward differences, 0: central differences, 1: forward differences). Default: 1
54:     --output-dir=STRING                           The directory to which result files are written. Default: "."
54:     --parameter-file=STRING                       An .ini file which contains a set of run-time parameters. Default: ""
54:     --preconditioner-relaxation=SCALAR            The relaxation factor of the preconditioner. Default: 1
54:     --predetermined-time-steps-file=STRING        A file with a list of predetermined time step sizes (one time step per line). Default: ""
54:     --print-parameters=INTEGER                    Print the values of the run-time parameters at the start of the simulation. Default: 2
54:     --print-properties=INTEGER                    Print the values of the compile time properties at the start of the simulation. Default: 2
54:     --restart-time=SCALAR                         The simulation time at which a restart should be attempted [s]. Default: -1e+35
54:     --threads-per-process=INTEGER                 The maximum number of threads to be instantiated per process ('-1' means 'automatic'). Default: 1
54:     --vtk-write-average-molar-masses=BOOLEAN      Include the average phase mass in the VTK output files. Default: false
54:     --vtk-write-densities=BOOLEAN                 Include the phase densities in the VTK output files. Default: true
54:     --vtk-write-dof-index=BOOLEAN                 Include the index of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-extrusion-factor=BOOLEAN          Include the extrusion factor of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-filter-velocities=BOOLEAN         Include in the filter velocities of the phases the VTK output files. Default: false
54:     --vtk-write-intrinsic-permeabilities=BOOLEAN  Include the intrinsic permeability in the VTK output files. Default: false
54:     --vtk-write-mobilities=BOOLEAN                Include the phase mobilities in the VTK output files. Default: false
54:     --vtk-write-porosity=BOOLEAN                  Include the porosity in the VTK output files. Default: true
54:     --vtk-write-potential-gradients=BOOLEAN       Include the phase pressure potential gradients in the VTK output files. Default: false
54:     --vtk-write-pressures=BOOLEAN                 Include the phase pressures in the VTK output files. Default: true
54:     --vtk-write-primary-vars=BOOLEAN              Include the primary variables into the VTK output files. Default: false
54:     --vtk-write-process-rank=BOOLEAN              Include the MPI process rank into the VTK output files. Default: false
54:     --vtk-write-relative-permeabilities=BOOLEAN   Include the phase relative permeabilities in the VTK output files. Default: true
54:     --vtk-write-saturations=BOOLEAN               Include the phase saturations in the VTK output files. Default: true
54:     --vtk-write-temperature=BOOLEAN               Include the temperature in the VTK output files. Default: true
54:     --vtk-write-viscosities=BOOLEAN               Include component phase viscosities in the VTK output files. Default: false
54: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh: line 245:  4964 Segmentation fault      (core dumped) $TEST_BINARY "$PARAM" --end-time=100 2>&1 > /dev/null
54: Parameter name of argument 1 ('--0foo') is invalid because it does not start with a letter.
54: 
54: Usage: ./bin/obstacle_immiscible [OPTIONS]
54: 
54: Recognized options:
54:     -h,--help                                     Print this help message and exit
54:     --help-all                                    Print all parameters, including obsolete, hidden and deprecated ones.
54:     --continue-on-convergence-error=BOOLEAN       Continue with a non-converged solution instead of giving up if we encounter a time step size smaller than the minimum time step size. Default: false
54:     --enable-async-vtk-output=BOOLEAN             Dispatch a separate thread to write the VTK output. Default: true
54:     --enable-gravity=BOOLEAN                      Use the gravity correction for the pressure gradients. Default: true
54:     --enable-grid-adaptation=BOOLEAN              Enable adaptive grid refinement/coarsening. Default: false
54:     --enable-intensive-quantity-cache=BOOLEAN     Turn on caching of intensive quantities. Default: false
54:     --enable-storage-cache=BOOLEAN                Store previous storage terms and avoid re-calculating them. Default: false
54:     --enable-thermodynamic-hints=BOOLEAN          Enable thermodynamic hints. Default: false
54:     --enable-vtk-output=BOOLEAN                   Global switch for turning on writing VTK files. Default: true
54:     --end-time=SCALAR                             The simulation time at which the simulation is finished [s]. Default: 10000
54:     --grid-file=STRING                            The file name of the DGF file to load. Default: "./data/obstacle_24x16.dgf"
54:     --grid-global-refinements=INTEGER             The number of global refinements of the grid executed after it was loaded. Default: 0
54:     --initial-time-step-size=SCALAR               The size of the initial time step [s]. Default: 250
54:     --linear-solver-abs-tolerance=SCALAR          The maximum accepted error of the norm of the residual. Default: -1
54:     --linear-solver-max-error=SCALAR              The maximum residual error which the linear solver tolerates without giving up. Default: 1e+07
54:     --linear-solver-max-iterations=INTEGER        The maximum number of iterations of the linear solver. Default: 1000
54:     --linear-solver-overlap-size=INTEGER          The size of the algebraic overlap for the linear solver. Default: 2
54:     --linear-solver-tolerance=SCALAR              The maximum allowed error between of the linear solver. Default: 0.001
54:     --linear-solver-verbosity=INTEGER             The verbosity level of the linear solver. Default: 0
54:     --max-time-step-divisions=INTEGER             The maximum number of divisions by two of the timestep size before the simulation bails out. Default: 10
54:     --max-time-step-size=SCALAR                   The maximum size to which all time steps are limited to [s]. Default: inf
54:     --min-time-step-size=SCALAR                   The minimum size to which all time steps are limited to [s]. Default: 0
54:     --newton-max-error=SCALAR                     The maximum error tolerated by the Newton method to which does not cause an abort. Default: 1e+100
54:     --newton-max-iterations=INTEGER               The maximum number of Newton iterations per time step. Default: 18
54:     --newton-target-iterations=INTEGER            The 'optimum' number of Newton iterations per time step. Default: 10
54:     --newton-tolerance=SCALAR                     The maximum raw error tolerated by the Newtonmethod for considering a solution to be converged. Default: 1e-08
54:     --newton-verbose=BOOLEAN                      Specify whether the Newton method should inform the user about its progress or not. Default: true
54:     --newton-write-convergence=BOOLEAN            Write the convergence behaviour of the Newton method to a VTK file. Default: false
54:     --numeric-difference-method=INTEGER           The method used for numeric differentiation (-1: backward differences, 0: central differences, 1: forward differences). Default: 1
54:     --output-dir=STRING                           The directory to which result files are written. Default: "."
54:     --parameter-file=STRING                       An .ini file which contains a set of run-time parameters. Default: ""
54:     --preconditioner-relaxation=SCALAR            The relaxation factor of the preconditioner. Default: 1
54:     --predetermined-time-steps-file=STRING        A file with a list of predetermined time step sizes (one time step per line). Default: ""
54:     --print-parameters=INTEGER                    Print the values of the run-time parameters at the start of the simulation. Default: 2
54:     --print-properties=INTEGER                    Print the values of the compile time properties at the start of the simulation. Default: 2
54:     --restart-time=SCALAR                         The simulation time at which a restart should be attempted [s]. Default: -1e+35
54:     --threads-per-process=INTEGER                 The maximum number of threads to be instantiated per process ('-1' means 'automatic'). Default: 1
54:     --vtk-write-average-molar-masses=BOOLEAN      Include the average phase mass in the VTK output files. Default: false
54:     --vtk-write-densities=BOOLEAN                 Include the phase densities in the VTK output files. Default: true
54:     --vtk-write-dof-index=BOOLEAN                 Include the index of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-extrusion-factor=BOOLEAN          Include the extrusion factor of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-filter-velocities=BOOLEAN         Include in the filter velocities of the phases the VTK output files. Default: false
54:     --vtk-write-intrinsic-permeabilities=BOOLEAN  Include the intrinsic permeability in the VTK output files. Default: false
54:     --vtk-write-mobilities=BOOLEAN                Include the phase mobilities in the VTK output files. Default: false
54:     --vtk-write-porosity=BOOLEAN                  Include the porosity in the VTK output files. Default: true
54:     --vtk-write-potential-gradients=BOOLEAN       Include the phase pressure potential gradients in the VTK output files. Default: false
54:     --vtk-write-pressures=BOOLEAN                 Include the phase pressures in the VTK output files. Default: true
54:     --vtk-write-primary-vars=BOOLEAN              Include the primary variables into the VTK output files. Default: false
54:     --vtk-write-process-rank=BOOLEAN              Include the MPI process rank into the VTK output files. Default: false
54:     --vtk-write-relative-permeabilities=BOOLEAN   Include the phase relative permeabilities in the VTK output files. Default: true
54:     --vtk-write-saturations=BOOLEAN               Include the phase saturations in the VTK output files. Default: true
54:     --vtk-write-temperature=BOOLEAN               Include the temperature in the VTK output files. Default: true
54:     --vtk-write-viscosities=BOOLEAN               Include component phase viscosities in the VTK output files. Default: false
54: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh: line 245:  4965 Segmentation fault      (core dumped) $TEST_BINARY "$PARAM" --end-time=100 2>&1 > /dev/null
54:     
54: stty: 'standard input': Inappropriate ioctl for device
54:     
54: stty: 'standard input': Inappropriate ioctl for device
54: Illegal parameter "-foo".
54: 
54: Usage: ./bin/obstacle_immiscible [OPTIONS]
54: 
54: Recognized options:
54:     -h,--help                                     Print this help message and exit
54:     --help-all                                    Print all parameters, including obsolete, hidden and deprecated ones.
54:     --continue-on-convergence-error=BOOLEAN       Continue with a non-converged solution instead of giving up if we encounter a time step size smaller than the minimum time step size. Default: false
54:     --enable-async-vtk-output=BOOLEAN             Dispatch a separate thread to write the VTK output. Default: true
54:     --enable-gravity=BOOLEAN                      Use the gravity correction for the pressure gradients. Default: true
54:     --enable-grid-adaptation=BOOLEAN              Enable adaptive grid refinement/coarsening. Default: false
54:     --enable-intensive-quantity-cache=BOOLEAN     Turn on caching of intensive quantities. Default: false
54:     --enable-storage-cache=BOOLEAN                Store previous storage terms and avoid re-calculating them. Default: false
54:     --enable-thermodynamic-hints=BOOLEAN          Enable thermodynamic hints. Default: false
54:     --enable-vtk-output=BOOLEAN                   Global switch for turning on writing VTK files. Default: true
54:     --end-time=SCALAR                             The simulation time at which the simulation is finished [s]. Default: 10000
54:     --grid-file=STRING                            The file name of the DGF file to load. Default: "./data/obstacle_24x16.dgf"
54:     --grid-global-refinements=INTEGER             The number of global refinements of the grid executed after it was loaded. Default: 0
54:     --initial-time-step-size=SCALAR               The size of the initial time step [s]. Default: 250
54:     --linear-solver-abs-tolerance=SCALAR          The maximum accepted error of the norm of the residual. Default: -1
54:     --linear-solver-max-error=SCALAR              The maximum residual error which the linear solver tolerates without giving up. Default: 1e+07
54:     --linear-solver-max-iterations=INTEGER        The maximum number of iterations of the linear solver. Default: 1000
54:     --linear-solver-overlap-size=INTEGER          The size of the algebraic overlap for the linear solver. Default: 2
54:     --linear-solver-tolerance=SCALAR              The maximum allowed error between of the linear solver. Default: 0.001
54:     --linear-solver-verbosity=INTEGER             The verbosity level of the linear solver. Default: 0
54:     --max-time-step-divisions=INTEGER             The maximum number of divisions by two of the timestep size before the simulation bails out. Default: 10
54:     --max-time-step-size=SCALAR                   The maximum size to which all time steps are limited to [s]. Default: inf
54:     --min-time-step-size=SCALAR                   The minimum size to which all time steps are limited to [s]. Default: 0
54:     --newton-max-error=SCALAR                     The maximum error tolerated by the Newton method to which does not cause an abort. Default: 1e+100
54:     --newton-max-iterations=INTEGER               The maximum number of Newton iterations per time step. Default: 18
54:     --newton-target-iterations=INTEGER            The 'optimum' number of Newton iterations per time step. Default: 10
54:     --newton-tolerance=SCALAR                     The maximum raw error tolerated by the Newtonmethod for considering a solution to be converged. Default: 1e-08
54:     --newton-verbose=BOOLEAN                      Specify whether the Newton method should inform the user about its progress or not. Default: true
54:     --newton-write-convergence=BOOLEAN            Write the convergence behaviour of the Newton method to a VTK file. Default: false
54:     --numeric-difference-method=INTEGER           The method used for numeric differentiation (-1: backward differences, 0: central differences, 1: forward differences). Default: 1
54:     --output-dir=STRING                           The directory to which result files are written. Default: "."
54:     --parameter-file=STRING                       An .ini file which contains a set of run-time parameters. Default: ""
54:     --preconditioner-relaxation=SCALAR            The relaxation factor of the preconditioner. Default: 1
54:     --predetermined-time-steps-file=STRING        A file with a list of predetermined time step sizes (one time step per line). Default: ""
54:     --print-parameters=INTEGER                    Print the values of the run-time parameters at the start of the simulation. Default: 2
54:     --print-properties=INTEGER                    Print the values of the compile time properties at the start of the simulation. Default: 2
54:     --restart-time=SCALAR                         The simulation time at which a restart should be attempted [s]. Default: -1e+35
54:     --threads-per-process=INTEGER                 The maximum number of threads to be instantiated per process ('-1' means 'automatic'). Default: 1
54:     --vtk-write-average-molar-masses=BOOLEAN      Include the average phase mass in the VTK output files. Default: false
54:     --vtk-write-densities=BOOLEAN                 Include the phase densities in the VTK output files. Default: true
54:     --vtk-write-dof-index=BOOLEAN                 Include the index of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-extrusion-factor=BOOLEAN          Include the extrusion factor of the degrees of freedom into the VTK output files. Default: false
54:     --vtk-write-filter-velocities=BOOLEAN         Include in the filter velocities of the phases the VTK output files. Default: false
54:     --vtk-write-intrinsic-permeabilities=BOOLEAN  Include the intrinsic permeability in the VTK output files. Default: false
54:     --vtk-write-mobilities=BOOLEAN                Include the phase mobilities in the VTK output files. Default: false
54:     --vtk-write-porosity=BOOLEAN                  Include the porosity in the VTK output files. Default: true
54:     --vtk-write-potential-gradients=BOOLEAN       Include the phase pressure potential gradients in the VTK output files. Default: false
54:     --vtk-write-pressures=BOOLEAN                 Include the phase pressures in the VTK output files. Default: true
54:     --vtk-write-primary-vars=BOOLEAN              Include the primary variables into the VTK output files. Default: false
54:     --vtk-write-process-rank=BOOLEAN              Include the MPI process rank into the VTK output files. Default: false
54:     --vtk-write-relative-permeabilities=BOOLEAN   Include the phase relative permeabilities in the VTK output files. Default: true
54:     --vtk-write-saturations=BOOLEAN               Include the phase saturations in the VTK output files. Default: true
54:     --vtk-write-temperature=BOOLEAN               Include the temperature in the VTK output files. Default: true
54:     --vtk-write-viscosities=BOOLEAN               Include component phase viscosities in the VTK output files. Default: false
54: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh: line 245:  4970 Segmentation fault      (core dumped) $TEST_BINARY "$PARAM" --end-time=100 2>&1 > /dev/null
54:     
54: stty: 'standard input': Inappropriate ioctl for device
54: Test successful
54/58 Test #54: obstacle_immiscible_parameters ......   Passed    4.28 sec
test 55
      Start 55: obstacle_pvs_restart
55: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--restart" "-e" "obstacle_pvs" "--" "--pvs-verbosity=2" "--end-time=30000"
55: Test timeout computed to be: 1500
55: ######################
55: # Running test 'obstacle_pvs'
55: ######################
55: executing "./bin/obstacle_pvs --pvs-verbosity=2 --end-time=30000"
55: eWoms  will now start the trip. Please sit back, relax and enjoy the ride.
55: # [known parameters which were specified at run-time]
55: PvsVerbosity="2" # default: "1"
55: EndTime="30000" # default: "10000"
55: # [parameters which were specified at compile-time]
55: ContinueOnConvergenceError="0"
55: EnableAsyncVtkOutput="1"
55: EnableGravity="1"
55: EnableGridAdaptation="0"
55: EnableIntensiveQuantityCache="0"
55: EnableStorageCache="0"
55: EnableThermodynamicHints="0"
55: EnableVtkOutput="1"
55: GridFile="./data/obstacle_24x16.dgf"
55: GridGlobalRefinements="0"
55: InitialTimeStepSize="250"
55: LinearSolverAbsTolerance="-1"
55: LinearSolverMaxError="1e+07"
55: LinearSolverMaxIterations="1000"
55: LinearSolverOverlapSize="2"
55: LinearSolverTolerance="0.001"
55: LinearSolverVerbosity="0"
55: MaxTimeStepDivisions="10"
55: MaxTimeStepSize="inf"
55: MinTimeStepSize="0"
55: NewtonMaxError="1e+100"
55: NewtonMaxIterations="18"
55: NewtonTargetIterations="10"
55: NewtonTolerance="1e-08"
55: NewtonVerbose="1"
55: NewtonWriteConvergence="0"
55: NumericDifferenceMethod="1"
55: OutputDir="."
55: ParameterFile=""
55: PreconditionerRelaxation="1"
55: PredeterminedTimeStepsFile=""
55: PrintParameters="2"
55: PrintProperties="2"
55: RestartTime="-1e+35"
55: ThreadsPerProcess="1"
55: VtkWriteAverageMolarMasses="0"
55: VtkWriteDensities="1"
55: VtkWriteDofIndex="0"
55: VtkWriteExtrusionFactor="0"
55: VtkWriteFilterVelocities="0"
55: VtkWriteFugacities="0"
55: VtkWriteFugacityCoeffs="0"
55: VtkWriteIntrinsicPermeabilities="0"
55: VtkWriteMassFractions="0"
55: VtkWriteMobilities="0"
55: VtkWriteMolarities="0"
55: VtkWriteMoleFractions="1"
55: VtkWritePhasePresence="0"
55: VtkWritePorosity="1"
55: VtkWritePotentialGradients="0"
55: VtkWritePressures="1"
55: VtkWritePrimaryVars="0"
55: VtkWriteProcessRank="0"
55: VtkWriteRelativePermeabilities="1"
55: VtkWriteSaturations="1"
55: VtkWriteTemperature="1"
55: VtkWriteTotalMassFractions="0"
55: VtkWriteTotalMoleFractions="0"
55: VtkWriteViscosities="0"
55: # [end of parameters]
55: The eWoms property system was compiled with the macro
55: NO_PROPERTY_INTROSPECTION defined.
55: No diagnostic messages this time, sorry.
55: Allocating the simulation vanguard
55: Distributing the vanguard's data
55: Allocating the model
55: Allocating the problem
55: Initializing the model
55: Initializing the problem
55: Simulator successfully set up
55: Applying the initial solution of the "obstacle_pvs" problem
55: Writing visualization results for the current time step.
55: Begin time step 1. Start time: 0 seconds, step size: 250 seconds (4 minutes, 10 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.143401
55: 'liquid' phase appears at position 60 0 sum x = 75.5218, new primary variables: (p_liquid = 120000, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 60 2.5 sum x = 75.4763, new primary variables: (p_liquid = 120000, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 60 5 sum x = 75.614, new primary variables: (p_liquid = 120000, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 60 7.5 sum x = 76.6082, new primary variables: (p_liquid = 120000, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 60 10 sum x = 96.8715, new primary variables: (p_liquid = 120000, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.114702
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 0.000787332
55: 'liquid' phase appears at position 57.5 0 sum x = 2.52226, new primary variables: (p_liquid = 105955, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 57.5 2.5 sum x = 2.49525, new primary variables: (p_liquid = 105868, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 57.5 5 sum x = 2.48035, new primary variables: (p_liquid = 105628, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 57.5 7.5 sum x = 2.38725, new primary variables: (p_liquid = 104881, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 57.5 10 sum x = 1.04397, new primary variables: (p_liquid = 102705, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 6.40187e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 5 error: 7.72723e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 6 error: 1.84214e-10
55: Linearization/solve/update time: 1.3125(93.6784%)/0.0870213(6.21104%)/0.00154964(0.110604%)
55: Storage in liquidPhase: [23146.8 0.27821]
55: Storage in gasPhase: [301.208 28753.4]
55: Storage total: [23448 28753.7]
55: Writing visualization results for the current time step.
55: Time step 1 done. CPU time: 1.562 seconds, end time: 250 seconds (4 minutes, 10 seconds), step size: 250 seconds (4 minutes, 10 seconds)
55: Begin time step 2. Start time: 250 seconds (4 minutes, 10 seconds), step size: 333.333 seconds (5 minutes, 33.33 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.133299
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.000105094
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 1.61832e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 4 error: 5.86436e-11
55: Linearization/solve/update time: 0.894999(94.5468%)/0.0506577(5.35143%)/0.000963774(0.101812%)
55: Storage in liquidPhase: [52740.8 0.657378]
55: Storage in gasPhase: [300.954 28753]
55: Storage total: [53041.7 28753.7]
55: Writing visualization results for the current time step.
55: Time step 2 done. CPU time: 2.65058 seconds, end time: 583.333 seconds (9 minutes, 43.33 seconds), step size: 333.333 seconds (5 minutes, 33.33 seconds)
55: Begin time step 3. Start time: 583.333 seconds (9 minutes, 43.33 seconds), step size: 500 seconds (8 minutes, 20 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.127513
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.0023463
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 7.36318e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 4 error: 9.94447e-10
55: Linearization/solve/update time: 0.896991(94.5135%)/0.0510815(5.38232%)/0.000988925(0.1042%)
55: Storage in liquidPhase: [94845.6 1.23193]
55: Storage in gasPhase: [301.182 28752.4]
55: Storage total: [95146.8 28753.6]
55: Writing visualization results for the current time step.
55: Time step 3 done. CPU time: 3.743 seconds, end time: 1083.33 seconds (18 minutes, 3.333 seconds), step size: 500 seconds (8 minutes, 20 seconds)
55: Begin time step 4. Start time: 1083.33 seconds (18 minutes, 3.333 seconds), step size: 750 seconds (12 minutes, 30 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.120284
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.0313462
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 6.07492e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 2.50423e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 5 error: 8.56239e-11
55: Linearization/solve/update time: 1.09571(93.9618%)/0.0691548(5.93032%)/0.0012584(0.107913%)
55: Storage in liquidPhase: [152104 2.11004]
55: Storage in gasPhase: [304.468 28751.4]
55: Storage total: [152409 28753.5]
55: Writing visualization results for the current time step.
55: Time step 4 done. CPU time: 5.063 seconds, end time: 1833.33 seconds (30 minutes, 33.33 seconds), step size: 750 seconds (12 minutes, 30 seconds)
55: Begin time step 5. Start time: 1833.33 seconds (30 minutes, 33.33 seconds), step size: 1062.5 seconds (17 minutes, 42.5 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.106163
55: 'liquid' phase appears at position 55 0 sum x = 3.05856, new primary variables: (p_liquid = 110361, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 55 2.5 sum x = 2.76828, new primary variables: (p_liquid = 109977, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 55 5 sum x = 2.62118, new primary variables: (p_liquid = 109048, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 55 7.5 sum x = 2.16437, new primary variables: (p_liquid = 107595, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.0277332
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 0.000473404
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 2.70901e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 5 error: 1.97613e-09
55: Linearization/solve/update time: 1.09775(93.9366%)/0.0695463(5.95119%)/0.00131177(0.11225%)
55: Storage in liquidPhase: [211947 3.37871]
55: Storage in gasPhase: [305.716 28750]
55: Storage total: [212252 28753.4]
55: Writing visualization results for the current time step.
55: Time step 5 done. CPU time: 6.38498 seconds, end time: 2895.83 seconds (48 minutes, 15.83 seconds), step size: 1062.5 seconds (17 minutes, 42.5 seconds)
55: Begin time step 6. Start time: 2895.83 seconds (48 minutes, 15.83 seconds), step size: 1505.21 seconds (25 minutes, 5.208 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0630484
55: 'gas' phase disappears at position 60 0. saturation=-0.0286254, new primary variables: (p_liquid = 176503, x_liquid^N2 = 2.00858e-05), phase presence: 1
55: 'gas' phase disappears at position 60 2.5. saturation=-0.0327235, new primary variables: (p_liquid = 170856, x_liquid^N2 = 1.94314e-05), phase presence: 1
55: 'gas' phase disappears at position 60 5. saturation=-0.031523, new primary variables: (p_liquid = 170635, x_liquid^N2 = 1.94058e-05), phase presence: 1
55: 'gas' phase disappears at position 60 7.5. saturation=-0.0310029, new primary variables: (p_liquid = 167638, x_liquid^N2 = 1.90586e-05), phase presence: 1
55: 'liquid' phase appears at position 55 10 sum x = 1.38021, new primary variables: (p_liquid = 106149, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.0205473
55: 'gas' phase appears at position 60 0 sum x = 3.62146, new primary variables: (p_liquid = 179209, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 60 2.5 sum x = 4.30726, new primary variables: (p_liquid = 174005, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 60 5 sum x = 3.99167, new primary variables: (p_liquid = 173759, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 60 7.5 sum x = 3.20693, new primary variables: (p_liquid = 170828, S_gas = 0), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 0.00011492
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 6.77404e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 5 error: 1.29319e-08
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 6 error: 8.23347e-12
55: Linearization/solve/update time: 1.31732(92.3389%)/0.107565(7.5399%)/0.00172963(0.12124%)
55: Storage in liquidPhase: [259332 4.63465]
55: Storage in gasPhase: [306.784 28748.5]
55: Storage total: [259638 28753.1]
55: Writing visualization results for the current time step.
55: Time step 6 done. CPU time: 7.97544 seconds, end time: 4401.04 seconds (1 hours, 13 minutes, 21.04 seconds), step size: 1505.21 seconds (25 minutes, 5.208 seconds)
55: Begin time step 7. Start time: 4401.04 seconds (1 hours, 13 minutes, 21.04 seconds), step size: 2006.94 seconds (33 minutes, 26.94 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0544027
55: 'gas' phase disappears at position 60 0. saturation=-0.00025795, new primary variables: (p_liquid = 185549, x_liquid^N2 = 2.11341e-05), phase presence: 1
55: 'gas' phase disappears at position 60 2.5. saturation=-0.000644126, new primary variables: (p_liquid = 181492, x_liquid^N2 = 2.0664e-05), phase presence: 1
55: 'gas' phase disappears at position 60 5. saturation=-0.000500248, new primary variables: (p_liquid = 180896, x_liquid^N2 = 2.05949e-05), phase presence: 1
55: 'gas' phase disappears at position 60 7.5. saturation=-0.000160127, new primary variables: (p_liquid = 177949, x_liquid^N2 = 2.02534e-05), phase presence: 1
55: 'gas' phase disappears at position 60 10. saturation=-0.0871138, new primary variables: (p_liquid = 142277, x_liquid^N2 = 1.61198e-05), phase presence: 1
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.0116296
55: 'gas' phase appears at position 60 0 sum x = 1.16474, new primary variables: (p_liquid = 185706, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 60 2.5 sum x = 1.26048, new primary variables: (p_liquid = 181710, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 60 5 sum x = 1.19933, new primary variables: (p_liquid = 181289, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 60 7.5 sum x = 1.03099, new primary variables: (p_liquid = 179420, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 60 10 sum x = 5.65133, new primary variables: (p_liquid = 154608, S_gas = 0), phase presence: 3
55: 'liquid' phase appears at position 60 12.5 sum x = 104.773, new primary variables: (p_liquid = 126147, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 60 15 sum x = 6.66798, new primary variables: (p_liquid = 114621, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 0.000709477
55: 'liquid' phase disappears at position 60 15. saturation=-1.4469e-05, new primary variables: (p_liquid = 104658, x_gas^N2 = 0.969714), phase presence: 2
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 5.21061e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 5 error: 1.28252e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 6 error: 1.84685e-09
55: Linearization/solve/update time: 1.32996(92.0939%)/0.112642(7.79991%)/0.00153354(0.10619%)
55: Storage in liquidPhase: [304373 5.5002]
55: Storage in gasPhase: [308.229 28747.2]
55: Storage total: [304681 28752.7]
55: Writing visualization results for the current time step.
55: Time step 7 done. CPU time: 9.585 seconds, end time: 6407.99 seconds (1 hours, 46 minutes, 47.99 seconds), step size: 2006.94 seconds (33 minutes, 26.94 seconds)
55: Begin time step 8. Start time: 6407.99 seconds (1 hours, 46 minutes, 47.99 seconds), step size: 2675.93 seconds (44 minutes, 35.93 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0154676
55: 'gas' phase disappears at position 60 0. saturation=-0.000191232, new primary variables: (p_liquid = 185954, x_liquid^N2 = 2.1181e-05), phase presence: 1
55: 'gas' phase disappears at position 60 2.5. saturation=-0.000262003, new primary variables: (p_liquid = 182080, x_liquid^N2 = 2.07321e-05), phase presence: 1
55: 'gas' phase disappears at position 60 5. saturation=-0.000270154, new primary variables: (p_liquid = 181531, x_liquid^N2 = 2.06685e-05), phase presence: 1
55: 'gas' phase disappears at position 60 7.5. saturation=-0.000286435, new primary variables: (p_liquid = 179815, x_liquid^N2 = 2.04696e-05), phase presence: 1
55: 'gas' phase disappears at position 60 10. saturation=-0.00221623, new primary variables: (p_liquid = 161290, x_liquid^N2 = 1.83229e-05), phase presence: 1
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.000242346
55: 'gas' phase appears at position 60 10 sum x = 1.18055, new primary variables: (p_liquid = 161455, S_gas = 0), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 7.2609e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 4 error: 3.68104e-09
55: Linearization/solve/update time: 0.889331(92.4498%)/0.0716615(7.44953%)/0.000968282(0.100657%)
55: Storage in liquidPhase: [359437 5.58272]
55: Storage in gasPhase: [309.198 28746.7]
55: Storage total: [359746 28752.3]
55: Writing visualization results for the current time step.
55: Time step 8 done. CPU time: 10.6913 seconds, end time: 9083.91 seconds (2 hours, 31 minutes, 23.91 seconds), step size: 2675.93 seconds (44 minutes, 35.93 seconds)
55: Begin time step 9. Start time: 9083.91 seconds (2 hours, 31 minutes, 23.91 seconds), step size: 4013.89 seconds (1 hours, 6 minutes, 53.89 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0160832
55: 'liquid' phase appears at position 52.5 0 sum x = 1.36837, new primary variables: (p_liquid = 106255, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 52.5 2.5 sum x = 1.10679, new primary variables: (p_liquid = 106110, S_gas = 1), phase presence: 3
55: 'liquid' phase appears at position 52.5 5 sum x = 1.01729, new primary variables: (p_liquid = 105777, S_gas = 1), phase presence: 3
55: 'gas' phase disappears at position 60 10. saturation=-0.000266362, new primary variables: (p_liquid = 162133, x_liquid^N2 = 1.84207e-05), phase presence: 1
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.00241254
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 2.2339e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 4 error: 2.03833e-09
55: Linearization/solve/update time: 0.897162(94.0684%)/0.0555827(5.8279%)/0.000989341(0.103733%)
55: Storage in liquidPhase: [441115 5.54921]
55: Storage in gasPhase: [311.749 28746]
55: Storage total: [441427 28751.6]
55: Writing visualization results for the current time step.
55: Time step 9 done. CPU time: 11.7907 seconds, end time: 13097.8 seconds (3 hours, 38 minutes, 17.8 seconds), step size: 4013.89 seconds (1 hours, 6 minutes, 53.89 seconds)
55: Begin time step 10. Start time: 13097.8 seconds (3 hours, 38 minutes, 17.8 seconds), step size: 6020.83 seconds (1 hours, 40 minutes, 20.83 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0171914
55: 'liquid' phase appears at position 52.5 7.5 sum x = 1.6398, new primary variables: (p_liquid = 105513, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.00802561
55: 'liquid' phase appears at position 57.5 12.5 sum x = 1.44549, new primary variables: (p_liquid = 105045, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 8.19207e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 5.45605e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 5 error: 5.79726e-08
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 6 error: 3.15423e-11
55: Linearization/solve/update time: 1.32126(91.7135%)/0.117753(8.17368%)/0.0016259(0.11286%)
55: Storage in liquidPhase: [560848 6.07516]
55: Storage in gasPhase: [313.303 28744.5]
55: Storage total: [561161 28750.6]
55: Writing visualization results for the current time step.
55: Time step 10 done. CPU time: 13.395 seconds, end time: 19118.6 seconds (5 hours, 18 minutes, 38.63 seconds), step size: 6020.83 seconds (1 hours, 40 minutes, 20.83 seconds)
55: Serialize to file 'obstacle_pvs_time=19118.6_rank=0.ers', next time step size: 5495.09
55: Begin time step 11. Start time: 19118.6 seconds (5 hours, 18 minutes, 38.63 seconds), step size: 5495.09 seconds (1 hours, 31 minutes, 35.09 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0164184
55: 'gas' phase disappears at position 57.5 0. saturation=-0.103241, new primary variables: (p_liquid = 129028, x_liquid^N2 = 1.45844e-05), phase presence: 1
55: 'liquid' phase appears at position 52.5 10 sum x = 1.21134, new primary variables: (p_liquid = 105267, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.00539078
55: 'gas' phase appears at position 57.5 0 sum x = 5.41081, new primary variables: (p_liquid = 139334, S_gas = 0), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 5.60732e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 8.45919e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 5 error: 1.9323e-09
55: Linearization/solve/update time: 1.1019(92.4499%)/0.0887086(7.44269%)/0.00127971(0.107368%)
55: Storage in liquidPhase: [662167 7.05881]
55: Storage in gasPhase: [313.834 28742.5]
55: Storage total: [662481 28749.6]
55: Writing visualization results for the current time step.
55: Time step 11 done. CPU time: 14.745 seconds, end time: 24613.7 seconds (6 hours, 50 minutes, 13.72 seconds), step size: 5495.09 seconds (1 hours, 31 minutes, 35.09 seconds)
55: Begin time step 12. Start time: 24613.7 seconds (6 hours, 50 minutes, 13.72 seconds), step size: 5386.28 seconds (1 hours, 29 minutes, 46.28 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0117742
55: 'gas' phase disappears at position 57.5 0. saturation=-0.00150052, new primary variables: (p_liquid = 154490, x_liquid^N2 = 1.7535e-05), phase presence: 1
55: 'gas' phase disappears at position 57.5 2.5. saturation=-0.0506631, new primary variables: (p_liquid = 134239, x_liquid^N2 = 1.51883e-05), phase presence: 1
55: 'gas' phase disappears at position 57.5 5. saturation=-0.0354783, new primary variables: (p_liquid = 124904, x_liquid^N2 = 1.41066e-05), phase presence: 1
55: 'gas' phase disappears at position 57.5 7.5. saturation=-0.0136493, new primary variables: (p_liquid = 116978, x_liquid^N2 = 1.31881e-05), phase presence: 1
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.00257328
55: 'gas' phase appears at position 57.5 0 sum x = 1.1927, new primary variables: (p_liquid = 157092, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 57.5 2.5 sum x = 4.59073, new primary variables: (p_liquid = 139610, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 57.5 5 sum x = 6.4419, new primary variables: (p_liquid = 129378, S_gas = 0), phase presence: 3
55: 'gas' phase appears at position 57.5 7.5 sum x = 8.55819, new primary variables: (p_liquid = 118914, S_gas = 0), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 1.26089e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 2.48968e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 5 error: 9.09876e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 6 error: 1.31478e-09
55: Linearization/solve/update time: 1.31441(91.3871%)/0.122346(8.50635%)/0.00153235(0.10654%)
55: Storage in liquidPhase: [746244 8.42132]
55: Storage in gasPhase: [314.443 28740]
55: Storage total: [746558 28748.4]
55: Writing visualization results for the current time step.
55: Time step 12 done. CPU time: 16.348 seconds, end time: 30000 seconds (8 hours, 20 minutes, 0 seconds), step size: 5386.28 seconds (1 hours, 29 minutes, 46.28 seconds)
55: Simulation of problem 'obstacle_pvs' finished.
55: 
55: ------------------------ Timing receipt ------------------------
55: Setup time: 0.493 seconds, 2.93%
55: Simulation time: 16.3 seconds, 97.1%
55:     Linearization time: 13.5 seconds, 82.4%
55:     Linear solve time: 1 seconds, 6.14%
55:     Newton update time: 0.0157 seconds, 0.0962%
55:     Pre/postprocess time: 1.47 seconds, 9.02%
55:     Output write time: 0.299 seconds, 1.83%
55: First process' simulation CPU time: 16.1 seconds
55: Number of processes: 1
55: Threads per processes: 1
55: Total CPU time: 16.1 seconds
55: 
55: Note 1: If not stated otherwise, all times are wall clock times
55: Note 2: Taxes and administrative overhead are 0.525%
55: 
55: Our simulation hours are 24/7. Thank you for choosing us.
55: ----------------------------------------------------------------
55: 
55: eWoms reached the destination. If it is not the one that was intended, change the booking and try again.
55: eWoms  will now start the trip. Please sit back, relax and enjoy the ride.
55: # [known parameters which were specified at run-time]
55: PvsVerbosity="2" # default: "1"
55: EndTime="30000" # default: "10000"
55: RestartTime="19118.6" # default: "-1e+35"
55: NewtonWriteConvergence="true" # default: "0"
55: # [parameters which were specified at compile-time]
55: ContinueOnConvergenceError="0"
55: EnableAsyncVtkOutput="1"
55: EnableGravity="1"
55: EnableGridAdaptation="0"
55: EnableIntensiveQuantityCache="0"
55: EnableStorageCache="0"
55: EnableThermodynamicHints="0"
55: EnableVtkOutput="1"
55: GridFile="./data/obstacle_24x16.dgf"
55: GridGlobalRefinements="0"
55: InitialTimeStepSize="250"
55: LinearSolverAbsTolerance="-1"
55: LinearSolverMaxError="1e+07"
55: LinearSolverMaxIterations="1000"
55: LinearSolverOverlapSize="2"
55: LinearSolverTolerance="0.001"
55: LinearSolverVerbosity="0"
55: MaxTimeStepDivisions="10"
55: MaxTimeStepSize="inf"
55: MinTimeStepSize="0"
55: NewtonMaxError="1e+100"
55: NewtonMaxIterations="18"
55: NewtonTargetIterations="10"
55: NewtonTolerance="1e-08"
55: NewtonVerbose="1"
55: NumericDifferenceMethod="1"
55: OutputDir="."
55: ParameterFile=""
55: PreconditionerRelaxation="1"
55: PredeterminedTimeStepsFile=""
55: PrintParameters="2"
55: PrintProperties="2"
55: ThreadsPerProcess="1"
55: VtkWriteAverageMolarMasses="0"
55: VtkWriteDensities="1"
55: VtkWriteDofIndex="0"
55: VtkWriteExtrusionFactor="0"
55: VtkWriteFilterVelocities="0"
55: VtkWriteFugacities="0"
55: VtkWriteFugacityCoeffs="0"
55: VtkWriteIntrinsicPermeabilities="0"
55: VtkWriteMassFractions="0"
55: VtkWriteMobilities="0"
55: VtkWriteMolarities="0"
55: VtkWriteMoleFractions="1"
55: VtkWritePhasePresence="0"
55: VtkWritePorosity="1"
55: VtkWritePotentialGradients="0"
55: VtkWritePressures="1"
55: VtkWritePrimaryVars="0"
55: VtkWriteProcessRank="0"
55: VtkWriteRelativePermeabilities="1"
55: VtkWriteSaturations="1"
55: VtkWriteTemperature="1"
55: VtkWriteTotalMassFractions="0"
55: VtkWriteTotalMoleFractions="0"
55: VtkWriteViscosities="0"
55: # [end of parameters]
55: The eWoms property system was compiled with the macro
55: NO_PROPERTY_INTROSPECTION defined.
55: No diagnostic messages this time, sorry.
55: Allocating the simulation vanguard
55: Distributing the vanguard's data
55: Allocating the model
55: Allocating the problem
55: Initializing the model
55: Initializing the problem
55: Simulator successfully set up
55: Deserialize from file 'obstacle_pvs_time=19118.6_rank=0.ers'
55: Deserialization done. Simulator time: 19118.6 (5 hours, 18 minutes, 38.63 seconds) Time step index: 10 Episode index: 0
55: Begin time step 11. Start time: 19118.6 seconds (5 hours, 18 minutes, 38.63 seconds), step size: 250 seconds (4 minutes, 10 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0164184
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 4.18578e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 3.42509e-08
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 4 error: 5.99954e-12
55: Linearization/solve/update time: 0.85737(62.2364%)/0.0506196(3.67448%)/0.469611(34.0891%)
55: Storage in liquidPhase: [565808 6.10436]
55: Storage in gasPhase: [313.328 28744.4]
55: Storage total: [566122 28750.5]
55: Writing visualization results for the current time step.
55: Time step 11 done. CPU time: 1.51963 seconds, end time: 19368.6 seconds (5 hours, 22 minutes, 48.63 seconds), step size: 250 seconds (4 minutes, 10 seconds)
55: Begin time step 12. Start time: 19368.6 seconds (5 hours, 22 minutes, 48.63 seconds), step size: 375 seconds (6 minutes, 15 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0160054
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 1.09327e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 1.28458e-08
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 4 error: 5.99837e-11
55: Linearization/solve/update time: 0.905915(63.0792%)/0.0565173(3.93532%)/0.473722(32.9854%)
55: Storage in liquidPhase: [573221 6.15223]
55: Storage in gasPhase: [313.367 28744.3]
55: Storage total: [573534 28750.5]
55: Writing visualization results for the current time step.
55: Time step 12 done. CPU time: 3.10022 seconds, end time: 19743.6 seconds (5 hours, 29 minutes, 3.63 seconds), step size: 375 seconds (6 minutes, 15 seconds)
55: Begin time step 13. Start time: 19743.6 seconds (5 hours, 29 minutes, 3.63 seconds), step size: 562.5 seconds (9 minutes, 22.5 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0152082
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 3.15992e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 1.68405e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 4 error: 1.82181e-09
55: Linearization/solve/update time: 0.897196(63.1704%)/0.0554909(3.90704%)/0.467593(32.9226%)
55: Storage in liquidPhase: [584258 6.23575]
55: Storage in gasPhase: [313.43 28744.1]
55: Storage total: [584571 28750.4]
55: Writing visualization results for the current time step.
55: Time step 13 done. CPU time: 4.66249 seconds, end time: 20306.1 seconds (5 hours, 38 minutes, 26.13 seconds), step size: 562.5 seconds (9 minutes, 22.5 seconds)
55: Begin time step 14. Start time: 20306.1 seconds (5 hours, 38 minutes, 26.13 seconds), step size: 843.75 seconds (14 minutes, 3.75 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0133447
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 9.58239e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 2.19523e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 1.31379e-08
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 5 error: 1.24918e-10
55: Linearization/solve/update time: 1.13327(61.1939%)/0.084188(4.54594%)/0.634477(34.2602%)
55: Storage in liquidPhase: [600562 6.38904]
55: Storage in gasPhase: [313.536 28743.8]
55: Storage total: [600875 28750.2]
55: Writing visualization results for the current time step.
55: Time step 14 done. CPU time: 6.67249 seconds, end time: 21149.9 seconds (5 hours, 52 minutes, 29.88 seconds), step size: 843.75 seconds (14 minutes, 3.75 seconds)
55: Begin time step 15. Start time: 21149.9 seconds (5 hours, 52 minutes, 29.88 seconds), step size: 1195.31 seconds (19 minutes, 55.31 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0135321
55: 'gas' phase disappears at position 57.5 0. saturation=-0.000836751, new primary variables: (p_liquid = 139593, x_liquid^N2 = 1.58087e-05), phase presence: 1
55: 'liquid' phase appears at position 52.5 10 sum x = 1.02143, new primary variables: (p_liquid = 105373, S_gas = 1), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 5.80598e-05
55: 'gas' phase appears at position 57.5 0 sum x = 1.73755, new primary variables: (p_liquid = 139641, S_gas = 0), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 4.74982e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 3.60732e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 5 error: 2.56008e-08
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 6 error: 3.38434e-12
55: Linearization/solve/update time: 1.34145(60.0827%)/0.102441(4.58826%)/0.788784(35.3291%)
55: Storage in liquidPhase: [623139 6.62196]
55: Storage in gasPhase: [313.655 28743.4]
55: Storage total: [623453 28750]
55: Writing visualization results for the current time step.
55: Time step 15 done. CPU time: 9.07093 seconds, end time: 22345.2 seconds (6 hours, 12 minutes, 25.2 seconds), step size: 1195.31 seconds (19 minutes, 55.31 seconds)
55: Begin time step 16. Start time: 22345.2 seconds (6 hours, 12 minutes, 25.2 seconds), step size: 1593.75 seconds (26 minutes, 33.75 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0142851
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 4.99951e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 1.13436e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 4 error: 1.23266e-09
55: Linearization/solve/update time: 0.910239(63.061%)/0.0608015(4.2123%)/0.472386(32.7267%)
55: Storage in liquidPhase: [652378 6.94116]
55: Storage in gasPhase: [313.69 28742.8]
55: Storage total: [652692 28749.8]
55: Writing visualization results for the current time step.
55: Time step 16 done. CPU time: 10.6615 seconds, end time: 23938.9 seconds (6 hours, 38 minutes, 58.95 seconds), step size: 1593.75 seconds (26 minutes, 33.75 seconds)
55: Begin time step 17. Start time: 23938.9 seconds (6 hours, 38 minutes, 58.95 seconds), step size: 2390.63 seconds (39 minutes, 50.62 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0129926
55: 'gas' phase disappears at position 57.5 0. saturation=-0.000103943, new primary variables: (p_liquid = 151493, x_liquid^N2 = 1.71876e-05), phase presence: 1
55: 'gas' phase disappears at position 57.5 2.5. saturation=-0.000363898, new primary variables: (p_liquid = 127996, x_liquid^N2 = 1.44649e-05), phase presence: 1
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 0.000110359
55: 'gas' phase appears at position 57.5 2.5 sum x = 5.15078, new primary variables: (p_liquid = 127824, S_gas = 0), phase presence: 3
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 1.2328e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 3.15737e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 5 error: 7.37232e-07
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 6 error: 1.51686e-09
55: Linearization/solve/update time: 1.34943(59.7188%)/0.112927(4.9976%)/0.797279(35.2836%)
55: Storage in liquidPhase: [693104 7.50843]
55: Storage in gasPhase: [313.792 28741.8]
55: Storage total: [693418 28749.3]
55: Writing visualization results for the current time step.
55: Time step 17 done. CPU time: 13.0887 seconds, end time: 26329.6 seconds (7 hours, 18 minutes, 49.57 seconds), step size: 2390.63 seconds (39 minutes, 50.62 seconds)
55: Begin time step 18. Start time: 26329.6 seconds (7 hours, 18 minutes, 49.57 seconds), step size: 1853.57 seconds (30 minutes, 53.57 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.00958936
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 6.61552e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 7.02987e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 5.29859e-08
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 5 error: 5.4363e-11
55: Linearization/solve/update time: 1.12214(61.0939%)/0.0838357(4.56437%)/0.630768(34.3417%)
55: Storage in liquidPhase: [721957 8.04422]
55: Storage in gasPhase: [313.912 28740.9]
55: Storage total: [722271 28749]
55: Writing visualization results for the current time step.
55: Time step 18 done. CPU time: 15.0925 seconds, end time: 28183.1 seconds (7 hours, 49 minutes, 43.14 seconds), step size: 1853.57 seconds (30 minutes, 53.57 seconds)
55: Begin time step 19. Start time: 28183.1 seconds (7 hours, 49 minutes, 43.14 seconds), step size: 1816.86 seconds (30 minutes, 16.86 seconds)
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 1 error: 0.0102826
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 2 error: 8.78371e-05
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 3 error: 3.51063e-06
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Solve: M deltax^k = r
55: Update: x^(k+1) = x^k - deltax^k
55: Newton iteration 4 error: 3.26715e-08
55: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
55: Newton iteration 5 error: 2.0029e-11
55: Linearization/solve/update time: 1.11403(60.9352%)/0.0855755(4.68083%)/0.628613(34.384%)
55: Storage in liquidPhase: [748083 8.52101]
55: Storage in gasPhase: [314.061 28740.1]
55: Storage total: [748398 28748.6]
55: Writing visualization results for the current time step.
55: Time step 19 done. CPU time: 17.0775 seconds, end time: 30000 seconds (8 hours, 20 minutes, 0 seconds), step size: 1816.86 seconds (30 minutes, 16.86 seconds)
55: Simulation of problem 'obstacle_pvs' finished.
55: 
55: ------------------------ Timing receipt ------------------------
55: Setup time: 0.354 seconds, 2.03%
55: Simulation time: 17.1 seconds, 98%
55:     Linearization time: 9.63 seconds, 56.4%
55:     Linear solve time: 0.692 seconds, 4.05%
55:     Newton update time: 5.36 seconds, 31.4%
55:     Pre/postprocess time: 1.11 seconds, 6.48%
55:     Output write time: 0.216 seconds, 1.26%
55: First process' simulation CPU time: 16.9 seconds
55: Number of processes: 1
55: Threads per processes: 1
55: Total CPU time: 16.9 seconds
55: 
55: Note 1: If not stated otherwise, all times are wall clock times
55: Note 2: Taxes and administrative overhead are 0.399%
55: 
55: Our simulation hours are 24/7. Thank you for choosing us.
55: ----------------------------------------------------------------
55: 
55: eWoms reached the destination. If it is not the one that was intended, change the booking and try again.
55/58 Test #55: obstacle_pvs_restart ................   Passed   34.76 sec
test 56
      Start 56: tutorial1
56: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--simulation" "-e" "tutorial1" "--"
56: Test timeout computed to be: 1500
56: ######################
56: # Running test 'tutorial1'
56: ######################
56: executing "./bin/tutorial1 "
56: eWoms  will now start the trip. Please sit back, relax and enjoy the ride.
56: # [parameters which were specified at compile-time]
56: CellsX="100"
56: CellsY="1"
56: ContinueOnConvergenceError="0"
56: DomainSizeX="300"
56: DomainSizeY="60"
56: EnableAsyncVtkOutput="1"
56: EnableGravity="0"
56: EnableGridAdaptation="0"
56: EnableIntensiveQuantityCache="0"
56: EnableStorageCache="0"
56: EnableThermodynamicHints="0"
56: EnableVtkOutput="1"
56: EndTime="100000"
56: GridGlobalRefinements="0"
56: InitialTimeStepSize="125"
56: LinearSolverAbsTolerance="-1"
56: LinearSolverMaxError="1e+07"
56: LinearSolverMaxIterations="1000"
56: LinearSolverOverlapSize="2"
56: LinearSolverTolerance="0.001"
56: LinearSolverVerbosity="0"
56: MaxTimeStepDivisions="10"
56: MaxTimeStepSize="inf"
56: MinTimeStepSize="0"
56: NewtonMaxError="1e+100"
56: NewtonMaxIterations="18"
56: NewtonTargetIterations="10"
56: NewtonTolerance="1e-08"
56: NewtonVerbose="1"
56: NewtonWriteConvergence="0"
56: NumericDifferenceMethod="1"
56: OutputDir="."
56: ParameterFile=""
56: PreconditionerRelaxation="1"
56: PredeterminedTimeStepsFile=""
56: PrintParameters="2"
56: PrintProperties="2"
56: RestartTime="-1e+35"
56: ThreadsPerProcess="1"
56: VtkWriteAverageMolarMasses="0"
56: VtkWriteDensities="1"
56: VtkWriteDofIndex="0"
56: VtkWriteExtrusionFactor="0"
56: VtkWriteFilterVelocities="0"
56: VtkWriteIntrinsicPermeabilities="0"
56: VtkWriteMobilities="0"
56: VtkWritePorosity="1"
56: VtkWritePotentialGradients="0"
56: VtkWritePressures="1"
56: VtkWritePrimaryVars="0"
56: VtkWriteProcessRank="0"
56: VtkWriteRelativePermeabilities="1"
56: VtkWriteSaturations="1"
56: VtkWriteTemperature="1"
56: VtkWriteViscosities="0"
56: # [end of parameters]
56: The eWoms property system was compiled with the macro
56: NO_PROPERTY_INTROSPECTION defined.
56: No diagnostic messages this time, sorry.
56: Allocating the simulation vanguard
56: Distributing the vanguard's data
56: Allocating the model
56: Allocating the problem
56: Initializing the model
56: Initializing the problem
56: Simulator successfully set up
56: Applying the initial solution of the "tutorial1" problem
56: Writing visualization results for the current time step.
56: Begin time step 1. Start time: 0 seconds, step size: 125 seconds (2 minutes, 5 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 92.2667
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 657.809
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 41.0576
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 21.7506
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 9.68138
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 17.884
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 4.06483
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 9.41422
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 9.96297
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 10 error: 6.57008
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 11 error: 3.21277
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 12 error: 1.1763
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 13 error: 0.851742
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 14 error: 0.113327
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 15 error: 0.020374
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 16 error: 1.1047e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 17 error: 3.1077e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 18 error: 2.42937e-10
56: Linearization/solve/update time: 0.81046(88.2301%)/0.105788(11.5165%)/0.00232731(0.253361%)
56: Writing visualization results for the current time step.
56: Time step 1 done. CPU time: 0.953999 seconds, end time: 125 seconds (2 minutes, 5 seconds), step size: 125 seconds (2 minutes, 5 seconds)
56: Begin time step 2. Start time: 125 seconds (2 minutes, 5 seconds), step size: 69.4444 seconds (1 minutes, 9.444 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.899416
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 2.2794
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.0295792
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 3.08055e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 4.75518e-07
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 6 error: 2.49964e-09
56: Linearization/solve/update time: 0.274103(87.1364%)/0.0397591(12.6393%)/0.000705463(0.224265%)
56: Writing visualization results for the current time step.
56: Time step 2 done. CPU time: 1.28299 seconds, end time: 194.444 seconds (3 minutes, 14.44 seconds), step size: 69.4444 seconds (1 minutes, 9.444 seconds)
56: Begin time step 3. Start time: 194.444 seconds (3 minutes, 14.44 seconds), step size: 92.5926 seconds (1 minutes, 32.59 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.250005
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.05973
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.00579288
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 1.47398e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 1.44891e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 6 error: 5.09377e-11
56: Linearization/solve/update time: 0.273869(86.1555%)/0.0432851(13.6169%)/0.000723264(0.227529%)
56: Writing visualization results for the current time step.
56: Time step 3 done. CPU time: 1.61899 seconds, end time: 287.037 seconds (4 minutes, 47.04 seconds), step size: 92.5926 seconds (1 minutes, 32.59 seconds)
56: Begin time step 4. Start time: 287.037 seconds (4 minutes, 47.04 seconds), step size: 123.457 seconds (2 minutes, 3.457 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.126091
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 0.358268
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.000290102
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 1.63111e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 5 error: 6.80117e-09
56: Linearization/solve/update time: 0.241012(86.9378%)/0.0356071(12.8442%)/0.000604381(0.218013%)
56: Writing visualization results for the current time step.
56: Time step 4 done. CPU time: 1.91286 seconds, end time: 410.494 seconds (6 minutes, 50.49 seconds), step size: 123.457 seconds (2 minutes, 3.457 seconds)
56: Begin time step 5. Start time: 410.494 seconds (6 minutes, 50.49 seconds), step size: 174.897 seconds (2 minutes, 54.9 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0804005
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 6.65272
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 3.23491
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 3.46365
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.522997
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00347731
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 1.0654e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 8 error: 9.75129e-10
56: Linearization/solve/update time: 0.363144(87.5465%)/0.0506737(12.2164%)/0.000983479(0.237096%)
56: Writing visualization results for the current time step.
56: Time step 5 done. CPU time: 2.34566 seconds, end time: 585.391 seconds (9 minutes, 45.39 seconds), step size: 174.897 seconds (2 minutes, 54.9 seconds)
56: Begin time step 6. Start time: 585.391 seconds (9 minutes, 45.39 seconds), step size: 204.047 seconds (3 minutes, 24.05 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0644713
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.67035
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.00976907
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 5.63462e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 4.8421e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 6 error: 3.33195e-11
56: Linearization/solve/update time: 0.276147(87.6756%)/0.0380983(12.096%)/0.000719291(0.228372%)
56: Writing visualization results for the current time step.
56: Time step 6 done. CPU time: 2.67777 seconds, end time: 789.438 seconds (13 minutes, 9.438 seconds), step size: 204.047 seconds (3 minutes, 24.05 seconds)
56: Begin time step 7. Start time: 789.438 seconds (13 minutes, 9.438 seconds), step size: 272.062 seconds (4 minutes, 32.06 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0477871
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 0.592194
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.000439303
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 2.49615e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 5 error: 1.10971e-09
56: Linearization/solve/update time: 0.230689(87.6667%)/0.031822(12.093%)/0.000632244(0.240266%)
56: Writing visualization results for the current time step.
56: Time step 7 done. CPU time: 2.95965 seconds, end time: 1061.5 seconds (17 minutes, 41.5 seconds), step size: 272.062 seconds (4 minutes, 32.06 seconds)
56: Begin time step 8. Start time: 1061.5 seconds (17 minutes, 41.5 seconds), step size: 385.421 seconds (6 minutes, 25.42 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.033568
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 7.97837
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 3.51891
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 3.65669
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.714577
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00880784
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.000332332
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 1.14736e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 8.81796e-10
56: Linearization/solve/update time: 0.409766(86.8961%)/0.0606339(12.8582%)/0.00115892(0.245763%)
56: Writing visualization results for the current time step.
56: Time step 8 done. CPU time: 3.45314 seconds, end time: 1446.92 seconds (24 minutes, 6.921 seconds), step size: 385.421 seconds (6 minutes, 25.42 seconds)
56: Begin time step 9. Start time: 1446.92 seconds (24 minutes, 6.921 seconds), step size: 417.54 seconds (6 minutes, 57.54 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0292815
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.63869
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.00667097
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 5.98805e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 3.76176e-07
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 6 error: 3.47743e-10
56: Linearization/solve/update time: 0.282694(86.6591%)/0.0427522(13.1056%)/0.000767595(0.235304%)
56: Writing visualization results for the current time step.
56: Time step 9 done. CPU time: 3.79544 seconds, end time: 1864.46 seconds (31 minutes, 4.461 seconds), step size: 417.54 seconds (6 minutes, 57.54 seconds)
56: Begin time step 10. Start time: 1864.46 seconds (31 minutes, 4.461 seconds), step size: 556.72 seconds (9 minutes, 16.72 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0231078
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 0.589078
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.12718
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.00135144
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 2.89116e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 1.12796e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 7 error: 8.21294e-11
56: Linearization/solve/update time: 0.34117(86.4561%)/0.0523155(13.2573%)/0.00113106(0.286622%)
56: Writing visualization results for the current time step.
56: Time step 10 done. CPU time: 4.20899 seconds, end time: 2421.18 seconds (40 minutes, 21.18 seconds), step size: 556.72 seconds (9 minutes, 16.72 seconds)
56: Serialize to file 'tutorial1_time=2421.18_rank=0.ers', next time step size: 695.9
56: Begin time step 11. Start time: 2421.18 seconds (40 minutes, 21.18 seconds), step size: 695.9 seconds (11 minutes, 35.9 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0181173
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 4.87769
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 1.32084
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 1.64251
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.273142
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00228851
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 1.78347e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 3.72175e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 8.71997e-11
56: Linearization/solve/update time: 0.429732(87.1136%)/0.0622123(12.6114%)/0.00135654(0.274992%)
56: Writing visualization results for the current time step.
56: Time step 11 done. CPU time: 4.728 seconds, end time: 3117.08 seconds (51 minutes, 57.08 seconds), step size: 695.9 seconds (11 minutes, 35.9 seconds)
56: Begin time step 12. Start time: 3117.08 seconds (51 minutes, 57.08 seconds), step size: 753.891 seconds (12 minutes, 33.89 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.015758
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 0.870876
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.206737
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.00133703
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 1.97119e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 2.96926e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 7 error: 3.78392e-11
56: Linearization/solve/update time: 0.330698(86.633%)/0.0501178(13.1294%)/0.000907099(0.237633%)
56: Writing visualization results for the current time step.
56: Time step 12 done. CPU time: 5.12655 seconds, end time: 3870.97 seconds (1 hours, 4 minutes, 30.97 seconds), step size: 753.891 seconds (12 minutes, 33.89 seconds)
56: Begin time step 13. Start time: 3870.97 seconds (1 hours, 4 minutes, 30.97 seconds), step size: 942.364 seconds (15 minutes, 42.36 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0124513
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 7.66159
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 2.01801
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 2.52296
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.501038
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00628242
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 4.62142e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 1.31522e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 2.55227e-11
56: Linearization/solve/update time: 0.409543(87.0027%)/0.0600805(12.7634%)/0.00110086(0.233866%)
56: Writing visualization results for the current time step.
56: Time step 13 done. CPU time: 5.61977 seconds, end time: 4813.34 seconds (1 hours, 20 minutes, 13.34 seconds), step size: 942.364 seconds (15 minutes, 42.36 seconds)
56: Begin time step 14. Start time: 4813.34 seconds (1 hours, 20 minutes, 13.34 seconds), step size: 1020.89 seconds (17 minutes, 0.895 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0112935
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.31713
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.00776679
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.00372442
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 2.30797e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 4.9315e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 7 error: 5.03335e-11
56: Linearization/solve/update time: 0.323726(88.201%)/0.0424198(11.5575%)/0.000886317(0.241482%)
56: Writing visualization results for the current time step.
56: Time step 14 done. CPU time: 6.00599 seconds, end time: 5834.23 seconds (1 hours, 37 minutes, 14.23 seconds), step size: 1020.89 seconds (17 minutes, 0.895 seconds)
56: Begin time step 15. Start time: 5834.23 seconds (1 hours, 37 minutes, 14.23 seconds), step size: 1276.12 seconds (21 minutes, 16.12 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00939934
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 10.1844
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 2.20997
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 2.85115
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.626425
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.0101313
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 1.10336e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 4.0972e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 2.18821e-10
56: Linearization/solve/update time: 0.405344(87.2136%)/0.0582499(12.533%)/0.00117778(0.25341%)
56: Writing visualization results for the current time step.
56: Time step 15 done. CPU time: 6.49299 seconds, end time: 7110.35 seconds (1 hours, 58 minutes, 30.35 seconds), step size: 1276.12 seconds (21 minutes, 16.12 seconds)
56: Begin time step 16. Start time: 7110.35 seconds (1 hours, 58 minutes, 30.35 seconds), step size: 1382.46 seconds (23 minutes, 2.462 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00846016
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.55764
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.317854
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.330194
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0945311
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00449885
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 2.91482e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 6.33764e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 7.42914e-11
56: Linearization/solve/update time: 0.408786(87.636%)/0.0565559(12.1245%)/0.00111704(0.239473%)
56: Writing visualization results for the current time step.
56: Time step 16 done. CPU time: 6.97899 seconds, end time: 8492.81 seconds (2 hours, 21 minutes, 32.81 seconds), step size: 1382.46 seconds (23 minutes, 2.462 seconds)
56: Begin time step 17. Start time: 8492.81 seconds (2 hours, 21 minutes, 32.81 seconds), step size: 1497.67 seconds (24 minutes, 57.67 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.0072127
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 7.58119
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 1.79009
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 2.23387
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.426516
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00526038
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 1.31416e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 1.22902e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 2.74216e-10
56: Linearization/solve/update time: 0.410041(87.2375%)/0.0586033(12.4681%)/0.00138385(0.294419%)
56: Writing visualization results for the current time step.
56: Time step 17 done. CPU time: 7.47103 seconds, end time: 9990.48 seconds (2 hours, 46 minutes, 30.48 seconds), step size: 1497.67 seconds (24 minutes, 57.67 seconds)
56: Begin time step 18. Start time: 9990.48 seconds (2 hours, 46 minutes, 30.48 seconds), step size: 1622.47 seconds (27 minutes, 2.472 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00663998
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.39234
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.397817
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.00199682
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 6.0197e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 1.73902e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 7 error: 6.60863e-10
56: Linearization/solve/update time: 0.325067(87.3776%)/0.0460762(12.3852%)/0.000882554(0.237229%)
56: Writing visualization results for the current time step.
56: Time step 18 done. CPU time: 7.86299 seconds, end time: 11612.9 seconds (3 hours, 13 minutes, 32.95 seconds), step size: 1622.47 seconds (27 minutes, 2.472 seconds)
56: Begin time step 19. Start time: 11612.9 seconds (3 hours, 13 minutes, 32.95 seconds), step size: 2028.09 seconds (33 minutes, 48.09 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00581019
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 8.85163
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.936697
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 1.41392
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.254671
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00341559
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 3.11884e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 3.16264e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 5.88365e-10
56: Linearization/solve/update time: 0.406693(85.5648%)/0.0674997(14.2014%)/0.0011112(0.233788%)
56: Writing visualization results for the current time step.
56: Time step 19 done. CPU time: 8.35811 seconds, end time: 13641 seconds (3 hours, 47 minutes, 21.04 seconds), step size: 2028.09 seconds (33 minutes, 48.09 seconds)
56: Begin time step 20. Start time: 13641 seconds (3 hours, 47 minutes, 21.04 seconds), step size: 2197.1 seconds (36 minutes, 37.1 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00541396
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.01727
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 3.88335
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 1.02928
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.518742
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.0135501
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 4.26046e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 2.24192e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 5.36823e-10
56: Linearization/solve/update time: 0.411278(87.6649%)/0.0566953(12.0847%)/0.00117476(0.250403%)
56: Writing visualization results for the current time step.
56: Time step 20 done. CPU time: 8.849 seconds, end time: 15838.1 seconds (4 hours, 23 minutes, 58.14 seconds), step size: 2197.1 seconds (36 minutes, 37.1 seconds)
56: Serialize to file 'tutorial1_time=15838.1_rank=0.ers', next time step size: 2380.19
56: Begin time step 21. Start time: 15838.1 seconds (4 hours, 23 minutes, 58.14 seconds), step size: 2380.19 seconds (39 minutes, 40.19 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00465085
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 4.11208
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.314141
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.445736
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0624343
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00179369
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 8.25128e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 3.45099e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 1.97223e-10
56: Linearization/solve/update time: 0.406769(85.5426%)/0.0675638(14.2085%)/0.00118316(0.248817%)
56: Writing visualization results for the current time step.
56: Time step 21 done. CPU time: 9.34902 seconds, end time: 18218.3 seconds (5 hours, 3 minutes, 38.33 seconds), step size: 2380.19 seconds (39 minutes, 40.19 seconds)
56: Begin time step 22. Start time: 18218.3 seconds (5 hours, 3 minutes, 38.33 seconds), step size: 2578.54 seconds (42 minutes, 58.54 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00444615
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 9.23784
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 2.48631
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 2.8998
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.65644
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.0120296
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 7.60651e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 2.82546e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 2.40412e-09
56: Linearization/solve/update time: 0.41077(87.1395%)/0.0594991(12.622%)/0.00112432(0.238509%)
56: Writing visualization results for the current time step.
56: Time step 22 done. CPU time: 9.843 seconds, end time: 20796.9 seconds (5 hours, 46 minutes, 36.87 seconds), step size: 2578.54 seconds (42 minutes, 58.54 seconds)
56: Begin time step 23. Start time: 20796.9 seconds (5 hours, 46 minutes, 36.87 seconds), step size: 2793.42 seconds (46 minutes, 33.42 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00405732
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.98605
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.226435
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.621666
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0375596
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.0228581
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.00127375
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 3.64662e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 1.55983e-07
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 10 error: 3.49472e-10
56: Linearization/solve/update time: 0.45824(86.0115%)/0.0732018(13.7399%)/0.00132448(0.248604%)
56: Writing visualization results for the current time step.
56: Time step 23 done. CPU time: 10.398 seconds, end time: 23590.3 seconds (6 hours, 33 minutes, 10.28 seconds), step size: 2793.42 seconds (46 minutes, 33.42 seconds)
56: Begin time step 24. Start time: 23590.3 seconds (6 hours, 33 minutes, 10.28 seconds), step size: 2793.42 seconds (46 minutes, 33.42 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00371558
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 5.41356
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.537527
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.744163
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.104107
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00196125
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 2.04077e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 3.90638e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 1.04076e-10
56: Linearization/solve/update time: 0.41065(85.0309%)/0.0710994(14.7221%)/0.00119265(0.246954%)
56: Writing visualization results for the current time step.
56: Time step 24 done. CPU time: 10.904 seconds, end time: 26383.7 seconds (7 hours, 19 minutes, 43.7 seconds), step size: 2793.42 seconds (46 minutes, 33.42 seconds)
56: Begin time step 25. Start time: 26383.7 seconds (7 hours, 19 minutes, 43.7 seconds), step size: 3026.2 seconds (50 minutes, 26.2 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00355867
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 5.02273
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.115857
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.00124922
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.00011888
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 1.22006e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 7 error: 1.15279e-09
56: Linearization/solve/update time: 0.317518(85.8904%)/0.0512659(13.8677%)/0.000894303(0.241914%)
56: Writing visualization results for the current time step.
56: Time step 25 done. CPU time: 11.293 seconds, end time: 29409.9 seconds (8 hours, 10 minutes, 9.9 seconds), step size: 3026.2 seconds (50 minutes, 26.2 seconds)
56: Begin time step 26. Start time: 29409.9 seconds (8 hours, 10 minutes, 9.9 seconds), step size: 3782.75 seconds (1 hours, 3 minutes, 2.75 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00323064
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 3.66009
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.0134858
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 2.07382
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0377594
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.0837192
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.00143694
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 0.000110468
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 9.15778e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 10 error: 3.40361e-09
56: Linearization/solve/update time: 0.451363(85.9239%)/0.0726433(13.8288%)/0.00129925(0.247332%)
56: Writing visualization results for the current time step.
56: Time step 26 done. CPU time: 11.8416 seconds, end time: 33192.7 seconds (9 hours, 13 minutes, 12.65 seconds), step size: 3782.75 seconds (1 hours, 3 minutes, 2.75 seconds)
56: Begin time step 27. Start time: 33192.7 seconds (9 hours, 13 minutes, 12.65 seconds), step size: 3782.75 seconds (1 hours, 3 minutes, 2.75 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00308367
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 6.12631
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.142926
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.361822
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0552336
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00178068
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 3.42068e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 5.42919e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 5.24871e-10
56: Linearization/solve/update time: 0.410253(85.5301%)/0.0682652(14.232%)/0.00114128(0.237936%)
56: Writing visualization results for the current time step.
56: Time step 27 done. CPU time: 12.344 seconds, end time: 36975.4 seconds (10 hours, 16 minutes, 15.4 seconds), step size: 3782.75 seconds (1 hours, 3 minutes, 2.75 seconds)
56: Begin time step 28. Start time: 36975.4 seconds (10 hours, 16 minutes, 15.4 seconds), step size: 4097.98 seconds (1 hours, 8 minutes, 17.98 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00293961
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 12.4975
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.603862
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 1.86457
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.052146
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00903259
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.000145036
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 9.71206e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 6.22435e-09
56: Linearization/solve/update time: 0.407816(86.53%)/0.0623196(13.2229%)/0.00116421(0.247022%)
56: Writing visualization results for the current time step.
56: Time step 28 done. CPU time: 12.837 seconds, end time: 41073.4 seconds (11 hours, 24 minutes, 33.38 seconds), step size: 4097.98 seconds (1 hours, 8 minutes, 17.98 seconds)
56: Begin time step 29. Start time: 41073.4 seconds (11 hours, 24 minutes, 33.38 seconds), step size: 4439.48 seconds (1 hours, 13 minutes, 59.48 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00275393
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 4.37824
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.348808
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.440373
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 1.18064
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00184035
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 3.03055e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 3.8482e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 6.42182e-10
56: Linearization/solve/update time: 0.410682(86.6423%)/0.0621357(13.1089%)/0.00117947(0.248834%)
56: Writing visualization results for the current time step.
56: Time step 29 done. CPU time: 13.3317 seconds, end time: 45512.9 seconds (12 hours, 38 minutes, 32.86 seconds), step size: 4439.48 seconds (1 hours, 13 minutes, 59.48 seconds)
56: Begin time step 30. Start time: 45512.9 seconds (12 hours, 38 minutes, 32.86 seconds), step size: 4809.44 seconds (1 hours, 20 minutes, 9.44 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00253232
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 3.04699
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 1.5019
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 3.88194
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0162254
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.123953
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.00157832
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 0.000224836
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 1.11295e-07
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 10 error: 3.3982e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 11 error: 7.52835e-11
56: Linearization/solve/update time: 0.5131(85.9592%)/0.0822319(13.7762%)/0.00157938(0.264592%)
56: Writing visualization results for the current time step.
56: Time step 30 done. CPU time: 13.956 seconds, end time: 50322.3 seconds (13 hours, 58 minutes, 42.3 seconds), step size: 4809.44 seconds (1 hours, 20 minutes, 9.44 seconds)
56: Serialize to file 'tutorial1_time=50322.3_rank=0.ers', next time step size: 4372.21
56: Begin time step 31. Start time: 50322.3 seconds (13 hours, 58 minutes, 42.3 seconds), step size: 4372.21 seconds (1 hours, 12 minutes, 52.21 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00245374
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 3.90018
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.146093
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.223452
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.044937
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.0149814
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 1.76641e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 0.000202282
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 9.26265e-09
56: Linearization/solve/update time: 0.408374(87.2452%)/0.0584912(12.4961%)/0.0012109(0.258698%)
56: Writing visualization results for the current time step.
56: Time step 31 done. CPU time: 14.447 seconds, end time: 54694.5 seconds (15 hours, 11 minutes, 34.51 seconds), step size: 4372.21 seconds (1 hours, 12 minutes, 52.21 seconds)
56: Begin time step 32. Start time: 54694.5 seconds (15 hours, 11 minutes, 34.51 seconds), step size: 4736.57 seconds (1 hours, 18 minutes, 56.57 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00238195
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 10.2871
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.184824
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.62565
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.088568
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00194898
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.000141859
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 9.6212e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 1.60235e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 10 error: 3.28749e-11
56: Linearization/solve/update time: 0.462098(86.1731%)/0.0726203(13.5424%)/0.00152531(0.284444%)
56: Writing visualization results for the current time step.
56: Time step 32 done. CPU time: 15.006 seconds, end time: 59431.1 seconds (16 hours, 30 minutes, 31.08 seconds), step size: 4736.57 seconds (1 hours, 18 minutes, 56.57 seconds)
56: Begin time step 33. Start time: 59431.1 seconds (16 hours, 30 minutes, 31.08 seconds), step size: 4736.57 seconds (1 hours, 18 minutes, 56.57 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00230267
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 8.00776
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 2.49766
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 4.20153
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.200179
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.0447294
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.00140278
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 0.000317073
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 5.99049e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 10 error: 6.64821e-09
56: Linearization/solve/update time: 0.456808(86.4931%)/0.0700316(13.26%)/0.00130434(0.246966%)
56: Writing visualization results for the current time step.
56: Time step 33 done. CPU time: 15.5575 seconds, end time: 64167.6 seconds (17 hours, 49 minutes, 27.65 seconds), step size: 4736.57 seconds (1 hours, 18 minutes, 56.57 seconds)
56: Begin time step 34. Start time: 64167.6 seconds (17 hours, 49 minutes, 27.65 seconds), step size: 4736.57 seconds (1 hours, 18 minutes, 56.57 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00217722
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.82822
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 3.01364
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 1.98705
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.019524
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.041787
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.00123868
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 0.000228161
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 5.74821e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 10 error: 1.41393e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 11 error: 3.83797e-11
56: Linearization/solve/update time: 0.497249(86.4723%)/0.0763483(13.2771%)/0.00144121(0.250628%)
56: Writing visualization results for the current time step.
56: Time step 34 done. CPU time: 16.154 seconds, end time: 68904.2 seconds (19 hours, 8 minutes, 24.21 seconds), step size: 4736.57 seconds (1 hours, 18 minutes, 56.57 seconds)
56: Begin time step 35. Start time: 68904.2 seconds (19 hours, 8 minutes, 24.21 seconds), step size: 4305.97 seconds (1 hours, 11 minutes, 45.97 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00212999
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 3.74633
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.296788
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.357606
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0498149
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00166006
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 1.24425e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 7.91054e-07
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 6.56866e-10
56: Linearization/solve/update time: 0.404564(86.2218%)/0.0635219(13.538%)/0.00112717(0.240226%)
56: Writing visualization results for the current time step.
56: Time step 35 done. CPU time: 16.648 seconds, end time: 73210.2 seconds (20 hours, 20 minutes, 10.18 seconds), step size: 4305.97 seconds (1 hours, 11 minutes, 45.97 seconds)
56: Begin time step 36. Start time: 73210.2 seconds (20 hours, 20 minutes, 10.18 seconds), step size: 4664.8 seconds (1 hours, 17 minutes, 44.8 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00211244
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 10.3628
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 1.15029
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 1.58766
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.282946
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00366372
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.000312135
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 9.07102e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 9.72714e-09
56: Linearization/solve/update time: 0.408031(85.491%)/0.0680933(14.267%)/0.00115509(0.242016%)
56: Writing visualization results for the current time step.
56: Time step 36 done. CPU time: 17.147 seconds, end time: 77875 seconds (21 hours, 37 minutes, 54.98 seconds), step size: 4664.8 seconds (1 hours, 17 minutes, 44.8 seconds)
56: Begin time step 37. Start time: 77875 seconds (21 hours, 37 minutes, 54.98 seconds), step size: 5053.53 seconds (1 hours, 24 minutes, 13.53 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00206199
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 1.04706
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 3.46331
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 2.46562
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.17365
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.0316143
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.00127112
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 0.000250676
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 3.08381e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 10 error: 4.39403e-09
56: Linearization/solve/update time: 0.455927(86.1551%)/0.0720257(13.6105%)/0.00124046(0.234406%)
56: Writing visualization results for the current time step.
56: Time step 37 done. CPU time: 17.701 seconds, end time: 82928.5 seconds (23 hours, 2 minutes, 8.51 seconds), step size: 5053.53 seconds (1 hours, 24 minutes, 13.53 seconds)
56: Begin time step 38. Start time: 82928.5 seconds (23 hours, 2 minutes, 8.51 seconds), step size: 5053.53 seconds (1 hours, 24 minutes, 13.53 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00197716
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 3.59516
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.118427
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.308724
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0372483
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.022144
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 0.00107463
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 0.000178557
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 9 error: 2.50341e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 10 error: 4.51708e-09
56: Linearization/solve/update time: 0.44988(86.335%)/0.0698841(13.4112%)/0.00132252(0.253801%)
56: Writing visualization results for the current time step.
56: Time step 38 done. CPU time: 18.245 seconds, end time: 87982 seconds (1 days, 0 hours, 26.37 minutes), step size: 5053.53 seconds (1 hours, 24 minutes, 13.53 seconds)
56: Begin time step 39. Start time: 87982 seconds (1 days, 0 hours, 26.37 minutes), step size: 5053.53 seconds (1 hours, 24 minutes, 13.53 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00197432
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 7.60767
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.285784
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.558584
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 0.0763043
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 0.00179398
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 7 error: 9.13956e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 8 error: 4.36665e-08
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 9 error: 7.67171e-09
56: Linearization/solve/update time: 0.408374(86.272%)/0.0638615(13.4912%)/0.00112061(0.236738%)
56: Writing visualization results for the current time step.
56: Time step 39 done. CPU time: 18.7389 seconds, end time: 93035.6 seconds (1 days, 1 hours, 50.59 minutes), step size: 5053.53 seconds (1 hours, 24 minutes, 13.53 seconds)
56: Begin time step 40. Start time: 93035.6 seconds (1 days, 1 hours, 50.59 minutes), step size: 3517.03 seconds (58 minutes, 37.03 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00194524
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 2.28312
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.0425892
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.00118449
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 2.21434e-05
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 6 error: 1.40811e-07
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 7 error: 9.61754e-11
56: Linearization/solve/update time: 0.326311(85.9895%)/0.0523133(13.7856%)/0.00085331(0.224864%)
56: Writing visualization results for the current time step.
56: Time step 40 done. CPU time: 19.139 seconds, end time: 96552.6 seconds (1 days, 2 hours, 49.21 minutes), step size: 3517.03 seconds (58 minutes, 37.03 seconds)
56: Serialize to file 'tutorial1_time=96552.6_rank=0.ers', next time step size: 3447.39
56: Begin time step 41. Start time: 96552.6 seconds (1 days, 2 hours, 49.21 minutes), step size: 3447.39 seconds (57 minutes, 27.39 seconds)
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 1 error: 0.00186372
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 2 error: 2.38064
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 3 error: 0.00745482
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 4 error: 0.000266996
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Solve: M deltax^k = r
56: Update: x^(k+1) = x^k - deltax^k
56: Newton iteration 5 error: 3.14824e-06
56: Linearize: r(x^k) = dS/dt + div F - q;   M = grad r
56: Newton iteration 6 error: 3.04407e-09
56: Linearization/solve/update time: 0.27783(87.2223%)/0.0399288(12.5353%)/0.000772286(0.242452%)
56: Writing visualization results for the current time step.
56: Time step 41 done. CPU time: 19.4742 seconds, end time: 100000 seconds (1 days, 3 hours, 46.67 minutes), step size: 3447.39 seconds (57 minutes, 27.39 seconds)
56: Simulation of problem 'tutorial1' finished.
56: 
56: ------------------------ Timing receipt ------------------------
56: Setup time: 0.0239 seconds, 0.123%
56: Simulation time: 19.5 seconds, 99.9%
56:     Linearization time: 16.1 seconds, 82.8%
56:     Linear solve time: 2.44 seconds, 12.5%
56:     Newton update time: 0.046 seconds, 0.236%
56:     Pre/postprocess time: 0.107 seconds, 0.549%
56:     Output write time: 0.337 seconds, 1.73%
56: First process' simulation CPU time: 18.9 seconds
56: Number of processes: 1
56: Threads per processes: 1
56: Total CPU time: 18.9 seconds
56: 
56: Note 1: If not stated otherwise, all times are wall clock times
56: Note 2: Taxes and administrative overhead are 2.22%
56: 
56: Our simulation hours are 24/7. Thank you for choosing us.
56: ----------------------------------------------------------------
56: 
56: eWoms reached the destination. If it is not the one that was intended, change the booking and try again.
56: ######################
56: # Comparing results
56: ######################
56: RND: '49481a0d0fa799aa9fd3046d2db49901'
56: Simulation name: 'tutorial1'
56: Number of timesteps: '41'
56/58 Test #56: tutorial1 ...........................   Passed   19.69 sec
test 57
      Start 57: tasklets
57: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--plain" "-e" "test_tasklets" "--"
57: Test timeout computed to be: 1500
57: ######################
57: # Running test 'test_tasklets'
57: ######################
57: executing "./bin/test_tasklets "
57: before barrier
57: Sleep tasklet 0 of 100 ms completed by worker thread 1
57: Sleep tasklet 1 of 100 ms completed by worker thread 0
57: Sleep tasklet 2 of 100 ms completed by worker thread 1
57: Sleep tasklet 3 of 100 ms completed by worker thread 0
57: Sleep tasklet 4 of 100 ms completed by worker thread 1
57: after barrier
57: Sleep completed by worker thread 1
57: Sleep completed by worker thread 0
57: Sleep completed by worker thread 1
57: Sleep completed by worker thread 0
57: Sleep completed by worker thread 1
57: Sleep completed by worker thread 0
57: Sleep completed by worker thread 1
57/58 Test #57: tasklets ............................   Passed    0.73 sec
test 58
      Start 58: mpiutil
58: Test command: /tmp/makepkg/opm-models/src/opm-models-release-2022.04-final/bin/runtest.sh "--parallel-program=4" "-e" "test_mpiutil" "--"
58: Test timeout computed to be: 1500
58: ######################
58: # Running test 'test_mpiutil'
58: ######################
58: executing "mpirun -np "4" ./bin/test_mpiutil "
58/58 Test #58: mpiutil .............................   Passed    0.29 sec
90% tests passed, 6 tests failed out of 58
Total Test time (real) = 4199.83 sec
The following tests FAILED:
	 48 - quadrature (Failed)
	 49 - co2injection_ncp_ni_ecfv_parallel (Failed)
	 50 - obstacle_immiscible_parallel (Failed)
	 51 - lens_immiscible_vcfv_fd_parallel (Failed)
	 52 - lens_immiscible_vcfv_ad_parallel (Failed)
	 53 - lens_immiscible_ecfv_ad_parallel (Failed)
Errors while running CTest
```
