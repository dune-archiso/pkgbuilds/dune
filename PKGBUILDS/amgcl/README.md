## [`amgcl`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/amgcl/PKGBUILD) [![Packaging status](https://badgen.net/badge/AUR/Package/blue)](https://aur.archlinux.org/packages/amgcl)

[![amgcl-git](https://img.shields.io/aur/version/amgcl-git?color=1793d1&label=amgcl-git&logo=arch-linux&style=for-the-badge)](https://aur.archlinux.org/packages/amgcl-git)

[![Packaging status](https://repology.org/badge/vertical-allrepos/amgcl.svg)](https://repology.org/project/amgcl/versions)

[](https://github.com/ddemidov/amgcl)
