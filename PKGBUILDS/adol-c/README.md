## [`adol-c`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/adol-c/PKGBUILD) [![Packaging status](https://badgen.net/badge/AUR/Package/blue)](https://aur.archlinux.org/packages/adol-c)

[![adol-c](https://img.shields.io/aur/version/adol-c?color=1793d1&label=adol-c&logo=arch-linux&style=for-the-badge)](https://aur.archlinux.org/packages/adol-c)

[![AUR version](https://img.shields.io/aur/version/adol-c?logo=Arch%20Linux&color=brightgreen)](https://aur.archlinux.org/packages/adol-c)

[![Packaging status](https://repology.org/badge/vertical-allrepos/adolc.svg)](https://repology.org/project/adolc/versions)
