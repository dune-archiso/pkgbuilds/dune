[](https://git.rwth-aachen.de/avt-svt/public/thirdparty/Adol-C)

```
`configure' configures adolc 2.7.2 to adapt to many kinds of systems.

Usage: ./configure [OPTION]... [VAR=VALUE]...

To assign environment variables (e.g., CC, CFLAGS...), specify them as
VAR=VALUE.  See below for descriptions of some of the useful variables.

Defaults for the options are specified in brackets.

Configuration:
  -h, --help              display this help and exit
      --help=short        display options specific to this package
      --help=recursive    display the short help of all the included packages
  -V, --version           display version information and exit
  -q, --quiet, --silent   do not print `checking ...' messages
      --cache-file=FILE   cache test results in FILE [disabled]
  -C, --config-cache      alias for `--cache-file=config.cache'
  -n, --no-create         do not create output files
      --srcdir=DIR        find the sources in DIR [configure dir or `..']

Installation directories:
  --prefix=PREFIX         install architecture-independent files in PREFIX
                          [/home/carlosal1015/adolc_base]
  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
                          [PREFIX]

By default, `make install' will install all the files in
`/home/carlosal1015/adolc_base/bin', `/home/carlosal1015/adolc_base/lib' etc.  You can specify
an installation prefix other than `/home/carlosal1015/adolc_base' using `--prefix',
for instance `--prefix=$HOME'.

For better control, use the options below.

Fine tuning of the installation directories:
  --bindir=DIR            user executables [EPREFIX/bin]
  --sbindir=DIR           system admin executables [EPREFIX/sbin]
  --libexecdir=DIR        program executables [EPREFIX/libexec]
  --sysconfdir=DIR        read-only single-machine data [PREFIX/etc]
  --sharedstatedir=DIR    modifiable architecture-independent data [PREFIX/com]
  --localstatedir=DIR     modifiable single-machine data [PREFIX/var]
  --runstatedir=DIR       modifiable per-process data [LOCALSTATEDIR/run]
  --libdir=DIR            object code libraries [EPREFIX/lib]
  --includedir=DIR        C header files [PREFIX/include]
  --oldincludedir=DIR     C header files for non-gcc [/usr/include]
  --datarootdir=DIR       read-only arch.-independent data root [PREFIX/share]
  --datadir=DIR           read-only architecture-independent data [DATAROOTDIR]
  --infodir=DIR           info documentation [DATAROOTDIR/info]
  --localedir=DIR         locale-dependent data [DATAROOTDIR/locale]
  --mandir=DIR            man documentation [DATAROOTDIR/man]
  --docdir=DIR            documentation root [DATAROOTDIR/doc/adolc]
  --htmldir=DIR           html documentation [DOCDIR]
  --dvidir=DIR            dvi documentation [DOCDIR]
  --pdfdir=DIR            pdf documentation [DOCDIR]
  --psdir=DIR             ps documentation [DOCDIR]

Program names:
  --program-prefix=PREFIX            prepend PREFIX to installed program names
  --program-suffix=SUFFIX            append SUFFIX to installed program names
  --program-transform-name=PROGRAM   run sed PROGRAM on installed program names

System types:
  --build=BUILD     configure for building on BUILD [guessed]
  --host=HOST       cross-compile to build programs to run on HOST [BUILD]

Optional Features:
  --disable-option-checking  ignore unrecognized --enable/--with options
  --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
  --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]
  --enable-silent-rules   less verbose build output (undo: "make V=1")
  --disable-silent-rules  verbose build output (undo: "make V=0")
  --enable-maintainer-mode
                          enable make rules and dependencies not useful (and
                          sometimes confusing) to the casual installer
  --enable-ampi           build ADOL-C with adjoinable MPI (AMPI) support
                          [default=disabled]
  --enable-medipack       build ADOL-C with MeDiPack (MPI) support
                          [default=disabled]
  --enable-dependency-tracking
                          do not reject slow dependency extractors
  --disable-dependency-tracking
                          speeds up one-time build
  --enable-static[=PKGS]  build static libraries [default=no]
  --enable-shared[=PKGS]  build shared libraries [default=yes]
  --enable-fast-install[=PKGS]
                          optimize for fast installation [default=yes]
  --disable-libtool-lock  avoid locking (might break parallel builds)
  --disable-use-calloc    disable use of calloc and use malloc instead for
                          memory allocation [default=calloc is used]
  --enable-atrig-erf      enable atrig and erf functions in ADOL-C
                          [default=no]
  --enable-ulong          enable 64-bit locations (only available on 64-bit
                          systems) [default=32-bit]
  --disable-double        disable double precision arithmetic [untested,
                          default=double is enabled]
  --enable-advanced-branching
                          enable advanced branching operations to reduce
                          retaping [default=no]. The boolean valued comparison
                          operators with two adouble arguments will not return
                          boolean results but the active results may be used
                          to automatically switch branches in conjunction with
                          condassign or advector (see manual).
  --enable-traceless-refcounting
                          enable reference counting for tapeless numbers
                          [default=no]. With this enabled some additional
                          checks will be conducted when setting the number of
                          directional derivatives for tapeless numbers using
                          the SetNumDir() function.
  --enable-activity-tracking
                          enable activity tracking to reduce trace size but
                          increased tracing time [default=no]. Only the
                          operations involving actual dependency relationships
                          from the independent variables will be recorded on
                          the trace, this however requires more checks to be
                          performed during the tracing and increases tracing
                          time. Useful only if memory is a constraint and
                          tracing is done fewer times than derivative
                          computations.

  --enable-debug          enable ADOL-C debug mode [default=no]

  --enable-harddebug      enable ADOL-C hard debug mode [default=no]

  --disable-stdczero      adouble default constructor does not initialize the
                          value to zero (improves performance but yields
                          incorrect results for implicit array
                          initializations, see manual) [default=enabled]

  --enable-lateinit       adouble constructors need to be called. With malloc
                          or realloc that is not the case. With this option
                          the adouble can do a late initialization.)
                          [default=disabled]

  --enable-tserrno        use errno as thread number cache [default=no]

  --enable-sparse         build sparse drivers [default=disabled]

  --enable-docexa         build documented examples [default=disabled]
  --enable-addexa         build additional examples [default=disabled]
  --enable-parexa         build parallel example [default=disabled], if
                          enabled -with-openmp-flag=FLAG required

		  --disable-tapedoc-values
                          should the tape_doc routine compute the values as it
                          interprets and prints the tape contents
                          [default=enabled]

Optional Packages:
  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
  --without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
  --with-pic[=PKGS]       try to use only PIC/non-PIC objects [default=use
                          both]
  --with-aix-soname=aix|svr4|both
                          shared library versioning (aka "SONAME") variant to
                          provide on AIX, [default=aix].
  --with-gnu-ld           assume the C compiler uses GNU ld [default=no]
  --with-sysroot[=DIR]    Search for dependent libraries within DIR (or the
                          compiler's sysroot if not specified).
  --with-mpi-root=MPIROOT absolute path to the MPI root directory
  --with-mpicc=MPICC      name of the MPI C++ compiler to use (default mpicc)
  --with-mpicxx=MPICXX    name of the MPI C++ compiler to use (default mpicxx)
  --with-openmp-flag=FLAG use FLAG to enable OpenMP at compile time
                          [default=none]
  --with-boost[=ARG]      use Boost library from a standard location
                          (ARG=yes), from the specified location (ARG=<path>),
                          or disable it (ARG=no) [ARG=yes]
  --with-boost-libdir=LIB_DIR
                          Force given directory for boost libraries. Note that
                          this will override library path detection, so use
                          this parameter only if default library detection
                          fails and you know exactly where your boost
                          libraries are located.
  --with-boost-system[=special-lib]
                          use the System library from boost - it is possible
                          to specify a certain library for the linker e.g.
                          --with-boost-system=boost_system-gcc-mt

  --with-colpack=DIR      path to the colpack library and headers
                          [default=system libraries]
  --with-cflags=FLAGS     use CFLAGS=FLAGS (default: -O2)
  --with-cxxflags=FLAGS   use CXXFLAGS=FLAGS (default: -O2)
  --with-ampi=AMPI_DIR    full path to the installation of adjoinable MPI
                          (AMPI)
  --with-medipack=MEDIPACK_DIR
                          full path to the installation of MeDiPack
  --with-soname=NAME      user can choose what to call his library here
                          [default: if ampi enabled then adolc_ampi otherwise
                          adolc]

Some influential environment variables:
  CXX         C++ compiler command
  CXXFLAGS    C++ compiler flags
  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
              nonstandard directory <lib dir>
  LIBS        libraries to pass to the linker, e.g. -l<library>
  CPPFLAGS    (Objective) C/C++ preprocessor flags, e.g. -I<include dir> if
              you have headers in a nonstandard directory <include dir>
  CC          C compiler command
  CFLAGS      C compiler flags
  CPP         C preprocessor
  LT_SYS_LIBRARY_PATH
              User-defined run-time library search path.
  CXXCPP      C++ preprocessor
  PYTHON      the Python interpreter

Use these variables to override the choices made by `configure' or to help
it to find libraries and programs with nonstandard names/locations.

Report bugs to <adol-c@list.coin-or.org>.
```

[](https://www.coin-or.org/CppAD/Doc/cmake.htm)
[](https://cmake.org/cmake/help/latest/command/find_package.html#search-procedure)
