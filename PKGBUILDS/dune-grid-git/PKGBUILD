# Maintainer: anon at sansorgan.es
_name=dune-grid
pkgbase="${_name}"-git
pkgname=("${pkgbase}" python-"${pkgbase}")
# _gitcommit=d1b659efbc484cc7382373b50735e4aceeb4bf8b
pkgver=2.9.r11244.ce5efe9e4
pkgrel=1
#epoch=
pkgdesc="Grid interface and core implementations (git version)"
arch=(x86_64)
url="https://dune-project.org/modules/${_name}"
license=('custom:GPL2 with runtime exception')
# groups=('dune-core-git')
makedepends=(dune-geometry-git dune-uggrid-git alberta-git doxygen graphviz python-setuptools) # alberta-git
checkdepends=(python-dune-geometry-git dune-uggrid-git parmetis gnuplot vtk ffmpeg python-mpi4py fmt pdal glew ospray qt5-base openvr unixodbc liblas cgns adios2 libharu gl2ps)
optdepends=('vc: C++ Vectorization library'
  'doxygen: Generate the class documentation from C++ sources'
  'parmetis: Parallel Graph Partitioning and Fill-reducing Matrix Ordering'
  'scotch: Software package and libraries for graph, mesh and hypergraph partitioning, static mapping, and sparse matrix block ordering')
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=("git+https://github.com/dune-project/${_name}#branch=master") # #commit=${_gitcommit} ${pkgbase}.opts
#noextract=()
sha512sums=('SKIP')

pkgver() {
  cd "${_name}"
  printf "2.10.r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  sed -i 's/^Version: '"${pkgver%%.r*}"'-git/Version: '"${pkgver%%.r*}"'/' ${_name}/dune.module
  sed -i 's/^              TIMEOUT 300/              TIMEOUT 3000/' ${_name}/dune/grid/test/CMakeLists.txt
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L76
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
  # export DUNE_PY_DIR="${BUILDDIR}/${pkgbase}"
  export DUNE_LOG_LEVEL=DEBUG
  export DUNE_SAVE_BUILD='console'
  # export DUNEPY_DISABLE_PLOTTING=1
}

# python -m venv --system-site-packages test-env
# . test-env/bin/activate
# dunecontrol --opts=${pkgbase}.opts cmake -Wno-dev : make
# deactivate

build() {
  cmake \
    -S "${_name}" \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DDUNE_ENABLE_PYTHONBINDINGS=ON \
    -DDUNE_PYTHON_INSTALL_LOCATION="none" \
    -DDUNE_PYTHON_USE_VENV=OFF \
    -DDUNE_RUNNING_IN_CI=OFF \
    -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE \
    -Wno-dev

  # sed -i 's/^include-system-site-packages = false/include-system-site-packages = true/' build-cmake/dune-env/pyvenv.cfg
  # cat build-cmake/dune-env/pyvenv.cfg
  cmake --build build-cmake --target all # help
  cd "build-cmake/python"
  python setup.py build
  # python -m venv --system-site-packages test-env
  # . test-env/bin/activate
  # export DUNE_PYTHON_WHEELHOUSE="${PWD}"
  # dunecontrol --opts=${pkgbase}.opts cmake -Wno-dev : make
  # deactivate
}

check() {
  if [ -z "$(ldconfig -p | grep libcuda.so.1)" ]; then
    export OMPI_MCA_opal_warn_on_missing_libcuda=0
  fi
  # dunecontrol --opts=${pkgbase}.opts make build_tests
  # cd "${srcdir}/${_name}/build-cmake"
  cmake --build build-cmake --target build_tests
  # DUNE_CONTROL_PATH="${srcdir}/${_name}" dune-ctest --output-on-failure
  mkdir -p ${CI_PROJECT_DIR}/x86_64
  ctest -E "(py*)" --verbose --output-on-failure --test-dir build-cmake 2>&1 | tee -a ${CI_PROJECT_DIR}/x86_64/log_dune-grid-git_$(date -u +"%Y-%m-%d-%H-%M-%S" --date='5 hours ago').log >/dev/null 2>&1
}

package_dune-grid-git() {
  depends=(dune-geometry-git dune-uggrid-git alberta)
  provides=("${_name}=${pkgver%%.r*}")
  conflicts=(${_name})
  # dunecontrol --only=${_name} make install DESTDIR="${pkgdir}"
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  install -Dm644 ${_name}/COPYING "${pkgdir}/usr/share/licenses/${_name}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}

package_python-dune-grid-git() {
  depends=(dune-grid-git python-dune-geometry-git)
  pkgdesc+=" (python bindings)"
  conflicts+=("python-${_name}")
  # cd "${_name}/build-cmake/python"
  cd "build-cmake/python"
  PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
}
