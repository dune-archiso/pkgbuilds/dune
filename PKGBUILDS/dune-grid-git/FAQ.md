## [`dune-grid-git`]( https://gitlab.com/dune-archiso/dune-core-git/-/raw/main/dune-grid-git/PKGBUILD)

When the package `dune-grid-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `bin`
- [x] `include`
- [x] `lib`
- [ ] `share`
  - [x] `bash-completion`
  - [ ] `doc`
  - [x] `dune`
  - [x] `dune-common`
  - [x] `licenses`
  - [ ] `man`

```console
-DCMAKE_DISABLE_DOCUMENTATION=TRUE
-DCMAKE_C_FLAGS_RELEASE='-fPIC'
```

### Remarks

No `.tex` for compile.

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-grid
doxygen_install
headercheck
install_python
install_python__tmp_makepkg_dune-grid-git_src_dune-grid_python
target_pybackrest1
target_pyinterpolate
target_pytest_gf1
target_pytest_gf2
target_pytest_indexset
test_python
_grid
albertagrid1d
albertagrid2d
albertagrid3d
amirameshtest
conformvolumevtktest
dgf2dgf
dunealbertagrid1d
dunealbertagrid2d
dunealbertagrid3d
dunegrid
geometrygrid-coordfunction-copyconstructor
globalindexsettest
gmsh2dgf
gmsh2dgfugsimplex
gmshtest-alberta2d
gmshtest-alberta3d
gmshtest-onedgrid
gmshtest-uggrid
gnuplottest
issue-53-uggrid-intersections
mcmgmappertest
nonconformboundaryvtktest
persistentcontainertest
printgridtest
recipe-integration
recipe-iterate-over-grid
scsgmappertest
starcdreadertest
structuredgridfactorytest
subsamplingvtktest
tensorgridfactorytest
test-dgf-alberta
test-dgf-gmsh-ug
test-dgf-oned
test-dgf-ug
test-dgf-yasp
test-dgf-yasp-offset
test-geogrid-uggrid
test-geogrid-yaspgrid
test-gridinfo
test-hierarchicsearch
test-identitygrid
test-loadbalancing
test-mcmg-geogrid
test-oned
test-parallel-ug
test-ug
test-yaspgrid-backuprestore-equidistant
test-yaspgrid-backuprestore-equidistantoffset
test-yaspgrid-backuprestore-tensor
test-yaspgrid-entityshifttable
test-yaspgrid-tensorgridfactory
test-yaspgrid-yaspfactory-1d
test-yaspgrid-yaspfactory-2d
test-yaspgrid-yaspfactory-3d
testiteratorranges
vertexordertest
vtksequencetest
vtktest
```

set(CMAKE_CXX_COMPILE_OPTIONS_PIE "-fPIC")

https://gitlab.kitware.com/cmake/cmake/-/issues/16915


```
53/60 Test #53: vertexordertest .......................................   Passed    0.22 sec
      Start 54: pytest_gf1
54/60 Test #54: pytest_gf1 ............................................***Failed   13.76 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
DUNE-INFO: Creating new dune-py module in /home/aur/.cache/dune-py
DUNE-INFO: Configuring dune-py module in /home/aur/.cache/dune-py
DUNE-INFO: Compiling HierarchicalGrid
Traceback (most recent call last):
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/dune/python/test/test_gf1.py", line 56, in <module>
    gridView = structuredGrid([0,0],[1,1],[10,10])
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/core.py", line 44, in structuredGrid
    return yaspGrid(domain, dimgrid=len(lower))
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/_grids.py", line 38, in yaspGrid
    gridModule = module(includes, typeName, ctor)
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/grid_generator.py", line 339, in module
    module = generator.load(includes, typeName, typeHash, *args, **kwargs)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 161, in load
    return self.post(moduleName, source, postscript)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 119, in post
    module = builder.load(moduleName, source, self.typeName[0])
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 144, in load
    self.compile(moduleName)
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 71, in compile
    raise CompileError(buffer_to_str(stderr))
dune.generator.exceptions.CompileError: make: *** No rule to make target 'hierarchicalgrid_966e2a5c8356c5b278ccd3acad180f0a'.  Stop.
      Start 55: pytest_gf2
55/60 Test #55: pytest_gf2 ............................................***Failed    0.46 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
Traceback (most recent call last):
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/dune/python/test/test_gf2.py", line 140, in <module>
    gridView = structuredGrid([0,0],[1,1],[10,10])
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/core.py", line 44, in structuredGrid
    return yaspGrid(domain, dimgrid=len(lower))
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/_grids.py", line 38, in yaspGrid
    gridModule = module(includes, typeName, ctor)
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/grid_generator.py", line 339, in module
    module = generator.load(includes, typeName, typeHash, *args, **kwargs)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 161, in load
    return self.post(moduleName, source, postscript)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 119, in post
    module = builder.load(moduleName, source, self.typeName[0])
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 144, in load
    self.compile(moduleName)
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 71, in compile
    raise CompileError(buffer_to_str(stderr))
dune.generator.exceptions.CompileError: make: *** No rule to make target 'hierarchicalgrid_966e2a5c8356c5b278ccd3acad180f0a'.  Stop.
      Start 56: pytest_indexset
56/60 Test #56: pytest_indexset .......................................***Failed    0.45 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
Traceback (most recent call last):
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/dune/python/test/test_indexset.py", line 16, in <module>
    gridView = structuredGrid([0,0],[1,1],[10,10])
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/core.py", line 44, in structuredGrid
    return yaspGrid(domain, dimgrid=len(lower))
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/_grids.py", line 38, in yaspGrid
    gridModule = module(includes, typeName, ctor)
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/grid_generator.py", line 339, in module
    module = generator.load(includes, typeName, typeHash, *args, **kwargs)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 161, in load
    return self.post(moduleName, source, postscript)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 119, in post
    module = builder.load(moduleName, source, self.typeName[0])
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 144, in load
    self.compile(moduleName)
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 71, in compile
    raise CompileError(buffer_to_str(stderr))
dune.generator.exceptions.CompileError: make: *** No rule to make target 'hierarchicalgrid_966e2a5c8356c5b278ccd3acad180f0a'.  Stop.
      Start 57: pyinterpolate
57/60 Test #57: pyinterpolate .........................................***Failed    0.47 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
Traceback (most recent call last):
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/dune/python/test/interpolate.py", line 69, in <module>
    view = gridView(domain)
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/_grids.py", line 38, in yaspGrid
    gridModule = module(includes, typeName, ctor)
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/grid_generator.py", line 339, in module
    module = generator.load(includes, typeName, typeHash, *args, **kwargs)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 161, in load
    return self.post(moduleName, source, postscript)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 119, in post
    module = builder.load(moduleName, source, self.typeName[0])
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 144, in load
    self.compile(moduleName)
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 71, in compile
    raise CompileError(buffer_to_str(stderr))
dune.generator.exceptions.CompileError: make: *** No rule to make target 'hierarchicalgrid_966e2a5c8356c5b278ccd3acad180f0a'.  Stop.
      Start 58: pybackrest1
58/60 Test #58: pybackrest1 ...........................................***Failed    0.45 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
Traceback (most recent call last):
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/dune/python/test/backrest1.py", line 26, in <module>
    grid = backup()
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/dune/python/test/backrest1.py", line 6, in backup
    grid = structuredGrid([0,0],[1,1],[2,2])
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/core.py", line 44, in structuredGrid
    return yaspGrid(domain, dimgrid=len(lower))
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/_grids.py", line 38, in yaspGrid
    gridModule = module(includes, typeName, ctor)
  File "/tmp/makepkg/dune-grid-git/src/dune-grid/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/grid/grid_generator.py", line 339, in module
    module = generator.load(includes, typeName, typeHash, *args, **kwargs)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 161, in load
    return self.post(moduleName, source, postscript)
  File "/usr/lib/python3.9/site-packages/dune/generator/generator.py", line 119, in post
    module = builder.load(moduleName, source, self.typeName[0])
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 144, in load
    self.compile(moduleName)
  File "/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 71, in compile
    raise CompileError(buffer_to_str(stderr))
dune.generator.exceptions.CompileError: make: *** No rule to make target 'hierarchicalgrid_966e2a5c8356c5b278ccd3acad180f0a'.  Stop.
      Start 59: recipe-iterate-over-grid
59/60 Test #59: recipe-iterate-over-grid ..............................   Passed    0.14 sec
      Start 60: recipe-integration
60/60 Test #60: recipe-integration ....................................   Passed    0.17 sec
92% tests passed, 5 tests failed out of 60
Label Time Summary:
quick    =  15.60 sec*proc (5 tests)
Total Test time (real) = 842.29 sec
The following tests did not run:
	  6 - printgridtest (Skipped)
	  7 - subsamplingvtktest (Skipped)
	  8 - vtktest (Skipped)
	  9 - vtktest-mpi-2 (Skipped)
	 11 - amirameshtest (Skipped)
The following tests FAILED:
	 54 - pytest_gf1 (Failed)
	 55 - pytest_gf2 (Failed)
	 56 - pytest_indexset (Failed)
	 57 - pyinterpolate (Failed)
	 58 - pybackrest1 (Failed)
Errors while running CTest
======================================================================
Name:      pytest_gf1
FullName:  ./dune/python/test/pytest_gf1
Status:    FAILED
======================================================================
Name:      pytest_gf2
FullName:  ./dune/python/test/pytest_gf2
Status:    FAILED
======================================================================
Name:      pytest_indexset
FullName:  ./dune/python/test/pytest_indexset
Status:    FAILED
======================================================================
Name:      pyinterpolate
FullName:  ./dune/python/test/pyinterpolate
Status:    FAILED
======================================================================
Name:      pybackrest1
FullName:  ./dune/python/test/pybackrest1
Status:    FAILED
JUnit report for CTest results written to /builds/dune-archiso/repository/dune-archiso-repository-pdelab-git/junit/dune-archiso-repository-pdelab-git-cmake.xml
```

```
prepare() {
  # Comment 1
  sed -i 's/^  add_executable/  #add_executable/' ${_name}/dune/grid/albertagrid/test/CMakeLists.txt
  sed -i 's/^  target_link_libraries/  #target_link_libraries/' ${_name}/dune/grid/albertagrid/test/CMakeLists.txt
  sed -i 's/^  add_dune_alberta_flags/  #add_dune_alberta_flags/' ${_name}/dune/grid/albertagrid/test/CMakeLists.txt
  sed -i 's/^  dune_add_test/  #dune_add_test/' ${_name}/dune/grid/albertagrid/test/CMakeLists.txt
  # Comment 2
  # sed -i 's/^  foreach(WORLDDIM/  #foreach(WORLDDIM/' ${_name}/dune/grid/test/CMakeLists.txt
  # sed -i 's/^    foreach(GRIDDIM/    #foreach(GRIDDIM/' ${_name}/dune/grid/test/CMakeLists.txt
  # sed -i 's/^      set(_test/      #set(_test/' ${_name}/dune/grid/test/CMakeLists.txt
  sed -i 's/^      add_executable/      #add_executable/' ${_name}/dune/grid/test/CMakeLists.txt
  sed -i 's/^      target_link_libraries/      #target_link_libraries/' ${_name}/dune/grid/test/CMakeLists.txt
  sed -i 's/^      add_dune_alberta_flags/      #add_dune_alberta_flags/' ${_name}/dune/grid/test/CMakeLists.txt
  sed -i 's/^      target_compile_definitions/      #target_compile_definitions/' ${_name}/dune/grid/test/CMakeLists.txt
  sed -i 's/^        GRIDDIM/        #GRIDDIM/' ${_name}/dune/grid/test/CMakeLists.txt
  sed -i 's/^        DUNE_GRID_EXAMPLE_GRIDS_PATH/        #DUNE_GRID_EXAMPLE_GRIDS_PATH/' ${_name}/dune/grid/test/CMakeLists.txt
  sed -i 's/^      dune_add_test/      #dune_add_test/' ${_name}/dune/grid/test/CMakeLists.txt
  # Comment 3
  sed -i 's/^add_subdirectory(amiramesh)/#add_subdirectory(amiramesh)/' ${_name}/doc/grids/CMakeLists.txt
  # Comment 4
  # No estoy seguro de aplicar el parche
  # sed -i 's/^/#/' ${_name}/CMakeLists.txt
  # Comment 5
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L76
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
  export DUNE_PY_DIR="${BUILDDIR}/${pkgbase}"
}
```

```
-DCMAKE_DISABLE_FIND_PACKAGE_Alberta=TRUE
```
