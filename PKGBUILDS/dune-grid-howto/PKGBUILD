# Maintainer: anon at sansorgan.es
pkgname=(dune-grid-howto)
_tarver=2.7.1
_tar="${_tarver}/${pkgname}-${_tarver}.tar.gz"
pkgver=${_tarver//-/_}
pkgrel=1
#epoch=
pkgdesc="Howto for the Grid Interface"
arch=('x86_64')
url="https://dune-project.org/modules/dune-grid-howto"
license=('GPL2')
groups=('dune-core')
depends=('dune-grid>=2.7.1')
makedepends=('vc' 'texlive-latexextra' 'texlive-pictures' 'imagemagick' 'doxygen' 'graphviz' 'inkscape' 'superlu' 'arpackpp' 'suitesparse' 'dune-python' 'dune-istl' 'dune-alugrid')
# 'biber' 'parmetis' 'scotch' 'zoltan' #sionlib
#checkdepends=()
optdepends=('vc: C++ Vectorization library'
  'texlive-latexextra: Type setting system'
  'imagemagick: image viewing/manipulation program'
  'doxygen: Generate the class documentation from C++ sources'
  'graphviz: Graph visualization software'
  'inkscape: converts SVG images'
  'superlu: Set of subroutines to solve a sparse linear system'
  'arpackpp: C++ interface to ARPACK'
  'suitesparse: A collection of sparse matrix libraries'
  'dune-python: Python bindings for the Dune'
  'dune-istl: Iterative Solver Template Library'
  'dune-alugrid: An adaptive, loadbalancing, unstructured implementation of the DUNE grid interface supporting either simplices or cubes')
#provides=()
#conflicts=()
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=(https://dune-project.org/download/${_tar}{,.asc})
#noextract=()
sha512sums=('f988f76870e6b79b3ed935c9394da42b02379a017b631c560ef57ee2704bad047c0409b02fc62f5c965e1e9fff1b34bbcbcb9b8d0b3739c1ddea0dc48fbbeac6'
  'SKIP')
validpgpkeys=('2AA99AA4E2D6214E6EA01C9A4AF42916F6E5B1CF') # Christoph Grüninger <gruenich@dune-project.org>

prepare() {
  sed -i 's/scrpage2/scrlayer-scrpage/' ${pkgname}-${pkgver}/doc/grid-howto.tex
  sed -i '10 a BUILD_ON_INSTALL' ${pkgname}-${pkgver}/doc/CMakeLists.txt
}

build() {
  cmake \
    -S ${pkgname}-${pkgver} \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_SKIP_RPATH=ON \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_FLAGS=" \
    -std=c++17 \
    -DDUNE_ENABLE_PYTHONBINDINGS=ON \
    -DDUNE_PYTHON_INSTALL_LOCATION='none'" \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -Wno-dev
  cmake --build build-cmake --target all
}

# check() {
#   cmake --build build-cmake --target build_tests
#   cd "build-cmake"
#   LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${srcdir}/build-cmake/lib/" dune-ctest
# }

package() {
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  # dunecontrol --only=${pkgname} make install DESTDIR="${pkgdir}"
  install -Dm644 ${pkgname}-${pkgver}/COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  # install -dm755 "${pkgdir}/usr/share/doc/${pkgname}/doxygen"
  find "${pkgdir}" -type d -empty -delete
}
