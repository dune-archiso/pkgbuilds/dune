When the package `dune-dune-grid-howto-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `bin`
- [x] `include`
- [x] `lib`
- [ ] `share`
  - [x] `bash-completion`
  - [ ] `doc`
  - [x] `dune`
  - [x] `dune-common`
  - [x] `licenses`
  - [ ] `man`

```console
-DCMAKE_C_FLAGS_RELEASE='-fPIC'
export CPPFLAGS="-I/usr/include/tirpc ${CPPFLAGS}"
```

### Remarks

We remove `alberta`, `dune-uggrid` as makedepends because we move to depends array in `dune-grid`.

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxygen_install
headercheck
install_python
test_python
adaptivefinitevolume
adaptiveintegration
finiteelements
finitevolume
gettingstarted
integration
othergrids
parfinitevolume
traversal
visualization
adaptivefinitevolume.o
adaptivefinitevolume.i
adaptivefinitevolume.s
adaptiveintegration.o
adaptiveintegration.i
adaptiveintegration.s
finiteelements.o
finiteelements.i
finiteelements.s
finitevolume.o
finitevolume.i
finitevolume.s
gettingstarted.o
gettingstarted.i
gettingstarted.s
integration.o
integration.i
integration.s
othergrids.o
othergrids.i
othergrids.s
parfinitevolume.o
parfinitevolume.i
parfinitevolume.s
traversal.o
traversal.i
traversal.s
visualization.o
visualization.i
visualization.s
```
