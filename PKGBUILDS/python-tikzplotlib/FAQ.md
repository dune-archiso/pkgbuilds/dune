```console
============================= test session starts ==============================
platform linux -- Python 3.9.6, pytest-6.2.4, py-1.10.0, pluggy-0.13.1
rootdir: /tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12
plugins: codeblocks-0.11.2
collected 112 items
README.md ....                                                           [  0%]
tests/test_annotate.py .                                                 [  1%]
tests/test_axvline.py .                                                  [  2%]
tests/test_barchart.py .                                                 [  3%]
tests/test_barchart_errorbars.py F                                       [  4%]
tests/test_barchart_legend.py .                                          [  5%]
tests/test_basic_sin.py .                                                [  6%]
tests/test_boxplot.py .                                                  [  7%]
tests/test_cleanfigure.py .............................                  [ 33%]
tests/test_colorbars.py F                                                [ 33%]
tests/test_context.py .                                                  [ 34%]
tests/test_contourf.py .                                                 [ 35%]
tests/test_custom_collection.py .                                        [ 36%]
tests/test_datetime.py .                                                 [ 37%]
tests/test_datetime_paths.py .                                           [ 38%]
tests/test_deterministic_output.py .                                     [ 39%]
tests/test_dual_axis.py .                                                [ 40%]
tests/test_errorband.py .                                                [ 41%]
tests/test_errorbar.py .                                                 [ 41%]
tests/test_escape_chars.py .                                             [ 42%]
tests/test_externalize_tables.py .                                       [ 43%]
tests/test_fancy_colorbar.py .                                           [ 44%]
tests/test_fancybox.py .                                                 [ 45%]
tests/test_fillstyle.py F                                                [ 46%]
tests/test_hatch.py .                                                    [ 47%]
tests/test_heat.py .                                                     [ 48%]
tests/test_histogram.py .                                                [ 49%]
tests/test_horizontal_alignment.py .                                     [ 50%]
tests/test_image_plot_lower.py .                                         [ 50%]
tests/test_image_plot_upper.py s                                         [ 51%]
tests/test_legend_best_location.py s                                     [ 52%]
tests/test_legend_columns.py .                                           [ 53%]
tests/test_legend_labels.py .                                            [ 54%]
tests/test_legend_line_scatter.py .                                      [ 55%]
tests/test_legends.py .                                                  [ 56%]
tests/test_legends2.py .                                                 [ 57%]
tests/test_line_collection.py .                                          [ 58%]
tests/test_line_color_marker.py .                                        [ 58%]
tests/test_line_dashes.py .                                              [ 59%]
tests/test_line_set_data.py .                                            [ 60%]
tests/test_loglogplot.py .                                               [ 61%]
tests/test_logplot.py .                                                  [ 62%]
tests/test_logplot_base.py .                                             [ 63%]
tests/test_marker.py .                                                   [ 64%]
tests/test_noise.py .                                                    [ 65%]
tests/test_noise2.py .                                                   [ 66%]
tests/test_pandas_dataframe.py .                                         [ 66%]
tests/test_patch_styles.py .                                             [ 67%]
tests/test_patches.py .                                                  [ 68%]
tests/test_quadmesh.py .                                                 [ 69%]
tests/test_rotated_labels.py ...................                         [ 86%]
tests/test_scatter.py .                                                  [ 87%]
tests/test_scatter_colormap.py .                                         [ 88%]
tests/test_scatter_different_colors.py .                                 [ 89%]
tests/test_scatter_different_sizes.py .                                  [ 90%]
tests/test_sharex_and_y.py .                                             [ 91%]
tests/test_steps.py .                                                    [ 91%]
tests/test_subplot4x4.py .                                               [ 92%]
tests/test_subplots.py .                                                 [ 93%]
tests/test_subplots_with_colorbars.py s                                  [ 94%]
tests/test_text_overlay.py .                                             [ 95%]
tests/test_tick_positions.py .                                           [ 96%]
tests/test_viridis.py .
=================================== FAILURES ===================================
_____________________________________ test _____________________________________
    def test():
        from .helpers import assert_equality
    
>       assert_equality(plot, "test_barchart_errorbars_reference.tex")
tests/test_barchart_errorbars.py:36: 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
plot = <function plot at 0x7f7e9c374ca0>
filename = 'test_barchart_errorbars_reference.tex', assert_compilation = False
flavor = 'latex', extra_get_tikz_code_args = {}
code = '\\begin{tikzpicture}\n\n\\begin{axis}[\ntick align=outside,\ntick pos=left,\nx grid style={white!69.019608!black},\nx...=8, mark options={solid}, only marks]\ntable {%\n0.25 5.1\n1.25 3.2\n2.25 1.1\n};\n\\end{axis}\n\n\\end{tikzpicture}\n'
this_dir = PosixPath('/tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests')
f = <_io.TextIOWrapper name='/tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests/test_barchart_errorbars_reference.tex' mode='r' encoding='utf-8'>
reference = '\\begin{tikzpicture}\n\n\\begin{axis}[\ntick align=outside,\ntick pos=left,\nx grid style={white!69.019608!black},\nx...=8, mark options={solid}, only marks]\ntable {%\n0.25 5.1\n1.25 3.2\n2.25 1.1\n};\n\\end{axis}\n\n\\end{tikzpicture}\n'
    def assert_equality(
        plot, filename, assert_compilation=False, flavor="latex", **extra_get_tikz_code_args
    ):
        plot()
        code = tikzplotlib.get_tikz_code(
            include_disclaimer=False,
            float_format=".8g",
            flavor=flavor,
            **extra_get_tikz_code_args,
        )
        plt.close()
    
        this_dir = pathlib.Path(__file__).resolve().parent
        with open(this_dir / filename, encoding="utf-8") as f:
            reference = f.read()
>       assert reference == code, filename + "\n" + _unidiff_output(reference, code)
E       AssertionError: test_barchart_errorbars_reference.tex
E       --- 
E       +++ 
E       @@ -55,37 +55,37 @@
E        (axis cs:2.25,0.9)
E        --(axis cs:2.25,1.1);
E        
E       -\addplot [semithick, black, mark=-, mark size=8, mark options={solid}, only marks]
E       +\addplot [line width=2pt, black, mark=-, mark size=8, mark options={solid}, only marks]
E        table {%
E        -0.25 0.9
E        0.75 1.8
E        1.75 2.5
E        };
E       -\addplot [semithick, black, mark=-, mark size=8, mark options={solid}, only marks]
E       +\addplot [line width=2pt, black, mark=-, mark size=8, mark options={solid}, only marks]
E        table {%
E        -0.25 1.1
E        0.75 2.2
E        1.75 3.5
E        };
E       -\addplot [semithick, black, mark=-, mark size=8, mark options={solid}, only marks]
E       +\addplot [line width=2pt, black, mark=-, mark size=8, mark options={solid}, only marks]
E        table {%
E        0 2.6
E        1 1.8
E        2 3.5
E        };
E       -\addplot [semithick, black, mark=-, mark size=8, mark options={solid}, only marks]
E       +\addplot [line width=2pt, black, mark=-, mark size=8, mark options={solid}, only marks]
E        table {%
E        0 3.4
E        1 2.2
E        2 4.5
E        };
E       -\addplot [semithick, black, mark=-, mark size=8, mark options={solid}, only marks]
E       +\addplot [line width=2pt, black, mark=-, mark size=8, mark options={solid}, only marks]
E        table {%
E        0.25 4.9
E        1.25 2.8
E        2.25 0.9
E        };
E       -\addplot [semithick, black, mark=-, mark size=8, mark options={solid}, only marks]
E       +\addplot [line width=2pt, black, mark=-, mark size=8, mark options={solid}, only marks]
E        table {%
E        0.25 5.1
E        1.25 3.2
tests/helpers.py:48: AssertionError
_____________________________________ test _____________________________________
    def test():
        from .helpers import assert_equality
    
>       assert_equality(plot, "test_colorbars_reference.tex", assert_compilation=False)
tests/test_colorbars.py:78: 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
plot = <function plot at 0x7f7e9c1acf70>
filename = 'test_colorbars_reference.tex', assert_compilation = False
flavor = 'latex', extra_get_tikz_code_args = {}
code = '\\begin{tikzpicture}\n\n\\begin{groupplot}[group style={group size=1 by 3}]\n\\nextgroupplot[\ntick align=outside,\nt...graphics cmd=\\pgfimage,xmin=-1.5, xmax=1.5, ymin=-1, ymax=1] {tmp-002.png};\n\\end{groupplot}\n\n\\end{tikzpicture}\n'
this_dir = PosixPath('/tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests')
f = <_io.TextIOWrapper name='/tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests/test_colorbars_reference.tex' mode='r' encoding='utf-8'>
reference = '\\begin{tikzpicture}\n\n\\begin{groupplot}[group style={group size=1 by 3}]\n\\nextgroupplot[\ntick align=outside,\nt...s cs:1.5,0)\n--(axis cs:1,1)\n--(axis cs:-1,1)\n--(axis cs:-1.5,0)\n--cycle;\n\\end{groupplot}\n\n\\end{tikzpicture}\n'
    def assert_equality(
        plot, filename, assert_compilation=False, flavor="latex", **extra_get_tikz_code_args
    ):
        plot()
        code = tikzplotlib.get_tikz_code(
            include_disclaimer=False,
            float_format=".8g",
            flavor=flavor,
            **extra_get_tikz_code_args,
        )
        plt.close()
    
        this_dir = pathlib.Path(__file__).resolve().parent
        with open(this_dir / filename, encoding="utf-8") as f:
            reference = f.read()
>       assert reference == code, filename + "\n" + _unidiff_output(reference, code)
E       AssertionError: test_colorbars_reference.tex
E       --- 
E       +++ 
E       @@ -19,18 +19,9 @@
E        --(axis cs:9.9414062,10)
E        --(axis cs:-4.9414062,10)
E        --(axis cs:-5,10)
E       +--(axis cs:-5,10)
E        --cycle;
E        \addplot graphics [includegraphics cmd=\pgfimage,xmin=-5, xmax=10, ymin=-5, ymax=10] {tmp-000.png};
E       -\path [draw=black, line width=0.32pt]
E       -(axis cs:-5,-5)
E       ---(axis cs:-4.9414062,-5)
E       ---(axis cs:9.9414062,-5)
E       ---(axis cs:10,-5)
E       ---(axis cs:10,10)
E       ---(axis cs:9.9414062,10)
E       ---(axis cs:-4.9414062,10)
E       ---(axis cs:-5,10)
E       ---cycle;
E        
E        \nextgroupplot[
E        tick align=outside,
E       @@ -54,16 +45,6 @@
E        --(axis cs:0.65,4.5)
E        --cycle;
E        \addplot graphics [includegraphics cmd=\pgfimage,xmin=0.65, xmax=8.35, ymin=1, ymax=8] {tmp-001.png};
E       -\path [draw=black, line width=0.32pt]
E       -(axis cs:0.65,4.5)
E       ---(axis cs:1,1)
E       ---(axis cs:8,1)
E       ---(axis cs:8.35,4.5)
E       ---(axis cs:8.35,4.5)
E       ---(axis cs:8,8)
E       ---(axis cs:1,8)
E       ---(axis cs:0.65,4.5)
E       ---cycle;
E        
E        \nextgroupplot[
E        tick align=outside,
E       @@ -72,6 +53,8 @@
E        xlabel={Custom extension lengths, some other units},
E        xmin=-1.5, xmax=1.5,
E        xtick style={color=black},
E       +xtick={-1,-0.5,0,0.5,1},
E       +xticklabels={\ensuremath{-}1.0,\ensuremath{-}0.5,0.0,0.5,1.0},
E        ymin=-1, ymax=1
E        ]
E        \path [draw=white, fill=white, line width=0.004pt]
E       @@ -85,16 +68,6 @@
E        --(axis cs:-1.5,0)
E        --cycle;
E        \addplot graphics [includegraphics cmd=\pgfimage,xmin=-1.5, xmax=1.5, ymin=-1, ymax=1] {tmp-002.png};
E       -\path [draw=black, line width=0.32pt]
E       -(axis cs:-1.5,0)
E       ---(axis cs:-1,-1)
E       ---(axis cs:1,-1)
E       ---(axis cs:1.5,0)
E       ---(axis cs:1.5,0)
E       ---(axis cs:1,1)
E       ---(axis cs:-1,1)
E       ---(axis cs:-1.5,0)
E       ---cycle;
E        \end{groupplot}
E        
E        \end{tikzpicture}
tests/helpers.py:48: AssertionError
_____________________________________ test _____________________________________
    def test():
        from .helpers import assert_equality
    
>       assert_equality(plot, "test_fillstyle_reference.tex")
tests/test_fillstyle.py:19: 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
plot = <function plot at 0x7f7e9c285790>
filename = 'test_fillstyle_reference.tex', assert_compilation = False
flavor = 'latex', extra_get_tikz_code_args = {}
code = '\\begin{tikzpicture}\n\n\\definecolor{color0}{rgb}{0.12156863,0.46666667,0.70588235}\n\n\\begin{axis}[\ntick align=ou... 3\n0.44444444 4\n0.55555556 5\n0.66666667 6\n0.77777778 7\n0.88888889 8\n1 9\n};\n\\end{axis}\n\n\\end{tikzpicture}\n'
this_dir = PosixPath('/tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests')
f = <_io.TextIOWrapper name='/tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests/test_fillstyle_reference.tex' mode='r' encoding='utf-8'>
reference = '\\begin{tikzpicture}\n\n\\definecolor{color0}{rgb}{0.12156863,0.46666667,0.70588235}\n\n\\begin{axis}[\ntick align=ou... 3\n0.44444444 4\n0.55555556 5\n0.66666667 6\n0.77777778 7\n0.88888889 8\n1 9\n};\n\\end{axis}\n\n\\end{tikzpicture}\n'
    def assert_equality(
        plot, filename, assert_compilation=False, flavor="latex", **extra_get_tikz_code_args
    ):
        plot()
        code = tikzplotlib.get_tikz_code(
            include_disclaimer=False,
            float_format=".8g",
            flavor=flavor,
            **extra_get_tikz_code_args,
        )
        plt.close()
    
        this_dir = pathlib.Path(__file__).resolve().parent
        with open(this_dir / filename, encoding="utf-8") as f:
            reference = f.read()
>       assert reference == code, filename + "\n" + _unidiff_output(reference, code)
E       AssertionError: test_fillstyle_reference.tex
E       --- 
E       +++ 
E       @@ -9,10 +9,12 @@
E        xmin=-0.05, xmax=1.05,
E        xtick style={color=black},
E        xtick={-0.2,0,0.2,0.4,0.6,0.8,1,1.2},
E       -xticklabels={−0.2,0.0,0.2,0.4,0.6,0.8,1.0,1.2},
E       +xticklabels={\ensuremath{-}0.2,0.0,0.2,0.4,0.6,0.8,1.0,1.2},
E        y grid style={white!69.019608!black},
E        ymin=-0.45, ymax=9.45,
E       -ytick style={color=black}
E       +ytick style={color=black},
E       +ytick={-2,0,2,4,6,8,10},
E       +yticklabels={\ensuremath{-}2,0,2,4,6,8,10}
E        ]
E        \addplot [semithick, color0, mark=o, mark size=3, mark options={solid,fill opacity=0}]
E        table {%
tests/helpers.py:48: AssertionError
=============================== warnings summary ===============================
tests/test_cleanfigure.py::Test_plottypes::test_surface3D
  /tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests/test_cleanfigure.py:177: MatplotlibDeprecationWarning: Calling gca() with keyword arguments was deprecated in Matplotlib 3.4. Starting two minor releases later, gca() will take no keyword arguments. The gca() function should only be used to get the current axes, or if no axes exist, create new axes with default keyword arguments. To create a new axes with non-default arguments, use plt.axes() or plt.subplot().
    ax = fig.gca(projection="3d")
tests/test_cleanfigure.py::Test_plottypes::test_trisurface3D
  /tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests/test_cleanfigure.py:220: MatplotlibDeprecationWarning: Calling gca() with keyword arguments was deprecated in Matplotlib 3.4. Starting two minor releases later, gca() will take no keyword arguments. The gca() function should only be used to get the current axes, or if no axes exist, create new axes with default keyword arguments. To create a new axes with non-default arguments, use plt.axes() or plt.subplot().
    ax = fig.gca(projection="3d")
tests/test_cleanfigure.py::Test_plottypes::test_polygon3D
  /tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests/test_cleanfigure.py:247: MatplotlibDeprecationWarning: Calling gca() with keyword arguments was deprecated in Matplotlib 3.4. Starting two minor releases later, gca() will take no keyword arguments. The gca() function should only be used to get the current axes, or if no axes exist, create new axes with default keyword arguments. To create a new axes with non-default arguments, use plt.axes() or plt.subplot().
    ax = fig.gca(projection="3d")
tests/test_cleanfigure.py::Test_plottypes::test_quiver3D
  /tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests/test_cleanfigure.py:305: MatplotlibDeprecationWarning: Calling gca() with keyword arguments was deprecated in Matplotlib 3.4. Starting two minor releases later, gca() will take no keyword arguments. The gca() function should only be used to get the current axes, or if no axes exist, create new axes with default keyword arguments. To create a new axes with non-default arguments, use plt.axes() or plt.subplot().
    ax = fig.gca(projection="3d")
tests/test_cleanfigure.py::Test_plottypes::test_2D_in_3D
  /tmp/makepkg/python-tikzplotlib/src/tikzplotlib-0.9.12/tests/test_cleanfigure.py:332: MatplotlibDeprecationWarning: Calling gca() with keyword arguments was deprecated in Matplotlib 3.4. Starting two minor releases later, gca() will take no keyword arguments. The gca() function should only be used to get the current axes, or if no axes exist, create new axes with default keyword arguments. To create a new axes with non-default arguments, use plt.axes() or plt.subplot().
    ax = fig.gca(projection="3d")
-- Docs: https://docs.pytest.org/en/stable/warnings.html
=========================== short test summary info ============================
FAILED tests/test_barchart_errorbars.py::test - AssertionError: test_barchart...
FAILED tests/test_colorbars.py::test - AssertionError: test_colorbars_referen...
FAILED tests/test_fillstyle.py::test - AssertionError: test_fillstyle_referen...
============ 3 failed, 106 passed, 3 skipped, 5 warnings in 13.84s =============
==> ERROR: A failure occurred in check().
```
