```console
# export CPPFLAGS="-I/usr/include/tirpc ${CPPFLAGS}"
# export CFLAGS="-fPIC ${CFLAGS}"
# export CXXFLAGS="-fPIC ${CFLAGS}"
# export LDFLAGS="-ldl -lm -ltirpc"
```

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-multidomaingrid
doxygen_install
headercheck
install_python
test_python
iterateallinterfaces
multidomain-leveliterator-bug
test-wrapper-1d-multidomaingrid
test-wrapper-1d-multidomaingrid-deprecated-api
test-wrapper-1d-subdomaingrid
test-wrapper-1d-subdomaingrid-deprecated-api
test-wrapper-2d-multidomaingrid
test-wrapper-2d-multidomaingrid-deprecated-api
test-wrapper-2d-subdomaingrid
test-wrapper-2d-subdomaingrid-deprecated-api
test-wrapper-3d-multidomaingrid
test-wrapper-3d-multidomaingrid-deprecated-api
test-wrapper-3d-subdomaingrid
test-wrapper-3d-subdomaingrid-deprecated-api
testadaptation
testintersectionconversion
testintersectiongeometrytypes
testlargedomainnumbers
testparallel
testpartitioning
```

```console
   Site: runner-fa6cab46-project-25568825-concurrent-0
   Build name: Linux-g++
Create new tag: 20210721-0105 - Experimental
Test project /tmp/makepkg/dune-multidomaingrid/src/build-cmake
      Start  1: test-wrapper-1d-multidomaingrid
 1/20 Test  #1: test-wrapper-1d-multidomaingrid ..................   Passed    0.15 sec
      Start  2: test-wrapper-1d-multidomaingrid-deprecated-api
 2/20 Test  #2: test-wrapper-1d-multidomaingrid-deprecated-api ...   Passed    0.13 sec
      Start  3: test-wrapper-1d-subdomaingrid
 3/20 Test  #3: test-wrapper-1d-subdomaingrid ....................   Passed    0.14 sec
      Start  4: test-wrapper-1d-subdomaingrid-deprecated-api
 4/20 Test  #4: test-wrapper-1d-subdomaingrid-deprecated-api .....   Passed    0.14 sec
      Start  5: test-wrapper-2d-multidomaingrid
 5/20 Test  #5: test-wrapper-2d-multidomaingrid ..................   Passed    0.60 sec
      Start  6: test-wrapper-2d-multidomaingrid-deprecated-api
 6/20 Test  #6: test-wrapper-2d-multidomaingrid-deprecated-api ...   Passed    0.61 sec
      Start  7: test-wrapper-2d-subdomaingrid
 7/20 Test  #7: test-wrapper-2d-subdomaingrid ....................   Passed    1.01 sec
      Start  8: test-wrapper-2d-subdomaingrid-deprecated-api
 8/20 Test  #8: test-wrapper-2d-subdomaingrid-deprecated-api .....   Passed    1.00 sec
      Start  9: test-wrapper-3d-multidomaingrid
 9/20 Test  #9: test-wrapper-3d-multidomaingrid ..................   Passed    1.78 sec
      Start 10: test-wrapper-3d-multidomaingrid-deprecated-api
10/20 Test #10: test-wrapper-3d-multidomaingrid-deprecated-api ...   Passed    1.77 sec
      Start 11: test-wrapper-3d-subdomaingrid
11/20 Test #11: test-wrapper-3d-subdomaingrid ....................   Passed    2.96 sec
      Start 12: test-wrapper-3d-subdomaingrid-deprecated-api
12/20 Test #12: test-wrapper-3d-subdomaingrid-deprecated-api .....   Passed    2.95 sec
      Start 13: iterateallinterfaces
13/20 Test #13: iterateallinterfaces .............................   Passed    0.13 sec
      Start 14: multidomain-leveliterator-bug
14/20 Test #14: multidomain-leveliterator-bug ....................   Passed    0.12 sec
      Start 15: testadaptation
15/20 Test #15: testadaptation ...................................Subprocess aborted***Exception:   0.23 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
[runner-fa6cab46-project-25568825-concurrent-0:19565] *** Process received signal ***
[runner-fa6cab46-project-25568825-concurrent-0:19565] Signal: Aborted (6)
[runner-fa6cab46-project-25568825-concurrent-0:19565] Signal code:  (-6)
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 0] /usr/lib/libc.so.6(+0x3cda0)[0x7fa4bc1b7da0]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 1] /usr/lib/libc.so.6(gsignal+0x142)[0x7fa4bc1b7d22]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 2] /usr/lib/libc.so.6(abort+0x116)[0x7fa4bc1a1862]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 3] /tmp/makepkg/dune-multidomaingrid/src/build-cmake/test/testadaptation(+0x338ea)[0x5634825d88ea]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 4] /tmp/makepkg/dune-multidomaingrid/src/build-cmake/test/testadaptation(_ZNK4Dune6mdgrid9subdomain13SubDomainGridINS0_15MultiDomainGridINS_6UGGridILi2EEENS0_19FewSubDomainsTraitsILi2ELm4ENS0_9AllCodimsEEEEEE13levelIndexSetEi+0xbf)[0x5634825e7e5f]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 5] /tmp/makepkg/dune-multidomaingrid/src/build-cmake/test/testadaptation(_ZNK4Dune6mdgrid9subdomain13SubDomainGridINS0_15MultiDomainGridINS_6UGGridILi2EEENS0_19FewSubDomainsTraitsILi2ELm4ENS0_9AllCodimsEEEEEE29subDomainIntersectionIteratorENS0_31LevelSubDomainInterfaceIteratorIKS9_EE+0x606)[0x5634825e8646]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 6] /tmp/makepkg/dune-multidomaingrid/src/build-cmake/test/testadaptation(_Z6vtkOutIN4Dune8GridViewINS0_6mdgrid19LevelGridViewTraitsIKNS2_15MultiDomainGridINS0_6UGGridILi2EEENS2_19FewSubDomainsTraitsILi2ELm4ENS2_9AllCodimsEEEEEEEEENS2_31LevelSubDomainInterfaceIteratorISB_EEEvT_NSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEET0_SN_+0xd7e)[0x5634825ff43e]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 7] /tmp/makepkg/dune-multidomaingrid/src/build-cmake/test/testadaptation(_Z11printStatusIN4Dune6mdgrid15MultiDomainGridINS0_6UGGridILi2EEENS1_19FewSubDomainsTraitsILi2ELm4ENS1_9AllCodimsEEEEEEvRT_NSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi+0x358)[0x563482600208]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 8] /tmp/makepkg/dune-multidomaingrid/src/build-cmake/test/testadaptation(main+0x773)[0x5634825d2bb3]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [ 9] /usr/lib/libc.so.6(__libc_start_main+0xd5)[0x7fa4bc1a2b25]
[runner-fa6cab46-project-25568825-concurrent-0:19565] [10] /tmp/makepkg/dune-multidomaingrid/src/build-cmake/test/testadaptation(_start+0x2e)[0x5634825d327e]
[runner-fa6cab46-project-25568825-concurrent-0:19565] *** End of error message ***
      Start 16: testintersectionconversion
16/20 Test #16: testintersectionconversion .......................   Passed    0.12 sec
      Start 17: testintersectiongeometrytypes
17/20 Test #17: testintersectiongeometrytypes ....................   Passed    0.13 sec
      Start 18: testlargedomainnumbers
18/20 Test #18: testlargedomainnumbers ...........................   Passed    0.46 sec
      Start 19: testparallel-mpi-2
19/20 Test #19: testparallel-mpi-2 ...............................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-multidomaingrid/src/build-cmake/test/testparallel
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start 20: testpartitioning
20/20 Test #20: testpartitioning .................................   Passed    0.18 sec
90% tests passed, 2 tests failed out of 20
Total Test time (real) =  14.68 sec
The following tests FAILED:
	 15 - testadaptation (Subprocess aborted)
	 19 - testparallel-mpi-2 (Failed)
Errors while running CTest
======================================================================
Name:      testadaptation
FullName:  ./test/testadaptation
Status:    FAILED
======================================================================
Name:      testparallel-mpi-2
FullName:  ./test/testparallel-mpi-2
Status:    FAILED
JUnit report for CTest results written to /builds/dune-archiso/repository/dune-extensions/junit/dune-extensions-cmake.xml
==> ERROR: A failure occurred in check().
```
