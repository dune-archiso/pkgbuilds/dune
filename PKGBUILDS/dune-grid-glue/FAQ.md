```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-grid-glue
doxygen_install
headercheck
install_python
test_python
callmergertwicetest
computecyclicordertest
contactmerge
disconnectedtest
dunegridglue
mixeddimcouplingtest
mixeddimoverlappingtest
mixeddimscalingtest
nonoverlappingcouplingtest
overlappingcouplingtest
projectiontest
ringcommtest
```

Python bindings? Yes/No

```console
   Build name: Linux-g++
Create new tag: 20210721-0017 - Experimental
Test project /tmp/makepkg/dune-grid-glue/src/build-cmake
      Start  1: callmergertwicetest
 1/12 Test  #1: callmergertwicetest ................   Passed    0.01 sec
      Start  2: ringcommtest
 2/12 Test  #2: ringcommtest .......................   Passed    0.13 sec
      Start  3: ringcommtest-mpi-2
 3/12 Test  #3: ringcommtest-mpi-2 .................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-grid-glue/src/build-cmake/dune/grid-glue/test/ringcommtest
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start  4: computecyclicordertest
 4/12 Test  #4: computecyclicordertest .............   Passed    0.00 sec
      Start  5: disconnectedtest
 5/12 Test  #5: disconnectedtest ...................   Passed    0.13 sec
      Start  6: mixeddimcouplingtest
 6/12 Test  #6: mixeddimcouplingtest ...............   Passed    0.13 sec
      Start  7: mixeddimoverlappingtest
 7/12 Test  #7: mixeddimoverlappingtest ............   Passed    0.13 sec
      Start  8: mixeddimscalingtest
 8/12 Test  #8: mixeddimscalingtest ................   Passed    0.14 sec
      Start  9: nonoverlappingcouplingtest
 9/12 Test  #9: nonoverlappingcouplingtest .........   Passed    0.13 sec
      Start 10: nonoverlappingcouplingtest-mpi-2
10/12 Test #10: nonoverlappingcouplingtest-mpi-2 ...***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-grid-glue/src/build-cmake/dune/grid-glue/test/nonoverlappingcouplingtest
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start 11: overlappingcouplingtest
11/12 Test #11: overlappingcouplingtest ............   Passed    0.18 sec
      Start 12: projectiontest
12/12 Test #12: projectiontest .....................   Passed    0.00 sec
83% tests passed, 2 tests failed out of 12
Total Test time (real) =   1.07 sec
The following tests FAILED:
	  3 - ringcommtest-mpi-2 (Failed)
	 10 - nonoverlappingcouplingtest-mpi-2 (Failed)
Errors while running CTest
======================================================================
Name:      ringcommtest-mpi-2
FullName:  ./dune/grid-glue/test/ringcommtest-mpi-2
Status:    FAILED
======================================================================
Name:      nonoverlappingcouplingtest-mpi-2
FullName:  ./dune/grid-glue/test/nonoverlappingcouplingtest-mpi-2
Status:    FAILED
JUnit report for CTest results written to /builds/dune-archiso/repository/dune-extensions/junit/dune-extensions-cmake.xml
==> ERROR: A failure occurred in check().
```
