## [`dune-grid-glue`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-grid-glue/PKGBUILD)

#### debian

- [](https://tracker.debian.org/pkg/dune-grid-glue)
- [](https://salsa.debian.org/pjaap/dune-grid-glue)
- [](https://salsa.debian.org/lisajuliafog/dune-grid-glue)

```console
-- The following OPTIONAL packages have been found:
 * dune-uggrid
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * BLAS, fast linear algebra routines
 * Threads, Multi-threading library
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
 * ParMETIS
 * Alberta
 * Psurface
-- The following REQUIRED packages have been found:
 * dune-common
 * dune-geometry
 * dune-grid
-- The following OPTIONAL packages have not been found:
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * AmiraMesh
```

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-grid-glue.svg)](https://repology.org/project/dune-grid-glue/versions)
