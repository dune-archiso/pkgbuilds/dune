```
   Site: runner-fa6cab46-project-24598907-concurrent-0
   Build name: Linux-g++
Create new tag: 20210721-0413 - Experimental
Test project /tmp/makepkg/dune-pdelab/src/build-cmake
        Start   1: recipe-geometry-grid
  1/118 Test   #1: recipe-geometry-grid ..................................   Passed    1.44 sec
        Start   2: recipe-integrating-grid-functions
  2/118 Test   #2: recipe-integrating-grid-functions .....................   Passed    0.19 sec
        Start   3: recipe-grid-function-operations
  3/118 Test   #3: recipe-grid-function-operations .......................   Passed    0.15 sec
        Start   4: recipe-blocking
  4/118 Test   #4: recipe-blocking .......................................   Passed    0.13 sec
        Start   5: recipe-linear-system-assembly
  5/118 Test   #5: recipe-linear-system-assembly .........................   Passed    0.14 sec
        Start   6: recipe-linear-system-solution-istl
  6/118 Test   #6: recipe-linear-system-solution-istl ....................   Passed    0.14 sec
        Start   7: recipe-linear-system-solution-pdelab
  7/118 Test   #7: recipe-linear-system-solution-pdelab ..................   Passed    0.14 sec
        Start   8: recipe-communication
  8/118 Test   #8: recipe-communication ..................................   Passed    0.14 sec
        Start   9: recipe-operator-splitting
  9/118 Test   #9: recipe-operator-splitting .............................   Passed    3.73 sec
        Start  10: recipe-operator-splitting-mpi-2
 10/118 Test  #10: recipe-operator-splitting-mpi-2 .......................***Failed    0.10 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-pdelab/src/build-cmake/doc/Recipes/recipe-operator-splitting
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
        Start  11: testcombinedoperator
 11/118 Test  #11: testcombinedoperator ..................................   Passed    0.14 sec
        Start  12: testconvectiondiffusiondg
 12/118 Test  #12: testconvectiondiffusiondg .............................   Passed    0.20 sec
        Start  13: testnewton
 13/118 Test  #13: testnewton ............................................   Passed    0.37 sec
        Start  14: testoldnewton
 14/118 Test  #14: testoldnewton .........................................   Passed    0.37 sec
        Start  15: testinstationary
 15/118 Test  #15: testinstationary ......................................   Passed    0.61 sec
        Start  16: testanalytic
 16/118 Test  #16: testanalytic ..........................................   Passed    0.13 sec
        Start  17: testbindtime
 17/118 Test  #17: testbindtime ..........................................   Passed    0.02 sec
        Start  18: testcomplexnumbers-istl
 18/118 Test  #18: testcomplexnumbers-istl ...............................   Passed    0.77 sec
        Start  19: testcomplexnumbers-istlbackend
 19/118 Test  #19: testcomplexnumbers-istlbackend ........................   Passed    0.41 sec
        Start  20: testconstraints
 20/118 Test  #20: testconstraints .......................................   Passed    0.27 sec
        Start  21: testdunefunctionsgfs
 21/118 Test  #21: testdunefunctionsgfs ..................................   Passed    0.55 sec
        Start  22: testdunefunctionsgfs-mpi-2
 22/118 Test  #22: testdunefunctionsgfs-mpi-2 ............................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testdunefunctionsgfs
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
        Start  23: testelectrodynamic
 23/118 Test  #23: testelectrodynamic ....................................   Passed    0.45 sec
        Start  24: testfunction
 24/118 Test  #24: testfunction ..........................................   Passed    0.24 sec
        Start  25: testgridfunctionspace
 25/118 Test  #25: testgridfunctionspace .................................   Passed    0.14 sec
        Start  26: testgridfunctionspace-fixedsize
 26/118 Test  #26: testgridfunctionspace-fixedsize .......................   Passed    0.12 sec
        Start  27: testpowergridfunctionspace-fixedsize
 27/118 Test  #27: testpowergridfunctionspace-fixedsize ..................   Passed    0.13 sec
        Start  28: testplasticitygfs
 28/118 Test  #28: testplasticitygfs .....................................   Passed    0.14 sec
        Start  29: testheat-instationary-periodic
 29/118 Test  #29: testheat-instationary-periodic ........................   Passed    0.20 sec
        Start  30: testelasticity
 30/118 Test  #30: testelasticity ........................................   Passed    0.15 sec
        Start  31: testgeneo-mpi-2
 31/118 Test  #31: testgeneo-mpi-2 .......................................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testgeneo
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
        Start  32: testdglegendre
 32/118 Test  #32: testdglegendre ........................................   Passed    0.14 sec
        Start  33: testfastdgassembler
 33/118 Test  #33: testfastdgassembler ...................................   Passed    0.18 sec
        Start  34: testinstationaryfastdgassembler
 34/118 Test  #34: testinstationaryfastdgassembler .......................   Passed    0.14 sec
        Start  35: testlocalfunctionspace
 35/118 Test  #35: testlocalfunctionspace ................................   Passed    0.14 sec
        Start  36: testlocalmatrix
 36/118 Test  #36: testlocalmatrix .......................................   Passed    0.02 sec
        Start  37: testlocaloperatorinterface
 37/118 Test  #37: testlocaloperatorinterface ............................   Passed    0.15 sec
        Start  38: testpk2dinterpolation
ERROR: Job failed: execution took longer than 1h0m0s seconds
```
```
test 52
        Start  52: testsimplebackend
52: Test command: /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend
52: Test timeout computed to be: 300
52: --------------------------------------------------------------------------
52: The library attempted to open the following supporting CUDA libraries,
52: but each of them failed.  CUDA-aware support is disabled.
52: libcuda.so.1: cannot open shared object file: No such file or directory
52: libcuda.dylib: cannot open shared object file: No such file or directory
52: /usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
52: /usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
52: If you are not interested in CUDA-aware support, then run with
52: --mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
52: in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
52: of libcuda.so.1 to get passed this issue.
52: --------------------------------------------------------------------------
52: two-norm: 2.65699
52: one-norm: 9.90212
52: inf-norm: 0.778801
52: dot-prod: 7.05959
52: === Dune::CGSolver
52:  Iter          Defect            Rate
52:     0          2.73981
52:     1          1.79099         0.653692
52:     2          1.41514         0.790144
52:     3          1.48199          1.04724
52:     4          1.50608          1.01625
52:     5          1.25481         0.833165
52:     6          0.74355         0.592557
52:     7         0.424549         0.570977
52:     8         0.296094         0.697432
52:     9         0.242893         0.820323
52:    10         0.156289         0.643448
52:    11        0.0963144         0.616259
52:    12        0.0565049         0.586671
52:    13        0.0397191         0.702933
52:    14        0.0296205         0.745749
52:    15         0.018839         0.636013
52:    16        0.0112697          0.59821
52:    17       0.00723643         0.642114
52:    18       0.00284205         0.392741
52:    19        0.0012433         0.437465
52:    20      0.000624801         0.502537
52:    21      0.000296408         0.474403
52:    22      0.000139021         0.469021
52:    23      4.42031e-05         0.317959
52:    24      1.79275e-05         0.405572
52:    25      9.95031e-06         0.555029
52:    26      4.57083e-06         0.459365
52:    27      3.01695e-06         0.660045
52:    28      8.85653e-07         0.293559
52:    29      3.72844e-07         0.420982
52:    30      9.98638e-08         0.267843
52:    31      4.16027e-08         0.416594
52:    32      9.88321e-09         0.237562
52:    33      3.92575e-09         0.397214
52:    34      1.20594e-09         0.307186
52:    35      4.16783e-10         0.345609
52:    36       9.0905e-11         0.218111
52: === rate=0.511579, T=0.00689961, TIT=0.000191656, IT=36
52: two-norm: 2.65699
52: one-norm: 9.90212
52: inf-norm: 0.778801
52: dot-prod: 7.05959
52: === Dune::CGSolver
52:  Iter          Defect            Rate
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] *** Process received signal ***
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] Signal: Aborted (6)
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] Signal code:  (-6)
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 0] /usr/lib64/libc.so.6(+0x3cda0)[0x7feeef844da0]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 1] /usr/lib64/libc.so.6(gsignal+0x142)[0x7feeef844d22]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 2] /usr/lib64/libc.so.6(abort+0x116)[0x7feeef82e862]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 3] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0x258da)[0x5592b19628da]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 4] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0x6009f)[0x5592b199d09f]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 5] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0x62840)[0x5592b199f840]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 6] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0xf047)[0x5592b194c047]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 7] /usr/lib64/libc.so.6(__libc_start_main+0xd5)[0x7feeef82fb25]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] [ 8] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0x104ce)[0x5592b194d4ce]
52: [runner-fa6cab46-project-24598907-concurrent-0:81139] *** End of error message ***
 52/118 Test  #52: testsimplebackend .....................................Subprocess aborted***Exception:   0.35 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
two-norm: 2.65699
one-norm: 9.90212
inf-norm: 0.778801
dot-prod: 7.05959
=== Dune::CGSolver
 Iter          Defect            Rate
    0          2.73981
    1          1.79099         0.653692
    2          1.41514         0.790144
    3          1.48199          1.04724
    4          1.50608          1.01625
    5          1.25481         0.833165
    6          0.74355         0.592557
    7         0.424549         0.570977
    8         0.296094         0.697432
    9         0.242893         0.820323
   10         0.156289         0.643448
   11        0.0963144         0.616259
   12        0.0565049         0.586671
   13        0.0397191         0.702933
   14        0.0296205         0.745749
   15         0.018839         0.636013
   16        0.0112697          0.59821
   17       0.00723643         0.642114
   18       0.00284205         0.392741
   19        0.0012433         0.437465
   20      0.000624801         0.502537
   21      0.000296408         0.474403
   22      0.000139021         0.469021
   23      4.42031e-05         0.317959
   24      1.79275e-05         0.405572
   25      9.95031e-06         0.555029
   26      4.57083e-06         0.459365
   27      3.01695e-06         0.660045
   28      8.85653e-07         0.293559
   29      3.72844e-07         0.420982
   30      9.98638e-08         0.267843
   31      4.16027e-08         0.416594
   32      9.88321e-09         0.237562
   33      3.92575e-09         0.397214
   34      1.20594e-09         0.307186
   35      4.16783e-10         0.345609
   36       9.0905e-11         0.218111
=== rate=0.511579, T=0.00689961, TIT=0.000191656, IT=36
two-norm: 2.65699
one-norm: 9.90212
inf-norm: 0.778801
dot-prod: 7.05959
=== Dune::CGSolver
 Iter          Defect            Rate
[runner-fa6cab46-project-24598907-concurrent-0:81139] *** Process received signal ***
[runner-fa6cab46-project-24598907-concurrent-0:81139] Signal: Aborted (6)
[runner-fa6cab46-project-24598907-concurrent-0:81139] Signal code:  (-6)
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 0] /usr/lib64/libc.so.6(+0x3cda0)[0x7feeef844da0]
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 1] /usr/lib64/libc.so.6(gsignal+0x142)[0x7feeef844d22]
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 2] /usr/lib64/libc.so.6(abort+0x116)[0x7feeef82e862]
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 3] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0x258da)[0x5592b19628da]
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 4] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0x6009f)[0x5592b199d09f]
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 5] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0x62840)[0x5592b199f840]
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 6] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0xf047)[0x5592b194c047]
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 7] /usr/lib64/libc.so.6(__libc_start_main+0xd5)[0x7feeef82fb25]
[runner-fa6cab46-project-24598907-concurrent-0:81139] [ 8] /tmp/makepkg/dune-pdelab/src/build-cmake/dune/pdelab/test/testsimplebackend(+0x104ce)[0x5592b194d4ce]
[runner-fa6cab46-project-24598907-concurrent-0:81139] *** End of error message ***
```

```
# rm "${pkgdir}/usr/share/dune/cmake/modules/CorrectWindowsPaths.cmake"
# rm "${pkgdir}/usr/share/dune/cmake/modules/FindPETSc.cmake"
# rm "${pkgdir}/usr/share/dune/cmake/modules/FindPackageMultipass.cmake"
# rm "${pkgdir}/usr/share/dune/cmake/modules/ResolveCompilerPaths.cmake"
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${srcdir}/build-cmake/lib/" # -E testsimplebackend
```

