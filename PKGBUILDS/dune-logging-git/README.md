## [`dune-logging-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-logging-git/PKGBUILD)

```console
-- The following OPTIONAL packages have been found:
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * BLAS, fast linear algebra routines
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library, <https://gmplib.org>
 * QuadMath, GCC Quad-Precision Math Library, <https://gcc.gnu.org/onlinedocs/libquadmath>
 * Threads, Multi-threading library
 * TBB, Intel's Threading Building Blocks
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * Python3
 * fmt
-- The following REQUIRED packages have been found:
 * dune-common
-- The following OPTIONAL packages have not been found:
 * LATEX
 * LatexMk
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * PTScotch, Sequential and Parallel Graph Partitioning
 * METIS, Serial Graph Partitioning
 * ParMETIS (required version >= 4.0), Parallel Graph Partitioning
```
