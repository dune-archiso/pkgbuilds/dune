```console
# export CPPFLAGS="-I/usr/include/tirpc ${CPPFLAGS}"
# export CFLAGS="-fPIC ${CFLAGS}"
# export CXXFLAGS="-fPIC ${CFLAGS}"
# export LDFLAGS="-ldl -lm -ltirpc"
```

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-logging
doxygen_install
headercheck
install_python
test_python
dune-logging
dune/logging/debugstreamsupport.o
dune/logging/debugstreamsupport.i
dune/logging/debugstreamsupport.s
dune/logging/logger.o
dune/logging/logger.i
dune/logging/logger.s
dune/logging/logging.o
dune/logging/logging.i
dune/logging/logging.s
dune/logging/loggingstream.o
dune/logging/loggingstream.i
dune/logging/loggingstream.s
dune/logging/loggingstreambuffer.o
dune/logging/loggingstreambuffer.i
dune/logging/loggingstreambuffer.s
dune/logging/logmessage.o
dune/logging/logmessage.i
dune/logging/logmessage.s
dune/logging/patternformatsink.o
dune/logging/patternformatsink.i
dune/logging/patternformatsink.s
dune/logging/sink.o
dune/logging/sink.i
dune/logging/sink.s
lib/libdune-logging_stub.o
lib/libdune-logging_stub.i
lib/libdune-logging_stub.s
```

```console
logging.dir/dune/logging/logger.cc.o -c /tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/logger.cc
/tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/logger.cc: In member function â€˜void Dune::Logging::LoggerBackend::handle(const Dune::Logging::Logger&, Dune::Logging::LogLevel, int, std::string_view, fmt::v7::format_args)â€™:
/tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/logger.cc:52:20: error: no matching function for call to â€˜vformat_to(fmt::v7::basic_memory_buffer<char, 200>&, std::string_view&, fmt::v7::format_args&)â€™
   52 |     fmt::vformat_to(buffer, format, args);
      |     ~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~
In file included from /tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/fmt.hh:10,
                 from /tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/loggerbackend.hh:10,
                 from /tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/logger.cc:11:
/usr/include/fmt/core.h:2896:6: note: candidate: â€˜template<class OutputIt, typename std::enable_if<fmt::v7::detail::is_output_iterator<OutputIt, char>::value, int>::type <anonymous> > OutputIt fmt::v7::vformat_to(OutputIt, fmt::v7::string_view, fmt::v7::format_args)â€™
 2896 | auto vformat_to(OutputIt out, string_view fmt, format_args args) -> OutputIt {
      |      ^~~~~~~~~~
/usr/include/fmt/core.h:2896:6: note:   template argument deduction/substitution failed:
/usr/include/fmt/core.h:2895:11: error: no type named â€˜typeâ€™ in â€˜struct std::enable_if<false, int>â€™
 2895 |           FMT_ENABLE_IF(detail::is_output_iterator<OutputIt, char>::value)>
      |           ^~~~~~~~~~~~~
In file included from /tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/fmt.hh:11,
                 from /tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/loggerbackend.hh:10,
                 from /tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/logger.cc:11:
/usr/include/fmt/format.h:2800:6: note: candidate: â€˜template<class OutputIt, class Locale, typename std::enable_if<(fmt::v7::detail::is_output_iterator<OutputIt, char>::value && fmt::v7::detail::is_locale<Locale>::value), int>::type <anonymous> > OutputIt fmt::v7::vformat_to(OutputIt, const Locale&, fmt::v7::string_view, fmt::v7::format_args)â€™
 2800 | auto vformat_to(OutputIt out, const Locale& loc, string_view fmt,
      |      ^~~~~~~~~~
/usr/include/fmt/format.h:2800:6: note:   template argument deduction/substitution failed:
/tmp/makepkg/dune-logging/src/dune-logging-v2.7.1/dune/logging/logger.cc:52:20: note:   candidate expects 4 arguments, 3 provided
   52 |     fmt::vformat_to(buffer, format, args);
      |     ~~~~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~
make[2]: *** [CMakeFiles/dune-logging.dir/build.make:107: CMakeFiles/dune-logging.dir/dune/logging/logger.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-logging/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:192: CMakeFiles/dune-logging.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-logging/src/build-cmake'
make: *** [Makefile:169: all] Error 2
==> ERROR: A failure occurred in build().
    Aborting...
```
