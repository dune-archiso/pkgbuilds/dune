## [`dune-logging`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-logging/PKGBUILD)

```console
-- The following OPTIONAL packages have been found:
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * BLAS, fast linear algebra routines
 * Threads, Multi-threading library
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
 * fmt
-- The following REQUIRED packages have been found:
 * dune-common
-- The following OPTIONAL packages have not been found:
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
```

```
usr/
usr/include/
usr/include/dune/
usr/include/dune/logging/
usr/include/dune/logging.hh
usr/include/dune/logging/checks.hh
usr/include/dune/logging/consolesink.hh
usr/include/dune/logging/debugstreamsupport.hh
usr/include/dune/logging/destructiblesingletonholder.hh
usr/include/dune/logging/exceptions.hh
usr/include/dune/logging/filesinks.hh
usr/include/dune/logging/fmt.hh
usr/include/dune/logging/logger.hh
usr/include/dune/logging/loggerbackend.hh
usr/include/dune/logging/logging.hh
usr/include/dune/logging/loggingstream.hh
usr/include/dune/logging/loggingstreambuffer.hh
usr/include/dune/logging/loglevel.hh
usr/include/dune/logging/logmessage.hh
usr/include/dune/logging/patternformatsink.hh
usr/include/dune/logging/sink.hh
usr/include/dune/logging/sinkmessageitems.hh
usr/include/dune/logging/type_traits.hh
usr/include/dune/logging/utility.hh
usr/lib/
usr/lib/cmake/
usr/lib/cmake/dune-logging/
usr/lib/cmake/dune-logging/dune-logging-config-version.cmake
usr/lib/cmake/dune-logging/dune-logging-config.cmake
usr/lib/cmake/dune-logging/dune-logging-targets-none.cmake
usr/lib/cmake/dune-logging/dune-logging-targets.cmake
usr/lib/dunecontrol/
usr/lib/dunecontrol/dune-logging/
usr/lib/dunecontrol/dune-logging/dune.module
usr/lib/libdune-logging.so
usr/lib/pkgconfig/
usr/lib/pkgconfig/dune-logging.pc
usr/share/
usr/share/doc/
usr/share/doc/dune-logging/
usr/share/doc/dune-logging/doxygen/
usr/share/doc/dune-logging/doxygen/index.html
usr/share/dune/
usr/share/dune-logging/
usr/share/dune-logging/config.h.cmake
usr/share/dune/cmake/
usr/share/dune/cmake/modules/
usr/share/dune/cmake/modules/DuneLoggingMacros.cmake
usr/share/licenses/
usr/share/licenses/dune-logging/
usr/share/licenses/dune-logging/LICENSE
```

<!-- [![Packaging status](
https://repology.org/badge/vertical-allrepos/dune-logging.svg
)](https://repology.org/project/dune-logging/versions) -->
