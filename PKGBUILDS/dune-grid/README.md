## [`dune-grid`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-grid/PKGBUILD)

[](https://sources.debian.org/src/dune-grid/2.8.0-2)

#### debian

- [](https://tracker.debian.org/pkg/dune-grid)
- [](https://salsa.debian.org/pjaap/dune-grid)
- [](https://salsa.debian.org/lisajuliafog/dune-grid)


```console
-- The following OPTIONAL packages have been found:

 * dune-uggrid
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * BLAS, fast linear algebra routines
 * Threads, Multi-threading library
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
 * ParMETIS
 * Alberta
 * Psurface

-- The following REQUIRED packages have been found:

 * dune-common
 * dune-geometry

-- The following OPTIONAL packages have not been found:

 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * AmiraMesh
```

```
usr/
usr/include/
usr/include/dune/
usr/include/dune/grid/
usr/include/dune/grid/albertagrid/
usr/include/dune/grid/albertagrid.hh
usr/include/dune/grid/albertagrid/agrid.hh
usr/include/dune/grid/albertagrid/albertagrid.cc
usr/include/dune/grid/albertagrid/albertaheader.hh
usr/include/dune/grid/albertagrid/albertareader.hh
usr/include/dune/grid/albertagrid/algebra.hh
usr/include/dune/grid/albertagrid/backuprestore.hh
usr/include/dune/grid/albertagrid/capabilities.hh
usr/include/dune/grid/albertagrid/coordcache.hh
usr/include/dune/grid/albertagrid/datahandle.hh
usr/include/dune/grid/albertagrid/dgfparser.hh
usr/include/dune/grid/albertagrid/dofadmin.hh
usr/include/dune/grid/albertagrid/dofvector.hh
usr/include/dune/grid/albertagrid/elementinfo.hh
usr/include/dune/grid/albertagrid/entity.cc
usr/include/dune/grid/albertagrid/entity.hh
usr/include/dune/grid/albertagrid/entityseed.hh
usr/include/dune/grid/albertagrid/geometry.cc
usr/include/dune/grid/albertagrid/geometry.hh
usr/include/dune/grid/albertagrid/geometrycache.hh
usr/include/dune/grid/albertagrid/geometryreference.hh
usr/include/dune/grid/albertagrid/gridfactory.hh
usr/include/dune/grid/albertagrid/gridfamily.hh
usr/include/dune/grid/albertagrid/gridview.hh
usr/include/dune/grid/albertagrid/hierarchiciterator.hh
usr/include/dune/grid/albertagrid/indexsets.hh
usr/include/dune/grid/albertagrid/indexstack.hh
usr/include/dune/grid/albertagrid/intersection.cc
usr/include/dune/grid/albertagrid/intersection.hh
usr/include/dune/grid/albertagrid/intersectioniterator.hh
usr/include/dune/grid/albertagrid/leafiterator.hh
usr/include/dune/grid/albertagrid/level.hh
usr/include/dune/grid/albertagrid/leveliterator.hh
usr/include/dune/grid/albertagrid/macrodata.hh
usr/include/dune/grid/albertagrid/macroelement.hh
usr/include/dune/grid/albertagrid/meshpointer.hh
usr/include/dune/grid/albertagrid/misc.hh
usr/include/dune/grid/albertagrid/persistentcontainer.hh
usr/include/dune/grid/albertagrid/projection.hh
usr/include/dune/grid/albertagrid/refinement.hh
usr/include/dune/grid/albertagrid/structuredgridfactory.hh
usr/include/dune/grid/albertagrid/transformation.hh
usr/include/dune/grid/albertagrid/treeiterator.hh
usr/include/dune/grid/albertagrid/undefine-2.0.hh
usr/include/dune/grid/albertagrid/undefine-3.0.hh
usr/include/dune/grid/common/
usr/include/dune/grid/common/adaptcallback.hh
usr/include/dune/grid/common/backuprestore.hh
usr/include/dune/grid/common/boundaryprojection.hh
usr/include/dune/grid/common/boundarysegment.hh
usr/include/dune/grid/common/capabilities.hh
usr/include/dune/grid/common/datahandleif.hh
usr/include/dune/grid/common/defaultgridview.hh
usr/include/dune/grid/common/entity.hh
usr/include/dune/grid/common/entityiterator.hh
usr/include/dune/grid/common/entityseed.hh
usr/include/dune/grid/common/exceptions.hh
usr/include/dune/grid/common/geometry.hh
usr/include/dune/grid/common/grid.hh
usr/include/dune/grid/common/gridenums.hh
usr/include/dune/grid/common/gridfactory.hh
usr/include/dune/grid/common/gridinfo.hh
usr/include/dune/grid/common/gridview.hh
usr/include/dune/grid/common/indexidset.hh
usr/include/dune/grid/common/intersection.hh
usr/include/dune/grid/common/intersectioniterator.hh
usr/include/dune/grid/common/mapper.hh
usr/include/dune/grid/common/mcmgmapper.hh
usr/include/dune/grid/common/partitionset.hh
usr/include/dune/grid/common/rangegenerators.hh
usr/include/dune/grid/common/scsgmapper.hh
usr/include/dune/grid/common/sizecache.hh
usr/include/dune/grid/geometrygrid/
usr/include/dune/grid/geometrygrid.hh
usr/include/dune/grid/geometrygrid/backuprestore.hh
usr/include/dune/grid/geometrygrid/cachedcoordfunction.hh
usr/include/dune/grid/geometrygrid/capabilities.hh
usr/include/dune/grid/geometrygrid/coordfunction.hh
usr/include/dune/grid/geometrygrid/coordfunctioncaller.hh
usr/include/dune/grid/geometrygrid/cornerstorage.hh
usr/include/dune/grid/geometrygrid/datahandle.hh
usr/include/dune/grid/geometrygrid/declaration.hh
usr/include/dune/grid/geometrygrid/entity.hh
usr/include/dune/grid/geometrygrid/entityseed.hh
usr/include/dune/grid/geometrygrid/geometry.hh
usr/include/dune/grid/geometrygrid/grid.hh
usr/include/dune/grid/geometrygrid/gridfamily.hh
usr/include/dune/grid/geometrygrid/gridview.hh
usr/include/dune/grid/geometrygrid/hostcorners.hh
usr/include/dune/grid/geometrygrid/identity.hh
usr/include/dune/grid/geometrygrid/idset.hh
usr/include/dune/grid/geometrygrid/indexsets.hh
usr/include/dune/grid/geometrygrid/intersection.hh
usr/include/dune/grid/geometrygrid/intersectioniterator.hh
usr/include/dune/grid/geometrygrid/iterator.hh
usr/include/dune/grid/geometrygrid/persistentcontainer.hh
usr/include/dune/grid/identitygrid/
usr/include/dune/grid/identitygrid.hh
usr/include/dune/grid/identitygrid/identitygridentity.hh
usr/include/dune/grid/identitygrid/identitygridentityseed.hh
usr/include/dune/grid/identitygrid/identitygridgeometry.hh
usr/include/dune/grid/identitygrid/identitygridhierarchiciterator.hh
usr/include/dune/grid/identitygrid/identitygridindexsets.hh
usr/include/dune/grid/identitygrid/identitygridintersectioniterator.hh
usr/include/dune/grid/identitygrid/identitygridintersections.hh
usr/include/dune/grid/identitygrid/identitygridleafiterator.hh
usr/include/dune/grid/identitygrid/identitygridleveliterator.hh
usr/include/dune/grid/io/
usr/include/dune/grid/io/file/
usr/include/dune/grid/io/file/amiramesh/
usr/include/dune/grid/io/file/amiramesh/amirameshreader.cc
usr/include/dune/grid/io/file/amiramesh/amirameshwriter.cc
usr/include/dune/grid/io/file/amiramesh/psurfaceboundary.hh
usr/include/dune/grid/io/file/amirameshreader.hh
usr/include/dune/grid/io/file/amirameshwriter.hh
usr/include/dune/grid/io/file/dgfparser/
usr/include/dune/grid/io/file/dgfparser.hh
usr/include/dune/grid/io/file/dgfparser/blocks/
usr/include/dune/grid/io/file/dgfparser/blocks/basic.hh
usr/include/dune/grid/io/file/dgfparser/blocks/boundarydom.hh
usr/include/dune/grid/io/file/dgfparser/blocks/boundaryseg.hh
usr/include/dune/grid/io/file/dgfparser/blocks/cube.hh
usr/include/dune/grid/io/file/dgfparser/blocks/dim.hh
usr/include/dune/grid/io/file/dgfparser/blocks/general.hh
usr/include/dune/grid/io/file/dgfparser/blocks/gridparameter.hh
usr/include/dune/grid/io/file/dgfparser/blocks/interval.hh
usr/include/dune/grid/io/file/dgfparser/blocks/periodicfacetrans.hh
usr/include/dune/grid/io/file/dgfparser/blocks/polygon.hh
usr/include/dune/grid/io/file/dgfparser/blocks/polyhedron.hh
usr/include/dune/grid/io/file/dgfparser/blocks/projection.hh
usr/include/dune/grid/io/file/dgfparser/blocks/simplex.hh
usr/include/dune/grid/io/file/dgfparser/blocks/simplexgeneration.hh
usr/include/dune/grid/io/file/dgfparser/blocks/vertex.hh
usr/include/dune/grid/io/file/dgfparser/dgfexception.hh
usr/include/dune/grid/io/file/dgfparser/dgfgeogrid.hh
usr/include/dune/grid/io/file/dgfparser/dgfgridfactory.hh
usr/include/dune/grid/io/file/dgfparser/dgfidentitygrid.hh
usr/include/dune/grid/io/file/dgfparser/dgfoned.hh
usr/include/dune/grid/io/file/dgfparser/dgfparser.hh
usr/include/dune/grid/io/file/dgfparser/dgfug.hh
usr/include/dune/grid/io/file/dgfparser/dgfwriter.hh
usr/include/dune/grid/io/file/dgfparser/dgfyasp.hh
usr/include/dune/grid/io/file/dgfparser/entitykey.hh
usr/include/dune/grid/io/file/dgfparser/entitykey_inline.hh
usr/include/dune/grid/io/file/dgfparser/gridptr.hh
usr/include/dune/grid/io/file/dgfparser/macrogrid.hh
usr/include/dune/grid/io/file/dgfparser/parser.hh
usr/include/dune/grid/io/file/gmshreader.hh
usr/include/dune/grid/io/file/gmshwriter.hh
usr/include/dune/grid/io/file/gnuplot/
usr/include/dune/grid/io/file/gnuplot.hh
usr/include/dune/grid/io/file/gnuplot/gnuplot.cc
usr/include/dune/grid/io/file/printgrid.hh
usr/include/dune/grid/io/file/starcdreader.hh
usr/include/dune/grid/io/file/vtk/
usr/include/dune/grid/io/file/vtk.hh
usr/include/dune/grid/io/file/vtk/b64enc.hh
usr/include/dune/grid/io/file/vtk/basicwriter.hh
usr/include/dune/grid/io/file/vtk/boundaryiterators.hh
usr/include/dune/grid/io/file/vtk/boundarywriter.hh
usr/include/dune/grid/io/file/vtk/common.hh
usr/include/dune/grid/io/file/vtk/corner.hh
usr/include/dune/grid/io/file/vtk/corneriterator.hh
usr/include/dune/grid/io/file/vtk/dataarraywriter.hh
usr/include/dune/grid/io/file/vtk/function.hh
usr/include/dune/grid/io/file/vtk/functionwriter.hh
usr/include/dune/grid/io/file/vtk/pointiterator.hh
usr/include/dune/grid/io/file/vtk/pvtuwriter.hh
usr/include/dune/grid/io/file/vtk/skeletonfunction.hh
usr/include/dune/grid/io/file/vtk/streams.hh
usr/include/dune/grid/io/file/vtk/subsamplingvtkwriter.hh
usr/include/dune/grid/io/file/vtk/volumeiterators.hh
usr/include/dune/grid/io/file/vtk/volumewriter.hh
usr/include/dune/grid/io/file/vtk/vtksequencewriter.hh
usr/include/dune/grid/io/file/vtk/vtksequencewriterbase.hh
usr/include/dune/grid/io/file/vtk/vtkwriter.hh
usr/include/dune/grid/io/file/vtk/vtuwriter.hh
usr/include/dune/grid/onedgrid/
usr/include/dune/grid/onedgrid.hh
usr/include/dune/grid/onedgrid/nulliteratorfactory.hh
usr/include/dune/grid/onedgrid/onedgridentity.hh
usr/include/dune/grid/onedgrid/onedgridentityseed.hh
usr/include/dune/grid/onedgrid/onedgridfactory.hh
usr/include/dune/grid/onedgrid/onedgridhieriterator.hh
usr/include/dune/grid/onedgrid/onedgridindexsets.hh
usr/include/dune/grid/onedgrid/onedgridintersectioniterators.hh
usr/include/dune/grid/onedgrid/onedgridintersections.hh
usr/include/dune/grid/onedgrid/onedgridleafiterator.hh
usr/include/dune/grid/onedgrid/onedgridleveliterator.hh
usr/include/dune/grid/onedgrid/onedgridlist.hh
usr/include/dune/grid/onedgrid/onedgridviews.hh
usr/include/dune/grid/test/
usr/include/dune/grid/test/basicunitcube.hh
usr/include/dune/grid/test/check-albertareader.hh
usr/include/dune/grid/test/checkadaptation.hh
usr/include/dune/grid/test/checkcomcorrectness.hh
usr/include/dune/grid/test/checkcommunicate.hh
usr/include/dune/grid/test/checkentitylifetime.hh
usr/include/dune/grid/test/checkentityseed.hh
usr/include/dune/grid/test/checkgeometry.hh
usr/include/dune/grid/test/checkgeometryinfather.hh
usr/include/dune/grid/test/checkgridfactory.hh
usr/include/dune/grid/test/checkidset.hh
usr/include/dune/grid/test/checkindexset.hh
usr/include/dune/grid/test/checkintersectionit.hh
usr/include/dune/grid/test/checkintersectionlifetime.hh
usr/include/dune/grid/test/checkiterators.hh
usr/include/dune/grid/test/checkjacobians.hh
usr/include/dune/grid/test/checkpartition.hh
usr/include/dune/grid/test/checktwists.hh
usr/include/dune/grid/test/functions.hh
usr/include/dune/grid/test/gridcheck.hh
usr/include/dune/grid/test/staticcheck.hh
usr/include/dune/grid/uggrid/
usr/include/dune/grid/uggrid.hh
usr/include/dune/grid/uggrid/ug_undefs.hh
usr/include/dune/grid/uggrid/uggridentity.hh
usr/include/dune/grid/uggrid/uggridentityseed.hh
usr/include/dune/grid/uggrid/uggridfactory.hh
usr/include/dune/grid/uggrid/uggridgeometry.hh
usr/include/dune/grid/uggrid/uggridhieriterator.hh
usr/include/dune/grid/uggrid/uggridindexsets.hh
usr/include/dune/grid/uggrid/uggridintersectioniterators.hh
usr/include/dune/grid/uggrid/uggridintersections.hh
usr/include/dune/grid/uggrid/uggridleafiterator.hh
usr/include/dune/grid/uggrid/uggridleveliterator.hh
usr/include/dune/grid/uggrid/uggridlocalgeometry.hh
usr/include/dune/grid/uggrid/uggridrenumberer.hh
usr/include/dune/grid/uggrid/uggridviews.hh
usr/include/dune/grid/uggrid/ugincludes.hh
usr/include/dune/grid/uggrid/uglbgatherscatter.hh
usr/include/dune/grid/uggrid/ugmessagebuffer.hh
usr/include/dune/grid/uggrid/ugwrapper.hh
usr/include/dune/grid/utility/
usr/include/dune/grid/utility/entitycommhelper.hh
usr/include/dune/grid/utility/globalindexset.hh
usr/include/dune/grid/utility/gridinfo-gmsh-main.hh
usr/include/dune/grid/utility/gridinfo.hh
usr/include/dune/grid/utility/gridtype.hh
usr/include/dune/grid/utility/hierarchicsearch.hh
usr/include/dune/grid/utility/hostgridaccess.hh
usr/include/dune/grid/utility/multiindex.hh
usr/include/dune/grid/utility/parmetisgridpartitioner.hh
usr/include/dune/grid/utility/persistentcontainer.hh
usr/include/dune/grid/utility/persistentcontainerinterface.hh
usr/include/dune/grid/utility/persistentcontainermap.hh
usr/include/dune/grid/utility/persistentcontainervector.hh
usr/include/dune/grid/utility/persistentcontainerwrapper.hh
usr/include/dune/grid/utility/structuredgridfactory.hh
usr/include/dune/grid/utility/tensorgridfactory.hh
usr/include/dune/grid/utility/vertexorderfactory.hh
usr/include/dune/grid/yaspgrid/
usr/include/dune/grid/yaspgrid.hh
usr/include/dune/grid/yaspgrid/backuprestore.hh
usr/include/dune/grid/yaspgrid/coordinates.hh
usr/include/dune/grid/yaspgrid/partitioning.hh
usr/include/dune/grid/yaspgrid/structuredyaspgridfactory.hh
usr/include/dune/grid/yaspgrid/torus.hh
usr/include/dune/grid/yaspgrid/yaspgridentity.hh
usr/include/dune/grid/yaspgrid/yaspgridentityseed.hh
usr/include/dune/grid/yaspgrid/yaspgridgeometry.hh
usr/include/dune/grid/yaspgrid/yaspgridhierarchiciterator.hh
usr/include/dune/grid/yaspgrid/yaspgrididset.hh
usr/include/dune/grid/yaspgrid/yaspgridindexsets.hh
usr/include/dune/grid/yaspgrid/yaspgridintersection.hh
usr/include/dune/grid/yaspgrid/yaspgridintersectioniterator.hh
usr/include/dune/grid/yaspgrid/yaspgridleveliterator.hh
usr/include/dune/grid/yaspgrid/yaspgridpersistentcontainer.hh
usr/include/dune/grid/yaspgrid/ygrid.hh
usr/include/dune/python/
usr/include/dune/python/grid/
usr/include/dune/python/grid/capabilities.hh
usr/include/dune/python/grid/commops.hh
usr/include/dune/python/grid/entity.hh
usr/include/dune/python/grid/enums.hh
usr/include/dune/python/grid/factory.hh
usr/include/dune/python/grid/function.hh
usr/include/dune/python/grid/geometry.hh
usr/include/dune/python/grid/gridview.hh
usr/include/dune/python/grid/hierarchical.hh
usr/include/dune/python/grid/idset.hh
usr/include/dune/python/grid/indexset.hh
usr/include/dune/python/grid/intersection.hh
usr/include/dune/python/grid/localview.hh
usr/include/dune/python/grid/mapper.hh
usr/include/dune/python/grid/numpy.hh
usr/include/dune/python/grid/numpycommdatahandle.hh
usr/include/dune/python/grid/object.hh
usr/include/dune/python/grid/pygridfunction.hh
usr/include/dune/python/grid/range.hh
usr/include/dune/python/grid/simplegridfunction.hh
usr/include/dune/python/grid/vtk.hh
usr/lib/
usr/lib/cmake/
usr/lib/cmake/dune-grid/
usr/lib/cmake/dune-grid/dune-grid-config-version.cmake
usr/lib/cmake/dune-grid/dune-grid-config.cmake
usr/lib/cmake/dune-grid/dune-grid-targets-none.cmake
usr/lib/cmake/dune-grid/dune-grid-targets.cmake
usr/lib/dunecontrol/
usr/lib/dunecontrol/dune-grid/
usr/lib/dunecontrol/dune-grid/dune.module
usr/lib/dunegridam2cmake.lib
usr/lib/libdunegrid.so
usr/lib/pkgconfig/
usr/lib/pkgconfig/dune-grid.pc
usr/python/
usr/python/dune/
usr/python/dune/grid/
usr/python/dune/grid/_grid.so
usr/share/
usr/share/doc/
usr/share/doc/dune-grid/
usr/share/doc/dune-grid/doxygen/
usr/share/doc/dune-grid/doxygen/index.html
usr/share/doc/dune-grid/grids/
usr/share/doc/dune-grid/grids/amc/
usr/share/doc/dune-grid/grids/amc/grid-1-1.amc
usr/share/doc/dune-grid/grids/amc/grid-1-2.amc
usr/share/doc/dune-grid/grids/amc/grid-1-3.amc
usr/share/doc/dune-grid/grids/amc/grid-2-2.amc
usr/share/doc/dune-grid/grids/amc/grid-2-3.amc
usr/share/doc/dune-grid/grids/amc/grid-2-4.amc
usr/share/doc/dune-grid/grids/amc/grid-3-3.amc
usr/share/doc/dune-grid/grids/amc/macro.amc
usr/share/doc/dune-grid/grids/amc/periodic-torus.amc
usr/share/doc/dune-grid/grids/amiramesh/
usr/share/doc/dune-grid/grids/amiramesh/cube-testgrid-3d.am
usr/share/doc/dune-grid/grids/amiramesh/hybrid-testgrid-2d.am
usr/share/doc/dune-grid/grids/amiramesh/hybrid-testgrid-3d.am
usr/share/doc/dune-grid/grids/amiramesh/simplex-testgrid-2d.am
usr/share/doc/dune-grid/grids/amiramesh/simplex-testgrid-3d.am
usr/share/doc/dune-grid/grids/dgf/
usr/share/doc/dune-grid/grids/dgf/basicunitcube-2d.dgf
usr/share/doc/dune-grid/grids/dgf/cube-2.dgf
usr/share/doc/dune-grid/grids/dgf/cube-testgrid-2-2.dgf
usr/share/doc/dune-grid/grids/dgf/cube-testgrid-2-3.dgf
usr/share/doc/dune-grid/grids/dgf/cube_grid.dgf
usr/share/doc/dune-grid/grids/dgf/distorted-cube-3.dgf
usr/share/doc/dune-grid/grids/dgf/example-projection.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid10.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid10a.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid10b.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid11.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid12.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid1c.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid1gen.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid1s.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid2a.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid2b.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid2c.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid2d.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid2e.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid5.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid6.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid7.dgf
usr/share/doc/dune-grid/grids/dgf/examplegrid9.dgf
usr/share/doc/dune-grid/grids/dgf/grid2Y.dgf
usr/share/doc/dune-grid/grids/dgf/grid3A.dgf
usr/share/doc/dune-grid/grids/dgf/grid3Y.dgf
usr/share/doc/dune-grid/grids/dgf/helix.dgf
usr/share/doc/dune-grid/grids/dgf/octahedron.dgf
usr/share/doc/dune-grid/grids/dgf/simplex-testgrid-1-1.dgf
usr/share/doc/dune-grid/grids/dgf/simplex-testgrid-1-2.dgf
usr/share/doc/dune-grid/grids/dgf/simplex-testgrid-1-3.dgf
usr/share/doc/dune-grid/grids/dgf/simplex-testgrid-2-2.dgf
usr/share/doc/dune-grid/grids/dgf/simplex-testgrid-2-3.dgf
usr/share/doc/dune-grid/grids/dgf/simplex-testgrid-3-3-large.dgf
usr/share/doc/dune-grid/grids/dgf/simplex-testgrid-3-3.dgf
usr/share/doc/dune-grid/grids/dgf/test1d-vertex.dgf
usr/share/doc/dune-grid/grids/dgf/test1d.dgf
usr/share/doc/dune-grid/grids/dgf/test2d.dgf
usr/share/doc/dune-grid/grids/dgf/test2d_offset.dgf
usr/share/doc/dune-grid/grids/dgf/test2ug.dgf
usr/share/doc/dune-grid/grids/dgf/test3d.dgf
usr/share/doc/dune-grid/grids/dgf/torus-2.dgf
usr/share/doc/dune-grid/grids/dgf/torus-3.dgf
usr/share/doc/dune-grid/grids/dgf/unstr_cube.dgf
usr/share/doc/dune-grid/grids/gmsh/
usr/share/doc/dune-grid/grids/gmsh/circle.geo
usr/share/doc/dune-grid/grids/gmsh/circle1storder.msh
usr/share/doc/dune-grid/grids/gmsh/circle2ndorder.msh
usr/share/doc/dune-grid/grids/gmsh/curved2d.geo
usr/share/doc/dune-grid/grids/gmsh/curved2d.msh
usr/share/doc/dune-grid/grids/gmsh/hybrid-testgrid-2d.msh
usr/share/doc/dune-grid/grids/gmsh/hybrid-testgrid-3d.msh
usr/share/doc/dune-grid/grids/gmsh/oned-testgrid.msh
usr/share/doc/dune-grid/grids/gmsh/pyramid.geo
usr/share/doc/dune-grid/grids/gmsh/pyramid.msh
usr/share/doc/dune-grid/grids/gmsh/pyramid1storder.msh
usr/share/doc/dune-grid/grids/gmsh/pyramid2ndorder.msh
usr/share/doc/dune-grid/grids/gmsh/pyramid4.msh
usr/share/doc/dune-grid/grids/gmsh/sphere.msh
usr/share/doc/dune-grid/grids/gmsh/telescope.geo
usr/share/doc/dune-grid/grids/gmsh/telescope.msh
usr/share/doc/dune-grid/grids/gmsh/telescope1storder.msh
usr/share/doc/dune-grid/grids/gmsh/telescope2ndorder.msh
usr/share/doc/dune-grid/grids/gmsh/twotets.geo
usr/share/doc/dune-grid/grids/gmsh/twotets.msh
usr/share/doc/dune-grid/grids/gmsh/unitcube.msh
usr/share/doc/dune-grid/grids/gridfactory/
usr/share/doc/dune-grid/grids/gridfactory/hybridtestgrids.hh
usr/share/doc/dune-grid/grids/gridfactory/testgrids.hh
usr/share/doc/dune-grid/grids/starcd/
usr/share/doc/dune-grid/grids/starcd/star.cel
usr/share/doc/dune-grid/grids/starcd/star.vrt
usr/share/doc/dune-grid/grids/starcd/tets.cel
usr/share/doc/dune-grid/grids/starcd/tets.vrt
usr/share/doc/dune-grid/grids/starcd/withprism.cel
usr/share/doc/dune-grid/grids/starcd/withprism.vrt
usr/share/doc/dune-grid/grids/starcd/withpyramid.cel
usr/share/doc/dune-grid/grids/starcd/withpyramid.vrt
usr/share/dune/
usr/share/dune-grid/
usr/share/dune-grid/config.h.cmake
usr/share/dune/cmake/
usr/share/dune/cmake/modules/
usr/share/dune/cmake/modules/AddAlbertaFlags.cmake
usr/share/dune/cmake/modules/AddAmiraMeshFlags.cmake
usr/share/dune/cmake/modules/AddPsurfaceFlags.cmake
usr/share/dune/cmake/modules/DuneGridMacros.cmake
usr/share/dune/cmake/modules/FindAlberta.cmake
usr/share/dune/cmake/modules/FindAmiraMesh.cmake
usr/share/dune/cmake/modules/FindPsurface.cmake
usr/share/dune/cmake/modules/GridType.cmake
usr/share/dune/cmake/modules/UseUG.cmake
usr/share/licenses/
usr/share/licenses/dune-grid/
usr/share/licenses/dune-grid/LICENSE
```

```
usr/
usr/lib/
usr/lib/python3.9/
usr/lib/python3.9/site-packages/
usr/lib/python3.9/site-packages/dune/
usr/lib/python3.9/site-packages/dune/grid/
usr/lib/python3.9/site-packages/dune/grid/__init__.py
usr/lib/python3.9/site-packages/dune/grid/__main__.py
usr/lib/python3.9/site-packages/dune/grid/_grid.so
usr/lib/python3.9/site-packages/dune/grid/_grids.py
usr/lib/python3.9/site-packages/dune/grid/core.py
usr/lib/python3.9/site-packages/dune/grid/datahandle.py
usr/lib/python3.9/site-packages/dune/grid/grid_generator.py
usr/lib/python3.9/site-packages/dune/grid/map.py
usr/lib/python3.9/site-packages/dune_grid-2.8.0-py3.9-nspkg.pth
usr/lib/python3.9/site-packages/dune_grid-2.8.0-py3.9.egg-info/
usr/lib/python3.9/site-packages/dune_grid-2.8.0-py3.9.egg-info/PKG-INFO
usr/lib/python3.9/site-packages/dune_grid-2.8.0-py3.9.egg-info/SOURCES.txt
usr/lib/python3.9/site-packages/dune_grid-2.8.0-py3.9.egg-info/dependency_links.txt
usr/lib/python3.9/site-packages/dune_grid-2.8.0-py3.9.egg-info/namespace_packages.txt
usr/lib/python3.9/site-packages/dune_grid-2.8.0-py3.9.egg-info/not-zip-safe
usr/lib/python3.9/site-packages/dune_grid-2.8.0-py3.9.egg-info/top_level.txt
```

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-grid.svg)](https://repology.org/project/dune-grid/versions)
