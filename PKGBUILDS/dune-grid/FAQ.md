When the package `dune-grid-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `include`
- [x] `lib`
- [x] `share`
  - [x] `doc`
  - [x] `dune`
  - [x] `dune-grid`
  - [x] `licenses`

```console
usr/
├── include
│   └── dune
│       └── grid
├── lib
│   ├── cmake
│   │   └── dune-grid
│   ├── dunecontrol
│   │   └── dune-grid
│   ├── dunegridam2cmake.lib
│   └── pkgconfig
└── share
    ├── doc
    │   └── dune-grid
    │       └── grids
    │           ├── amc
    │           ├── amiramesh
    │           ├── dgf
    │           ├── gmsh
    │           ├── gridfactory
    │           └── starcd
    ├── dune
    │   └── cmake
    │       └── modules
    ├── dune-grid
    │   └── config.h.cmake
    └── licenses
        └── dune-grid

43 directories, 2330 files
```

If we put `alberta`, `dune-uggrid` as makedepends, then both must also belongs to depends.

Maybe try with `-DCMAKE_DISABLE_FIND_PACKAGE_Alberta=TRUE`.

```console
-DCMAKE_DISABLE_FIND_PACKAGE_SuperLU=true
```

```console
DUNE_CMAKE_FLAGS: "-DCMAKE_CXX_COMPILER=g++-9 -DCMAKE_C_COMPILER=gcc-9 -DCMAKE_CXX_FLAGS='-std=c++17 -O2 -g -Wall -fdiagnostics-color=always' -DDUNE_ENABLE_PYTHONBINDINGS=ON -DDUNE_MAX_TEST_CORES=4 -DBUILD_SHARED_LIBS=TRUE -DDUNE_PYTHON_INSTALL_LOCATION=none -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_Alberta=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE -DCMAKE_DISABLE_DOCUMENTATION=TRUE"
```

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-grid
doxygen_install
headercheck
install_python
test_python
amirameshtest
conformvolumevtktest
dgf2dgf
dunealbertagrid_1d
dunealbertagrid_2d
dunealbertagrid_3d
dunegrid
geometrygrid-coordfunction-copyconstructor
globalindexsettest
gmsh2dgf
gmsh2dgfugsimplex
gmshtest-alberta2d
gmshtest-alberta3d
gmshtest-onedgrid
gmshtest-uggrid
gnuplottest
issue-53-uggrid-intersections
mcmgmappertest
nonconformboundaryvtktest
persistentcontainertest
printgridtest
scsgmappertest
starcdreadertest
structuredgridfactorytest
subsamplingvtktest
tensorgridfactorytest
test-alberta
test-alberta-1-1
test-alberta-1-1-no-deprecated
test-alberta-1-2
test-alberta-1-3
test-alberta-2-2
test-alberta-2-3
test-alberta-3-3
test-alberta-generic
test-alberta3d-refine
test-dgf-alberta
test-dgf-gmsh-ug
test-dgf-oned
test-dgf-ug
test-dgf-yasp
test-dgf-yasp-offset
test-geogrid-uggrid
test-geogrid-yaspgrid
test-gridinfo
test-hierarchicsearch
test-identitygrid
test-loadbalancing
test-mcmg-geogrid
test-oned
test-parallel-ug
test-ug
test-yaspgrid-backuprestore-equidistant
test-yaspgrid-backuprestore-equidistantoffset
test-yaspgrid-backuprestore-tensor
test-yaspgrid-entityshifttable
test-yaspgrid-tensorgridfactory
test-yaspgrid-yaspfactory-1d
test-yaspgrid-yaspfactory-2d
test-yaspgrid-yaspfactory-3d
testiteratorranges
vertexordertest
vtksequencetest
vtktest
```

```console
-DCMAKE_C_FLAGS_RELEASE='-fPIC'
```

https://github.com/freebsd/freebsd-ports/blob/main/math/dune-grid/Makefile

```
# export CPPFLAGS="-I/usr/include/tirpc ${CPPFLAGS}"
```

```
13/61 Test #13: gmshtest-onedgrid .....................................Subprocess aborted***Exception:   0.32 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
Using OneDGrid
Reading mesh file /tmp/makepkg/dune-grid/src/dune-grid-2.8.0rc1/doc/grids/gmsh/oned-testgrid.msh
Reading 1d Gmsh grid...
version 2.1 Gmsh file detected
file contains 10 nodes
file contains 11 elements
number of real vertices = 10
number of boundary elements = 2
number of elements = 9
[runner-72989761-project-24524067-concurrent-0:28091] *** Process received signal ***
[runner-72989761-project-24524067-concurrent-0:28091] Signal: Aborted (6)
[runner-72989761-project-24524067-concurrent-0:28091] Signal code:  (-6)
[runner-72989761-project-24524067-concurrent-0:28091] [ 0] /usr/lib64/libc.so.6(+0x3cda0)[0x7fced1b6fda0]
[runner-72989761-project-24524067-concurrent-0:28091] [ 1] /usr/lib64/libc.so.6(gsignal+0x142)[0x7fced1b6fd22]
[runner-72989761-project-24524067-concurrent-0:28091] [ 2] /usr/lib64/libc.so.6(abort+0x116)[0x7fced1b59862]
[runner-72989761-project-24524067-concurrent-0:28091] [ 3] /tmp/makepkg/dune-grid/src/build-cmake/dune/grid/io/file/test/gmshtest-onedgrid(+0xcada)[0x5651f4331ada]
[runner-72989761-project-24524067-concurrent-0:28091] [ 4] /tmp/makepkg/dune-grid/src/build-cmake/dune/grid/io/file/test/gmshtest-onedgrid(+0x414cf)[0x5651f43664cf]
[runner-72989761-project-24524067-concurrent-0:28091] [ 5] /tmp/makepkg/dune-grid/src/build-cmake/dune/grid/io/file/test/gmshtest-onedgrid(+0x83b6)[0x5651f432d3b6]
[runner-72989761-project-24524067-concurrent-0:28091] [ 6] /usr/lib64/libc.so.6(__libc_start_main+0xd5)[0x7fced1b5ab25]
[runner-72989761-project-24524067-concurrent-0:28091] [ 7] /tmp/makepkg/dune-grid/src/build-cmake/dune/grid/io/file/test/gmshtest-onedgrid(+0x85ae)[0x5651f432d5ae]
[runner-72989761-project-24524067-concurrent-0:28091] *** End of error message ***
```

[](https://fossies.org/diffs/dune-grid)
