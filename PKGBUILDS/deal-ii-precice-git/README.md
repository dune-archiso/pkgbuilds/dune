[](https://github.com/precice/dealii-adapter)

```
CMake Warning:
  No source or binary directory provided.  Both will be assumed to be the
  same as the current working directory, but note that this warning will
  become a fatal error in future CMake releases.
-- Using the deal.II-9.3.3 installation found at /usr
-- Include macro /usr/share/deal-ii//macros/macro_deal_ii_add_test.cmake
-- Include macro /usr/share/deal-ii//macros/macro_deal_ii_initialize_cached_variables.cmake
-- Include macro /usr/share/deal-ii//macros/macro_deal_ii_invoke_autopilot.cmake
-- Include macro /usr/share/deal-ii//macros/macro_deal_ii_pickup_tests.cmake
-- Include macro /usr/share/deal-ii//macros/macro_deal_ii_query_git_information.cmake
-- Include macro /usr/share/deal-ii//macros/macro_deal_ii_setup_target.cmake
-- The C compiler identification is GNU 11.2.0
-- The CXX compiler identification is GNU 11.2.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/sbin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/sbin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Query git repository information.
-- Found Git: /usr/sbin/git (found version "2.35.1")
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Success
-- Found Threads: TRUE
-- Using the preCICE version found at /usr/lib64/cmake/precice
-- Configuring done
-- Generating done
-- Build files have been written to: /tmp/makepkg/deal-ii-precice-git/src/dealii-adapter
-- Cache values
// Path to a program.
CMAKE_ADDR2LINE:FILEPATH=/usr/sbin/addr2line
// Path to a program.
CMAKE_AR:FILEPATH=/usr/sbin/ar
// Choose the type of build, options are: Debug, Release
CMAKE_BUILD_TYPE:STRING=Debug
// Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON
// CXX compiler
CMAKE_CXX_COMPILER:STRING=/usr/sbin/c++
// A wrapper around 'ar' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_CXX_COMPILER_AR:FILEPATH=/usr/sbin/gcc-ar
// A wrapper around 'ranlib' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_CXX_COMPILER_RANLIB:FILEPATH=/usr/sbin/gcc-ranlib
// Flags used by the compiler during all build types.
CMAKE_CXX_FLAGS:STRING=
// Flags used by the compiler during debug builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=
// Flags used by the CXX compiler during MINSIZEREL builds.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG
// Flags used by the compiler during release builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=
// Flags used by the CXX compiler during RELWITHDEBINFO builds.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG
// C compiler
CMAKE_C_COMPILER:FILEPATH=/usr/sbin/cc
// A wrapper around 'ar' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_C_COMPILER_AR:FILEPATH=/usr/sbin/gcc-ar
// A wrapper around 'ranlib' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_C_COMPILER_RANLIB:FILEPATH=/usr/sbin/gcc-ranlib
// Flags used by the C compiler during all build types.
CMAKE_C_FLAGS:STRING=-march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection
// Flags used by the C compiler during DEBUG builds.
CMAKE_C_FLAGS_DEBUG:STRING=-g
// Flags used by the C compiler during MINSIZEREL builds.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG
// Flags used by the C compiler during RELEASE builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG
// Flags used by the C compiler during RELWITHDEBINFO builds.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG
// Path to a program.
CMAKE_DLLTOOL:FILEPATH=CMAKE_DLLTOOL-NOTFOUND
// Flags used by the linker during all build types.
CMAKE_EXE_LINKER_FLAGS:STRING=-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
// Flags used by the linker during DEBUG builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=
// Flags used by the linker during MINSIZEREL builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=
// Flags used by the linker during RELEASE builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=
// Flags used by the linker during RELWITHDEBINFO builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=
// Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=
// Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/usr/local
// Path to a program.
CMAKE_LINKER:FILEPATH=/usr/sbin/ld
// Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/sbin/make
// Flags used by the linker during the creation of modules during all build types.
CMAKE_MODULE_LINKER_FLAGS:STRING=-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
// Flags used by the linker during the creation of modules during DEBUG builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=
// Flags used by the linker during the creation of modules during MINSIZEREL builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=
// Flags used by the linker during the creation of modules during RELEASE builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=
// Flags used by the linker during the creation of modules during RELWITHDEBINFO builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=
// Path to a program.
CMAKE_NM:FILEPATH=/usr/sbin/nm
// Path to a program.
CMAKE_OBJCOPY:FILEPATH=/usr/sbin/objcopy
// Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/sbin/objdump
// Path to a program.
CMAKE_RANLIB:FILEPATH=/usr/sbin/ranlib
// Path to a program.
CMAKE_READELF:FILEPATH=/usr/sbin/readelf
// Flags used by the linker during the creation of shared libraries during all build types.
CMAKE_SHARED_LINKER_FLAGS:STRING=-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
// Flags used by the linker during the creation of shared libraries during DEBUG builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=
// Flags used by the linker during the creation of shared libraries during MINSIZEREL builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=
// Flags used by the linker during the creation of shared libraries during RELEASE builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=
// Flags used by the linker during the creation of shared libraries during RELWITHDEBINFO builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=
// If set, runtime paths are not added when installing shared libraries, but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO
// If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO
// Flags used by the linker during the creation of static libraries during all build types.
CMAKE_STATIC_LINKER_FLAGS:STRING=
// Flags used by the linker during the creation of static libraries during DEBUG builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=
// Flags used by the linker during the creation of static libraries during MINSIZEREL builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=
// Flags used by the linker during the creation of static libraries during RELEASE builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=
// Flags used by the linker during the creation of static libraries during RELWITHDEBINFO builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=
// Path to a program.
CMAKE_STRIP:FILEPATH=/usr/sbin/strip
// If this value is on, makefiles will be generated without the .SILENT directive, and all commands will be echoed to the console during the make.  This is useful for debugging only. With Visual Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE
// Git command line client
GIT_EXECUTABLE:FILEPATH=/usr/sbin/git
// The directory containing a CMake configuration file for deal.II.
deal.II_DIR:PATH=/usr/lib/cmake/deal.II
// The directory containing a CMake configuration file for precice.
precice_DIR:PATH=/usr/lib64/cmake/precice
```

```
all (the default if no target is provided)
clean
depend
edit_cache
rebuild_cache
debug
release
elasticity
elasticity.o
elasticity.i
elasticity.s
include/adapter/parameters.o
include/adapter/parameters.i
include/adapter/parameters.s
source/linear_elasticity/linear_elasticity.o
source/linear_elasticity/linear_elasticity.i
source/linear_elasticity/linear_elasticity.s
source/nonlinear_elasticity/nonlinear_elasticity.o
source/nonlinear_elasticity/nonlinear_elasticity.i
source/nonlinear_elasticity/nonlinear_elasticity.s
```
