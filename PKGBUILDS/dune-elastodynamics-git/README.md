[](https://github.com/maxfirmbach/dune-elastodynamics)

```
Checking test dependency graph...
Checking test dependency graph end
test 1
    Start 1: hrzlumpingtest
1: Test command: /tmp/makepkg/dune-elastodynamics-git/src/build-cmake/test/hrzlumpingtest
1: Test timeout computed to be: 300
1: Test: HRZ lumping for linear bar
1: Test: HRZ lumping for linear simplex
1: Test: HRZ lumping for quadratic simplex
1: Test: HRZ lumping for cubic simplex
1: Test: HRZ lumping for linear cube
1: Test: HRZ lumping for quadratic cube
1/4 Test #1: hrzlumpingtest ...................   Passed    0.14 sec
test 2
    Start 2: consistentmasstest
2: Test command: /tmp/makepkg/dune-elastodynamics-git/src/build-cmake/test/consistentmasstest
2: Test timeout computed to be: 300
2: Test: Consistent mass for linear bar
2: Test: Consistent mass for linear simplex
2: Test: Consistent mass for quadratic simplex
2/4 Test #2: consistentmasstest ...............   Passed    0.14 sec
test 3
    Start 3: staticbeambendingtest
3: Test command: /tmp/makepkg/dune-elastodynamics-git/src/build-cmake/test/staticbeambendingtest
3: Test timeout computed to be: 300
3: Reading 2d Gmsh grid...
3: version 2.2 Gmsh file detected
3: file contains 42 nodes
3: file contains 62 elements
3: number of real vertices = 42
3: number of boundary elements = 42
3: number of elements = 20
3: === Dune::CGSolver
3: === rate=0.824075, T=0.187009, TIT=0.000959018, IT=195
3/4 Test #3: staticbeambendingtest ............***Failed    0.36 sec
Reading 2d Gmsh grid...
version 2.2 Gmsh file detected
file contains 42 nodes
file contains 62 elements
number of real vertices = 42
number of boundary elements = 42
number of elements = 20
=== Dune::CGSolver
=== rate=0.824075, T=0.187009, TIT=0.000959018, IT=195
test 4
    Start 4: dynamicbeambendingtest
4: Test command: /tmp/makepkg/dune-elastodynamics-git/src/build-cmake/test/dynamicbeambendingtest
4: Test timeout computed to be: 300
4: Reading 2d Gmsh grid...
4: version 2.2 Gmsh file detected
4: file contains 42 nodes
4: file contains 62 elements
4: number of real vertices = 42
4: number of boundary elements = 42
4: number of elements = 20
4:     PowerIteration_Algorithms: Performing inverse iteration with shift gamma = 20 approximating the eigenvalue closest to gamma.
4:                                Result (#iterations = 88, ║residual║_2 = 8.92219e-07): λ = 34.9027
4: smallest eigenvalue: 5.90785
4/4 Test #4: dynamicbeambendingtest ...........***Failed    0.19 sec
Reading 2d Gmsh grid...
version 2.2 Gmsh file detected
file contains 42 nodes
file contains 62 elements
number of real vertices = 42
number of boundary elements = 42
number of elements = 20
    PowerIteration_Algorithms: Performing inverse iteration with shift gamma = 20 approximating the eigenvalue closest to gamma.
                               Result (#iterations = 88, ║residual║_2 = 8.92219e-07): λ = 34.9027
smallest eigenvalue: 5.90785
50% tests passed, 2 tests failed out of 4
Total Test time (real) =   0.83 sec
The following tests FAILED:
	  3 - staticbeambendingtest (Failed)
	  4 - dynamicbeambendingtest (Failed)
Errors while running CTest
==> ERROR: A failure occurred in check().
```
