```
The following are some of the valid targets for this Makefile:
... all (the default if no target is provided)
... clean
... depend
... edit_cache
... rebuild_cache
... BoundaryConditions
... DirectedGraph
... TestCase
... basis
... ddr-magnetostatics
... ddrcore
... hho-diff-advec-reac
... hho-diffusion
... hho-general
... hho-locvardiff
... hybridcore
... mesh
... mesh-coarsen
... plot
... quadrature
```

<!-- [![Packaging status](https://repology.org/badge/vertical-allrepos/hardcore3d.svg)](https://repology.org/project/hardcore3d/versions) -->
