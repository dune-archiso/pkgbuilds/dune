## Changelog

`FindQuadMath.cmake` is included in the CMakeLists.txt in the latest version.

- `v2.7.1`: https://gitlab.dune-project.org/core/dune-common/-/blob/releases/2.7/cmake/modules/CMakeLists.txt
- `v.2.8.X`: https://gitlab.dune-project.org/core/dune-common/-/blob/master/cmake/modules/CMakeLists.txt#L47

When the package `dune-geometry-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `include`
- [x] `lib`
- [x] `share`
  - [x] `doc`
  - [x] `dune-geometry`
  - [x] `licenses`

```console
usr/
├── include
│   └── dune
│       └── geometry
├── lib
│   ├── cmake
│   │   └── dune-geometry
│   ├── dunecontrol
│   │   └── dune-geometry
│   │       └── dune.module
│   ├── libdunegeometry.a
│   └── pkgconfig
│       └── dune-geometry.pc
└── share
    ├── doc
    │   └── dune-geometry
    │       ├── appl
    │       └── doxygen
    ├── dune-geometry
    │   └── config.h.cmake
    └── licenses
        └── dune-geometry

23 directories, 464 files
```

## Tests

OK

```yml
- ls /usr/bin/dune*
- >
  if [ -z "$(ls -A /usr/include/dune/common)" ]; then
    echo "Empty"
  else
    echo "Not Empty"
  fi
- >
  if [ -z "$(ls -A /usr/lib/dunecontrol)" ]; then
    echo "Empty"
  else
    echo "Not Empty"
  fi
- >
  if [ -z "$(ls -A /usr/lib/cmake/dune-common)" ]; then
    echo "Empty"
  else
    echo "Not Empty"
  fi
- >
  if [ -z "$(ls -A /usr/lib/pkgconfig)" ]; then
    echo "Empty"
  else
    echo "Not Empty"
  fi
- >
  if [ -z "$(ls -A /usr/share/bash-completion)" ]; then
    echo "Empty"
  else
    echo "Not Empty"
  fi
- >
  if [ -z "$(ls -A /usr/share/doc/dune-common)" ]; then
    echo "Empty"
  else
    echo "Not Empty"
  fi
- >
  if [ -z "$(ls -A /usr/share/dune)" ]; then
    echo "Empty"
  else
    echo "Not Empty"
  fi
- >
  if [ -z "$(ls -A /usr/share/dune-common)" ]; then
    echo "Empty"
  else
    echo "Not Empty"
  fi
- ls /usr/share/licenses/dune-common
- ls /usr/share/man/man1/dunecontrol.1.gz
```

### Remarks

inkscape will not be necessary on 2.8

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
auxclean
build_quick_tests
build_tests
clean_latex
doc
doc_refinement_refinement_tex
doc_refinement_refinement_tex_clean
doxyfile
doxygen_dune-geometry
doxygen_install
dvi
headercheck
html
install_python
pdf
ps
safepdf
test_python
update_images
dunegeometry
test-affinegeometry
test-axisalignedcubegeometry
test-constexpr-geometrytype
test-cornerstoragerefwrap
test-fromvertexcount
test-geometrytype-id
test-multilineargeometry
test-nonetype
test-quadrature
test-referenceelements
test-refinement
```

```
build() {
  dunecontrol --opts=${pkgname}.opts cmake : make
}
```

```
package() {
  dunecontrol --only=${pkgname} make install DESTDIR="${pkgdir}"

  install -Dm644 ${pkgname}-${pkgver}/COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

  # install -dm755 "${pkgdir}/usr/share/doc/${pkgname}/doxygen"

  find "${pkgdir}" -type d -empty -delete
}
```
[](https://fossies.org/diffs/dune-geometry)

[](https://gitlab.dune-project.org/core/dune-geometry/-/issues/28)
