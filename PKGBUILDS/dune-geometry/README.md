## [`dune-geometry`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-geometry/PKGBUILD)

#### debian

- [](https://tracker.debian.org/pkg/dune-geometry)
- [](https://salsa.debian.org/pjaap/dune-geometry)
- [](https://salsa.debian.org/lisajuliafog/dune-geometry)

```console
-- The following OPTIONAL packages have been found:
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * UnixCommands, Some common Unix commands
   To generate the documentation with LaTeX
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * BLAS, fast linear algebra routines
 * Threads, Multi-threading library
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
-- The following REQUIRED packages have been found:
 * dune-common
-- The following OPTIONAL packages have not been found:
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * QuadMath
```

```
.BUILDINFO
.MTREE
.PKGINFO
usr/
usr/include/
usr/include/dune/
usr/include/dune/geometry/
usr/include/dune/geometry/affinegeometry.hh
usr/include/dune/geometry/axisalignedcubegeometry.hh
usr/include/dune/geometry/deprecated_topology.hh
usr/include/dune/geometry/dimension.hh
usr/include/dune/geometry/generalvertexorder.hh
usr/include/dune/geometry/multilineargeometry.hh
usr/include/dune/geometry/quadraturerules/
usr/include/dune/geometry/quadraturerules.hh
usr/include/dune/geometry/quadraturerules/compositequadraturerule.hh
usr/include/dune/geometry/quadraturerules/gausslobattoquadrature.hh
usr/include/dune/geometry/quadraturerules/gaussquadrature.hh
usr/include/dune/geometry/quadraturerules/gaussradauleftquadrature.hh
usr/include/dune/geometry/quadraturerules/gaussradaurightquadrature.hh
usr/include/dune/geometry/quadraturerules/jacobi1quadrature.hh
usr/include/dune/geometry/quadraturerules/jacobi2quadrature.hh
usr/include/dune/geometry/quadraturerules/jacobiNquadrature.hh
usr/include/dune/geometry/quadraturerules/pointquadrature.hh
usr/include/dune/geometry/quadraturerules/prismquadrature.hh
usr/include/dune/geometry/quadraturerules/simplexquadrature.hh
usr/include/dune/geometry/quadraturerules/tensorproductquadrature.hh
usr/include/dune/geometry/referenceelement.hh
usr/include/dune/geometry/referenceelementimplementation.hh
usr/include/dune/geometry/referenceelements.hh
usr/include/dune/geometry/refinement/
usr/include/dune/geometry/refinement.hh
usr/include/dune/geometry/refinement/base.cc
usr/include/dune/geometry/refinement/hcube.cc
usr/include/dune/geometry/refinement/hcubetriangulation.cc
usr/include/dune/geometry/refinement/prismtriangulation.cc
usr/include/dune/geometry/refinement/pyramidtriangulation.cc
usr/include/dune/geometry/refinement/simplex.cc
usr/include/dune/geometry/test/
usr/include/dune/geometry/test/checkgeometry.hh
usr/include/dune/geometry/topologyfactory.hh
usr/include/dune/geometry/type.hh
usr/include/dune/geometry/typeindex.hh
usr/include/dune/geometry/utility/
usr/include/dune/geometry/utility/typefromvertexcount.hh
usr/include/dune/geometry/virtualrefinement.cc
usr/include/dune/geometry/virtualrefinement.hh
usr/include/dune/python/
usr/include/dune/python/geometry/
usr/include/dune/python/geometry/multilineargeometry.hh
usr/include/dune/python/geometry/quadraturerules.hh
usr/include/dune/python/geometry/referenceelements.hh
usr/include/dune/python/geometry/type.hh
usr/lib/
usr/lib/cmake/
usr/lib/cmake/dune-geometry/
usr/lib/cmake/dune-geometry/dune-geometry-config-version.cmake
usr/lib/cmake/dune-geometry/dune-geometry-config.cmake
usr/lib/cmake/dune-geometry/dune-geometry-targets-none.cmake
usr/lib/cmake/dune-geometry/dune-geometry-targets.cmake
usr/lib/dunecontrol/
usr/lib/dunecontrol/dune-geometry/
usr/lib/dunecontrol/dune-geometry/dune.module
usr/lib/libdunegeometry.so
usr/lib/pkgconfig/
usr/lib/pkgconfig/dune-geometry.pc
usr/python/
usr/python/dune/
usr/python/dune/geometry/
usr/python/dune/geometry/_geometry.so
usr/share/
usr/share/doc/
usr/share/doc/dune-geometry/
usr/share/doc/dune-geometry/appl/
usr/share/doc/dune-geometry/appl/refelements/
usr/share/doc/dune-geometry/appl/refelements/gg_hexahedron.png
usr/share/doc/dune-geometry/appl/refelements/gg_hexahedron_edges.png
usr/share/doc/dune-geometry/appl/refelements/gg_line.png
usr/share/doc/dune-geometry/appl/refelements/gg_prism.png
usr/share/doc/dune-geometry/appl/refelements/gg_prism_edges.png
usr/share/doc/dune-geometry/appl/refelements/gg_pyramid.png
usr/share/doc/dune-geometry/appl/refelements/gg_pyramid_edges.png
usr/share/doc/dune-geometry/appl/refelements/gg_quadrilateral.png
usr/share/doc/dune-geometry/appl/refelements/gg_tetrahedron.png
usr/share/doc/dune-geometry/appl/refelements/gg_tetrahedron_edges.png
usr/share/doc/dune-geometry/appl/refelements/gg_triangle.png
usr/share/doc/dune-geometry/doxygen/
usr/share/doc/dune-geometry/doxygen/index.html
usr/share/doc/dune-geometry/refinement.pdf
usr/share/dune-geometry/
usr/share/dune-geometry/config.h.cmake
usr/share/licenses/
usr/share/licenses/dune-geometry/
usr/share/licenses/dune-geometry/LICENSE
```

```
usr/
usr/lib/
usr/lib/python3.9/
usr/lib/python3.9/site-packages/
usr/lib/python3.9/site-packages/dune/
usr/lib/python3.9/site-packages/dune/geometry/
usr/lib/python3.9/site-packages/dune/geometry/__init__.py
usr/lib/python3.9/site-packages/dune/geometry/_geometry.so
usr/lib/python3.9/site-packages/dune/geometry/_referenceelements.py
usr/lib/python3.9/site-packages/dune/geometry/quadpy.py
usr/lib/python3.9/site-packages/dune_geometry-2.8.0-py3.9-nspkg.pth
usr/lib/python3.9/site-packages/dune_geometry-2.8.0-py3.9.egg-info/
usr/lib/python3.9/site-packages/dune_geometry-2.8.0-py3.9.egg-info/PKG-INFO
usr/lib/python3.9/site-packages/dune_geometry-2.8.0-py3.9.egg-info/SOURCES.txt
usr/lib/python3.9/site-packages/dune_geometry-2.8.0-py3.9.egg-info/dependency_links.txt
usr/lib/python3.9/site-packages/dune_geometry-2.8.0-py3.9.egg-info/namespace_packages.txt
usr/lib/python3.9/site-packages/dune_geometry-2.8.0-py3.9.egg-info/not-zip-safe
usr/lib/python3.9/site-packages/dune_geometry-2.8.0-py3.9.egg-info/top_level.txt
```

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-geometry.svg)](https://repology.org/project/dune-geometry/versions)
