## [`cminpack`](https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=cminpack)

[![Packaging status](https://repology.org/badge/vertical-allrepos/cminpack.svg)](https://repology.org/project/cminpack/versions)

[](https://git.rwth-aachen.de/avt-svt/public/thirdparty/cminpack)
