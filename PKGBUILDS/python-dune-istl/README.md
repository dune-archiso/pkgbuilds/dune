- [dune-istl 2.8.0.dev20210617](https://pypi.org/project/dune-istl)

```
_dunecontrol="./${_name}/bin/dunecontrol"
_setupdunepy="./${_name}/bin/setup-dunepy.py"

export PYTHONPATH="${srcdir}/${_name}/build-cmake/python:$PYTHONPATH"
export DUNE_CONTROL_PATH="${srcdir}/${_name}"
export BUILDDIR="${srcdir}/${_name}/build-cmake"
export DUNE_PY_DIR="${pkgdir}/usr/lib/python3.9/site-packages/dune-py"
```

```
pkgver() {
  cd "${srcdir}/${_name}"
  printf "%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}
```

```
${_dunecontrol} --opts=${pkgname}.opts cmake : make
```

```
$_dunecontrol --only=${_name} make install DESTDIR="${pkgdir}"
echo ${PYTHONPATH} ${DUNE_CONTROL_PATH} ${BUILDDIR} ${DUNE_PY_DIR}
$_dunecontrol --only=${_name} make install_python DESTDIR="${pkgdir}"

# ${_setupdunepy} --opts=${pkgname}.opts install --prefix=/usr --root="${pkgdir}" --optimize=1
# ${_setupdunepy} --opts=${pkgname}.opts --module=${_name} install DESTDIR="${pkgdir}"
```