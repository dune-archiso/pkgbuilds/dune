```console
-DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS=TRUE
```

```console
# export CPPFLAGS="-I/usr/include/tirpc ${CPPFLAGS}"
# export CFLAGS="-fPIC ${CFLAGS}"
# export CXXFLAGS="-fPIC ${CFLAGS}"
# export LDFLAGS="-ldl -lm -ltirpc"
```

[](https://gitlab.dune-project.org/staging/dune-python/-/blob/master/SOURCEINSTALL.md)

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-python
doxygen_install
headercheck
install_python
sphinx.developers.rst
sphinx.index.rst
sphinx.usage.rst
sphinx_doc_html
sphinx_files
test_python
_common
_geometry
_grid
_istl
_typeregistry
test_eval
```

[](https://gitlab.cc-asp.fraunhofer.de/users/cbauer/projects)

```
# Maintainer: anon at sansorgan.es
pkgname=dune-python
pkgname=('dune-python')
pkgver=2.7.1
pkgrel=1
pkgdesc='Python bindings for the DUNE core modules'
groups=('dune')
url='https://dune-project.org/modules/dune-python'

arch=('x86_64')
license=('LGPL3')

makedepends=('cmake' 'gcc-fortran' 'python-pandocfilters' 'python-scipy' 'python-matplotlib')

# export CPPFLAGS="-I/usr/include/tirpc ${CPPFLAGS}"
# export CFLAGS="-fPIC ${CFLAGS}"
# export CXXFLAGS="-fPIC ${CFLAGS}"

# export LDFLAGS="-ldl -lm -ltirpc"

source=("https://gitlab.dune-project.org/staging/${pkgname}/-/archive/releases/${pkgver: : 3}/${pkgname}-releases-${pkgver: : 3}.tar.gz")
sha256sums=('facc1aa8152d65e2cd7ec02a395e52dc71cade2fc5bb804faf08005b5eee7a0a')

package() {
	dunecontrol --only=${pkgname} make install DESTDIR="${pkgdir}"
	install -Dm644 ${pkgname}-releases-${pkgver: : 3}/COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
	find "${pkgdir}" -type d -empty -delete
}

build() {
	CMAKE_FLAGS='-DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=/usr/lib -fPIC -DBUILD_SHARED_LIBS:BOOL=OFF' \
		dunecontrol configure --enabled-shared
	dunecontrol make
}

# -DDUNE_ENABLE_PYTHONBINDINGS=ON -DBUILD_SHARED_LIBS=TRUE
```

```
WARNING: autodoc: failed to import class 'Generator' from module 'dune.generator.generator'; the following exception was raised:
Traceback (most recent call last):
  File "/usr/lib/python3.9/site-packages/sphinx/util/inspect.py", line 412, in safe_getattr
    return getattr(obj, name, *defargs)
AttributeError: module 'dune.generator.generator' has no attribute 'Generator'
The above exception was the direct cause of the following exception:
Traceback (most recent call last):
  File "/usr/lib/python3.9/site-packages/sphinx/ext/autodoc/importer.py", line 110, in import_object
    obj = attrgetter(obj, mangled_name)
  File "/usr/lib/python3.9/site-packages/sphinx/ext/autodoc/__init__.py", line 318, in get_attr
    return autodoc_attrgetter(self.env.app, obj, name, *defargs)
  File "/usr/lib/python3.9/site-packages/sphinx/ext/autodoc/__init__.py", line 2606, in autodoc_attrgetter
    return safe_getattr(obj, name, *defargs)
  File "/usr/lib/python3.9/site-packages/sphinx/util/inspect.py", line 428, in safe_getattr
    raise AttributeError(name) from exc
AttributeError: Generator
```
[](https://github.com/sphinx-doc/sphinx/issues/4025)
