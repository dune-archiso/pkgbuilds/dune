## [`dune-python`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-python/PKGBUILD)

```console
-- The following OPTIONAL packages have been found:
 * dune-istl
 * dune-uggrid
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonLibs
 * SuperLU, Supernodal LU
   Direct solver for linear system, based on LU decomposition
 * ARPACK, ARnoldi PACKage
   Solve large scale eigenvalue problems
 * ARPACKPP, ARPACK++
   C++ interface for ARPACK
 * Threads, Multi-threading library
 * SuiteSparse
 * ParMETIS
 * Alberta
 * Psurface
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * PythonInterp
-- The following REQUIRED packages have been found:
 * dune-common
 * dune-grid
 * dune-geometry
-- The following OPTIONAL packages have not been found:
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * AmiraMesh
```

<!-- [![Packaging status](
https://repology.org/badge/vertical-allrepos/dune-python.svg
)](https://repology.org/project/dune-python/versions) -->
