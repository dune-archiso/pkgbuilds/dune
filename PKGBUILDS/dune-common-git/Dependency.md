## Dependency

| Package description                                                     |     Package name     | Source path                                 | Arch |
| :---------------------------------------------------------------------- | :------------------: | :------------------------------------------ | ---: |
| Threading Building Blocks                                               |       `onetbb`       |                                             |  Yes |
| SIMD Vector Classes for C++                                             |         `vc`         | `dune/common/vc.hh`                         |  Yes |
| Serial Graph Partitioning, Fill-reducing Matrix Ordering                |       `metis`        | `dune/common/test/metistest.cc`             |  Yes |
| Graph and mesh/hypergraph partitioning, graph clustering, sparse matrix |       `scotch`       | `dune/common/test/metistest.cc`             |  Yes |
| High performance message passing library                                |      `openmpi`       | `dune/common/parallel/mpihelper.hh`         |  Yes |
| The GNU Compiler Collection - C and C++ frontends                       |        `gcc`         | `dune/common/classname.hh`                  |  Yes |
| Linear Algebra PACKage                                                  |       `lapack`       | `dune/common/fmatrixev.cc`                  |  Yes |
| Template library for constructing, operating on statically typed trees  |   `dune-typetree`    | `dune/python/common/dimrange.hh`            |  Yes |
| Parallel graph partitioning library                                     |      `parmetis`      | `dune/common/test/parmetistest.cc`          |  Yes |
| Implementations of AlbertaGrid, GeometryGrid, OneDGrid and YaspGrid     |     `dune-grid`      | `dune/python/common/numpycommdatahandle.hh` |  Yes |
| GNU C Library                                                           |       `glibc`        | `dune/common/debugallocator.hh`             |  Yes |
| Free library for arbitrary precision arithmetic                         |        `gmp`         | `dune/common/gmpfield.hh`                   |  Yes |
| Documentation system for C++, C, Java, IDL and PHP                      |      `doxygen`       | `doc/doxygen/CMakeLists.txt`                |  Yes |
| Easy, portable file locking API                                         | `python-portalocker` | `python/dune/common/locking.py`             |  Yes |
| Easily download, build, install, upgrade, and uninstall Python packages | `python-setuptools`  | `python/dune/packagemetadata.py`            |  Yes |
| Python plotting library, making publication quality plots               | `python-matplotlib`  | `python/dune/plotting.py`                   |  Yes |
| Set of tools for processing plaintext docs into formats                 |  `python-docutils`   | `cmake/scripts/sphinx_cmake_dune.py`        |  Yes |
| Cross-platform open-source make system                                  |       `cmake`        | `pyproject.toml`                            |  Yes |
| Python bindings for the Message Passing Interface                       |   `python-mpi4py`    | `python/dune/common/__init__.py`            |  Yes |
| The PyPA recommended tool for installing Python packages                |     `python-pip`     | `bin/dunepackaging.py`                      |  Yes |
| Improved build system generator for CPython C, C++, Cython and Fortran  |     `python-pip`     | `bin/dunepackaging.py`                      |  Yes |
| Package compiler and linker metadata toolkit                            |      `pkgconf`       | `bin/duneproject`                           |  Yes |
