# Maintainer: anon at sansorgan.es

_name=dune-common
pkgbase=${_name}-git
pkgname=(${pkgbase} python-${pkgbase})
# _gitcommit=5dcb1e2b96d99e758ad06894218b8ba786725e15
pkgver=2.10.r1.1b79c9e
pkgrel=1
#epoch=
pkgdesc="Infrastructure and foundation classes (git version)"
arch=(x86_64)
url="https://dune-project.org/modules/${_name}"
license=('BSD-3-Clause' 'custom:GPL2 with runtime exception')
# groups=('dune-core-git')
makedepends=(cmake texlive-latexextra biber doxygen graphviz python-sphinx python-setuptools openmpi git) # python-wheel python-pip
checkdepends=(scotch parmetis-git python-numpy python-mpi4py)
# vc
optdepends=('vc: C++ Vectorization library'
  'texlive-latexextra: Type setting system'
  'doxygen: Generate the class documentation from C++ sources'
  'graphviz: Graph visualization software'
  'inkscape: Conversion routines for generate PNG images'
  'bash-completion: Bash completions'
  'openmpi: for mpi support'
  'parmetis: Parallel Graph Partitioning and Fill-reducing Matrix Ordering'
  'scotch: Software package and libraries for graph, mesh and hypergraph partitioning, static mapping, and sparse matrix block ordering'
  'man-db: manual pages for dune'
  'python-fenics-ufl: for dunepackaging.py'
  'python-requests: for dunepackaging.py'
  'python-pip: for dunepackaging.py'
  'python-scipy: for dunepackaging.py'
  'python-scikit-build: for dunepackaging.py')
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=("git+https://github.com/dune-project/${_name}#branch=master") # commit=${_gitcommit} ${pkgbase}.opts
#noextract=()
sha512sums=('SKIP')

pkgver() {
  cd ${_name}
  printf "2.10.r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  # https://salsa.debian.org/science-team/dune-common/-/blob/master/debian/patches/skip-dirs-starting-with-dot.patch
  sed -i 's/^        $(find -H "$dir" -name $CONTROL | $GREP -v '\''dune-\[-_a-zA-Z\]\/dune-\[-a-zA-Z_\]\*-\[0-9\]\\{1,\\}.\[0-9\]\\{1,\\}\/'\'')/        $(find -H "$dir" -name '\''.?\*'\'' -prune -o -name $CONTROL -print | $GREP -v '\''dune-\[-_a-zA-Z\]\/dune-\[-a-zA-Z_\]\*-\[0-9\]\\{1,\\}.\[0-9\]\\{1,\\}\/'\'')/' ${_name}/lib/dunemodules.lib
  sed -i 's/^Version: '"${pkgver%%.r*}"'-git/Version: '"${pkgver%%.r*}"'/' "${_name}"/dune.module
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L96
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
  # export DUNE_PY_DIR="${BUILDDIR}/${pkgbase}"
  export DUNE_LOG_LEVEL=DEBUG
  export DUNE_SAVE_BUILD='console'
  # export DUNEPY_DISABLE_PLOTTING=1
  # export PATH="${srcdir}/${_name}/bin:${PATH}"
}
# -DDDUNE_OPTS_FILE=
# -DDUNE_PYTHON_INSTALL_LOCATION='none' \
# -DCMAKE_INSTALL_LIBDIR=/usr/lib \
# -DCMAKE_SKIP_RPATH=ON \
build() {
  cmake \
    -S ${_name} \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DDUNE_ENABLE_PYTHONBINDINGS=ON \
    -DDUNE_PYTHON_INSTALL_LOCATION="none" \
    -DDUNE_PYTHON_USE_VENV=OFF \
    -DDUNE_RUNNING_IN_CI=OFF \
    -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE \
    -Wno-dev

  # sed -i 's/^include-system-site-packages = false/include-system-site-packages = true/' build-cmake/dune-env/pyvenv.cfg
  # cat build-cmake/dune-env/pyvenv.cfg

  # python -m venv --system-site-packages build-cmake/dune-env
  # . build-cmake/dune-env/bin/activate
  cmake --build build-cmake --target all # help
  # ls build-cmake # source: filename argument required
  cd build-cmake/python
  python setup.py build
  # export DUNE_PYTHON_WHEELHOUSE="${PWD}"
  # dunecontrol --opts=${pkgbase}.opts cmake -Wno-dev : make
  # deactivate
}

# check() {
#   # cd build-cmake/python
#   # python -m venv --system-site-packages test-env
#   # test-env/bin/python setup.py install --skip-build
#   # . test-env/bin/activate
#   # source build-cmake/set-dune-pythonpath || true
#   # dunecontrol make build_tests # --opts=${pkgbase}.opts
#   cmake --build build-cmake --target build_tests
#   # cd ${srcdir}/${_name}/build-cmake
#   # DUNE_CONTROL_PATH="${srcdir}/${_name}" dune-ctest --verbose --output-on-failure # -E "pythontests"
#   # . build-cmake/set-dune-pythonpath
#   # PYTHONPATH="$PWD/build-cmake/python/build:$PYTHONPATH"
#   ctest -E "(metis*)" --verbose --output-on-failure --test-dir build-cmake
# }

package_dune-common-git() {
  depends=(cmake python-docutils openmpi lapack git)
  # https://gitlab.dune-project.org/core/dune-common/-/issues/265 tbb
  provides=("${_name}=${pkgver%%.r*}" 'dunecontrol' 'dune-ctest' 'dune-git-whitespace-hook' 'dunepackaging.py' 'duneproject' 'rmgenerated.py')
  conflicts=(${_name})
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install sphinx_html
  # DESTDIR="${pkgdir}" dunecontrol --only=${_name} make install sphinx_html
  mv "build-cmake/doc/buildsystem/html" "${pkgdir}/usr/share/doc/${_name}/buildsystem"
  install -Dm644 ${_name}/COPYING "${pkgdir}/usr/share/licenses/${_name}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}

package_python-dune-common-git() {
  depends=(dune-common-git python-setuptools python-portalocker python-mpi4py python-numpy python-jinja) # python-matplotlib python-mayavi
  pkgdesc+=" (python bindings)"
  conflicts+=(python-${_name})
  cd build-cmake/python
  PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
}
