```console
_setupdunepy="./${_name}/bin/setup-dunepy.py"
# PYTHONPATH="${srcdir}/${_name}/build-cmake/python" #/dune
# DUNE_CONTROL_PATH="${srcdir}/${_name}"
# BUILDDIR="${srcdir}/${_name}/build-cmake"
# DUNE_PY_DIR="${pkgdir}/usr/lib/python3.9/site-packages" #/dune
# echo ${PYTHONPATH} ${DUNE_CONTROL_PATH} ${BUILDDIR} ${DUNE_PY_DIR}
# ls
# pwd
# echo "Python path:"
# ls $PYTHONPATH
# echo "Dune control path:"
# ls $DUNE_CONTROL_PATH
# echo "Buildir path:"
# ls $BUILDDIR
# $_dunecontrol --only=${_name} make install_python DESTDIR="${DUNE_PY_DIR}"
# ${_setupdunepy} --opts=${pkgname}.opts install --prefix=/usr --root="${pkgdir}" --optimize=1
# ${_setupdunepy} --opts=${pkgname}.opts --module=${_name} install DESTDIR="${pkgdir}"
```

- [](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/900)
- [](https://wiki.archlinux.org/title/Python_package_guidelines#Reproducible_bytecode)

```console
-DDUNE_ENABLE_PYTHONBINDINGS=ON
-DDUNE_MAX_TEST_CORES=4
-DBUILD_SHARED_LIBS=TRUE
-DDUNE_PYTHON_INSTALL_LOCATION=none
-DCMAKE_POSITION_INDEPENDENT_CODE=TRUE
```

```console
-DDUNE_PYTHON_VIRTUALENV_SETUP=1
-DDUNE_PYTHON_VIRTUALENV_PATH=/tmp/dune-python-venv
-DADDITIONAL_PIP_PARAMS='-upgrade'
-DCMAKE_DISABLE_DOCUMENTATION=TRUE
```

```console
-DDUNE_ENABLE_PYTHONBINDINGS=1
-DCMAKE_POSITION_INDEPENDENT_CODE=1
```

```console
DUNE_CONTROL_PATH=/usr/include/dune/common:/usr/include/dune/geometry
DUNE_BUILDDIR=BUILDDIR_PATH
DUNE_PY_DIR=DUNEPY_PATH
```

```console
-DDUNE_ENABLE_PYTHONBINDINGS=ON
-DBUILD_SHARED_LIBS=TRUE
-DCMAKE_POSITION_INDEPENDENT_CODE=TRUE
-DPYTHON_INSTALL_LOCATION=none
-DADDITIONAL_PIP_PARAMS="-upgrade"
-DDUNE_PYTHON_VIRTUALENV_SETUP:BOOL=TRUE
-DDUNE_PYTHON_ALLOW_GET_PIP:BOOL=TRUE
```

```console
-DPYTHON_INSTALL_LOCATION=system
```

### Python dependencies

## [`dune-common-git`](https://gitlab.com/dune-archiso/dune-core-git/-/raw/main/dune-common-git/PKGBUILD)

When the package `dune-common-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `bin`
- [x] `include`
- [x] `lib`
- [ ] `share`
  - [x] `bash-completion`
  - [x] `doc`
  - [x] `dune`
  - [x] `dune-common`
  - [x] `licenses`
  - [ ] `man`

```console
usr/
├── bin
│   ├── dunecontrol
│   ├── dune-ctest
│   ├── dune-git-whitespace-hook
│   └── duneproject
├── include
│   └── dune
│       └── common
├── lib
│   ├── cmake
│   │   └── dune-common
│   ├── dunecontrol
│   │   └── dune-common
│   │       └── dune.module
│   ├── dunemodules.lib
│   ├── libdunecommon.a
│   └── pkgconfig
│       └── dune-common.pc
└── share
    ├── bash-completion
    │   └── completions
    │       └── dunecontrol
    ├── doc
    │   └── dune-common
    ├── dune
    │   └── cmake
    │       ├── modules
    │       └── scripts
    ├── dune-common
    │   ├── config.h.cmake
    │   └── doc
    │       └── doxygen
    ├── licenses
    │   └── dune-common
    └── man
        └── man1

31 directories, 1492 files
```

### Remarks

Found `doc/comm/communication.tex` for compile.

[.gitlab-ci.yml](https://gitlab.dune-project.org/core/dune-common/-/raw/master/.gitlab-ci.yml)

[Debian's patches](https://salsa.debian.org/science-team/dune-common/-/tree/master/debian/patches)

```
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
build_quick_tests
build_tests
clean_latex
doc
doc_comm_communication_tex
doc_comm_communication_tex_clean
doxyfile
doxygen_dune-common
doxygen_install
env_install_python__tmp_makepkg_dune-common-git_src_dune-common_python_.
headercheck
install_python
metadata_env_install_python__tmp_makepkg_dune-common-git_src_dune-common_python_.
sphinx_html
target_pythontests
test_python
_common
_typeregistry
alignedallocatortest
arithmetictestsuitetest
arraylisttest
arraytest
assertandreturntest
assertandreturntest_compiletime_fail
assertandreturntest_ndebug
autocopytest
bigunsignedinttest
bitsetvectortest
boundscheckingmvtest
boundscheckingoptest
boundscheckingtest
calloncetest
check_fvector_size
check_fvector_size_fail1
check_fvector_size_fail2
classnametest-demangled
classnametest-fallback
communicationtest
concept
constexprifelsetest
debugalignsimdtest
debugaligntest
densematrixassignmenttest
densematrixassignmenttest_fail0
densematrixassignmenttest_fail1
densematrixassignmenttest_fail2
densematrixassignmenttest_fail3
densematrixassignmenttest_fail4
densematrixassignmenttest_fail5
densematrixassignmenttest_fail6
densevectorassignmenttest
densevectortest
diagonalmatrixtest
dunecommon
dynmatrixtest
dynvectortest
eigenvaluestest
enumsettest
filledarraytest
fmatrixtest
functiontest
fvectorconversion1d
fvectortest
genericiterator_compile_fail
headercheck__doc_comm_buildindexset.hh
headercheck__doc_comm_reverse.hh
headercheck__dune_common_alignedallocator.hh
headercheck__dune_common_arraylist.hh
headercheck__dune_common_assertandreturn.hh
headercheck__dune_common_bartonnackmanifcheck.hh
headercheck__dune_common_bigunsignedint.hh
headercheck__dune_common_binaryfunctions.hh
headercheck__dune_common_bitsetvector.hh
headercheck__dune_common_boundschecking.hh
headercheck__dune_common_classname.hh
headercheck__dune_common_concept.hh
headercheck__dune_common_conditional.hh
headercheck__dune_common_debugalign.hh
headercheck__dune_common_debugallocator.hh
headercheck__dune_common_debugstream.hh
headercheck__dune_common_densematrix.hh
headercheck__dune_common_densevector.hh
headercheck__dune_common_deprecated.hh
headercheck__dune_common_diagonalmatrix.hh
headercheck__dune_common_documentation.hh
headercheck__dune_common_dotproduct.hh
headercheck__dune_common_dynmatrix.hh
headercheck__dune_common_dynmatrixev.hh
headercheck__dune_common_dynvector.hh
headercheck__dune_common_enumset.hh
headercheck__dune_common_exceptions.hh
headercheck__dune_common_filledarray.hh
headercheck__dune_common_float_cmp.hh
headercheck__dune_common_fmatrix.hh
headercheck__dune_common_fmatrixev.hh
headercheck__dune_common_ftraits.hh
headercheck__dune_common_function.hh
headercheck__dune_common_fvector.hh
headercheck__dune_common_gcd.hh
headercheck__dune_common_genericiterator.hh
headercheck__dune_common_gmpfield.hh
headercheck__dune_common_hash.hh
headercheck__dune_common_hybridutilities.hh
headercheck__dune_common_indent.hh
headercheck__dune_common_indices.hh
headercheck__dune_common_interfaces.hh
headercheck__dune_common_ios_state.hh
headercheck__dune_common_iteratorfacades.hh
headercheck__dune_common_iteratorrange.hh
headercheck__dune_common_keywords.hh
headercheck__dune_common_lcm.hh
headercheck__dune_common_lru.hh
headercheck__dune_common_mallocallocator.hh
headercheck__dune_common_math.hh
headercheck__dune_common_matvectraits.hh
headercheck__dune_common_overloadset.hh
headercheck__dune_common_parallel_collectivecommunication.hh
headercheck__dune_common_parallel_communication.hh
headercheck__dune_common_parallel_communicator.hh
headercheck__dune_common_parallel_future.hh
headercheck__dune_common_parallel_indexset.hh
headercheck__dune_common_parallel_indicessyncer.hh
headercheck__dune_common_parallel_interface.hh
headercheck__dune_common_parallel_localindex.hh
headercheck__dune_common_parallel_mpicollectivecommunication.hh
headercheck__dune_common_parallel_mpicommunication.hh
headercheck__dune_common_parallel_mpidata.hh
headercheck__dune_common_parallel_mpifuture.hh
headercheck__dune_common_parallel_mpiguard.hh
headercheck__dune_common_parallel_mpihelper.hh
headercheck__dune_common_parallel_mpipack.hh
headercheck__dune_common_parallel_mpitraits.hh
headercheck__dune_common_parallel_plocalindex.hh
headercheck__dune_common_parallel_remoteindices.hh
headercheck__dune_common_parallel_selection.hh
headercheck__dune_common_parallel_variablesizecommunicator.hh
headercheck__dune_common_parameterizedobject.hh
headercheck__dune_common_parametertree.hh
headercheck__dune_common_parametertreeparser.hh
headercheck__dune_common_path.hh
headercheck__dune_common_poolallocator.hh
headercheck__dune_common_power.hh
headercheck__dune_common_precision.hh
headercheck__dune_common_promotiontraits.hh
headercheck__dune_common_propertymap.hh
headercheck__dune_common_proxymemberaccess.hh
headercheck__dune_common_quadmath.hh
headercheck__dune_common_rangeutilities.hh
headercheck__dune_common_reservedvector.hh
headercheck__dune_common_scalarmatrixview.hh
headercheck__dune_common_scalarvectorview.hh
headercheck__dune_common_shared_ptr.hh
headercheck__dune_common_simd.hh
headercheck__dune_common_singleton.hh
headercheck__dune_common_sllist.hh
headercheck__dune_common_std_apply.hh
headercheck__dune_common_std_functional.hh
headercheck__dune_common_std_make_array.hh
headercheck__dune_common_std_optional.hh
headercheck__dune_common_std_type_traits.hh
headercheck__dune_common_std_utility.hh
headercheck__dune_common_std_variant.hh
headercheck__dune_common_stdstreams.hh
headercheck__dune_common_stdthread.hh
headercheck__dune_common_streamoperators.hh
headercheck__dune_common_stringutility.hh
headercheck__dune_common_test_arithmetictestsuite.hh
headercheck__dune_common_test_checkmatrixinterface.hh
headercheck__dune_common_test_collectorstream.hh
headercheck__dune_common_test_dummyiterator.hh
headercheck__dune_common_test_iteratorfacadetest.hh
headercheck__dune_common_test_iteratortest.hh
headercheck__dune_common_test_parameterizedobjectfactorysingleton.hh
headercheck__dune_common_test_testsuite.hh
headercheck__dune_common_timer.hh
headercheck__dune_common_to_unique_ptr.hh
headercheck__dune_common_transpose.hh
headercheck__dune_common_tupleutility.hh
headercheck__dune_common_tuplevector.hh
headercheck__dune_common_typelist.hh
headercheck__dune_common_typetraits.hh
headercheck__dune_common_typeutilities.hh
headercheck__dune_common_unused.hh
headercheck__dune_common_vc.hh
headercheck__dune_common_version.hh
headercheck__dune_common_visibility.hh
headercheck__dune_python_common_densematrix.hh
headercheck__dune_python_common_densevector.hh
headercheck__dune_python_common_dimrange.hh
headercheck__dune_python_common_dynmatrix.hh
headercheck__dune_python_common_dynvector.hh
headercheck__dune_python_common_fmatrix.hh
headercheck__dune_python_common_fvecmatregistry.hh
headercheck__dune_python_common_fvector.hh
headercheck__dune_python_common_getdimension.hh
headercheck__dune_python_common_logger.hh
headercheck__dune_python_common_mpihelper.hh
headercheck__dune_python_common_numpycommdatahandle.hh
headercheck__dune_python_common_numpyvector.hh
headercheck__dune_python_common_pythonvector.hh
headercheck__dune_python_common_string.hh
headercheck__dune_python_common_typeregistry.hh
headercheck__dune_python_common_vector.hh
hybridutilitiestest
indexsettest
indicestest
iscallabletest
iteratorfacadetest
iteratorfacadetest2
looptest
lrutest
mathclassifierstest
mathtest
metistest
mpi_collective_benchmark
mpicommunicationtest
mpidatatest
mpifuturetest
mpigatherscattertest
mpiguardtest
mpihelpertest
mpihelpertest2
mpipacktest
overloadsettest
parameterizedobjecttest
parametertreelocaletest
parametertreetest
parmetistest
pathtest
poolallocatortest
powertest
quadmathtest
rangeutilitiestest
remoteindicestest
reservedvectortest
scotchtest
selectiontest
shared_ptrtest
singletontest
sllisttest
standardtest
stdapplytest
stdidentity
streamoperatorstest
streamtest
stringutilitytest
syncertest
test_embed1
test_embed2
testdebugallocator
testdebugallocator_fail1
testdebugallocator_fail2
testdebugallocator_fail3
testdebugallocator_fail4
testdebugallocator_fail5
testfloatcmp
transposetest
tupleutilitytest
typelisttest
typeutilitytest
utilitytest
variablesizecommunicatortest
vcarraytest
vcexpectedimpltest
vcvectortest
versiontest
headercheck/doc/comm/buildindexset.hh.o
headercheck/doc/comm/buildindexset.hh.i
headercheck/doc/comm/buildindexset.hh.s
headercheck/doc/comm/reverse.hh.o
headercheck/doc/comm/reverse.hh.i
headercheck/doc/comm/reverse.hh.s
headercheck/dune/common/alignedallocator.hh.o
headercheck/dune/common/alignedallocator.hh.i
headercheck/dune/common/alignedallocator.hh.s
headercheck/dune/common/arraylist.hh.o
headercheck/dune/common/arraylist.hh.i
headercheck/dune/common/arraylist.hh.s
headercheck/dune/common/assertandreturn.hh.o
headercheck/dune/common/assertandreturn.hh.i
headercheck/dune/common/assertandreturn.hh.s
headercheck/dune/common/bartonnackmanifcheck.hh.o
headercheck/dune/common/bartonnackmanifcheck.hh.i
headercheck/dune/common/bartonnackmanifcheck.hh.s
headercheck/dune/common/bigunsignedint.hh.o
headercheck/dune/common/bigunsignedint.hh.i
headercheck/dune/common/bigunsignedint.hh.s
headercheck/dune/common/binaryfunctions.hh.o
headercheck/dune/common/binaryfunctions.hh.i
headercheck/dune/common/binaryfunctions.hh.s
headercheck/dune/common/bitsetvector.hh.o
headercheck/dune/common/bitsetvector.hh.i
headercheck/dune/common/bitsetvector.hh.s
headercheck/dune/common/boundschecking.hh.o
headercheck/dune/common/boundschecking.hh.i
headercheck/dune/common/boundschecking.hh.s
headercheck/dune/common/classname.hh.o
headercheck/dune/common/classname.hh.i
headercheck/dune/common/classname.hh.s
headercheck/dune/common/concept.hh.o
headercheck/dune/common/concept.hh.i
headercheck/dune/common/concept.hh.s
headercheck/dune/common/conditional.hh.o
headercheck/dune/common/conditional.hh.i
headercheck/dune/common/conditional.hh.s
headercheck/dune/common/debugalign.hh.o
headercheck/dune/common/debugalign.hh.i
headercheck/dune/common/debugalign.hh.s
headercheck/dune/common/debugallocator.hh.o
headercheck/dune/common/debugallocator.hh.i
headercheck/dune/common/debugallocator.hh.s
headercheck/dune/common/debugstream.hh.o
headercheck/dune/common/debugstream.hh.i
headercheck/dune/common/debugstream.hh.s
headercheck/dune/common/densematrix.hh.o
headercheck/dune/common/densematrix.hh.i
headercheck/dune/common/densematrix.hh.s
headercheck/dune/common/densevector.hh.o
headercheck/dune/common/densevector.hh.i
headercheck/dune/common/densevector.hh.s
headercheck/dune/common/deprecated.hh.o
headercheck/dune/common/deprecated.hh.i
headercheck/dune/common/deprecated.hh.s
headercheck/dune/common/diagonalmatrix.hh.o
headercheck/dune/common/diagonalmatrix.hh.i
headercheck/dune/common/diagonalmatrix.hh.s
headercheck/dune/common/documentation.hh.o
headercheck/dune/common/documentation.hh.i
headercheck/dune/common/documentation.hh.s
headercheck/dune/common/dotproduct.hh.o
headercheck/dune/common/dotproduct.hh.i
headercheck/dune/common/dotproduct.hh.s
headercheck/dune/common/dynmatrix.hh.o
headercheck/dune/common/dynmatrix.hh.i
headercheck/dune/common/dynmatrix.hh.s
headercheck/dune/common/dynmatrixev.hh.o
headercheck/dune/common/dynmatrixev.hh.i
headercheck/dune/common/dynmatrixev.hh.s
headercheck/dune/common/dynvector.hh.o
headercheck/dune/common/dynvector.hh.i
headercheck/dune/common/dynvector.hh.s
headercheck/dune/common/enumset.hh.o
headercheck/dune/common/enumset.hh.i
headercheck/dune/common/enumset.hh.s
headercheck/dune/common/exceptions.hh.o
headercheck/dune/common/exceptions.hh.i
headercheck/dune/common/exceptions.hh.s
headercheck/dune/common/filledarray.hh.o
headercheck/dune/common/filledarray.hh.i
headercheck/dune/common/filledarray.hh.s
headercheck/dune/common/float_cmp.hh.o
headercheck/dune/common/float_cmp.hh.i
headercheck/dune/common/float_cmp.hh.s
headercheck/dune/common/fmatrix.hh.o
headercheck/dune/common/fmatrix.hh.i
headercheck/dune/common/fmatrix.hh.s
headercheck/dune/common/fmatrixev.hh.o
headercheck/dune/common/fmatrixev.hh.i
headercheck/dune/common/fmatrixev.hh.s
headercheck/dune/common/ftraits.hh.o
headercheck/dune/common/ftraits.hh.i
headercheck/dune/common/ftraits.hh.s
headercheck/dune/common/function.hh.o
headercheck/dune/common/function.hh.i
headercheck/dune/common/function.hh.s
headercheck/dune/common/fvector.hh.o
headercheck/dune/common/fvector.hh.i
headercheck/dune/common/fvector.hh.s
headercheck/dune/common/gcd.hh.o
headercheck/dune/common/gcd.hh.i
headercheck/dune/common/gcd.hh.s
headercheck/dune/common/genericiterator.hh.o
headercheck/dune/common/genericiterator.hh.i
headercheck/dune/common/genericiterator.hh.s
headercheck/dune/common/gmpfield.hh.o
headercheck/dune/common/gmpfield.hh.i
headercheck/dune/common/gmpfield.hh.s
headercheck/dune/common/hash.hh.o
headercheck/dune/common/hash.hh.i
headercheck/dune/common/hash.hh.s
headercheck/dune/common/hybridutilities.hh.o
headercheck/dune/common/hybridutilities.hh.i
headercheck/dune/common/hybridutilities.hh.s
headercheck/dune/common/indent.hh.o
headercheck/dune/common/indent.hh.i
headercheck/dune/common/indent.hh.s
headercheck/dune/common/indices.hh.o
headercheck/dune/common/indices.hh.i
headercheck/dune/common/indices.hh.s
headercheck/dune/common/interfaces.hh.o
headercheck/dune/common/interfaces.hh.i
headercheck/dune/common/interfaces.hh.s
headercheck/dune/common/ios_state.hh.o
headercheck/dune/common/ios_state.hh.i
headercheck/dune/common/ios_state.hh.s
headercheck/dune/common/iteratorfacades.hh.o
headercheck/dune/common/iteratorfacades.hh.i
headercheck/dune/common/iteratorfacades.hh.s
headercheck/dune/common/iteratorrange.hh.o
headercheck/dune/common/iteratorrange.hh.i
headercheck/dune/common/iteratorrange.hh.s
headercheck/dune/common/keywords.hh.o
headercheck/dune/common/keywords.hh.i
headercheck/dune/common/keywords.hh.s
headercheck/dune/common/lcm.hh.o
headercheck/dune/common/lcm.hh.i
headercheck/dune/common/lcm.hh.s
headercheck/dune/common/lru.hh.o
headercheck/dune/common/lru.hh.i
headercheck/dune/common/lru.hh.s
headercheck/dune/common/mallocallocator.hh.o
headercheck/dune/common/mallocallocator.hh.i
headercheck/dune/common/mallocallocator.hh.s
headercheck/dune/common/math.hh.o
headercheck/dune/common/math.hh.i
headercheck/dune/common/math.hh.s
headercheck/dune/common/matvectraits.hh.o
headercheck/dune/common/matvectraits.hh.i
headercheck/dune/common/matvectraits.hh.s
headercheck/dune/common/overloadset.hh.o
headercheck/dune/common/overloadset.hh.i
headercheck/dune/common/overloadset.hh.s
headercheck/dune/common/parallel/collectivecommunication.hh.o
headercheck/dune/common/parallel/collectivecommunication.hh.i
headercheck/dune/common/parallel/collectivecommunication.hh.s
headercheck/dune/common/parallel/communication.hh.o
headercheck/dune/common/parallel/communication.hh.i
headercheck/dune/common/parallel/communication.hh.s
headercheck/dune/common/parallel/communicator.hh.o
headercheck/dune/common/parallel/communicator.hh.i
headercheck/dune/common/parallel/communicator.hh.s
headercheck/dune/common/parallel/future.hh.o
headercheck/dune/common/parallel/future.hh.i
headercheck/dune/common/parallel/future.hh.s
headercheck/dune/common/parallel/indexset.hh.o
headercheck/dune/common/parallel/indexset.hh.i
headercheck/dune/common/parallel/indexset.hh.s
headercheck/dune/common/parallel/indicessyncer.hh.o
headercheck/dune/common/parallel/indicessyncer.hh.i
headercheck/dune/common/parallel/indicessyncer.hh.s
headercheck/dune/common/parallel/interface.hh.o
headercheck/dune/common/parallel/interface.hh.i
headercheck/dune/common/parallel/interface.hh.s
headercheck/dune/common/parallel/localindex.hh.o
headercheck/dune/common/parallel/localindex.hh.i
headercheck/dune/common/parallel/localindex.hh.s
headercheck/dune/common/parallel/mpicollectivecommunication.hh.o
headercheck/dune/common/parallel/mpicollectivecommunication.hh.i
headercheck/dune/common/parallel/mpicollectivecommunication.hh.s
headercheck/dune/common/parallel/mpicommunication.hh.o
headercheck/dune/common/parallel/mpicommunication.hh.i
headercheck/dune/common/parallel/mpicommunication.hh.s
headercheck/dune/common/parallel/mpidata.hh.o
headercheck/dune/common/parallel/mpidata.hh.i
headercheck/dune/common/parallel/mpidata.hh.s
headercheck/dune/common/parallel/mpifuture.hh.o
headercheck/dune/common/parallel/mpifuture.hh.i
headercheck/dune/common/parallel/mpifuture.hh.s
headercheck/dune/common/parallel/mpiguard.hh.o
headercheck/dune/common/parallel/mpiguard.hh.i
headercheck/dune/common/parallel/mpiguard.hh.s
headercheck/dune/common/parallel/mpihelper.hh.o
headercheck/dune/common/parallel/mpihelper.hh.i
headercheck/dune/common/parallel/mpihelper.hh.s
headercheck/dune/common/parallel/mpipack.hh.o
headercheck/dune/common/parallel/mpipack.hh.i
headercheck/dune/common/parallel/mpipack.hh.s
headercheck/dune/common/parallel/mpitraits.hh.o
headercheck/dune/common/parallel/mpitraits.hh.i
headercheck/dune/common/parallel/mpitraits.hh.s
headercheck/dune/common/parallel/plocalindex.hh.o
headercheck/dune/common/parallel/plocalindex.hh.i
headercheck/dune/common/parallel/plocalindex.hh.s
headercheck/dune/common/parallel/remoteindices.hh.o
headercheck/dune/common/parallel/remoteindices.hh.i
headercheck/dune/common/parallel/remoteindices.hh.s
headercheck/dune/common/parallel/selection.hh.o
headercheck/dune/common/parallel/selection.hh.i
headercheck/dune/common/parallel/selection.hh.s
headercheck/dune/common/parallel/variablesizecommunicator.hh.o
headercheck/dune/common/parallel/variablesizecommunicator.hh.i
headercheck/dune/common/parallel/variablesizecommunicator.hh.s
headercheck/dune/common/parameterizedobject.hh.o
headercheck/dune/common/parameterizedobject.hh.i
headercheck/dune/common/parameterizedobject.hh.s
headercheck/dune/common/parametertree.hh.o
headercheck/dune/common/parametertree.hh.i
headercheck/dune/common/parametertree.hh.s
headercheck/dune/common/parametertreeparser.hh.o
headercheck/dune/common/parametertreeparser.hh.i
headercheck/dune/common/parametertreeparser.hh.s
headercheck/dune/common/path.hh.o
headercheck/dune/common/path.hh.i
headercheck/dune/common/path.hh.s
headercheck/dune/common/poolallocator.hh.o
headercheck/dune/common/poolallocator.hh.i
headercheck/dune/common/poolallocator.hh.s
headercheck/dune/common/power.hh.o
headercheck/dune/common/power.hh.i
headercheck/dune/common/power.hh.s
headercheck/dune/common/precision.hh.o
headercheck/dune/common/precision.hh.i
headercheck/dune/common/precision.hh.s
headercheck/dune/common/promotiontraits.hh.o
headercheck/dune/common/promotiontraits.hh.i
headercheck/dune/common/promotiontraits.hh.s
headercheck/dune/common/propertymap.hh.o
headercheck/dune/common/propertymap.hh.i
headercheck/dune/common/propertymap.hh.s
headercheck/dune/common/proxymemberaccess.hh.o
headercheck/dune/common/proxymemberaccess.hh.i
headercheck/dune/common/proxymemberaccess.hh.s
headercheck/dune/common/quadmath.hh.o
headercheck/dune/common/quadmath.hh.i
headercheck/dune/common/quadmath.hh.s
headercheck/dune/common/rangeutilities.hh.o
headercheck/dune/common/rangeutilities.hh.i
headercheck/dune/common/rangeutilities.hh.s
headercheck/dune/common/reservedvector.hh.o
headercheck/dune/common/reservedvector.hh.i
headercheck/dune/common/reservedvector.hh.s
headercheck/dune/common/scalarmatrixview.hh.o
headercheck/dune/common/scalarmatrixview.hh.i
headercheck/dune/common/scalarmatrixview.hh.s
headercheck/dune/common/scalarvectorview.hh.o
headercheck/dune/common/scalarvectorview.hh.i
headercheck/dune/common/scalarvectorview.hh.s
headercheck/dune/common/shared_ptr.hh.o
headercheck/dune/common/shared_ptr.hh.i
headercheck/dune/common/shared_ptr.hh.s
headercheck/dune/common/simd.hh.o
headercheck/dune/common/simd.hh.i
headercheck/dune/common/simd.hh.s
headercheck/dune/common/singleton.hh.o
headercheck/dune/common/singleton.hh.i
headercheck/dune/common/singleton.hh.s
headercheck/dune/common/sllist.hh.o
headercheck/dune/common/sllist.hh.i
headercheck/dune/common/sllist.hh.s
headercheck/dune/common/std/apply.hh.o
headercheck/dune/common/std/apply.hh.i
headercheck/dune/common/std/apply.hh.s
headercheck/dune/common/std/functional.hh.o
headercheck/dune/common/std/functional.hh.i
headercheck/dune/common/std/functional.hh.s
headercheck/dune/common/std/make_array.hh.o
headercheck/dune/common/std/make_array.hh.i
headercheck/dune/common/std/make_array.hh.s
headercheck/dune/common/std/optional.hh.o
headercheck/dune/common/std/optional.hh.i
headercheck/dune/common/std/optional.hh.s
headercheck/dune/common/std/type_traits.hh.o
headercheck/dune/common/std/type_traits.hh.i
headercheck/dune/common/std/type_traits.hh.s
headercheck/dune/common/std/utility.hh.o
headercheck/dune/common/std/utility.hh.i
headercheck/dune/common/std/utility.hh.s
headercheck/dune/common/std/variant.hh.o
headercheck/dune/common/std/variant.hh.i
headercheck/dune/common/std/variant.hh.s
headercheck/dune/common/stdstreams.hh.o
headercheck/dune/common/stdstreams.hh.i
headercheck/dune/common/stdstreams.hh.s
headercheck/dune/common/stdthread.hh.o
headercheck/dune/common/stdthread.hh.i
headercheck/dune/common/stdthread.hh.s
headercheck/dune/common/streamoperators.hh.o
headercheck/dune/common/streamoperators.hh.i
headercheck/dune/common/streamoperators.hh.s
headercheck/dune/common/stringutility.hh.o
headercheck/dune/common/stringutility.hh.i
headercheck/dune/common/stringutility.hh.s
headercheck/dune/common/test/arithmetictestsuite.hh.o
headercheck/dune/common/test/arithmetictestsuite.hh.i
headercheck/dune/common/test/arithmetictestsuite.hh.s
headercheck/dune/common/test/checkmatrixinterface.hh.o
headercheck/dune/common/test/checkmatrixinterface.hh.i
headercheck/dune/common/test/checkmatrixinterface.hh.s
headercheck/dune/common/test/collectorstream.hh.o
headercheck/dune/common/test/collectorstream.hh.i
headercheck/dune/common/test/collectorstream.hh.s
headercheck/dune/common/test/dummyiterator.hh.o
headercheck/dune/common/test/dummyiterator.hh.i
headercheck/dune/common/test/dummyiterator.hh.s
headercheck/dune/common/test/iteratorfacadetest.hh.o
headercheck/dune/common/test/iteratorfacadetest.hh.i
headercheck/dune/common/test/iteratorfacadetest.hh.s
headercheck/dune/common/test/iteratortest.hh.o
headercheck/dune/common/test/iteratortest.hh.i
headercheck/dune/common/test/iteratortest.hh.s
headercheck/dune/common/test/parameterizedobjectfactorysingleton.hh.o
headercheck/dune/common/test/parameterizedobjectfactorysingleton.hh.i
headercheck/dune/common/test/parameterizedobjectfactorysingleton.hh.s
headercheck/dune/common/test/testsuite.hh.o
headercheck/dune/common/test/testsuite.hh.i
headercheck/dune/common/test/testsuite.hh.s
headercheck/dune/common/timer.hh.o
headercheck/dune/common/timer.hh.i
headercheck/dune/common/timer.hh.s
headercheck/dune/common/to_unique_ptr.hh.o
headercheck/dune/common/to_unique_ptr.hh.i
headercheck/dune/common/to_unique_ptr.hh.s
headercheck/dune/common/transpose.hh.o
headercheck/dune/common/transpose.hh.i
headercheck/dune/common/transpose.hh.s
headercheck/dune/common/tupleutility.hh.o
headercheck/dune/common/tupleutility.hh.i
headercheck/dune/common/tupleutility.hh.s
headercheck/dune/common/tuplevector.hh.o
headercheck/dune/common/tuplevector.hh.i
headercheck/dune/common/tuplevector.hh.s
headercheck/dune/common/typelist.hh.o
headercheck/dune/common/typelist.hh.i
headercheck/dune/common/typelist.hh.s
headercheck/dune/common/typetraits.hh.o
headercheck/dune/common/typetraits.hh.i
headercheck/dune/common/typetraits.hh.s
headercheck/dune/common/typeutilities.hh.o
headercheck/dune/common/typeutilities.hh.i
headercheck/dune/common/typeutilities.hh.s
headercheck/dune/common/unused.hh.o
headercheck/dune/common/unused.hh.i
headercheck/dune/common/unused.hh.s
headercheck/dune/common/vc.hh.o
headercheck/dune/common/vc.hh.i
headercheck/dune/common/vc.hh.s
headercheck/dune/common/version.hh.o
headercheck/dune/common/version.hh.i
headercheck/dune/common/version.hh.s
headercheck/dune/common/visibility.hh.o
headercheck/dune/common/visibility.hh.i
headercheck/dune/common/visibility.hh.s
headercheck/dune/python/common/densematrix.hh.o
headercheck/dune/python/common/densematrix.hh.i
headercheck/dune/python/common/densematrix.hh.s
headercheck/dune/python/common/densevector.hh.o
headercheck/dune/python/common/densevector.hh.i
headercheck/dune/python/common/densevector.hh.s
headercheck/dune/python/common/dimrange.hh.o
headercheck/dune/python/common/dimrange.hh.i
headercheck/dune/python/common/dimrange.hh.s
headercheck/dune/python/common/dynmatrix.hh.o
headercheck/dune/python/common/dynmatrix.hh.i
headercheck/dune/python/common/dynmatrix.hh.s
headercheck/dune/python/common/dynvector.hh.o
headercheck/dune/python/common/dynvector.hh.i
headercheck/dune/python/common/dynvector.hh.s
headercheck/dune/python/common/fmatrix.hh.o
headercheck/dune/python/common/fmatrix.hh.i
headercheck/dune/python/common/fmatrix.hh.s
headercheck/dune/python/common/fvecmatregistry.hh.o
headercheck/dune/python/common/fvecmatregistry.hh.i
headercheck/dune/python/common/fvecmatregistry.hh.s
headercheck/dune/python/common/fvector.hh.o
headercheck/dune/python/common/fvector.hh.i
headercheck/dune/python/common/fvector.hh.s
headercheck/dune/python/common/getdimension.hh.o
headercheck/dune/python/common/getdimension.hh.i
headercheck/dune/python/common/getdimension.hh.s
headercheck/dune/python/common/logger.hh.o
headercheck/dune/python/common/logger.hh.i
headercheck/dune/python/common/logger.hh.s
headercheck/dune/python/common/mpihelper.hh.o
headercheck/dune/python/common/mpihelper.hh.i
headercheck/dune/python/common/mpihelper.hh.s
headercheck/dune/python/common/numpycommdatahandle.hh.o
headercheck/dune/python/common/numpycommdatahandle.hh.i
headercheck/dune/python/common/numpycommdatahandle.hh.s
headercheck/dune/python/common/numpyvector.hh.o
headercheck/dune/python/common/numpyvector.hh.i
headercheck/dune/python/common/numpyvector.hh.s
headercheck/dune/python/common/pythonvector.hh.o
headercheck/dune/python/common/pythonvector.hh.i
headercheck/dune/python/common/pythonvector.hh.s
headercheck/dune/python/common/string.hh.o
headercheck/dune/python/common/string.hh.i
headercheck/dune/python/common/string.hh.s
headercheck/dune/python/common/typeregistry.hh.o
headercheck/dune/python/common/typeregistry.hh.i
headercheck/dune/python/common/typeregistry.hh.s
headercheck/dune/python/common/vector.hh.o
headercheck/dune/python/common/vector.hh.i
headercheck/dune/python/common/vector.hh.s
```

[When is it necessary to use the flag -stdlib=libstdc++?](https://stackoverflow.com/a/19774902/9302545)

```
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE -DDUNE_ENABLE_PYTHONBINDINGS=TRUE -DDUNE_PYTHON_INSTALL_LOCATION=none -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen=TRUE -DINKSCAPE=FALSE -DDUNE_MAX_TEST_CORES=4 -DDUNE_ENABLE_PYTHONBINDINGS=ON -DDUNE_PYTHON_ADDITIONAL_PIP_PARAMS=--index-url=https://gitlab.dune-project.org/api/v4/projects/133/packages/pypi/simple;--no-build-isolation;-v -Ddune-common_DIR=/duneci/modules/dune-common/build-cmake /builds/infrastructure/dune-nightly-test/modules/dune-env/.cache/dune-py"
```

[](https://en.cppreference.com/w/cpp/experimental/make_array)
[](https://unix.stackexchange.com/q/552257/292992)
[](https://man.archlinux.org/man/mmap.2.en)
[](https://man.archlinux.org/man/mprotect.2.en)
[](https://gitlab.dune-project.org/core/dune-common/-/issues/164)

```
        Start 115: test_embed1
115: Test command: /tmp/makepkg/dune-common-git/src/dune-common/build-cmake/dune/python/test/test_embed1
115: Test timeout computed to be: 300
115: terminate called after throwing an instance of 'pybind11::error_already_set'
115:   what():  ModuleNotFoundError: No module named 'dune'
115/115 Test #115: test_embed1 ............................Subprocess aborted***Exception:   0.38 sec
terminate called after throwing an instance of 'pybind11::error_already_set'
  what():  ModuleNotFoundError: No module named 'dune'
99% tests passed, 1 tests failed out of 115
```

```
# setup-dunepy.py --module=dune-common
```

```
-DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE
```
```
+ python -c 'import dune.common;'
Traceback (most recent call last):
  File "/usr/lib/python3.10/site-packages/dune/common/__init__.py", line 26, in <module>
    from mpi4py import MPI
ModuleNotFoundError: No module named 'mpi4py'
During handling of the above exception, another exception occurred:
Traceback (most recent call last):
  File "<string>", line 1, in <module>
  File "/usr/lib/python3.10/site-packages/dune/common/__init__.py", line 31, in <module>
    raise RuntimeError(textwrap.dedent("""
RuntimeError: 
The Dune modules were configured using MPI. For the Python bindings to work,
the Python package 'mpi4py' is required.
Please run
    pip install mpi4py
before rerunning your Dune script.
```

```
sed -i 's/^              COMMAND/              #COMMAND/' ${_name}/dune/python/test/CMakeLists.txt
sed -i 's/^              CMD_ARGS/              #CMD_ARGS/' ${_name}/dune/python/test/CMakeLists.txt
sed -i '10 a configure_file(setup.py.in setup.py)' ${_name}/python/CMakeLists.txt
```

```bash
#!/usr/sbin/bash
MODULE_PATHS="/tmp/makepkg/dune-common-git/src/build-cmake/python"
# for all module paths: check if path is contain in PYTHONPATH otherwise add
for MODPATH in $MODULE_PATHS; do 
  MODULEFOUND=`echo $PYTHONPATH | grep $MODPATH` 
  if [ "$MODULEFOUND" == "" ]; then 
    export PYTHONPATH=$PYTHONPATH:$MODPATH
  fi  
done
```
