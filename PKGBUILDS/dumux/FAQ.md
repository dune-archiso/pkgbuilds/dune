```console
-DCMAKE_PREFIX_PATH=/usr/include/valgrind
```

```console
'opm-common'
'opm-grid'
'dune-subgrid'
'dune-spgrid'
'dune-mmesh'
'dune-alugrid'
```

[](https://arxiv.org/pdf/1909.05052.pdf)
[](https://github.com/dumux/dumux)

```console
----- using default flags $CMAKE_FLAGS from /builds/dune-archiso/repository/dumux/dumux.opts -----
WARNING: could not find module 'dune-alugrid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-alugrid' is suggested by dumux
Skipping 'dune-alugrid'!
WARNING: could not find module 'dune-foamgrid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-foamgrid' is suggested by dumux
Skipping 'dune-foamgrid'!
WARNING: could not find module 'dune-functions',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-functions' is suggested by dumux
Skipping 'dune-functions'!
WARNING: could not find module 'opm-common',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'opm-common' is suggested by dumux
Skipping 'opm-common'!
WARNING: could not find module 'opm-grid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'opm-grid' is suggested by dumux
Skipping 'opm-grid'!
WARNING: could not find module 'dune-subgrid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-subgrid' is suggested by dumux
Skipping 'dune-subgrid'!
WARNING: could not find module 'dune-spgrid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-spgrid' is suggested by dumux
Skipping 'dune-spgrid'!
WARNING: could not find module 'dune-mmesh',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-mmesh' is suggested by dumux
Skipping 'dune-mmesh'!
--- going to build dumux  ---
WARNING: commandline parameters will overwrite setting in opts file "/builds/dune-archiso/repository/dumux/dumux.opts"
--- calling cmake for dumux ---
WARNING: could not find module 'dune-alugrid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-alugrid' is suggested by dumux
Skipping 'dune-alugrid'!
WARNING: could not find module 'dune-foamgrid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-foamgrid' is suggested by dumux
Skipping 'dune-foamgrid'!
WARNING: could not find module 'dune-functions',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-functions' is suggested by dumux
Skipping 'dune-functions'!
WARNING: could not find module 'opm-common',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'opm-common' is suggested by dumux
Skipping 'opm-common'!
WARNING: could not find module 'opm-grid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'opm-grid' is suggested by dumux
Skipping 'opm-grid'!
WARNING: could not find module 'dune-subgrid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-subgrid' is suggested by dumux
Skipping 'dune-subgrid'!
WARNING: could not find module 'dune-spgrid',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-spgrid' is suggested by dumux
Skipping 'dune-spgrid'!
WARNING: could not find module 'dune-mmesh',
       module is also unknown to pkg-config.
       Maybe you need to adjust PKG_CONFIG_PATH!
       'dune-mmesh' is suggested by dumux
Skipping 'dune-mmesh'!
cmake    "-Wno-dev" "-Ddune-common_DIR=/usr/lib//cmake/dune-common" "-Ddune-geometry_DIR=/usr/lib//cmake/dune-geometry" "-Ddune-uggrid_DIR=/usr/lib//cmake/dune-uggrid" "-Ddune-grid_DIR=/usr/lib//cmake/dune-grid" "-Ddune-localfunctions_DIR=/usr/lib//cmake/dune-localfunctions" "-Ddune-istl_DIR=/usr/lib//cmake/dune-istl" -DCMAKE_C_COMPILER=/usr/bin/gcc -DCMAKE_CXX_COMPILER=/usr/bin/g++ -DCMAKE_CXX_FLAGS_RELEASE=' -fdiagnostics-color=always -fno-strict-aliasing -fstrict-overflow -fno-finite-math-only -DNDEBUG=1 -O3 -march=native -funroll-loops -g0 -Wall -Wunused -Wmissing-include-dirs -Wcast-align -Wno-missing-braces -Wmissing-field-initializers -Wno-sign-compare ' -DCMAKE_CXX_FLAGS_DEBUG='-O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter -Wno-sign-compare ' -DCMAKE_CXX_FLAGS_RELWITHDEBINFO=' -fdiagnostics-color=always -fno-strict-aliasing -fstrict-overflow -fno-finite-math-only -DNDEBUG=1 -O3 -march=native -funroll-loops -g0 -Wall -Wunused -Wmissing-include-dirs -Wcast-align -Wno-missing-braces -Wmissing-field-initializers -Wno-sign-compare -g -ggdb -Wall ' -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -DBUILD_SHARED_LIBS=TRUE -DENABLE_HEADERCHECK=ON "/tmp/makepkg/dumux/src/dumux-3.4.0"
-- The C compiler identification is GNU 11.1.0
-- The CXX compiler identification is GNU 11.1.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/gcc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/g++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Dependencies for dumux: dune-common (>= 2.7);dune-grid (>= 2.7);dune-localfunctions (>= 2.7);dune-istl (>= 2.7)
-- Suggestions for dumux: dune-alugrid (>=2.7);dune-foamgrid (>=2.7);dune-uggrid (>=2.7);dune-functions (>=2.7);opm-common;opm-grid;dune-subgrid;dune-spgrid (>= 2.7);dune-mmesh (>= 1.1)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-istl: dune-common (>= 2.8)
-- Dependencies for dune-istl: dune-common (>= 2.8)
-- Could NOT find dune-alugrid (missing: dune-alugrid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Found PkgConfig: /usr/sbin/pkg-config (found version "1.8.0") 
-- Checking for module 'dune-alugrid>=2.7'
--   Package 'dune-alugrid', required by 'virtual:world', not found
-- Could NOT find dune-foamgrid (missing: dune-foamgrid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-foamgrid>=2.7'
--   Package 'dune-foamgrid', required by 'virtual:world', not found
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Could NOT find dune-functions (missing: dune-functions_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-functions>=2.7'
--   Package 'dune-functions', required by 'virtual:world', not found
-- Could NOT find opm-common (missing: opm-common_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'opm-common '
--   Package 'opm-common', required by 'virtual:world', not found
-- Could NOT find opm-grid (missing: opm-grid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'opm-grid '
--   Package 'opm-grid', required by 'virtual:world', not found
-- Could NOT find dune-subgrid (missing: dune-subgrid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-subgrid '
--   Package 'dune-subgrid', required by 'virtual:world', not found
-- Could NOT find dune-spgrid (missing: dune-spgrid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-spgrid>= 2.7'
--   Package 'dune-spgrid', required by 'virtual:world', not found
-- Could NOT find dune-mmesh (missing: dune-mmesh_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-mmesh>= 1.1'
--   Package 'dune-mmesh', required by 'virtual:world', not found
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Performing Test cxx_std_flag_17
-- Performing Test cxx_std_flag_17 - Success
-- Performing Test compiler_supports_cxx17
-- Performing Test compiler_supports_cxx17 - Success
-- Performing Test HAS_ATTRIBUTE_UNUSED
-- Performing Test HAS_ATTRIBUTE_UNUSED - Success
-- Performing Test HAS_ATTRIBUTE_DEPRECATED
-- Performing Test HAS_ATTRIBUTE_DEPRECATED - Success
-- Performing Test HAS_ATTRIBUTE_DEPRECATED_MSG
-- Performing Test HAS_ATTRIBUTE_DEPRECATED_MSG - Success
-- Looking for std::experimental::make_array<int,int>
-- Looking for std::experimental::make_array<int,int> - found
-- Looking for std::move<std::experimental::detected_t<std::decay_t,int>>
-- Looking for std::move<std::experimental::detected_t<std::decay_t,int>> - found
-- Looking for std::identity
-- Looking for std::identity - not found
-- Could NOT find LATEX (missing: LATEX_COMPILER) 
-- Could NOT find LatexMk (missing: LATEXMK_EXECUTABLE) 
-- Could NOT find Sphinx (missing: SPHINX_EXECUTABLE) 
-- Found Doxygen: /usr/sbin/doxygen (found version "1.9.2") found components: doxygen dot 
-- Searching for macro file 'DuneCommonMacros' for module 'dune-common'.
-- Performing tests specific to dune-common from file /usr/share/dune/cmake/modules/DuneCommonMacros.cmake.
-- Set Minimal Debug Level to 4
-- Looking for sgemm_
-- Looking for sgemm_ - not found
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
-- Check if compiler accepts -pthread
-- Check if compiler accepts -pthread - yes
-- Found Threads: TRUE  
-- Looking for sgemm_
-- Looking for sgemm_ - not found
-- Looking for sgemm_
-- Looking for sgemm_ - found
-- Found BLAS: /usr/lib/libblas.so  
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - found
-- Found LAPACK: /usr/lib/liblapack.so;/usr/lib/libblas.so  
-- Looking for dsyev_
-- Looking for dsyev_ - found
-- Found GMP: /usr/lib/libgmpxx.so  
-- Performing Test QuadMath_COMPILES
-- Performing Test QuadMath_COMPILES - Success
-- Found QuadMath: (Supported by compiler)  
-- Found MPI_C: /usr/lib/openmpi/libmpi.so (found suitable version "3.1", minimum required is "3.0") 
-- Found MPI: TRUE (found suitable version "3.1", minimum required is "3.0") found components: C 
-- Found TBB: using configuration from TBB_DIR=/usr/lib64/cmake/TBB (found version "2020.3")
-- Could NOT find PTScotch (missing: SCOTCH_LIBRARY SCOTCHERR_LIBRARY SCOTCH_INCLUDE_DIR) 
-- Could NOT find METIS (missing: METIS_LIBRARY METIS_INCLUDE_DIR METIS_API_VERSION) 
-- Could NOT find METIS (missing: METIS_LIBRARY METIS_INCLUDE_DIR METIS_API_VERSION) 
-- Found MPI_C: /usr/lib/openmpi/libmpi.so (found version "3.1") 
-- Found MPI: TRUE (found version "3.1") found components: C 
-- Could NOT find ParMETIS (missing: PARMETIS_LIBRARY PARMETIS_INCLUDE_DIR METIS_FOUND) (Required is at least version "4.0")
-- Could NOT find Vc (missing: Vc_DIR)
-- Found Python3: /usr/sbin/python3 (found version "3.9.7") found components: Interpreter Development Development.Module Development.Embed 
-- Failed to find the python package pip with interpreter /usr/sbin/python3. (missing: DUNE_PYTHON_pip_FOUND) 
-- Setting dune-common_INCLUDE_DIRS=/usr/include
-- Setting dune-common_LIBRARIES=dunecommon
-- Searching for macro file 'DuneUggridMacros' for module 'dune-uggrid'.
-- Performing tests specific to dune-uggrid from file /usr/share/dune/cmake/modules/DuneUggridMacros.cmake.
-- Setting dune-uggrid_INCLUDE_DIRS=/usr/include
-- Setting dune-uggrid_LIBRARIES=duneuggrid
-- Searching for macro file 'DuneGeometryMacros' for module 'dune-geometry'.
-- No module specific tests performed for module 'dune-geometry' because macro file 'DuneGeometryMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Setting dune-geometry_INCLUDE_DIRS=/usr/include
-- Setting dune-geometry_LIBRARIES=dunegeometry
-- Searching for macro file 'DuneMmeshMacros' for module 'dune-mmesh'.
-- No module specific tests performed for module 'dune-mmesh' because macro file 'DuneMmeshMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneSpgridMacros' for module 'dune-spgrid'.
-- No module specific tests performed for module 'dune-spgrid' because macro file 'DuneSpgridMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneSubgridMacros' for module 'dune-subgrid'.
-- No module specific tests performed for module 'dune-subgrid' because macro file 'DuneSubgridMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'OpmGridMacros' for module 'opm-grid'.
-- No module specific tests performed for module 'opm-grid' because macro file 'OpmGridMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'OpmCommonMacros' for module 'opm-common'.
-- No module specific tests performed for module 'opm-common' because macro file 'OpmCommonMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneFunctionsMacros' for module 'dune-functions'.
-- No module specific tests performed for module 'dune-functions' because macro file 'DuneFunctionsMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneFoamgridMacros' for module 'dune-foamgrid'.
-- No module specific tests performed for module 'dune-foamgrid' because macro file 'DuneFoamgridMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneAlugridMacros' for module 'dune-alugrid'.
-- No module specific tests performed for module 'dune-alugrid' because macro file 'DuneAlugridMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneIstlMacros' for module 'dune-istl'.
-- Performing tests specific to dune-istl from file /usr/share/dune/cmake/modules/DuneIstlMacros.cmake.
-- Could NOT find METIS (missing: METIS_LIBRARY METIS_INCLUDE_DIR METIS_API_VERSION) 
-- Could NOT find METIS (missing: METIS_LIBRARY METIS_INCLUDE_DIR METIS_API_VERSION) 
-- Could NOT find ParMETIS (missing: PARMETIS_LIBRARY PARMETIS_INCLUDE_DIR METIS_FOUND) 
-- Could NOT find SuperLU (missing: SUPERLU_LIBRARY SUPERLU_INCLUDE_DIR SLU_UTIL_HEADER) (Required is at least version "5.0")
-- Could NOT find ARPACK (missing: ARPACK_LIBRARY) 
-- Could NOT find ARPACKPP (missing: ARPACK_FOUND ARPACKPP_INCLUDE_DIR) 
-- Could NOT find SuiteSparse (missing: SUITESPARSE_CONFIG_LIB SUITESPARSE_INCLUDE_DIR) 
-- Setting dune-istl_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneLocalfunctionsMacros' for module 'dune-localfunctions'.
-- No module specific tests performed for module 'dune-localfunctions' because macro file 'DuneLocalfunctionsMacros.cmake' not found in /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Setting dune-localfunctions_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneGridMacros' for module 'dune-grid'.
-- Performing tests specific to dune-grid from file /usr/share/dune/cmake/modules/DuneGridMacros.cmake.
-- Looking for mkstemp
-- Looking for mkstemp - found
-- Could NOT find METIS (missing: METIS_LIBRARY METIS_INCLUDE_DIR METIS_API_VERSION) 
-- Could NOT find METIS (missing: METIS_LIBRARY METIS_INCLUDE_DIR METIS_API_VERSION) 
-- Could NOT find ParMETIS (missing: PARMETIS_LIBRARY PARMETIS_INCLUDE_DIR METIS_FOUND) 
-- Checking for module 'alberta-grid_1d>=3.0'
--   Package 'alberta-grid_1d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_2d>=3.0'
--   Package 'alberta-grid_2d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_3d>=3.0'
--   Package 'alberta-grid_3d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_4d>=3.0'
--   Package 'alberta-grid_4d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_5d>=3.0'
--   Package 'alberta-grid_5d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_6d>=3.0'
--   Package 'alberta-grid_6d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_7d>=3.0'
--   Package 'alberta-grid_7d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_8d>=3.0'
--   Package 'alberta-grid_8d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_9d>=3.0'
--   Package 'alberta-grid_9d', required by 'virtual:world', not found
-- Could NOT find Alberta (set PKG_CONFIG_PATH to include the location of the alberta-grid_[n]d.pc files) (missing: ALBERTA_GRID_PREFIX) (Required is at least version "3.0")
-- Checking for one of the modules 'psurface'
-- Checking for module 'psurface'
--   Package 'psurface', required by 'virtual:world', not found
-- Could NOT find Psurface (missing: PSURFACE_INCLUDE_DIR PSURFACE_LIBRARY) 
-- Could NOT find AmiraMesh (missing: AMIRAMESH_INCLUDE_DIR AMIRAMESH_LIBRARY) 
-- Setting dune-grid_INCLUDE_DIRS=/usr/include
-- Setting dune-grid_LIBRARIES=dunegrid
-- Searching for macro file 'DumuxMacros' for module 'dumux'.
-- Performing tests specific to dumux from file /tmp/makepkg/dumux/src/dumux-3.4.0/cmake/modules/DumuxMacros.cmake.
-- Could NOT find GLPK (missing: GLPK_INCLUDE_DIR GLPK_LIBRARY) 
-- Could NOT find Gnuplot (missing: GNUPLOT_EXECUTABLE) 
-- Could NOT find Gstat (missing: GSTAT_EXECUTABLE) 
-- Could NOT find gmsh (missing: GMSH_EXECUTABLE) 
-- Could NOT find NLOPT (missing: NLOPT_INCLUDE_DIR NLOPT_LIBRARY) 
-- Could NOT find PTScotch (missing: SCOTCH_LIBRARY SCOTCHERR_LIBRARY SCOTCH_INCLUDE_DIR) 
-- Could NOT find PVPython (missing: PVPYTHON_EXECUTABLE) 
CMake Warning at cmake/modules/DumuxMacros.cmake:16 (find_package):
  By not providing "FindValgrind.cmake" in CMAKE_MODULE_PATH this project has
  asked CMake to find a package configuration file provided by "Valgrind",
  but CMake did not find one.
  Could not find a package configuration file provided by "Valgrind" with any
  of the following names:
    ValgrindConfig.cmake
    valgrind-config.cmake
  Add the installation prefix of "Valgrind" to CMAKE_PREFIX_PATH or set
  "Valgrind_DIR" to a directory containing one of the above files.  If
  "Valgrind" provides a separate development package or SDK, be sure it has
  been installed.
Call Stack (most recent call first):
  /usr/share/dune/cmake/modules/DuneMacros.cmake:609 (include)
  /usr/share/dune/cmake/modules/DuneMacros.cmake:699 (dune_process_dependency_macros)
  CMakeLists.txt:24 (dune_project)
-- Using scripts from /usr/share/dune/cmake/scripts for creating doxygen stuff.
-- using /usr/share/dune-common/doc/doxygen//Doxystyle to create doxystyle file
-- using C macro definitions from /usr/share/dune-common/doc/doxygen//doxygen-macros for Doxygen
-- Adding custom target for config.h generation
-- The following OPTIONAL packages have been found:
 * dune-uggrid
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * GMP, GNU multi-precision library, <https://gmplib.org>
 * QuadMath, GCC Quad-Precision Math Library, <https://gcc.gnu.org/onlinedocs/libquadmath>
 * TBB, Intel's Threading Building Blocks
 * Python3
 * Threads, Multi-threading library
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
-- The following REQUIRED packages have been found:
 * dune-common
 * dune-grid
 * dune-localfunctions
 * dune-istl
 * dune-geometry
-- The following OPTIONAL packages have not been found:
 * dune-alugrid
 * dune-foamgrid
 * dune-functions
 * opm-common
 * opm-grid
 * dune-subgrid
 * dune-spgrid
 * dune-mmesh
 * LATEX
 * LatexMk
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * SuperLU (required version >= 5.0), Supernodal LU
   Direct solver for linear system, based on LU decomposition
 * ARPACK, ARnoldi PACKage
   Solve large scale eigenvalue problems
 * ARPACKPP, ARPACK++
   C++ interface for ARPACK
 * SuiteSparse, A suite of sparse matrix software, <http://faculty.cse.tamu.edu/davis/suitesparse.html>
 * METIS, Serial Graph Partitioning
 * ParMETIS, Parallel Graph Partitioning
 * Alberta (required version >= 3.0), An adaptive hierarchical finite element toolbox and grid manager
 * Psurface, Piecewise linear bijections between triangulated surfaces
 * AmiraMesh
 * GLPK, GNU Linear Programming Kit
 * Gnuplot
 * Gstat, Geostatistic library
   Generate random permeability and porosity fields
 * Gmsh, Meshing tool
   Generate structured and unstructured grids
 * NLOPT, Library for nonlinear optimization
 * PTScotch, Sequential and Parallel Graph Partitioning
 * PVPython, ParaView python client
   Extract data over line or time in post-processing
 * Valgrind
```

[](https://github.com/dumux/dumux)

```
[ 33%] Building CXX object test/linear/CMakeFiles/test_linearsolver.dir/test_linearsolver.cc.o
/tmp/makepkg/dumux/src/dumux-3.4.0/test/linear/test_linearsolver.cc:18:10: fatal error: dune/istl/paamg/test/anisotropic.hh: No such file or directory
   18 | #include <dune/istl/paamg/test/anisotropic.hh>
      |          ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
compilation terminated.
make[3]: *** [test/linear/CMakeFiles/test_linearsolver.dir/build.make:76: test/linear/CMakeFiles/test_linearsolver.dir/test_linearsolver.cc.o] Error 1
make[2]: *** [CMakeFiles/Makefile2:47582: test/linear/CMakeFiles/test_linearsolver.dir/all] Error 2
make[1]: *** [CMakeFiles/Makefile2:26201: CMakeFiles/build_tests.dir/rule] Error 2
```

[](https://texfaq.org/FAQ-includeother)
[](https://library.itc.utwente.nl/papers_2021/phd/balugani.pdf)

[](https://www.sciencedirect.com/science/article/pii/S0898122120300791)
[](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-coverage)
[](https://www.tik.uni-stuttgart.de/dienste-a-z/Virtuelles-Programmierlabor-ViPLab)
[](https://www.tik.uni-stuttgart.de/forschung-und-lehre/forschungs-und-entwicklungsprojekte/#id-0d764ccf-0)
[](https://dumux.org/docs/usermeeting2015/DUM_dumux-lecture.pdf)
