[](https://gitlab.dune-project.org/felix.gruber/dune-dpg/-/tags/v0.5)

```console
[  0%] Building CXX object src/CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o
cd /tmp/makepkg/dune-dpg/src/build-cmake/src && /usr/sbin/g++ -DENABLE_MPI=1 -DENABLE_SUITESPARSE=1 -DENABLE_UG=1 -DHAVE_CONFIG_H -DModelP -I/tmp/makepkg/dune-dpg/src/build-cmake -I/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5 -I/usr/include/eigen3 -std=c++17 -Wall -fdiagnostics-color=always -mavx -fPIE -pthread -MD -MT src/CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o -MF CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o.d -o CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o -c /tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/src/dune_dpg_opt_testspace.cc
In file included from /tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/functions/functionspacebases/lagrangedgrefineddgbasis.hh:17,
                 from /tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/src/dune_dpg_opt_testspace.cc:28:
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/functions/functionspacebases/refinedglobalbasis.hh: In instantiation of ‘class Dune::Functions::RefinedGlobalBasis<Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double> >’:
/usr/include/c++/11.1.0/tuple:69:28:   required from ‘struct std::_Tuple_impl<0, Dune::Functions::RefinedGlobalBasis<Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double> > >’
/usr/include/c++/11.1.0/tuple:599:11:   required from ‘class std::tuple<Dune::Functions::RefinedGlobalBasis<Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double> > >’
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/src/dune_dpg_opt_testspace.cc:98:56:   required from here
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/functions/functionspacebases/refinedglobalbasis.hh:73:9: error: no type named ‘IndexSet’ in ‘using PreBasis = class Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double>’ {aka ‘class Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double>’}
   73 |   using NodeIndexSet = typename PreBasis::IndexSet;
      |         ^~~~~~~~~~~~
In file included from /tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/src/dune_dpg_opt_testspace.cc:34:
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/dpg/boundarytools.hh: In instantiation of ‘static void Dune::BoundaryTools::getInflowBoundaryMask_(const FEBasis&, std::vector<bool>&, const Direction&) [with FEBasis = Dune::Functions::DefaultGlobalBasis<Dune::Functions::LagrangePreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 2, Dune::Functions::FlatMultiIndex<long unsigned int>, double> >; Direction = Dune::Functions::ConstantGridViewFunction<Dune::FieldVector<double, 2>, Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > > >]’:
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/dpg/boundarytools.hh:108:27:   required from ‘static void Dune::BoundaryTools::getInflowBoundaryMask(const FEBasis&, std::vector<bool>&, const Direction&) [with FEBasis = Dune::Functions::DefaultGlobalBasis<Dune::Functions::LagrangePreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 2, Dune::Functions::FlatMultiIndex<long unsigned int>, double> >; Direction = Dune::FieldVector<double, 2>]’
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/src/dune_dpg_opt_testspace.cc:160:41:   required from here
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/dpg/boundarytools.hh:133:20: warning: loop variable ‘e’ creates a copy from type ‘const Dune::Entity<0, 2, const Dune::UGGrid<2>, Dune::UGGridEntity>’ [-Wrange-loop-construct]
  133 |     for(const auto e : elements(gridView))
      |                    ^
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/dpg/boundarytools.hh:133:20: note: use reference type to prevent copying
  133 |     for(const auto e : elements(gridView))
      |                    ^
      |                    &
make[2]: *** [src/CMakeFiles/dune_dpg_opt_testspace.dir/build.make:79: src/CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-dpg/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:5433: src/CMakeFiles/dune_dpg_opt_testspace.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-dpg/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
```

```console
[  0%] Building CXX object src/CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o
cd /tmp/makepkg/dune-dpg/src/build-cmake/src && /usr/sbin/g++ -DENABLE_MPI=1 -DENABLE_SUITESPARSE=1 -DENABLE_UG=1 -DHAVE_CONFIG_H -DModelP -I/tmp/makepkg/dune-dpg/src/build-cmake -I/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5 -I/usr/include/eigen3 -std=c++17 -Wall -fdiagnostics-color=always -mavx -fPIE -pthread -std=gnu++17 -MD -MT src/CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o -MF CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o.d -o CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o -c /tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/src/dune_dpg_opt_testspace.cc
In file included from /tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/functions/functionspacebases/lagrangedgrefineddgbasis.hh:17,
                 from /tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/src/dune_dpg_opt_testspace.cc:28:
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/functions/functionspacebases/refinedglobalbasis.hh: In instantiation of ‘class Dune::Functions::RefinedGlobalBasis<Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double> >’:
/usr/include/c++/11.2.0/tuple:69:28:   required from ‘struct std::_Tuple_impl<0, Dune::Functions::RefinedGlobalBasis<Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double> > >’
/usr/include/c++/11.2.0/tuple:599:11:   required from ‘class std::tuple<Dune::Functions::RefinedGlobalBasis<Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double> > >’
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/src/dune_dpg_opt_testspace.cc:98:56:   required from here
/tmp/makepkg/dune-dpg/src/dune-dpg-v0.5/dune/functions/functionspacebases/refinedglobalbasis.hh:73:9: error: no type named ‘IndexSet’ in ‘using PreBasis = class Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double>’ {aka ‘class Dune::Functions::LagrangeDGRefinedDGPreBasis<Dune::GridView<Dune::UGGridLeafGridViewTraits<const Dune::UGGrid<2> > >, 1, 3, Dune::Functions::FlatMultiIndex<long unsigned int>, double>’}
   73 |   using NodeIndexSet = typename PreBasis::IndexSet;
      |         ^~~~~~~~~~~~
make[2]: *** [src/CMakeFiles/dune_dpg_opt_testspace.dir/build.make:79: src/CMakeFiles/dune_dpg_opt_testspace.dir/dune_dpg_opt_testspace.cc.o] Error 1
make[2]: Leaving directory '/tmp/makepkg/dune-dpg/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:5433: src/CMakeFiles/dune_dpg_opt_testspace.dir/all] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-dpg/src/build-cmake'
make: *** [Makefile:149: all] Error 2
==> ERROR: A failure occurred in build().
```
