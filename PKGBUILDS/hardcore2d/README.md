[](https://github.com/jdroniou/HArDCore2D-release)
[](https://hal.archives-ouvertes.fr/hal-02151813v3/document)
[](https://users.monash.edu/~jdroniou/presentations/algoritmy-2020.pdf)
[](https://users.monash.edu/~jdroniou/presentations/anziam-2021.pdf)
[](https://www.cse-lab.ethz.ch/wp-content/papercite-data/pdf/hejlesen2013a.pdf)

```
==> Starting build()...
-- Found Boost 1.78.0 at /usr/lib64/cmake/Boost-1.78.0
--   Requested configuration: QUIET REQUIRED COMPONENTS filesystem;program_options;timer;chrono
-- BoostConfig: find_package(boost_headers 1.78.0 EXACT CONFIG REQUIRED QUIET HINTS /usr/lib64/cmake)
-- Found boost_headers 1.78.0 at /usr/lib64/cmake/boost_headers-1.78.0
-- BoostConfig: find_package(boost_filesystem 1.78.0 EXACT CONFIG REQUIRED QUIET HINTS /usr/lib64/cmake)
-- Found boost_filesystem 1.78.0 at /usr/lib64/cmake/boost_filesystem-1.78.0
-- Boost toolset is gcc11 (GNU 11.1.0)
-- Scanning /usr/lib64/cmake/boost_filesystem-1.78.0/libboost_filesystem-variant*.cmake
--   Including /usr/lib64/cmake/boost_filesystem-1.78.0/libboost_filesystem-variant-shared.cmake
--   [ ] libboost_filesystem.so.1.78.0
--   Including /usr/lib64/cmake/boost_filesystem-1.78.0/libboost_filesystem-variant-static.cmake
--   [x] libboost_filesystem.a
-- Adding boost_filesystem dependencies: atomic;headers
-- Found boost_atomic 1.78.0 at /usr/lib64/cmake/boost_atomic-1.78.0
-- Boost toolset is gcc11 (GNU 11.1.0)
-- Scanning /usr/lib64/cmake/boost_atomic-1.78.0/libboost_atomic-variant*.cmake
--   Including /usr/lib64/cmake/boost_atomic-1.78.0/libboost_atomic-variant-shared.cmake
--   [ ] libboost_atomic.so.1.78.0
--   Including /usr/lib64/cmake/boost_atomic-1.78.0/libboost_atomic-variant-static.cmake
--   [x] libboost_atomic.a
-- Adding boost_atomic dependencies: headers
-- BoostConfig: find_package(boost_program_options 1.78.0 EXACT CONFIG REQUIRED QUIET HINTS /usr/lib64/cmake)
-- Found boost_program_options 1.78.0 at /usr/lib64/cmake/boost_program_options-1.78.0
-- Boost toolset is gcc11 (GNU 11.1.0)
-- Scanning /usr/lib64/cmake/boost_program_options-1.78.0/libboost_program_options-variant*.cmake
--   Including /usr/lib64/cmake/boost_program_options-1.78.0/libboost_program_options-variant-shared.cmake
--   [ ] libboost_program_options.so.1.78.0
--   Including /usr/lib64/cmake/boost_program_options-1.78.0/libboost_program_options-variant-static.cmake
--   [x] libboost_program_options.a
-- Adding boost_program_options dependencies: headers
-- BoostConfig: find_package(boost_timer 1.78.0 EXACT CONFIG REQUIRED QUIET HINTS /usr/lib64/cmake)
-- Found boost_timer 1.78.0 at /usr/lib64/cmake/boost_timer-1.78.0
-- Boost toolset is gcc11 (GNU 11.1.0)
-- Scanning /usr/lib64/cmake/boost_timer-1.78.0/libboost_timer-variant*.cmake
--   Including /usr/lib64/cmake/boost_timer-1.78.0/libboost_timer-variant-shared.cmake
--   [ ] libboost_timer.so.1.78.0
--   Including /usr/lib64/cmake/boost_timer-1.78.0/libboost_timer-variant-static.cmake
--   [x] libboost_timer.a
-- Adding boost_timer dependencies: chrono;headers
-- Found boost_chrono 1.78.0 at /usr/lib64/cmake/boost_chrono-1.78.0
-- Boost toolset is gcc11 (GNU 11.1.0)
-- Scanning /usr/lib64/cmake/boost_chrono-1.78.0/libboost_chrono-variant*.cmake
--   Including /usr/lib64/cmake/boost_chrono-1.78.0/libboost_chrono-variant-shared.cmake
--   [ ] libboost_chrono.so.1.78.0
--   Including /usr/lib64/cmake/boost_chrono-1.78.0/libboost_chrono-variant-static.cmake
--   [x] libboost_chrono.a
-- Adding boost_chrono dependencies: headers
-- BoostConfig: find_package(boost_chrono 1.78.0 EXACT CONFIG REQUIRED QUIET HINTS /usr/lib64/cmake)
-- Configuring done
-- Generating done
-- Build files have been written to: /tmp/makepkg/HArDCore2D/src/build-cmake
/usr/bin/cmake -S/tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 -B/tmp/makepkg/HArDCore2D/src/build-cmake --check-build-system CMakeFiles/Makefile.cmake 0
/usr/bin/cmake -E cmake_progress_start /tmp/makepkg/HArDCore2D/src/build-cmake/CMakeFiles /tmp/makepkg/HArDCore2D/src/build-cmake//CMakeFiles/progress.marks
/usr/sbin/make  -f CMakeFiles/Makefile2 all
make[1]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f src/Mesh/CMakeFiles/mesh.dir/build.make src/Mesh/CMakeFiles/mesh.dir/depend
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/src/Mesh /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/src/Mesh /tmp/makepkg/HArDCore2D/src/build-cmake/src/Mesh/CMakeFiles/mesh.dir/DependInfo.cmake --color=
/usr/sbin/make  -f src/Quadrature/CMakeFiles/quadrature.dir/build.make src/Quadrature/CMakeFiles/quadrature.dir/depend
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/src/Quadrature /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/src/Quadrature /tmp/makepkg/HArDCore2D/src/build-cmake/src/Quadrature/CMakeFiles/quadrature.dir/DependInfo.cmake --color=
/usr/sbin/make  -f Schemes/CMakeFiles/BoundaryConditions.dir/build.make Schemes/CMakeFiles/BoundaryConditions.dir/depend
Dependencies file "src/Quadrature/CMakeFiles/quadrature.dir/quad1d.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Quadrature/CMakeFiles/quadrature.dir/compiler_depend.internal".
Dependencies file "src/Quadrature/CMakeFiles/quadrature.dir/quad2d.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Quadrature/CMakeFiles/quadrature.dir/compiler_depend.internal".
Dependencies file "src/Quadrature/CMakeFiles/quadrature.dir/quadraturerule.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Quadrature/CMakeFiles/quadrature.dir/compiler_depend.internal".
Dependencies file "src/Quadrature/CMakeFiles/quadrature.dir/triangle_dunavant_rule.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Quadrature/CMakeFiles/quadrature.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target quadrature
Dependencies file "src/Mesh/CMakeFiles/mesh.dir/cell.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Mesh/CMakeFiles/mesh.dir/compiler_depend.internal".
Dependencies file "src/Mesh/CMakeFiles/mesh.dir/edge.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Mesh/CMakeFiles/mesh.dir/compiler_depend.internal".
Dependencies file "src/Mesh/CMakeFiles/mesh.dir/import_mesh.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Mesh/CMakeFiles/mesh.dir/compiler_depend.internal".
Dependencies file "src/Mesh/CMakeFiles/mesh.dir/mesh.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Mesh/CMakeFiles/mesh.dir/compiler_depend.internal".
Dependencies file "src/Mesh/CMakeFiles/mesh.dir/mesh_builder.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Mesh/CMakeFiles/mesh.dir/compiler_depend.internal".
Dependencies file "src/Mesh/CMakeFiles/mesh.dir/vertex.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Mesh/CMakeFiles/mesh.dir/compiler_depend.internal".
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/BoundaryConditions.dir/DependInfo.cmake --color=
/usr/sbin/make  -f Schemes/CMakeFiles/TestCase.dir/build.make Schemes/CMakeFiles/TestCase.dir/depend
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
Consolidate compiler generated dependencies of target mesh
/usr/sbin/make  -f src/Quadrature/CMakeFiles/quadrature.dir/build.make src/Quadrature/CMakeFiles/quadrature.dir/build
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/TestCase.dir/DependInfo.cmake --color=
Dependencies file "Schemes/CMakeFiles/BoundaryConditions.dir/BoundaryConditions/BoundaryConditions.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/BoundaryConditions.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target BoundaryConditions
/usr/sbin/make  -f Schemes/CMakeFiles/TestCaseStefanPME.dir/build.make Schemes/CMakeFiles/TestCaseStefanPME.dir/depend
/usr/sbin/make  -f Schemes/CMakeFiles/TestCaseTransient.dir/build.make Schemes/CMakeFiles/TestCaseTransient.dir/depend
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/TestCaseStefanPME.dir/DependInfo.cmake --color=
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/BoundaryConditions.dir/build.make Schemes/CMakeFiles/BoundaryConditions.dir/build
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/TestCaseTransient.dir/DependInfo.cmake --color=
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'src/Quadrature/CMakeFiles/quadrature.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/hho-general.dir/build.make Schemes/CMakeFiles/hho-general.dir/depend
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/BoundaryConditions.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hho-general.dir/DependInfo.cmake --color=
Dependencies file "Schemes/CMakeFiles/TestCaseStefanPME.dir/TestCase/TestCaseNonLinearity.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/TestCaseStefanPME.dir/compiler_depend.internal".
Dependencies file "Schemes/CMakeFiles/TestCaseStefanPME.dir/TestCase/TestCaseStefanPME.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/TestCaseStefanPME.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target TestCaseStefanPME
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
Dependencies file "Schemes/CMakeFiles/TestCase.dir/TestCase/TestCase.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/TestCase.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target TestCase
[ 10%] Built target quadrature
Dependencies file "Schemes/CMakeFiles/hho-general.dir/HHO-general/HHO2D.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hho-general.dir/compiler_depend.internal".
/usr/sbin/make  -f src/Mesh/CMakeFiles/mesh.dir/build.make src/Mesh/CMakeFiles/mesh.dir/build
Consolidate compiler generated dependencies of target hho-general
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
Dependencies file "Schemes/CMakeFiles/TestCaseTransient.dir/TestCase/TestCaseTransient.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/TestCaseTransient.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target TestCaseTransient
/usr/sbin/make  -f Schemes/CMakeFiles/TestCase.dir/build.make Schemes/CMakeFiles/TestCase.dir/build
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 14%] Built target BoundaryConditions
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'src/Mesh/CMakeFiles/mesh.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/TestCaseStefanPME.dir/build.make Schemes/CMakeFiles/TestCaseStefanPME.dir/build
/usr/sbin/make  -f Schemes/CMakeFiles/hho-general.dir/build.make Schemes/CMakeFiles/hho-general.dir/build
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/hho-general.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/TestCaseStefanPME.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/TestCase.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/TestCaseTransient.dir/build.make Schemes/CMakeFiles/TestCaseTransient.dir/build
[ 28%] Built target mesh
[ 32%] Built target TestCaseStefanPME
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/TestCaseTransient.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 34%] Built target TestCase
/usr/sbin/make  -f src/Common/CMakeFiles/basis.dir/build.make src/Common/CMakeFiles/basis.dir/depend
[ 36%] Built target hho-general
[ 38%] Built target TestCaseTransient
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/src/Common /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/src/Common /tmp/makepkg/HArDCore2D/src/build-cmake/src/Common/CMakeFiles/basis.dir/DependInfo.cmake --color=
/usr/sbin/make  -f src/Plot/CMakeFiles/plot.dir/build.make src/Plot/CMakeFiles/plot.dir/depend
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/src/Plot /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/src/Plot /tmp/makepkg/HArDCore2D/src/build-cmake/src/Plot/CMakeFiles/plot.dir/DependInfo.cmake --color=
Dependencies file "src/Common/CMakeFiles/basis.dir/basis.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Common/CMakeFiles/basis.dir/compiler_depend.internal".
Dependencies file "src/Common/CMakeFiles/basis.dir/dofspace.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Common/CMakeFiles/basis.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target basis
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f src/Common/CMakeFiles/basis.dir/build.make src/Common/CMakeFiles/basis.dir/build
Dependencies file "src/Plot/CMakeFiles/plot.dir/vtu_writer.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/Plot/CMakeFiles/plot.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target plot
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f src/Plot/CMakeFiles/plot.dir/build.make src/Plot/CMakeFiles/plot.dir/build
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'src/Common/CMakeFiles/basis.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'src/Plot/CMakeFiles/plot.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 42%] Built target plot
[ 48%] Built target basis
/usr/sbin/make  -f src/HybridCore/CMakeFiles/hybridcore.dir/build.make src/HybridCore/CMakeFiles/hybridcore.dir/depend
/usr/sbin/make  -f src/DDRCore/CMakeFiles/ddrcore.dir/build.make src/DDRCore/CMakeFiles/ddrcore.dir/depend
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/src/HybridCore /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/src/HybridCore /tmp/makepkg/HArDCore2D/src/build-cmake/src/HybridCore/CMakeFiles/hybridcore.dir/DependInfo.cmake --color=
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/src/DDRCore /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/src/DDRCore /tmp/makepkg/HArDCore2D/src/build-cmake/src/DDRCore/CMakeFiles/ddrcore.dir/DependInfo.cmake --color=
Dependencies file "src/HybridCore/CMakeFiles/hybridcore.dir/elementquad.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/HybridCore/CMakeFiles/hybridcore.dir/compiler_depend.internal".
Dependencies file "src/HybridCore/CMakeFiles/hybridcore.dir/hybridcore.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/HybridCore/CMakeFiles/hybridcore.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target hybridcore
Dependencies file "src/DDRCore/CMakeFiles/ddrcore.dir/ddrcore.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/DDRCore/CMakeFiles/ddrcore.dir/compiler_depend.internal".
Dependencies file "src/DDRCore/CMakeFiles/ddrcore.dir/ddrspace.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/DDRCore/CMakeFiles/ddrcore.dir/compiler_depend.internal".
Dependencies file "src/DDRCore/CMakeFiles/ddrcore.dir/excurl.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/DDRCore/CMakeFiles/ddrcore.dir/compiler_depend.internal".
Dependencies file "src/DDRCore/CMakeFiles/ddrcore.dir/xcurl.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/DDRCore/CMakeFiles/ddrcore.dir/compiler_depend.internal".
Dependencies file "src/DDRCore/CMakeFiles/ddrcore.dir/xgrad.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/src/DDRCore/CMakeFiles/ddrcore.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target ddrcore
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f src/HybridCore/CMakeFiles/hybridcore.dir/build.make src/HybridCore/CMakeFiles/hybridcore.dir/build
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'src/HybridCore/CMakeFiles/hybridcore.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 55%] Built target hybridcore
/usr/sbin/make  -f Schemes/CMakeFiles/hho-diffusion.dir/build.make Schemes/CMakeFiles/hho-diffusion.dir/depend
/usr/sbin/make  -f Schemes/CMakeFiles/hho-diff-advec-reac.dir/build.make Schemes/CMakeFiles/hho-diff-advec-reac.dir/depend
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hho-diffusion.dir/DependInfo.cmake --color=
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hho-diff-advec-reac.dir/DependInfo.cmake --color=
/usr/sbin/make  -f src/DDRCore/CMakeFiles/ddrcore.dir/build.make src/DDRCore/CMakeFiles/ddrcore.dir/build
/usr/sbin/make  -f Schemes/CMakeFiles/hho-locvardiff.dir/build.make Schemes/CMakeFiles/hho-locvardiff.dir/depend
/usr/sbin/make  -f Schemes/CMakeFiles/lepnc-diffusion.dir/build.make Schemes/CMakeFiles/lepnc-diffusion.dir/depend
/usr/sbin/make  -f Schemes/CMakeFiles/lepnc-stefanpme.dir/build.make Schemes/CMakeFiles/lepnc-stefanpme.dir/depend
/usr/sbin/make  -f Schemes/CMakeFiles/lepnc-stefanpme-transient.dir/build.make Schemes/CMakeFiles/lepnc-stefanpme-transient.dir/depend
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hho-locvardiff.dir/DependInfo.cmake --color=
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/lepnc-diffusion.dir/DependInfo.cmake --color=
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/lepnc-stefanpme.dir/DependInfo.cmake --color=
Dependencies file "Schemes/CMakeFiles/hho-diff-advec-reac.dir/HHO-diff-advec-reac/HHO_DiffAdvecReac.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hho-diff-advec-reac.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target hho-diff-advec-reac
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'src/DDRCore/CMakeFiles/ddrcore.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/hho-diff-advec-reac.dir/build.make Schemes/CMakeFiles/hho-diff-advec-reac.dir/build
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/lepnc-stefanpme-transient.dir/DependInfo.cmake --color=
Dependencies file "Schemes/CMakeFiles/lepnc-stefanpme.dir/LEPNC/LEPNC_StefanPME.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/lepnc-stefanpme.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target lepnc-stefanpme
Dependencies file "Schemes/CMakeFiles/hho-diffusion.dir/HHO-diffusion/HHO_Diffusion.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hho-diffusion.dir/compiler_depend.internal".
Dependencies file "Schemes/CMakeFiles/hho-locvardiff.dir/HHO-locvardiff/HHO_LocVarDiff.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hho-locvardiff.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target hho-diffusion
Consolidate compiler generated dependencies of target hho-locvardiff
Dependencies file "Schemes/CMakeFiles/lepnc-diffusion.dir/LEPNC/LEPNC_diffusion.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/lepnc-diffusion.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target lepnc-diffusion
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
Dependencies file "Schemes/CMakeFiles/lepnc-stefanpme-transient.dir/LEPNC/LEPNC_StefanPME_transient.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/lepnc-stefanpme-transient.dir/compiler_depend.internal".
/usr/sbin/make  -f Schemes/CMakeFiles/hho-diffusion.dir/build.make Schemes/CMakeFiles/hho-diffusion.dir/build
Consolidate compiler generated dependencies of target lepnc-stefanpme-transient
/usr/sbin/make  -f Schemes/CMakeFiles/hho-locvardiff.dir/build.make Schemes/CMakeFiles/hho-locvardiff.dir/build
/usr/sbin/make  -f Schemes/CMakeFiles/lepnc-diffusion.dir/build.make Schemes/CMakeFiles/lepnc-diffusion.dir/build
/usr/sbin/make  -f Schemes/CMakeFiles/lepnc-stefanpme.dir/build.make Schemes/CMakeFiles/lepnc-stefanpme.dir/build
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 67%] Built target ddrcore
make[2]: Nothing to be done for 'Schemes/CMakeFiles/hho-diff-advec-reac.dir/build'.
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/hmm-stefanpme-transient.dir/build.make Schemes/CMakeFiles/hmm-stefanpme-transient.dir/depend
make[2]: Nothing to be done for 'Schemes/CMakeFiles/hho-locvardiff.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/lepnc-stefanpme.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hmm-stefanpme-transient.dir/DependInfo.cmake --color=
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/lepnc-stefanpme-transient.dir/build.make Schemes/CMakeFiles/lepnc-stefanpme-transient.dir/build
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/lepnc-diffusion.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/hho-diffusion.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 71%] Built target hho-diff-advec-reac
/usr/sbin/make  -f Schemes/CMakeFiles/ddr-rmplate.dir/build.make Schemes/CMakeFiles/ddr-rmplate.dir/depend
Dependencies file "Schemes/CMakeFiles/hmm-stefanpme-transient.dir/HMM/HMM_StefanPME_transient.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/hmm-stefanpme-transient.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target hmm-stefanpme-transient
[ 79%] Built target lepnc-diffusion
[ 75%] Built target hho-locvardiff
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 83%] Built target lepnc-stefanpme
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/ddr-rmplate.dir/DependInfo.cmake --color=
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/hmm-stefanpme-transient.dir/build.make Schemes/CMakeFiles/hmm-stefanpme-transient.dir/build
make[2]: Nothing to be done for 'Schemes/CMakeFiles/lepnc-stefanpme-transient.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 87%] Built target hho-diffusion
Dependencies file "Schemes/CMakeFiles/ddr-rmplate.dir/DDR-rmplate/ddr-rmplate.cpp.o.d" is newer than depends file "/tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/ddr-rmplate.dir/compiler_depend.internal".
Consolidate compiler generated dependencies of target ddr-rmplate
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/ddr-rmplate.dir/build.make Schemes/CMakeFiles/ddr-rmplate.dir/build
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/hmm-stefanpme-transient.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[2]: Nothing to be done for 'Schemes/CMakeFiles/ddr-rmplate.dir/build'.
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[ 91%] Built target lepnc-stefanpme-transient
[ 95%] Built target hmm-stefanpme-transient
[100%] Built target ddr-rmplate
make[1]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/bin/cmake -E cmake_progress_start /tmp/makepkg/HArDCore2D/src/build-cmake/CMakeFiles 0
==> Starting check()...
/usr/bin/cmake -S/tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 -B/tmp/makepkg/HArDCore2D/src/build-cmake --check-build-system CMakeFiles/Makefile.cmake 0
/usr/sbin/make  -f CMakeFiles/Makefile2 TestCase
make[1]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/bin/cmake -S/tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 -B/tmp/makepkg/HArDCore2D/src/build-cmake --check-build-system CMakeFiles/Makefile.cmake 0
/usr/bin/cmake -E cmake_progress_start /tmp/makepkg/HArDCore2D/src/build-cmake/CMakeFiles 1
/usr/sbin/make  -f CMakeFiles/Makefile2 Schemes/CMakeFiles/TestCase.dir/all
make[2]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/TestCase.dir/build.make Schemes/CMakeFiles/TestCase.dir/depend
make[3]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
cd /tmp/makepkg/HArDCore2D/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0 /tmp/makepkg/HArDCore2D/src/HArDCore2D-release-5.0/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes /tmp/makepkg/HArDCore2D/src/build-cmake/Schemes/CMakeFiles/TestCase.dir/DependInfo.cmake --color=
make[3]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/sbin/make  -f Schemes/CMakeFiles/TestCase.dir/build.make Schemes/CMakeFiles/TestCase.dir/build
make[3]: Entering directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
make[3]: Nothing to be done for 'Schemes/CMakeFiles/TestCase.dir/build'.
make[3]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
[100%] Built target TestCase
make[2]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
/usr/bin/cmake -E cmake_progress_start /tmp/makepkg/HArDCore2D/src/build-cmake/CMakeFiles 0
make[1]: Leaving directory '/tmp/makepkg/HArDCore2D/src/build-cmake'
Internal ctest changing into directory: /tmp/makepkg/HArDCore2D/src/build-cmake
UpdateCTestConfiguration  from :/tmp/makepkg/HArDCore2D/src/build-cmake/DartConfiguration.tcl
UpdateCTestConfiguration  from :/tmp/makepkg/HArDCore2D/src/build-cmake/DartConfiguration.tcl
Test project /tmp/makepkg/HArDCore2D/src/build-cmake
Constructing a list of tests
Updating test list for fixtures
Added 0 tests to meet fixture requirements
Checking test dependency graph...
Checking test dependency graph end
No tests were found!!!
==> Entering fakeroot environment...
==> Starting package()...
make: *** No rule to make target 'install'.  Stop.
==> ERROR: A failure occurred in package().
    Aborting...
```

```
all (the default if no target is provided)
clean
depend
edit_cache
rebuild_cache
BoundaryConditions
TestCase
TestCaseStefanPME
TestCaseTransient
basis
ddr-rmplate
ddrcore
hho-diff-advec-reac
hho-diffusion
hho-general
hho-locvardiff
hmm-stefanpme-transient
hybridcore
lepnc-diffusion
lepnc-stefanpme
lepnc-stefanpme-transient
mesh
plot
quadrature
```

<!-- [![Packaging status](https://repology.org/badge/vertical-allrepos/hardcore2d.svg)](https://repology.org/project/hardcore2d/versions) -->
