GXX_RELEASE_WARNING_OPTS=" \
    -Wall \
    -Wunused \
    -Wmissing-include-dirs \
    -Wcast-align \
    -Wno-missing-braces \
    -Wmissing-field-initializers \
    -Wno-sign-compare"

GXX_RELEASE_OPTS=" \
    -fdiagnostics-color=always \
    -fno-strict-aliasing \
    -fstrict-overflow \
    -fno-finite-math-only \
    -DNDEBUG=1 \
    -O3 \
    -march=native \
    -funroll-loops \
    -g0"

SPECIFIC_COMPILER="
    -DCMAKE_C_COMPILER=/usr/bin/gcc
    -DCMAKE_CXX_COMPILER=/usr/bin/g++"

SPECIFIC_GENERATOR="
    -DCMAKE_GENERATOR='Unix Makefiles'
    -DCMAKE_MAKE_PROGRAM='/usr/bin/make'"

CMAKE_FLAGS="
    $SPECIFIC_COMPILER
    $SPECIFIC_GENERATOR
    -DCMAKE_CXX_FLAGS_RELEASE='$GXX_RELEASE_OPTS $GXX_RELEASE_WARNING_OPTS'
    -DCMAKE_CXX_FLAGS_DEBUG='-O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter -Wno-sign-compare'
    -DCMAKE_CXX_FLAGS_RELWITHDEBINFO='$GXX_RELEASE_OPTS $GXX_RELEASE_WARNING_OPTS -g -ggdb -Wall'
    -DCMAKE_C_FLAGS_RELEASE='-fPIC'
    -DCMAKE_BUILD_TYPE=Release
    -DCMAKE_INSTALL_PREFIX=/usr
    -DDUNE_ENABLE_PYTHONBINDINGS=ON
    -DBUILD_SHARED_LIBS=TRUE
    -DPYTHON_INSTALL_LOCATION=system
    -DADDITIONAL_PIP_PARAMS='-upgrade'
    -DCMAKE_DISABLE_DOCUMENTATION=TRUE"
