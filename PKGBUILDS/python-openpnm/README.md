[](https://github.com/PMEAL/OpenPNM)

python-chemicals wip
python-docrep (aur)
python-flatdict ok
python-gitpython ok
python-h5py ok also python-h5py-openmpi
ipython ok
python-jsonschema ok
python-json-tricks (aur)
python-matplotlib ok
python-networkx ok
python-numba (aur)
python-numpy ok
python-pandas ok
python-scikit-image (aur)
python-pypardiso wip
python-scipy ok
python-sympy ok
python-terminaltables ok
python-tqdm ok
python-transforms3d (aur)
