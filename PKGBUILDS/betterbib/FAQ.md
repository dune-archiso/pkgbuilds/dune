- [GitHub](https://github.com/nschloe/betterbib)
- [Pypi](https://pypi.org/project/betterbib)

```console
Installing collected packages: url-normalize, PyYAML, pygments, latexcodec, itsdangerous, commonmark, rich, requests-cache, pyenchant, pybtex, betterbib
Successfully installed PyYAML-5.4.1 betterbib-3.5.16 commonmark-0.9.1 itsdangerous-2.0.1 latexcodec-2.0.1 pybtex-0.24.0 pyenchant-3.2.0 pygments-2.9.0 requests-cache-0.6.4 rich-10.4.0 url-normalize-1.4.3
```

```
running install_scripts
Installing betterbib script to /tmp/makepkg/betterbib/src/betterbib/tmp_install/usr/bin
/usr/lib/python3.9/site-packages/pep8.py:110: FutureWarning: Possible nested set at position 1
  EXTRANEOUS_WHITESPACE_REGEX = re.compile(r'[[({] | []}),;:]')
============================= test session starts ==============================
platform linux -- Python 3.9.6, pytest-6.2.4, py-1.10.0, pluggy-0.13.1
rootdir: /tmp/makepkg/betterbib/src/betterbib
plugins: cov-2.12.1, pep8-1.0.6, flakes-4.0.3
collected 44 items
tests/test_bibtex_title.py F.F..FF....                                   [ 25%]
tests/test_crossref.py ............                                      [ 52%]
tests/test_dblp.py .                                                     [ 54%]
tests/test_journal_abbrev.py ....                                        [ 63%]
tests/test_tools.py .........                                            [ 84%]
tests/cli/test_doi_to_bibtex_cli.py .                                    [ 86%]
tests/cli/test_format_cli.py ....                                        [ 95%]
tests/cli/test_journal_abbrev_cli.py .                                   [ 97%]
tests/cli/test_update_cli.py .                                           [100%]
=================================== FAILURES ===================================
_ test_translate_title[The Magnus expansion and some of its applications-The {Magnus} expansion and some of its applications] _
string = 'The Magnus expansion and some of its applications'
ref = 'The {Magnus} expansion and some of its applications'
    @pytest.mark.parametrize(
        "string,ref",
        [
            (
                "The Magnus expansion and some of its applications",
                "The {Magnus} expansion and some of its applications",
            ),
            (
                "On generalized averaged Gaussian formulas, II",
                "On generalized averaged {Gaussian} formulas, {II}",
            ),
            ("Gaussian Hermitian Jacobian", "{Gaussian} {Hermitian} {Jacobian}"),
            (
                "VODE: a variable-coefficient ODE solver",
                "{VODE:} {A} variable-coefficient {ODE} solver",
            ),
            (
                "GMRES: A generalized minimal residual algorithm",
                "{GMRES:} {A} generalized minimal residual algorithm",
            ),
            (
                "Peano's kernel theorem for vector-valued functions",
                "{Peano's} kernel theorem for vector-valued functions",
            ),
            (
                "Exponential Runge-Kutta methods for parabolic problems",
                "Exponential {Runge}-{Kutta} methods for parabolic problems",
            ),
            (
                "Dash-Dash Double--Dash Triple---Dash",
                "Dash-Dash Double--Dash Triple---Dash",
            ),
            ("x: {X}", "x: {X}"),
            (
                "{Aaa ${\\text{Pt/Co/AlO}}_{x}$ aaa bbb}",
                "{Aaa {${\\text{Pt/Co/AlO}}_{x}$} aaa bbb}",
            ),
            ("z*", "z*"),
        ],
    )
    def test_translate_title(string, ref):
>       assert betterbib.tools._translate_title(string) == ref
E       AssertionError: assert 'The Magnus e... applications' == 'The {Magnus}... applications'
E         - The {Magnus} expansion and some of its applications
E         ?     -      -
E         + The Magnus expansion and some of its applications
tests/test_bibtex_title.py:47: AssertionError
_ test_translate_title[Gaussian Hermitian Jacobian-{Gaussian} {Hermitian} {Jacobian}] _
string = 'Gaussian Hermitian Jacobian'
ref = '{Gaussian} {Hermitian} {Jacobian}'
    @pytest.mark.parametrize(
        "string,ref",
        [
            (
                "The Magnus expansion and some of its applications",
                "The {Magnus} expansion and some of its applications",
            ),
            (
                "On generalized averaged Gaussian formulas, II",
                "On generalized averaged {Gaussian} formulas, {II}",
            ),
            ("Gaussian Hermitian Jacobian", "{Gaussian} {Hermitian} {Jacobian}"),
            (
                "VODE: a variable-coefficient ODE solver",
                "{VODE:} {A} variable-coefficient {ODE} solver",
            ),
            (
                "GMRES: A generalized minimal residual algorithm",
                "{GMRES:} {A} generalized minimal residual algorithm",
            ),
            (
                "Peano's kernel theorem for vector-valued functions",
                "{Peano's} kernel theorem for vector-valued functions",
            ),
            (
                "Exponential Runge-Kutta methods for parabolic problems",
                "Exponential {Runge}-{Kutta} methods for parabolic problems",
            ),
            (
                "Dash-Dash Double--Dash Triple---Dash",
                "Dash-Dash Double--Dash Triple---Dash",
            ),
            ("x: {X}", "x: {X}"),
            (
                "{Aaa ${\\text{Pt/Co/AlO}}_{x}$ aaa bbb}",
                "{Aaa {${\\text{Pt/Co/AlO}}_{x}$} aaa bbb}",
            ),
            ("z*", "z*"),
        ],
    )
    def test_translate_title(string, ref):
>       assert betterbib.tools._translate_title(string) == ref
E       AssertionError: assert '{Gaussian} H...tian Jacobian' == '{Gaussian} {...n} {Jacobian}'
E         - {Gaussian} {Hermitian} {Jacobian}
E         ?            -         ^^^        -
E         + {Gaussian} Hermitian Jacobian
E         ?                     ^
tests/test_bibtex_title.py:47: AssertionError
_ test_translate_title[Peano's kernel theorem for vector-valued functions-{Peano's} kernel theorem for vector-valued functions] _
string = "Peano's kernel theorem for vector-valued functions"
ref = "{Peano's} kernel theorem for vector-valued functions"
    @pytest.mark.parametrize(
        "string,ref",
        [
            (
                "The Magnus expansion and some of its applications",
                "The {Magnus} expansion and some of its applications",
            ),
            (
                "On generalized averaged Gaussian formulas, II",
                "On generalized averaged {Gaussian} formulas, {II}",
            ),
            ("Gaussian Hermitian Jacobian", "{Gaussian} {Hermitian} {Jacobian}"),
            (
                "VODE: a variable-coefficient ODE solver",
                "{VODE:} {A} variable-coefficient {ODE} solver",
            ),
            (
                "GMRES: A generalized minimal residual algorithm",
                "{GMRES:} {A} generalized minimal residual algorithm",
            ),
            (
                "Peano's kernel theorem for vector-valued functions",
                "{Peano's} kernel theorem for vector-valued functions",
            ),
            (
                "Exponential Runge-Kutta methods for parabolic problems",
                "Exponential {Runge}-{Kutta} methods for parabolic problems",
            ),
            (
                "Dash-Dash Double--Dash Triple---Dash",
                "Dash-Dash Double--Dash Triple---Dash",
            ),
            ("x: {X}", "x: {X}"),
            (
                "{Aaa ${\\text{Pt/Co/AlO}}_{x}$ aaa bbb}",
                "{Aaa {${\\text{Pt/Co/AlO}}_{x}$} aaa bbb}",
            ),
            ("z*", "z*"),
        ],
    )
    def test_translate_title(string, ref):
>       assert betterbib.tools._translate_title(string) == ref
E       assert "Peano's kern...ued functions" == "{Peano's} ke...ued functions"
E         - {Peano's} kernel theorem for vector-valued functions
E         ? -       -
E         + Peano's kernel theorem for vector-valued functions
tests/test_bibtex_title.py:47: AssertionError
_ test_translate_title[Exponential Runge-Kutta methods for parabolic problems-Exponential {Runge}-{Kutta} methods for parabolic problems] _
string = 'Exponential Runge-Kutta methods for parabolic problems'
ref = 'Exponential {Runge}-{Kutta} methods for parabolic problems'
    @pytest.mark.parametrize(
        "string,ref",
        [
            (
                "The Magnus expansion and some of its applications",
                "The {Magnus} expansion and some of its applications",
            ),
            (
                "On generalized averaged Gaussian formulas, II",
                "On generalized averaged {Gaussian} formulas, {II}",
            ),
            ("Gaussian Hermitian Jacobian", "{Gaussian} {Hermitian} {Jacobian}"),
            (
                "VODE: a variable-coefficient ODE solver",
                "{VODE:} {A} variable-coefficient {ODE} solver",
            ),
            (
                "GMRES: A generalized minimal residual algorithm",
                "{GMRES:} {A} generalized minimal residual algorithm",
            ),
            (
                "Peano's kernel theorem for vector-valued functions",
                "{Peano's} kernel theorem for vector-valued functions",
            ),
            (
                "Exponential Runge-Kutta methods for parabolic problems",
                "Exponential {Runge}-{Kutta} methods for parabolic problems",
            ),
            (
                "Dash-Dash Double--Dash Triple---Dash",
                "Dash-Dash Double--Dash Triple---Dash",
            ),
            ("x: {X}", "x: {X}"),
            (
                "{Aaa ${\\text{Pt/Co/AlO}}_{x}$ aaa bbb}",
                "{Aaa {${\\text{Pt/Co/AlO}}_{x}$} aaa bbb}",
            ),
            ("z*", "z*"),
        ],
    )
    def test_translate_title(string, ref):
>       assert betterbib.tools._translate_title(string) == ref
E       AssertionError: assert 'Exponential ...olic problems' == 'Exponential ...olic problems'
E         - Exponential {Runge}-{Kutta} methods for parabolic problems
E         ?             -     - -     -
E         + Exponential Runge-Kutta methods for parabolic problems
tests/test_bibtex_title.py:47: AssertionError
=============================== warnings summary ===============================
../../../../../usr/lib/python3.9/site-packages/packaging/version.py:127: 362 warnings
  /usr/lib/python3.9/site-packages/packaging/version.py:127: DeprecationWarning: Creating a LegacyVersion has been deprecated and will be removed in the next major release
    warnings.warn(
-- Docs: https://docs.pytest.org/en/stable/warnings.html
=========================== short test summary info ============================
FAILED tests/test_bibtex_title.py::test_translate_title[The Magnus expansion and some of its applications-The {Magnus} expansion and some of its applications]
FAILED tests/test_bibtex_title.py::test_translate_title[Gaussian Hermitian Jacobian-{Gaussian} {Hermitian} {Jacobian}]
FAILED tests/test_bibtex_title.py::test_translate_title[Peano's kernel theorem for vector-valued functions-{Peano's} kernel theorem for vector-valued functions]
FAILED tests/test_bibtex_title.py::test_translate_title[Exponential Runge-Kutta methods for parabolic problems-Exponential {Runge}-{Kutta} methods for parabolic problems]
================= 4 failed, 40 passed, 362 warnings in 15.13s ==================
==> ERROR: A failure occurred in check().
    Aborting...
```
