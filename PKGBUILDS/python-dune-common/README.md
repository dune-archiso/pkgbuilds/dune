- [dune-common 2.8.0rc1](https://pypi.org/project/dune-common)

```
_dunecontrol="./${_name}/bin/dunecontrol"
_setupdunepy="./${_name}/bin/setup-dunepy.py"

export PYTHONPATH="${srcdir}/${_name}/build-cmake/python:$PYTHONPATH"
export DUNE_CONTROL_PATH="${srcdir}/${_name}"
export BUILDDIR="${srcdir}/${_name}/build-cmake"
export DUNE_PY_DIR="${pkgdir}/usr/lib/python3.9/site-packages/dune-py"
```

```
pkgver() {
  cd "${srcdir}/${_name}"
  printf "%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}
```

```
${_dunecontrol} --opts=${pkgname}.opts cmake : make
```

```
$_dunecontrol --only=${_name} make install DESTDIR="${pkgdir}"
echo ${PYTHONPATH} ${DUNE_CONTROL_PATH} ${BUILDDIR} ${DUNE_PY_DIR}
$_dunecontrol --only=${_name} make install_python DESTDIR="${pkgdir}"

# ${_setupdunepy} --opts=${pkgname}.opts install --prefix=/usr --root="${pkgdir}" --optimize=1
# ${_setupdunepy} --opts=${pkgname}.opts --module=${_name} install DESTDIR="${pkgdir}"
```

#### `python setup.py --help-commands`

```
scikit-build options:
                [--skip-generator-test]
  --build-type          specify the CMake build type (e.g. Debug or Release)
  -G , --generator      specify the CMake build system generator
  -j N                  allow N build jobs at once
  --cmake-executable    specify the path to the cmake executable
  --skip-generator-test
                        skip generator test when a generator is explicitly
                        selected using --generator
Arguments following a "--" are passed directly to CMake (e.g. -DMY_VAR:BOOL=TRUE).
Arguments following a second "--" are passed directly to  the build tool.
Standard commands:
  build                     build everything needed to install
  build_py                  "build" pure Python modules (copy to build directory)
  build_ext                 build C/C++ extensions (compile/link to build directory)
  build_clib                build C/C++ libraries used by Python extensions
  build_scripts             "build" scripts (copy and fixup #! line)
  clean                     clean up temporary files from 'build' command
  install                   install everything from build directory
  install_lib               install all Python modules (extensions and pure Python)
  install_headers           install C/C++ header files
  install_scripts           install scripts (Python or otherwise)
  install_data              install data files
  sdist                     create a source distribution (tarball, zip file, etc.)
  register                  register the distribution with the Python package index
  bdist                     create a built (binary) distribution
  bdist_dumb                create a "dumb" built distribution
  bdist_rpm                 create an RPM distribution
  bdist_wininst             create an executable installer for MS Windows
  check                     perform some checks on the package
  upload                    upload binary package to PyPI
Extra commands:
  bdist_wheel               create a wheel distribution
  egg_info                  create a distribution's .egg-info directory
  generate_source_manifest  generate source MANIFEST
  test                      run unit tests after in-place build (deprecated)
  build_sphinx              Build Sphinx documentation
  alias                     define a shortcut to invoke one or more commands
  bdist_egg                 create an "egg" distribution
  develop                   install package in 'development mode'
  dist_info                 create a .dist-info directory
  easy_install              Find/get/install Python packages
  install_egg_info          Install an .egg-info directory for the package
  rotate                    delete older distributions, keeping N newest files
  saveopts                  save supplied options to setup.cfg or other config file
  setopt                    set an option in setup.cfg or another config file
  upload_docs               Upload documentation to sites other than PyPi such as devpi
  compile_catalog           compile message catalogs to binary MO files
  extract_messages          extract localizable strings from the project code
  init_catalog              create a new catalog based on a POT file
  update_catalog            update message catalogs from a POT file
usage: setup.py [global_opts] cmd1 [cmd1_opts] [cmd2 [cmd2_opts] ...]
   or: setup.py --help [cmd1 cmd2 ...]
   or: setup.py --help-commands
   or: setup.py cmd --help
```

#### `python setup.py build --help`

```
Common commands: (see '--help-commands' for more)
  setup.py build      will build the package underneath 'build/'
  setup.py install    will install the package
Global options:
  --verbose (-v)  run verbosely (default)
  --quiet (-q)    run quietly (turns verbosity off)
  --dry-run (-n)  don't actually do anything
  --help (-h)     show detailed help message
  --no-user-cfg   ignore pydistutils.cfg in your home directory
  --hide-listing  do not display list of files being included in the
                  distribution
  --force-cmake   always run CMake
  --skip-cmake    do not run CMake
Options for 'build' command:
  --build-base (-b)  base directory for build library
  --build-purelib    build directory for platform-neutral distributions
  --build-platlib    build directory for platform-specific distributions
  --build-lib        build directory for all distribution (defaults to either
                     build-purelib or build-platlib
  --build-scripts    build directory for scripts
  --build-temp (-t)  temporary build directory
  --plat-name (-p)   platform name to build for, if supported (default: linux-
                     x86_64)
  --compiler (-c)    specify the compiler type
  --parallel (-j)    number of parallel build jobs
  --debug (-g)       compile extensions and libraries with debugging
                     information
  --force (-f)       forcibly build everything (ignore file timestamps)
  --executable (-e)  specify final destination interpreter path (build.py)
  --help-compiler    list available compilers
usage: setup.py [global_opts] cmd1 [cmd1_opts] [cmd2 [cmd2_opts] ...]
   or: setup.py --help [cmd1 cmd2 ...]
   or: setup.py --help-commands
   or: setup.py cmd --help
```

#### `python setup.py buildpy --help`

```
Common commands: (see '--help-commands' for more)
  setup.py build      will build the package underneath 'build/'
  setup.py install    will install the package
Global options:
  --verbose (-v)  run verbosely (default)
  --quiet (-q)    run quietly (turns verbosity off)
  --dry-run (-n)  don't actually do anything
  --help (-h)     show detailed help message
  --no-user-cfg   ignore pydistutils.cfg in your home directory
  --hide-listing  do not display list of files being included in the
                  distribution
  --force-cmake   always run CMake
  --skip-cmake    do not run CMake
Options for 'DunepyConfigure' command:
  --build-lib (-d)  directory to "build" (copy) to
  --compile (-c)    compile .py to .pyc
  --no-compile      don't compile .py files [default]
  --optimize (-O)   also compile with optimization: -O1 for "python -O", -O2
                    for "python -OO", and -O0 to disable [default: -O0]
  --force (-f)      forcibly build everything (ignore file timestamps)
usage: setup.py [global_opts] cmd1 [cmd1_opts] [cmd2 [cmd2_opts] ...]
   or: setup.py --help [cmd1 cmd2 ...]
   or: setup.py --help-commands
   or: setup.py cmd --help
```

#### `python setup.py build_ext --help`

```
Common commands: (see '--help-commands' for more)
  setup.py build      will build the package underneath 'build/'
  setup.py install    will install the package
Global options:
  --verbose (-v)  run verbosely (default)
  --quiet (-q)    run quietly (turns verbosity off)
  --dry-run (-n)  don't actually do anything
  --help (-h)     show detailed help message
  --no-user-cfg   ignore pydistutils.cfg in your home directory
  --hide-listing  do not display list of files being included in the
                  distribution
  --force-cmake   always run CMake
  --skip-cmake    do not run CMake
Options for 'build_ext' command:
  --build-lib (-b)     directory for compiled extension modules
  --build-temp (-t)    directory for temporary files (build by-products)
  --plat-name (-p)     platform name to cross-compile for, if supported
                       (default: linux-x86_64)
  --inplace (-i)       ignore build-lib and put compiled extensions into the
                       source directory alongside your pure Python modules
  --include-dirs (-I)  list of directories to search for header files
                       (separated by ':')
  --define (-D)        C preprocessor macros to define
  --undef (-U)         C preprocessor macros to undefine
  --libraries (-l)     external C libraries to link with
  --library-dirs (-L)  directories to search for external C libraries
                       (separated by ':')
  --rpath (-R)         directories to search for shared C libraries at runtime
  --link-objects (-O)  extra explicit link objects to include in the link
  --debug (-g)         compile/link with debugging information
  --force (-f)         forcibly build everything (ignore file timestamps)
  --compiler (-c)      specify the compiler type
  --parallel (-j)      number of parallel build jobs
  --swig-cpp           make SWIG create C++ files (default is C)
  --swig-opts          list of SWIG command line options
  --swig               path to the SWIG executable
  --user               add user include, library and rpath
  --help-compiler      list available compilers
usage: setup.py [global_opts] cmd1 [cmd1_opts] [cmd2 [cmd2_opts] ...]
   or: setup.py --help [cmd1 cmd2 ...]
   or: setup.py --help-commands
   or: setup.py cmd --help
```

#### `python setup.py build_clib --help`

```
Common commands: (see '--help-commands' for more)
  setup.py build      will build the package underneath 'build/'
  setup.py install    will install the package
Global options:
  --verbose (-v)  run verbosely (default)
  --quiet (-q)    run quietly (turns verbosity off)
  --dry-run (-n)  don't actually do anything
  --help (-h)     show detailed help message
  --no-user-cfg   ignore pydistutils.cfg in your home directory
  --hide-listing  do not display list of files being included in the
                  distribution
  --force-cmake   always run CMake
  --skip-cmake    do not run CMake
Options for 'build_clib' command:
  --build-clib (-b)  directory to build C/C++ libraries to
  --build-temp (-t)  directory to put temporary build by-products
  --debug (-g)       compile with debugging information
  --force (-f)       forcibly build everything (ignore file timestamps)
  --compiler (-c)    specify the compiler type
  --help-compiler    list available compilers
usage: setup.py [global_opts] cmd1 [cmd1_opts] [cmd2 [cmd2_opts] ...]
   or: setup.py --help [cmd1 cmd2 ...]
   or: setup.py --help-commands
   or: setup.py cmd --help
```
