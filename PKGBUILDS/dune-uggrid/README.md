## [`dune-uggrid`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-uggrid/PKGBUILD)

#### debian

- [](https://tracker.debian.org/pkg/dune-uggrid)
- [](https://salsa.debian.org/pjaap/dune-uggrid)
- [](https://salsa.debian.org/lisajuliafog/dune-uggrid)

```console
-- The following OPTIONAL packages have been found:
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * BLAS, fast linear algebra routines
 * Threads, Multi-threading library
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
-- The following REQUIRED packages have been found:
 * dune-common
-- The following OPTIONAL packages have not been found:
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
```

```
usr/
usr/include/
usr/include/dune/
usr/include/dune/uggrid/
usr/include/dune/uggrid/domain/
usr/include/dune/uggrid/domain/domain.h
usr/include/dune/uggrid/domain/std_domain.h
usr/include/dune/uggrid/gm/
usr/include/dune/uggrid/gm/algebra.h
usr/include/dune/uggrid/gm/cw.h
usr/include/dune/uggrid/gm/dlmgr.h
usr/include/dune/uggrid/gm/elements.h
usr/include/dune/uggrid/gm/evm.h
usr/include/dune/uggrid/gm/gm.h
usr/include/dune/uggrid/gm/pargm.h
usr/include/dune/uggrid/gm/refine.h
usr/include/dune/uggrid/gm/rm-write2file.h
usr/include/dune/uggrid/gm/rm.h
usr/include/dune/uggrid/gm/shapes.h
usr/include/dune/uggrid/gm/ugm.h
usr/include/dune/uggrid/initug.h
usr/include/dune/uggrid/low/
usr/include/dune/uggrid/low/debug.h
usr/include/dune/uggrid/low/dimension.h
usr/include/dune/uggrid/low/fileopen.h
usr/include/dune/uggrid/low/heaps.h
usr/include/dune/uggrid/low/misc.h
usr/include/dune/uggrid/low/namespace.h
usr/include/dune/uggrid/low/scan.h
usr/include/dune/uggrid/low/ugenv.h
usr/include/dune/uggrid/low/ugstruct.h
usr/include/dune/uggrid/low/ugtimer.h
usr/include/dune/uggrid/low/ugtypes.h
usr/include/dune/uggrid/parallel/
usr/include/dune/uggrid/parallel/ddd/
usr/include/dune/uggrid/parallel/ddd/basic/
usr/include/dune/uggrid/parallel/ddd/basic/lowcomm.h
usr/include/dune/uggrid/parallel/ddd/basic/notify.h
usr/include/dune/uggrid/parallel/ddd/basic/oopp.h
usr/include/dune/uggrid/parallel/ddd/basic/ooppcc.h
usr/include/dune/uggrid/parallel/ddd/ctrl/
usr/include/dune/uggrid/parallel/ddd/ctrl/stat.h
usr/include/dune/uggrid/parallel/ddd/dddconstants.hh
usr/include/dune/uggrid/parallel/ddd/dddcontext.hh
usr/include/dune/uggrid/parallel/ddd/dddtypes.hh
usr/include/dune/uggrid/parallel/ddd/dddtypes_impl.hh
usr/include/dune/uggrid/parallel/ddd/if/
usr/include/dune/uggrid/parallel/ddd/if/if.h
usr/include/dune/uggrid/parallel/ddd/include/
usr/include/dune/uggrid/parallel/ddd/include/ddd.h
usr/include/dune/uggrid/parallel/ddd/join/
usr/include/dune/uggrid/parallel/ddd/join/join.h
usr/include/dune/uggrid/parallel/ddd/xfer/
usr/include/dune/uggrid/parallel/ddd/xfer/sll.h
usr/include/dune/uggrid/parallel/ddd/xfer/xfer.h
usr/include/dune/uggrid/parallel/dddif/
usr/include/dune/uggrid/parallel/dddif/parallel.h
usr/include/dune/uggrid/parallel/ppif/
usr/include/dune/uggrid/parallel/ppif/ppif.h
usr/include/dune/uggrid/parallel/ppif/ppifcontext.hh
usr/include/dune/uggrid/parallel/ppif/ppiftypes.hh
usr/include/dune/uggrid/ugdevices.h
usr/lib/
usr/lib/cmake/
usr/lib/cmake/dune-uggrid/
usr/lib/cmake/dune-uggrid/dune-uggrid-config-version.cmake
usr/lib/cmake/dune-uggrid/dune-uggrid-config.cmake
usr/lib/cmake/dune-uggrid/dune-uggrid-targets-none.cmake
usr/lib/cmake/dune-uggrid/dune-uggrid-targets.cmake
usr/lib/dunecontrol/
usr/lib/dunecontrol/dune-uggrid/
usr/lib/dunecontrol/dune-uggrid/dune.module
usr/lib/libduneuggrid.so
usr/lib/pkgconfig/
usr/lib/pkgconfig/dune-uggrid.pc
usr/share/
usr/share/RefRules.data
usr/share/dune/
usr/share/dune-uggrid/
usr/share/dune-uggrid/config.h.cmake
usr/share/dune/cmake/
usr/share/dune/cmake/modules/
usr/share/dune/cmake/modules/DuneUggridMacros.cmake
usr/share/licenses/
usr/share/licenses/dune-uggrid/
usr/share/licenses/dune-uggrid/LICENSE
usr/share/tetra.rls
usr/share/triangle.rls
```

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-uggrid.svg)](https://repology.org/project/dune-uggrid/versions)
