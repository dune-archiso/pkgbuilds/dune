```console
# cd "${srcdir}/${pkgname}-${_tarver}/doc"
# pdflatex applmanual.tex
# pdflatex progmanual.tex
# pdflatex refmanualI.tex
# pdflatex refmanualII.tex
# for i in *.tex; do pdflatex "$i"; done
# mv "${srcdir}/${pkgname}-${_tarver}/doc" "${pkgdir}/usr/share/doc/${pkgname}/"
```

```console
# export CFLAGS="-I/usr/include/tirpc ${CFLAGS}"
# export LDFLAGS="-ldl -lm -ltirpc"
```

```console
CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr"

-DCMAKE_C_COMPILER=clang-7
-DCMAKE_CXX_COMPILER=clang++-7
-DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always '
-DCMAKE_CXX_FLAGS='-Wall -fdiagnostics-color=always -std=c++17 -stdlib=libc++ '
-DCMAKE_BUILD_TYPE=Release
-DCMAKE_C_FLAGS_RELEASE='-O3 -g -UNDEBUG'
-DCMAKE_CXX_FLAGS_RELEASE='-O3 -g -UNDEBUG'
-DCMAKE_INSTALL_PREFIX=/duneci/install
-DTARGET_ARCHITECTURE=generic
-DCMAKE_DISABLE_FIND_PACKAGE_GMP=1
-DCXX_MAX_STANDARD=17
-DTARGET_ARCHITECTURE=none "/builds/agnumpde/dune-matrix-vector"
```

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
auxclean
build_quick_tests
build_tests
clean_latex
doc
doxygen_install
dvi
headercheck
html
install_python
pdf
ps
safepdf
test_python
analyser2
analyser3
basic
ctrl2
ctrl3
ddd2
ddd3
dddif2
dddif3
devices
domS2
domS3
ident2
ident3
if2
if3
join2
join3
low
mgr2
mgr3
numerics2
numerics3
ppifmpi
prio2
prio3
rm3-show
rm3-tetrahedron-rules-test
rm3-writeRefRules2file
test-fifo
testbtree
ugL
ugS2
ugS3
ug_gm2
ug_gm3
ugui2
ugui3
xfer2
xfer3
```

```console
# cat > /work/tutorial/hdnum-tutorial-beamer.tex <<EOF
# \documentclass[ignorenonframetext]{beamer}
# EOF
# echo 
# sed -i 's/^set(BUILD_DOCS 1/set(BUILD_DOCS 0/' ${srcdir}/${pkgname}-${pkgver}/CMakeLists.txt
```
