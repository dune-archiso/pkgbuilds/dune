[](https://mfem.org/building)

```console
arch4edu/rocthrust 4.2.0-1 (459.0 KiB 5.2 MiB) 
    Port of the Thrust parallel algorithm library atop HIP/ROCm
arch4edu/rocprim 4.2.0-1 (105.3 KiB 1.3 MiB) 
    Header-only library providing HIP parallel primitives
arch4edu/miopen-hip 4.2.0-1 (27.6 MiB 737.8 MiB) 
    AMD's Machine Intelligence Library (HIP backend)
arch4edu/hipsparse 4.2.0-1 (60.3 KiB 553.5 KiB) 
    rocSPARSE marshalling library.
arch4edu/hipfft 4.2.0-1 (19.8 KiB 62.8 KiB) 
    rocFFT marshalling library.
arch4edu/hipcub 4.2.0-1 (35.5 KiB 368.1 KiB) 
    Header-only library on top of rocPRIM or CUB
arch4edu/hipblas 4.2.0-1 (78.9 KiB 1.3 MiB) 
    ROCm BLAS marshalling library
arch4edu/hip-rocclr 4.2.0-4 (1.0 MiB 5.5 MiB) 
    Heterogeneous Interface for Portability ROCm
```

[](https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=freefem)
[](https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=elmerfem)
