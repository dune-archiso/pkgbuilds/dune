- [GitHub](https://github.com/inducer/cgen)
- [Pypi](https://pypi.org/project/cgen)

[![Packaging status](https://repology.org/badge/vertical-allrepos/cgen.svg)](https://repology.org/project/cgen/versions)
