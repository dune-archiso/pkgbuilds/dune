- [cuda](https://archlinux.org/packages/community/x86_64/cuda)
- [arch-ci-linux-viennacl.py](https://github.com/petsc/petsc/blob/main/config/examples/arch-ci-linux-viennacl.py)
- [viennacl](https://aur.archlinux.org/packages/viennacl/?comments=all)
- [opencl-nvidia](https://archlinux.org/packages/extra/x86_64/opencl-nvidia)

```console
Looking for PAPI_flops in papi - not found
Could NOT find PAPI (missing: PAPI_LIB_WORKS)
```

```console
/usr/share/dune/cmake/modules/FindPThreads.cmake exists in both 'dune-alugrid' and 'dune-fem'
/usr/share/dune/cmake/modules/FindSIONlib.cmake exists in both 'dune-alugrid' and 'dune-fem'
```

[A generic interface for parallel and adaptive discretization schemes: abstraction principles and the DUNE-FEM module](https://link.springer.com/article/10.1007/s00607-010-0110-3)
[A generic grid interface for parallel and adaptive scientific computing. Part II: implementation and tests in DUNE](https://link.springer.com/article/10.1007/s00607-008-0004-9)

[](https://stackoverflow.com/a/66604934)
[](https://dune-project.org/pdf/meetings/2013-08-globalfunctions/dune-fem-talk.pdf)

```
what():  InvalidStateException [initialize:/tmp/makepkg/dune-fem/src/dune-fem-v2.8.0.5/dune/fem/solver/petscinverseoperators.hh:313]: PetscInverseOperator: petsc not build with hypre support.
```

[OpenMP is considered as an optional dependency in dune-fem](https://github.com/samuelburbulla/dune-mmesh/issues/2#issuecomment-993670408).
