```console
usage  : ./configure [options]
options: [--prefix=DIR]                                  # Installation directory (default: ./install/<platform>)
         [--compiler=(gnu|pgi|intel|path|ibm|sun)]       # Compiler (Linux only)
         [--mpi=(mpich|mpich2|lam|openmpi|intel|intel2|  # MPI-Lib  (Linux/Solaris only)
                 hp|scali|mpibull2|sun)]
         [--force-(basic|32|64|32-64|cross)]             # Force basic, 32-bit, 64-bit,
                                                         # 32-bit and 64-bit, or cross-compiling
                                                         # installation
         [--msa=(hostname-regex|deep-est-sdv)]           # Build with optimized collectives for MSA
         [--disable-(mpi|omp|ompi)]                      # Disable MPI, OpenMP, or hybrid
         [--disable-fortran]                             # Disable Fortran support
         [--disable-cxx]                                 # Disable C++ support
         [--enable-ime[=DIR]]                            # Enable IME native API, optional pass IME client lib instalation directory
         [--enable-python=(2|3)]                         # Enable Python support
         [--disable-mic]                                 # Disable compilation for MICs
         [--disable-parutils]                            # Disable compilation of parutils (used for benchmarking)
         [--enable-debug]                                # enable SIONlib debug
         [--enable-kcov]                                 # collect test suite coverage data with kcov (if available)
         [--enable-gcovr]                                # collect test suite coverage data with gcovr (if available)
         [--enable-cuda]                                 # enable CUDA aware interface
         [--enable-cuda=/path/to/cuda]                   # ditto and specify CUDA installation path
         [--enable-sionfwd]                              # enable I/O forwarding with SIONfwd
         [--enable-sionfwd=/path/to/sionfwd]             # ditto and specify SIONfwd installation path
         [--disable-pthreads]                            # configure SIONlib not to use pthreads for locking
```

```console
# cat configure.log GNUmakefile Makefile Makefile.defs
% ./configure --prefix=/usr --mpi=openmpi --enable-python=3
WARNING: Installation directory /usr not writable.
INFO: Found /usr/sbin/gcc
Configuration for Linux cluster with GNU compilers and openmpi MPI
- Setting up build configuration (build-linux-gomp10-openmpi)
- Generating GNUmakefile (using Makefile.basic)
- Generating Makefile.defs (using Makefile.defs.linux-gomp10 + optional.defs + lock.defs)
Configuring Makefile.defs
- Setting PREFIX=/usr
- Configuring for gnu v11.1.0 compilers
- Using gfortran instead of g77
- Configuring openmpi MPI library
- Enabling Python support
- Using default 64-bit compilation mode
NOTE: Configured for PTHREADS support (using mutex_lock/unlock)
.NOTPARALLEL:
TOPDIR = .
include Makefile.defs
TOPDIR = ..
include $(TOPDIR)/mf/common.defs
BSUF=
all: setup
	cd build; $(MAKE) -f RealMakefile all
test: all
	@echo ""
	@echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	@echo "+  run Tests                                         +"
	@echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	@echo ""
	cd build; $(MAKE) -f RealMakefile test
comptest: all
	@echo ""
	@echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	@echo "+  compile Tests                                     +"
	@echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	@echo ""
	cd build; $(MAKE) -f RealMakefile comptest
packtest: all
	@echo ""
	@echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	@echo "+  run Packtests                                     +"
	@echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	@echo ""
	cd build; $(MAKE) -f RealMakefile packtest
depend: all
	@echo ""
	@echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	@echo "+  run make depend                                   +"
	@echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	@echo ""
	cd build; $(MAKE) -f RealMakefile depend
install: all
	cd build; $(MAKE) -f RealMakefile install
installtests: all install
	cd build; $(MAKE) -f RealMakefile installtests
checkinst: all install
	cd build; $(MAKE) -f RealMakefile checkinst
clean:
	cd build; $(MAKE) -f RealMakefile clean
relink: setup mklinks chklinks
setup:
	@if test ! -d build; then $(MAKE) setup-real; fi
setup-real: mk-build-dir build/lndir mklinks
mk-build-dir:
	@echo ""
	@echo "++++++++++ SETUP ++++++++++++++++++++++++++++++"
	-mkdir build
mklinks:
	cd build; ./lndir ../../src
	-mkdir build/mf
	cd build/mf; ../lndir ../../../mf
	cd build; rm -f RealMakefile; ln -s ../../mf/RealMakefile .
	cd build; rm -f GNUmakefile; ln -s RealMakefile GNUmakefile
	cd build; rm -f Makefile; \
		ln -s ../../mf/Makefile.redirect Makefile
	cd build; rm -f Makefile.defs; ln -s ../Makefile.defs Makefile.defs
	-mkdir build/examples
	cd build/examples; ../lndir ../../../examples
	-mkdir build/test
	cd build/test; ../lndir ../../../test
	cd build/test; rm -f RealMakefile; ln -s ../../../mf/RealMakefile .
chklinks:
	$(TOPDIR)/config/cleanlinks.sh build
build/lndir: $(TOPDIR)/config/lndir.c
	$(CC) $(TOPDIR)/config/lndir.c -o build/lndir
realclean:
	rm -rf build
TOPDIR = .
include Makefile.defs
.DEFAULT all:
	@echo "INFO: make replaced by GNU make (${MAKE}) ..."
	@echo ""
	@${MAKE} -f GNUmakefile $@
#------------------------------------------------------------------------------
# SIONlib Makefile Definitions (Makefile.defs.linux-gomp)
#------------------------------------------------------------------------------
CFGKEY = linux-gomp10-openmpi
CONFIG = Linux cluster with GNU compilers and openmpi MPI
PREFIX = /usr
#------------------------------------------------------------------------------
# Platform Settings
#------------------------------------------------------------------------------
PREC   = 64
PFLAG  = -m$(PREC)
AFLAG  =
PLAT     = linux
MACHINE  = -DMACHINE=\"Linux\ Cluster\"
#PLAT_LDIR= -DPFORM_LDIR=\"/tmp\"
PLATCC   = $(MPICC) $(PFLAG)
PLAT_CONF= $(MACHINE) $(PLAT_LDIR)
#------------------------------------------------------------------------------
# SIONlib General Settings
#------------------------------------------------------------------------------
OPTFLAGS = -g -O0 -Wall
CC       = gcc
CFLAGS   = -std=c99 $(PFLAG) $(OPTFLAGS) -fPIC
CXX      = g++
CXXFLAGS = $(PFLAG) $(OPTFLAGS) -O3
F77      = gfortran
FFLAGS   = $(PFLAG) $(OPTFLAGS) -fallow-argument-mismatch
F90      = gfortran
F90FLAGS = $(PFLAG) $(OPTFLAGS) -ffree-form -fallow-argument-mismatch
FPP      =
FDOPT    = -D
LDFLAGS  = $(PFLAG) $(OPTFLAGS) $(HINTSLIB)
UTILLIB  =
FOBASE   = pomp_fwrapper_base.o
#------------------------------------------------------------------------------
# SIONlib Settings
#------------------------------------------------------------------------------
SION_PLATFORM    = -D_SION_LINUX 
SION_MSA         =
ARCH             = LINUX
# Library Naming Scheme: sion{API}{FE}${DEVICE}${GCC}${LANG}${PREC}
#                        API=(com|ser|gen|omp|mpi|ompi) 
#			 FE=(|fe) 
#			 DEVICE=(|mic) 
#			 GCC=(|gcc) 
#			 LANG=(|_cxx|_f77|_f90) 
#			 PREC=(_32|_64)
# Library Naming Scheme: see Makefile.defs.linux-gomp
SION_LIBNAME_COM = sioncom_$(PREC)
SION_LIBNAME_SER = sionser_$(PREC)
SION_LIBNAME_GEN = siongen_$(PREC)
SION_LIBNAME_OMP     = sionomp_$(PREC)
SION_LIBNAME_MPI     = sionmpi_$(PREC)
SION_LIBNAME_OMPI    = sionompi_$(PREC)
PARUTILENABLE    = 1
FORTRANENABLE    = 1
FORTRANNAMES     = -D_FORTRANUNDERSCORE
SION_LIBNAME_F77_SER = sionser_f77_$(PREC)
SION_LIBNAME_F77_MPI = sionmpi_f77_$(PREC)
SION_LIBNAME_F90_SER = sionser_f90_$(PREC)
SION_LIBNAME_F90_MPI = sionmpi_f90_$(PREC)
SION_MODPATH = mod_$(PREC)
CXXENABLE            = 1
SION_LIBNAME_CXX_SER = sionser_cxx_$(PREC)
SION_LIBNAME_CXX_MPI = sionmpi_cxx_$(PREC)
PYTHONENABLE = 1
SION_DEBUG       = 
COVERAGE         =
ifeq ($(COVERAGE),gcovr)
	CFLAGS += -fprofile-arcs -ftest-coverage
	CXXFLAGS += -fprofile-arcs -ftest-coverage
	FFLAGS += -fprofile-arcs -ftest-coverage
	F90FLAGS += -fprofile-arcs -ftest-coverage
	LDFLAGS += -lgcov
endif
#------------------------------------------------------------------------------
# MPI Settings
#------------------------------------------------------------------------------
MPIENABLE = 1
MPICC     = OMPI_CC="$(CC)" mpicc
MPICXX    = OMPI_CXX="$(CXX)" mpicxx
MPIF77    = OMPI_FC="$(F77)" mpif77
MPIF90    = OMPI_FC="$(F90)" mpif90
MPILIB    = 
MPITESTRUN = mpirun -np 4 --oversubscribe
#------------------------------------------------------------------------------
# OpenMP Settings
#------------------------------------------------------------------------------
OMPENABLE = 1
OMPCC      = $(CC)
OMPCXX     = $(CXX)
OMPF77     = $(F77) 
OMPF90     = $(F90) 
OMPFLAG    = -fopenmp
#------------------------------------------------------------------------------
# Hybrid MPI/OpenMP Settings
#------------------------------------------------------------------------------
HYBENABLE  = 1
HYBCC      = $(MPICC)
HYBCXX     = $(MPICXX)
HYBF77     = $(MPIF77)
HYBF90     = $(MPIF90)
HYBFLAG    = $(OMPFLAG)
#------------------------------------------------------------------------------
# Compression Support
#------------------------------------------------------------------------------
SZLIB          = szlib
SZLIB_OPTFLAGS = -O3
SZLIB_CFLAGS   = -I$(TOPDIR)/utils/szlib -DELG_COMPRESSED -DCUBE_COMPRESSED
SZLIB_LIBPATH  = -L$(TOPDIR)/utils/szlib
SZLIB_LIB      = -lsc.z
#------------------------------------------------------------------------------
# IME Support
#------------------------------------------------------------------------------
IMEENABLE = 0
#IMELIB          = im_client
#IMELIB_CFLAGS   = -I/opt/ddn/ime/include -D_SION_IME_NATIVE
#IMELIB_LIBPATH  = -L/opt/ddn/ime/lib/
#IMELIB_LIB      = -lim_client
#------------------------------------------------------------------------------
# SIONlib HINTS library
#------------------------------------------------------------------------------
GPFS_HINTS_LIB  = -L/usr/lpp/mmfs/lib -lgpfs
GPFS_HINTS_INC  = -I/usr/lpp/mmfs/include
LINUX_HINTS_LIB = -L/usr/lib
LINUX_HINTS_INC = -I/usr/include
HINTSDEF=
HINTSINC=
HINTSLIB=
#------------------------------------------------------------------------------
# SIONlib Cache Settings
#------------------------------------------------------------------------------
CACHEFLAGS=
CACHELIB=
#CACHEFLAGS=-D_SION_CACHE
#CACHELIB=-lrt
#------------------------------------------------------------------------------
# CUDA Settings
#------------------------------------------------------------------------------
SION_CUDA =
CUDA_INCLUDE_PATHS =
CUDA_LIBRARY_PATHS =
CUDA_LIBRARIES =
#------------------------------------------------------------------------------
# SIONFWD Settings
#------------------------------------------------------------------------------
SION_SIONFWD =
SIONFWD_CFLAGS =
SIONFWD_LIBS =
SION_PTHREADS = -DSION_PTHREADS
PTHREADSUPPORT = yes 
SION_LIBNAME_COM_LOCK_PTHREADS = $(SION_LIBNAME_COM)_lock_pthreads
SION_LIBNAME_COM_LOCK_NONE = $(SION_LIBNAME_COM)_lock_none
SION_DEBUG = 
#------------------------------------------------------------------------------
# GNU Make Support
#-----------------------------------------------------------------------------
MAKE = make
```

[](https://apps.fz-juelich.de/jsc/sionlib/docu/1.7.7/installation_page.html)
[](https://gpo.zugaina.org/Overlays/guru/sys-cluster/sionlib)
[](https://prace-ri.eu/wp-content/uploads/wp165.pdf)
[DUNE-ISTL (Multigrid solver, Univ. Heidelberg)](https://core.ac.uk/download/pdf/35033161.pdf)
[](https://pdfs.semanticscholar.org/fc4d/32f5d3da2676b19ca9d394ee54fd24bd427f.pdf)
[](https://arxiv.org/pdf/1904.07725.pdf)
[](https://www.hpci-office.jp/output/KforEnhancement/h25.ra000016/outcome.pdf)
[](https://www.routledge.com/Exascale-Scientific-Applications-Scalability-and-Performance-Portability/Straatsma-Antypas-Williams/p/book/9780367572716)
[](https://docs.oracle.com/cd/E19205-01/819-5267/bkamn/index.html)
[](https://events.prace-ri.eu/event/549/sessions/1680/attachments/459/664/BERND-MOHR-perftools-2.pdf)
[](https://juser.fz-juelich.de/record/852451/files/tut108_VI-HPS.pdf)
[](https://dashboard.hpc.unimelb.edu.au/softwarelist.txt)
[](https://link.springer.com/book/10.1007%2F978-3-319-53862-4)
[](https://www.vi-hps.org/cms/upload/material/general/ToolsGuide.pdf)
[](https://github.com/bsc-pm/TCL/issues/1)
[](https://gitlab.com/petsc/pkg-sionlib/-/tree/master/src)
[](https://gitlab.inria.fr/cconrads/fti)
[](http://apps.fz-juelich.de/jsc/sionlib/docu/1.7.6/sioninter_8pyx_source.html)
[](http://apps.fz-juelich.de/jsc/sionlib/docu/1.6.2/setup_8py_source.html)
[](https://github.com/nest/nest-sionlib-reader)
[](https://spack.readthedocs.io/en/latest/package_list.html#sionlib)
