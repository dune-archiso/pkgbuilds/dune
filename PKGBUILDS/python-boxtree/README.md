- [GitHub](https://github.com/inducer/boxtree)
- [Pypi](https://pypi.org/project/boxtree)

[![Packaging status](https://repology.org/badge/vertical-allrepos/boxtree.svg)](https://repology.org/project/boxtree/versions)
