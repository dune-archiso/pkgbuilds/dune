[](https://github.com/arch4edu/arch4edu/pull/96)

##### `ubuntu-hirsute-hippo`

```
$ ./Allclean && ./Allwmake >/dev/null 2>&1
Cleaning complete!
$ ls -l lnInclude
total 0
lrwxrwxrwx 1 root root 12 Mar 28 18:53 Adapter.C -> ../Adapter.C
lrwxrwxrwx 1 root root 12 Mar 28 18:53 Adapter.H -> ../Adapter.H
lrwxrwxrwx 1 root root 12 Mar 28 18:53 CHT.C -> ../CHT/CHT.C
lrwxrwxrwx 1 root root 12 Mar 28 18:53 CHT.H -> ../CHT/CHT.H
lrwxrwxrwx 1 root root 21 Mar 28 18:53 CouplingDataUser.C -> ../CouplingDataUser.C
lrwxrwxrwx 1 root root 21 Mar 28 18:53 CouplingDataUser.H -> ../CouplingDataUser.H
lrwxrwxrwx 1 root root 21 Mar 28 18:53 Displacement.C -> ../FSI/Displacement.C
lrwxrwxrwx 1 root root 21 Mar 28 18:53 Displacement.H -> ../FSI/Displacement.H
lrwxrwxrwx 1 root root 26 Mar 28 18:53 DisplacementDelta.C -> ../FSI/DisplacementDelta.C
lrwxrwxrwx 1 root root 26 Mar 28 18:53 DisplacementDelta.H -> ../FSI/DisplacementDelta.H
lrwxrwxrwx 1 root root 10 Mar 28 18:53 FF.C -> ../FF/FF.C
lrwxrwxrwx 1 root root 10 Mar 28 18:53 FF.H -> ../FF/FF.H
lrwxrwxrwx 1 root root 12 Mar 28 18:53 FSI.C -> ../FSI/FSI.C
lrwxrwxrwx 1 root root 12 Mar 28 18:53 FSI.H -> ../FSI/FSI.H
lrwxrwxrwx 1 root root 14 Mar 28 18:53 Force.C -> ../FSI/Force.C
lrwxrwxrwx 1 root root 14 Mar 28 18:53 Force.H -> ../FSI/Force.H
lrwxrwxrwx 1 root root 18 Mar 28 18:53 ForceBase.C -> ../FSI/ForceBase.C
lrwxrwxrwx 1 root root 18 Mar 28 18:53 ForceBase.H -> ../FSI/ForceBase.H
lrwxrwxrwx 1 root root 17 Mar 28 18:53 HeatFlux.C -> ../CHT/HeatFlux.C
lrwxrwxrwx 1 root root 17 Mar 28 18:53 HeatFlux.H -> ../CHT/HeatFlux.H
lrwxrwxrwx 1 root root 32 Mar 28 18:53 HeatTransferCoefficient.C -> ../CHT/HeatTransferCoefficient.C
lrwxrwxrwx 1 root root 32 Mar 28 18:53 HeatTransferCoefficient.H -> ../CHT/HeatTransferCoefficient.H
lrwxrwxrwx 1 root root 14 Mar 28 18:53 Interface.C -> ../Interface.C
lrwxrwxrwx 1 root root 14 Mar 28 18:53 Interface.H -> ../Interface.H
lrwxrwxrwx 1 root root 23 Mar 28 18:53 KappaEffective.C -> ../CHT/KappaEffective.C
lrwxrwxrwx 1 root root 23 Mar 28 18:53 KappaEffective.H -> ../CHT/KappaEffective.H
lrwxrwxrwx 1 root root 16 Mar 28 18:53 Pressure.C -> ../FF/Pressure.C
lrwxrwxrwx 1 root root 16 Mar 28 18:53 Pressure.H -> ../FF/Pressure.H
lrwxrwxrwx 1 root root 24 Mar 28 18:53 PressureGradient.C -> ../FF/PressureGradient.C
lrwxrwxrwx 1 root root 24 Mar 28 18:53 PressureGradient.H -> ../FF/PressureGradient.H
lrwxrwxrwx 1 root root 24 Mar 28 18:53 SinkTemperature.C -> ../CHT/SinkTemperature.C
lrwxrwxrwx 1 root root 24 Mar 28 18:53 SinkTemperature.H -> ../CHT/SinkTemperature.H
lrwxrwxrwx 1 root root 15 Mar 28 18:53 Stress.C -> ../FSI/Stress.C
lrwxrwxrwx 1 root root 15 Mar 28 18:53 Stress.H -> ../FSI/Stress.H
lrwxrwxrwx 1 root root 20 Mar 28 18:53 Temperature.C -> ../CHT/Temperature.C
lrwxrwxrwx 1 root root 20 Mar 28 18:53 Temperature.H -> ../CHT/Temperature.H
lrwxrwxrwx 1 root root 14 Mar 28 18:53 Utilities.C -> ../Utilities.C
lrwxrwxrwx 1 root root 14 Mar 28 18:53 Utilities.H -> ../Utilities.H
lrwxrwxrwx 1 root root 16 Mar 28 18:53 Velocity.C -> ../FF/Velocity.C
lrwxrwxrwx 1 root root 16 Mar 28 18:53 Velocity.H -> ../FF/Velocity.H
lrwxrwxrwx 1 root root 24 Mar 28 18:53 VelocityGradient.C -> ../FF/VelocityGradient.C
lrwxrwxrwx 1 root root 24 Mar 28 18:53 VelocityGradient.H -> ../FF/VelocityGradient.H
lrwxrwxrwx 1 root root 33 Mar 28 18:53 preciceAdapterFunctionObject.C -> ../preciceAdapterFunctionObject.C
lrwxrwxrwx 1 root root 33 Mar 28 18:53 preciceAdapterFunctionObject.H -> ../preciceAdapterFunctionObject.H
$ cat Allwmake.log ldd.log wmake.log
Building the OpenFOAM-preCICE adapter...
WARNING: No configuration file 'libprecice.pc' was found in the PKG_CONFIG_PATH.
Check the preCICE documentation page 'Linking to preCICE' for more details, or proceed if you know what you are doing.
If not already known by the system, preCICE may be located using:
  pkg-config --cflags libprecice  = 
  pkg-config --libs libprecice    = 
Current OpenFOAM environment:
  WM_PROJECT = OpenFOAM
  WM_PROJECT_VERSION = v2112
The adapter will be built into /root/OpenFOAM/user-v2112/platforms/linux64GccDPInt32Opt/lib
Additional preprocessor/compiler options: 
Building with WMake (see the wmake.log log file)...
=== OK: Building completed successfully! ===
	linux-vdso.so.1 (0x00007ffd485f7000)
	libfiniteVolume.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libfiniteVolume.so (0x00007fa15432c000)
	libmeshTools.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libmeshTools.so (0x00007fa153ddc000)
	libcompressibleTurbulenceModels.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libcompressibleTurbulenceModels.so (0x00007fa153b53000)
	libincompressibleTurbulenceModels.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libincompressibleTurbulenceModels.so (0x00007fa15391d000)
	libimmiscibleIncompressibleTwoPhaseMixture.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libimmiscibleIncompressibleTwoPhaseMixture.so (0x00007fa1538fe000)
	libprecice.so.2 => /lib/x86_64-linux-gnu/libprecice.so.2 (0x00007fa153430000)
	libstdc++.so.6 => /lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007fa153217000)
	libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007fa1530c9000)
	libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007fa1530ae000)
	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007fa15308c000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fa152e9e000)
	libOpenFOAM.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libOpenFOAM.so (0x00007fa152617000)
	libPstream.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/sys-openmpi/libPstream.so (0x00007fa1525fc000)
	libturbulenceModels.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libturbulenceModels.so (0x00007fa15244f000)
	libincompressibleTransportModels.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libincompressibleTransportModels.so (0x00007fa1523b8000)
	libfileFormats.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libfileFormats.so (0x00007fa15223d000)
	libsurfMesh.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libsurfMesh.so (0x00007fa152087000)
	libcompressibleTransportModels.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libcompressibleTransportModels.so (0x00007fa152081000)
	libradiationModels.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libradiationModels.so (0x00007fa151f4d000)
	libfluidThermophysicalModels.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libfluidThermophysicalModels.so (0x00007fa151a91000)
	libsolidThermo.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libsolidThermo.so (0x00007fa1519a7000)
	libsolidSpecie.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libsolidSpecie.so (0x00007fa151981000)
	libspecie.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libspecie.so (0x00007fa1516de000)
	libtwoPhaseMixture.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libtwoPhaseMixture.so (0x00007fa1516b8000)
	libinterfaceProperties.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libinterfaceProperties.so (0x00007fa15163e000)
	libtwoPhaseProperties.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libtwoPhaseProperties.so (0x00007fa151605000)
	libboost_log_setup.so.1.74.0 => /lib/x86_64-linux-gnu/libboost_log_setup.so.1.74.0 (0x00007fa15154e000)
	libboost_program_options.so.1.74.0 => /lib/x86_64-linux-gnu/libboost_program_options.so.1.74.0 (0x00007fa1514de000)
	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007fa1514d7000)
	libxml2.so.2 => /lib/x86_64-linux-gnu/libxml2.so.2 (0x00007fa1512f6000)
	libpython3.9.so.1.0 => /lib/x86_64-linux-gnu/libpython3.9.so.1.0 (0x00007fa150d62000)
	libboost_log.so.1.74.0 => /lib/x86_64-linux-gnu/libboost_log.so.1.74.0 (0x00007fa150c92000)
	libboost_filesystem.so.1.74.0 => /lib/x86_64-linux-gnu/libboost_filesystem.so.1.74.0 (0x00007fa150c70000)
	libboost_thread.so.1.74.0 => /lib/x86_64-linux-gnu/libboost_thread.so.1.74.0 (0x00007fa150c4d000)
	libmpi_cxx.so.40 => /lib/x86_64-linux-gnu/libmpi_cxx.so.40 (0x00007fa150c2f000)
	libmpi.so.40 => /lib/x86_64-linux-gnu/libmpi.so.40 (0x00007fa150b09000)
	libpetsc_real.so.3.14 => /lib/x86_64-linux-gnu/libpetsc_real.so.3.14 (0x00007fa14f7d8000)
	/lib64/ld-linux-x86-64.so.2 (0x00007fa155746000)
	libz.so.1 => /lib/x86_64-linux-gnu/libz.so.1 (0x00007fa14f7ba000)
	libSLGThermo.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libSLGThermo.so (0x00007fa14f7b0000)
	libthermophysicalProperties.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libthermophysicalProperties.so (0x00007fa14f716000)
	libdistributed.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libdistributed.so (0x00007fa14f690000)
	libreactionThermophysicalModels.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libreactionThermophysicalModels.so (0x00007fa14ed84000)
	libboost_regex.so.1.74.0 => /lib/x86_64-linux-gnu/libboost_regex.so.1.74.0 (0x00007fa14ec64000)
	libicuuc.so.67 => /lib/x86_64-linux-gnu/libicuuc.so.67 (0x00007fa14ea79000)
	liblzma.so.5 => /lib/x86_64-linux-gnu/liblzma.so.5 (0x00007fa14ea4e000)
	libexpat.so.1 => /lib/x86_64-linux-gnu/libexpat.so.1 (0x00007fa14ea1f000)
	libutil.so.1 => /lib/x86_64-linux-gnu/libutil.so.1 (0x00007fa14ea18000)
	librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007fa14ea0d000)
	libopen-rte.so.40 => /lib/x86_64-linux-gnu/libopen-rte.so.40 (0x00007fa14e951000)
	libopen-pal.so.40 => /lib/x86_64-linux-gnu/libopen-pal.so.40 (0x00007fa14e89f000)
	libhwloc.so.15 => /lib/x86_64-linux-gnu/libhwloc.so.15 (0x00007fa14e846000)
	libHYPRE_core-2.18.2.so => /lib/x86_64-linux-gnu/libHYPRE_core-2.18.2.so (0x00007fa14e46d000)
	libdmumps-5.3.so => /lib/x86_64-linux-gnu/libdmumps-5.3.so (0x00007fa14e267000)
	libscalapack-openmpi.so.2.1 => /lib/x86_64-linux-gnu/libscalapack-openmpi.so.2.1 (0x00007fa14dcbf000)
	libumfpack.so.5 => /lib/x86_64-linux-gnu/libumfpack.so.5 (0x00007fa14dc11000)
	libamd.so.2 => /lib/x86_64-linux-gnu/libamd.so.2 (0x00007fa14dc06000)
	libcholmod.so.3 => /lib/x86_64-linux-gnu/libcholmod.so.3 (0x00007fa14db24000)
	libklu.so.1 => /lib/x86_64-linux-gnu/libklu.so.1 (0x00007fa14daf3000)
	libsuperlu.so.5 => /lib/x86_64-linux-gnu/libsuperlu.so.5 (0x00007fa14da7f000)
	libsuperlu_dist.so.6 => /lib/x86_64-linux-gnu/libsuperlu_dist.so.6 (0x00007fa14d946000)
	libfftw3.so.3 => /lib/x86_64-linux-gnu/libfftw3.so.3 (0x00007fa14d740000)
	libfftw3_mpi.so.3 => /lib/x86_64-linux-gnu/libfftw3_mpi.so.3 (0x00007fa14d728000)
	liblapack.so.3 => /lib/x86_64-linux-gnu/liblapack.so.3 (0x00007fa14d014000)
	libblas.so.3 => /lib/x86_64-linux-gnu/libblas.so.3 (0x00007fa14cf6e000)
	libptscotch-6.1.so => /lib/x86_64-linux-gnu/libptscotch-6.1.so (0x00007fa14ce9b000)
	libhdf5_openmpi.so.103 => /lib/x86_64-linux-gnu/libhdf5_openmpi.so.103 (0x00007fa14cb09000)
	libOpenCL.so.1 => /lib/x86_64-linux-gnu/libOpenCL.so.1 (0x00007fa14caf7000)
	libmpi_mpifh.so.40 => /lib/x86_64-linux-gnu/libmpi_mpifh.so.40 (0x00007fa14ca93000)
	libgfortran.so.5 => /lib/x86_64-linux-gnu/libgfortran.so.5 (0x00007fa14c7d7000)
	libdecompositionMethods.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libdecompositionMethods.so (0x00007fa14c73e000)
	libicui18n.so.67 => /lib/x86_64-linux-gnu/libicui18n.so.67 (0x00007fa14c436000)
	libicudata.so.67 => /lib/x86_64-linux-gnu/libicudata.so.67 (0x00007fa14a91d000)
	libevent_core-2.1.so.7 => /lib/x86_64-linux-gnu/libevent_core-2.1.so.7 (0x00007fa14a8e5000)
	libevent_pthreads-2.1.so.7 => /lib/x86_64-linux-gnu/libevent_pthreads-2.1.so.7 (0x00007fa14a8de000)
	libudev.so.1 => /lib/x86_64-linux-gnu/libudev.so.1 (0x00007fa14a8b5000)
	libmumps_common-5.3.so => /lib/x86_64-linux-gnu/libmumps_common-5.3.so (0x00007fa14a863000)
	libsuitesparseconfig.so.5 => /lib/x86_64-linux-gnu/libsuitesparseconfig.so.5 (0x00007fa14a85e000)
	libcolamd.so.2 => /lib/x86_64-linux-gnu/libcolamd.so.2 (0x00007fa14a855000)
	libccolamd.so.2 => /lib/x86_64-linux-gnu/libccolamd.so.2 (0x00007fa14a846000)
	libcamd.so.2 => /lib/x86_64-linux-gnu/libcamd.so.2 (0x00007fa14a83a000)
	libmetis.so.5 => /lib/x86_64-linux-gnu/libmetis.so.5 (0x00007fa14a7ce000)
	libgomp.so.1 => /lib/x86_64-linux-gnu/libgomp.so.1 (0x00007fa14a787000)
	libbtf.so.1 => /lib/x86_64-linux-gnu/libbtf.so.1 (0x00007fa14a781000)
	libCombBLAS.so.1.16.0 => /lib/x86_64-linux-gnu/libCombBLAS.so.1.16.0 (0x00007fa14a762000)
	libptscotcherr-6.1.so => /lib/x86_64-linux-gnu/libptscotcherr-6.1.so (0x00007fa14a75d000)
	libbz2.so.1.0 => /lib/x86_64-linux-gnu/libbz2.so.1.0 (0x00007fa14a74a000)
	libsz.so.2 => /lib/x86_64-linux-gnu/libsz.so.2 (0x00007fa14a745000)
	libquadmath.so.0 => /lib/x86_64-linux-gnu/libquadmath.so.0 (0x00007fa14a6fb000)
	libdynamicMesh.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libdynamicMesh.so (0x00007fa14a2c5000)
	libpord-5.3.so => /lib/x86_64-linux-gnu/libpord-5.3.so (0x00007fa14a2aa000)
	libaec.so.0 => /lib/x86_64-linux-gnu/libaec.so.0 (0x00007fa14a2a1000)
	libextrudeModel.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libextrudeModel.so (0x00007fa14a27d000)
	libblockMesh.so => /usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/libblockMesh.so (0x00007fa14a1f5000)
Compiling enabled on 4 cores
wmake libso (openfoam-adapter_v1.1.0_OpenFOAMv1812-v2112-newer)
    ln: ./lnInclude
Making dependency list for source file preciceAdapterFunctionObject.C
Making dependency list for source file Adapter.C
Making dependency list for source file PressureGradient.C
Making dependency list for source file Pressure.C
wmkdepend: could not open 'wmkdepend: could not open 'precice/SolverInterface.hpp' for source file 'precice/SolverInterface.hpppreciceAdapterFunctionObject.C': ' for source file 'No such file or directoryAdapter.C
': No such file or directory
Making dependency list for source file VelocityGradient.C
Making dependency list for source file Velocity.C
Making dependency list for source file FF.C
Making dependency list for source file DisplacementDelta.C
Making dependency list for source file Displacement.C
Making dependency list for source file Stress.C
wmkdepend: could not open 'precice/SolverInterface.hpp' for source file 'FF/FF.C': No such file or directory
Making dependency list for source file Force.C
Making dependency list for source file ForceBase.C
Making dependency list for source file FSI.C
Making dependency list for source file CHT.C
Making dependency list for source file SinkTemperature.C
Making dependency list for source file HeatTransferCoefficient.C
wmkdepend: could not open 'precice/SolverInterface.hpp' for source file 'FSI/FSI.C': No such file or directory
Making dependency list for source file HeatFlux.C
wmkdepend: could not open 'precice/SolverInterface.hpp' for source file 'CHT/CHT.C': No such file or directory
Making dependency list for source file KappaEffective.C
Making dependency list for source file Temperature.C
Making dependency list for source file CouplingDataUser.C
Making dependency list for source file Interface.C
Making dependency list for source file Utilities.C
wmkdepend: could not open 'precice/SolverInterface.hpp' for source file 'Interface.C': No such file or directory
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c Utilities.C -o Make/linux64GccDPInt32Opt/Utilities.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c Interface.C -o Make/linux64GccDPInt32Opt/Interface.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c CouplingDataUser.C -o Make/linux64GccDPInt32Opt/CouplingDataUser.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c CHT/Temperature.C -o Make/linux64GccDPInt32Opt/CHT/Temperature.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c CHT/KappaEffective.C -o Make/linux64GccDPInt32Opt/CHT/KappaEffective.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c CHT/HeatFlux.C -o Make/linux64GccDPInt32Opt/CHT/HeatFlux.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c CHT/HeatTransferCoefficient.C -o Make/linux64GccDPInt32Opt/CHT/HeatTransferCoefficient.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c CHT/SinkTemperature.C -o Make/linux64GccDPInt32Opt/CHT/SinkTemperature.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c CHT/CHT.C -o Make/linux64GccDPInt32Opt/CHT/CHT.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FSI/FSI.C -o Make/linux64GccDPInt32Opt/FSI/FSI.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FSI/ForceBase.C -o Make/linux64GccDPInt32Opt/FSI/ForceBase.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FSI/Force.C -o Make/linux64GccDPInt32Opt/FSI/Force.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FSI/Stress.C -o Make/linux64GccDPInt32Opt/FSI/Stress.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FSI/Displacement.C -o Make/linux64GccDPInt32Opt/FSI/Displacement.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FSI/DisplacementDelta.C -o Make/linux64GccDPInt32Opt/FSI/DisplacementDelta.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FF/FF.C -o Make/linux64GccDPInt32Opt/FF/FF.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FF/Velocity.C -o Make/linux64GccDPInt32Opt/FF/Velocity.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FF/VelocityGradient.C -o Make/linux64GccDPInt32Opt/FF/VelocityGradient.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FF/Pressure.C -o Make/linux64GccDPInt32Opt/FF/Pressure.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c FF/PressureGradient.C -o Make/linux64GccDPInt32Opt/FF/PressureGradient.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c Adapter.C -o Make/linux64GccDPInt32Opt/Adapter.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -c preciceAdapterFunctionObject.C -o Make/linux64GccDPInt32Opt/preciceAdapterFunctionObject.o
g++ -std=c++14 -m64 -pthread -DOPENFOAM=2112 -DWM_DP -DWM_LABEL_SIZE=32 -Wall -Wextra -Wold-style-cast -Wnon-virtual-dtor -Wno-unused-parameter -Wno-invalid-offsetof -Wno-attributes -Wno-unknown-pragmas  -O3  -DNoRepository -ftemplate-depth-100 -I/usr/lib/openfoam/openfoam2112/src/finiteVolume/lnInclude -I/usr/lib/openfoam/openfoam2112/src/meshTools/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/ -I/usr/lib/openfoam/openfoam2112/src/transportModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/twoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/interfaceProperties/lnInclude -I/usr/lib/openfoam/openfoam2112/src/transportModels/immiscibleIncompressibleTwoPhaseMixture/lnInclude -I/usr/lib/openfoam/openfoam2112/src/thermophysicalModels/basic/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/turbulenceModels/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/compressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/TurbulenceModels/incompressible/lnInclude -I/usr/lib/openfoam/openfoam2112/src/triSurface/lnInclude  -I../  -iquote. -IlnInclude -I/usr/lib/openfoam/openfoam2112/src/OpenFOAM/lnInclude -I/usr/lib/openfoam/openfoam2112/src/OSspecific/POSIX/lnInclude   -fPIC -shared -Xlinker --add-needed -Xlinker --no-as-needed  Make/linux64GccDPInt32Opt/Utilities.o Make/linux64GccDPInt32Opt/Interface.o Make/linux64GccDPInt32Opt/CouplingDataUser.o Make/linux64GccDPInt32Opt/CHT/Temperature.o Make/linux64GccDPInt32Opt/CHT/KappaEffective.o Make/linux64GccDPInt32Opt/CHT/HeatFlux.o Make/linux64GccDPInt32Opt/CHT/HeatTransferCoefficient.o Make/linux64GccDPInt32Opt/CHT/SinkTemperature.o Make/linux64GccDPInt32Opt/CHT/CHT.o Make/linux64GccDPInt32Opt/FSI/FSI.o Make/linux64GccDPInt32Opt/FSI/ForceBase.o Make/linux64GccDPInt32Opt/FSI/Force.o Make/linux64GccDPInt32Opt/FSI/Stress.o Make/linux64GccDPInt32Opt/FSI/Displacement.o Make/linux64GccDPInt32Opt/FSI/DisplacementDelta.o Make/linux64GccDPInt32Opt/FF/FF.o Make/linux64GccDPInt32Opt/FF/Velocity.o Make/linux64GccDPInt32Opt/FF/VelocityGradient.o Make/linux64GccDPInt32Opt/FF/Pressure.o Make/linux64GccDPInt32Opt/FF/PressureGradient.o Make/linux64GccDPInt32Opt/Adapter.o Make/linux64GccDPInt32Opt/preciceAdapterFunctionObject.o -L/usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib \
    -lfiniteVolume -lmeshTools -lcompressibleTurbulenceModels -lincompressibleTurbulenceModels -limmiscibleIncompressibleTwoPhaseMixture  -lprecice  -o /root/OpenFOAM/user-v2112/platforms/linux64GccDPInt32Opt/lib/libpreciceAdapterFunctionObject.so
```

```console
$ wmake -help-full

Usage: wmake [OPTION] [dir]
       wmake [OPTION] target [dir [MakeDir]]
       wmake -subcommand ...

options:
  -s | -silent      Silent mode (do not echo commands)
  -a | -all         wmake all sub-directories, runs Allwmake if present
  -q | -queue       Collect as single Makefile, runs Allwmake if present
  -k | -keep-going  Keep going even when errors occur (-non-stop)
  -j | -jN | -j N   Compile using all or specified N cores/hyperthreads
  -update           Update lnInclude, dep files, remove deprecated files/dirs
  -debug            Define c++DBUG='-DFULLDEBUG -g' as override
  -build-root=PATH  Specify FOAM_BUILDROOT for compilation intermediates
  -module-prefix=PATH   Specify FOAM_MODULE_PREFIX as absolute/relative path
  -module-prefix=TYPE   Specify FOAM_MODULE_PREFIX as predefined type
                        (u,user | g,group | o,openfoam)
  -no-scheduler     Disable scheduled parallel compilation

  -show-api         Print api value (from Make rules)
  -show-ext-so      Print shared library extension (with '.' separator)
  -show-c           Print C compiler value
  -show-cflags      Print C compiler flags
  -show-cxx         Print C++ compiler value
  -show-cxxflags    Print C++ compiler flags
  -show-cflags-arch     The C compiler arch flag (eg, -m64 etc)
  -show-cxxflags-arch   The C++ compiler arch flag (eg, -m64 etc)
  -show-compile-c   Same as '-show-c -show-cflags'
  -show-compile-cxx Same as '-show-cxx -show-cxxflags'
  -show-path-c      Print path to C compiler
  -show-path-cxx    Print path to C++ compiler
  -show-mpi-compile Print mpi-related flags used when compiling
  -show-mpi-link    Print mpi-related flags used when linking

  -pwd              Print root directory containing a Make/ directory
  -version | --version  Print version (same as -show-api)
  -help             Display short help and exit
  -help-full        Display full help and exit

subcommands (wmake subcommand -help for more information):
  -build-info       Query/manage status of {api,branch,build} information
  -check-dir        Check directory equality
  -with-bear        Call wmake via 'bear' to create json output

General, wrapped make system for multi-platform development.

Makefile targets:   platforms/linux64GccDPInt32Opt/.../fvMesh.o (for example)
Special targets:
  all | queue       Same as -all | -queue options
  exe               Create executable
  lib               Create statically linked archive lib (.a)
  libo              Create statically linked lib (.o)
  libso             Create dynamically linked lib (.so)
  dep               Create lnInclude and dependencies only
  updatedep         Create dependencies only (in case of broken dependencies)
  objects           Compile but not link

Environment
  FOAM_BUILDROOT
  FOAM_EXTRA_CFLAGS FOAM_EXTRA_CXXFLAGS FOAM_EXTRA_LDFLAGS
  FOAM_MODULE_PREFIX
```

[](https://sourceforge.net/projects/foam-extend) look tree structure
[](http://www.tfd.chalmers.se/~hani/kurser/OS_CFD_2020/lectureNotes/08_userDirectoryOrganizationAndCompilation.pdf)
[](https://openfoamwiki.net/index.php/Main_Page)
[](http://extensionuniversitaria.unileon.es/euniversitaria/curso.aspx?id=1903)
[](https://www.cfd-online.com/Forums/openfoam/60899-openfoam-extensions-project.html)
[](https://www.cfdsupport.com/OpenFOAM-Training-by-CFD-Support/node300.html)

```
--> FOAM Warning : 
    From void* Foam::dlLibraryTable::openLibrary(const Foam::fileName&, bool)
    in file db/dynamicLibrary/dlLibraryTable/dlLibraryTable.C at line 188
    Could not load "libpreciceAdapterFunctionObject.so"
libpreciceAdapterFunctionObject.so: cannot open shared object file: No such file or directory
--> FOAM Warning : 
Unknown function type preciceAdapterFunctionObject
Valid function types :
8(ObukhovLength patchProbes probes psiReactionThermoMoleFractions rhoReactionThermoMoleFractions setTimeStepFaRegion sets surfaces)
    From static Foam::autoPtr<Foam::functionObject> Foam::functionObject::New(const Foam::word&, const Foam::Time&, const Foam::dictionary&)
    in file db/functionObjects/functionObject/functionObject.C at line 129.
--> loading function object 'preCICE_Adapter'
```

```
SHELL=/bin/bash
WM_COMPILER=Gcc
SU2_RUN=/home/vagrant/SU2-6.0.0/SU2_CFD/bin
CGAL_ARCH_PATH=/usr/lib/openfoam/openfoam2112/ThirdParty/platforms/linux64Gcc/cgal-system
FOAM_MPI=sys-openmpi
WM_PRECISION_OPTION=DP
WM_PROJECT_USER_DIR=/home/vagrant/OpenFOAM/vagrant-v2112
WM_MPLIB=SYSTEMOPENMPI
FOAM_RUN=/home/vagrant/OpenFOAM/vagrant-v2112/run
ASTER_ROOT=/home/vagrant/code_aster
PWD=/home/vagrant/OpenFOAM/vagrant-v2112/platforms/linux64GccDPInt32Opt
LOGNAME=vagrant
XDG_SESSION_TYPE=tty
WM_OPTIONS=linux64GccDPInt32Opt
WM_ARCH=linux64
FFTW_ARCH_PATH=/usr/lib/openfoam/openfoam2112/ThirdParty/platforms/linux64Gcc/fftw-system
FOAM_TUTORIALS=/usr/lib/openfoam/openfoam2112/tutorials
WM_LABEL_SIZE=32
WM_PROJECT=OpenFOAM
MOTD_SHOWN=pam
FOAM_APPBIN=/usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/bin
WM_THIRD_PARTY_DIR=/usr/lib/openfoam/openfoam2112/ThirdParty
HOME=/home/vagrant
WISHEXECUTABLE=/usr/bin/wish
LANG=en_US.UTF-8
WM_LABEL_OPTION=Int32
LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:
FOAM_LIBBIN=/usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib
DUNE_CONTROL_PATH=/home/vagrant/dune
FOAM_ETC=/usr/lib/openfoam/openfoam2112/etc
SSH_CONNECTION=192.168.121.1 37760 192.168.121.82 22
FOAM_UTILITIES=/usr/lib/openfoam/openfoam2112/applications/utilities
FOAM_SITE_LIBBIN=/usr/lib/openfoam/openfoam2112/site/2112/platforms/linux64GccDPInt32Opt/lib
WM_PROJECT_VERSION=v2112
LESSCLOSE=/usr/bin/lesspipe %s %s
XDG_SESSION_CLASS=user
WM_DIR=/usr/lib/openfoam/openfoam2112/wmake
PYTHONPATH=/home/vagrant/code_aster/lib/python3.8/site-packages:/home/vagrant/SU2-6.0.0/SU2_CFD/bin:
TERM=xterm-256color
ADIOS2_ARCH_PATH=/usr/lib/openfoam/openfoam2112/ThirdParty/platforms/linux64Gcc/ADIOS2-2.6.0
FOAM_SITE_APPBIN=/usr/lib/openfoam/openfoam2112/site/2112/platforms/linux64GccDPInt32Opt/bin
LESSOPEN=| /usr/bin/lesspipe %s
USER=vagrant
MPI_ARCH_PATH=/usr/lib/x86_64-linux-gnu/openmpi
FOAM_SRC=/usr/lib/openfoam/openfoam2112/src
SCOTCH_ARCH_PATH=/usr/lib/openfoam/openfoam2112/ThirdParty/platforms/linux64GccDPInt32/scotch-system
DISPLAY=localhost:10.0
BOOST_ARCH_PATH=/usr/lib/openfoam/openfoam2112/ThirdParty/platforms/linux64Gcc/boost-system
SHLVL=1
FOAM_USER_LIBBIN=/home/vagrant/OpenFOAM/vagrant-v2112/platforms/linux64GccDPInt32Opt/lib
XDG_SESSION_ID=5
FOAM_APP=/usr/lib/openfoam/openfoam2112/applications
ASTER_TMPDIR=/tmp
FOAM_API=2112
LD_LIBRARY_PATH=/usr/lib:/home/vagrant/OpenFOAM/vagrant-v2112/platforms/linux64GccDPInt32Opt/lib:/usr/lib/openfoam/openfoam2112/site/2112/platforms/linux64GccDPInt32Opt/lib:/usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/sys-openmpi:/usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib:/usr/lib/x86_64-linux-gnu/openmpi/lib:/usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/lib/dummy
XDG_RUNTIME_DIR=/run/user/1000
WM_COMPILE_OPTION=Opt
SSH_CLIENT=192.168.121.1 37760 22
FOAM_SOLVERS=/usr/lib/openfoam/openfoam2112/applications/solvers
WM_PROJECT_DIR=/usr/lib/openfoam/openfoam2112
WM_COMPILER_TYPE=system
XDG_DATA_DIRS=/usr/local/share:/usr/share:/var/lib/snapd/desktop
FOAM_USER_APPBIN=/home/vagrant/OpenFOAM/vagrant-v2112/platforms/linux64GccDPInt32Opt/bin
PATH=/home/vagrant/.local/bin:/home/vagrant/.bin:/home/vagrant/code_aster/bin:/home/vagrant/code_aster/outils:/home/vagrant/SU2-6.0.0/SU2_CFD/bin:/home/vagrant/calculix-adapter/bin:/home/vagrant/dealii-adapter:/usr/lib/x86_64-linux-gnu/openmpi/bin:/home/vagrant/OpenFOAM/vagrant-v2112/platforms/linux64GccDPInt32Opt/bin:/usr/lib/openfoam/openfoam2112/site/2112/platforms/linux64GccDPInt32Opt/bin:/usr/lib/openfoam/openfoam2112/platforms/linux64GccDPInt32Opt/bin:/usr/lib/openfoam/openfoam2112/bin:/usr/lib/openfoam/openfoam2112/wmake:/home/vagrant/config-visualizer/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
WM_COMPILER_LIB_ARCH=64
SU2_HOME=/home/vagrant/SU2-6.0.0
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
SSH_TTY=/dev/pts/0
PYTHONEXECUTABLE=/usr/bin/python3
ASTER_ETC=/home/vagrant/code_aster/etc
_=/usr/bin/env
OLDPWD=/home/vagrant/OpenFOAM/vagrant-v2112/platforms/linux64GccDPInt32Opt/lib
```
