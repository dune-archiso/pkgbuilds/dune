# Maintainer: anon at sansorgan.es
pkgname=dune-vem
_tarver=v2.9.0.2
_tar=${_tarver}/${pkgname}-${_tarver}.tar.gz
pkgver=${_tarver/v/}
pkgrel=1
#epoch=
pkgdesc="Implementation of a number of virtual element spaces and bilinear forms"
arch=(x86_64)
url="https://dune-project.org/modules/${pkgname}"
license=('GPL2')
# groups=('dune-extensions')
depends=("dune-fem>=${pkgver}" python-sortedcontainers python-triangle)
makedepends=(doxygen graphviz python-scikit-build)
# python-sphinx texlive-core | superlu
checkdepends=() # python-pygmsh
# optdepends=(
#   'texlive-core: Type setting system'
#   'biber: A Unicode-capable BibTeX replacement for biblatex users'
#   'python-sphinx: Building Sphinx documentation'
#   'doxygen: Generate the class documentation from C++ sources')
#provides=()
#conflicts=()
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=(https://gitlab.dune-project.org/${pkgname/v/f}/${pkgname}/-/archive/${_tar})
#noextract=()
sha512sums=('b8194b88c1e03b45bc1349aaf72e8605b2644eb6cc6518349bfba12b93e126c6f6e76363d48a1f2744ee56e4b62c6f1388f1bc28609bf00b219e4392b7d30b32')
# validpgpkeys=('E5B47782DE09813BCC3518E159DA94F1FC8FD313') # Andreas Dedner <a.s.dedner@warwick.ac.uk>

prepare() {
  # cd ${pkgname}-${_tarver}
  export _pyversion=$(python -c "import sys; print(f'{sys.version_info.major}.{sys.version_info.minor}')")
  sed -i 's/^Version: '"${pkgver::3}"'/Version: '"${pkgver}"'/' ${pkgname}-${_tarver}/dune.module
  cat ${pkgname}-${_tarver}/dune.module
  python -m venv --system-site-packages _skbuild/linux-${CARCH}-${_pyversion}/cmake-build/dune-env
  # sed -i 's/^Version: '"${pkgver}"'/Version: '"${pkgver}"'/' ${pkgname}-${pkgver}/dune.module
}

build() {
  cd ${pkgname}-${_tarver}

  XDG_CACHE_HOME="${PWD}" \
    python setup.py build \
    --build-type=None \
    -G 'Unix Makefiles' \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-O2 -Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=FALSE \
    -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen=FALSE \
    -DDUNE_GRID_GRIDTYPE_SELECTOR=ON \
    -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE \
    -DCMAKE_DISABLE_FIND_PACKAGE_PETSc=TRUE \
    -DCMAKE_DISABLE_FIND_PACKAGE_PTScotch=FALSE \
    -DENABLE_HEADERCHECK=ON \
    -DDUNE_ENABLE_PYTHONBINDINGS=ON \
    -DDUNE_PYTHON_INSTALL_LOCATION='none' \
    -DDUNE_PYTHON_WHEELHOUSE="dist"
}
# -DENABLE_HEADERCHECK=ON \
check() {
  if [ -z "$(ldconfig -p | grep libcuda.so.1)" ]; then
    export OMPI_MCA_opal_warn_on_missing_libcuda=0
  fi

  cd ${pkgname}-${_tarver}
  cmake --build _skbuild/linux-${CARCH}-${_pyversion}/cmake-build --target build_tests
  ctest --verbose --output-on-failure --test-dir _skbuild/linux-${CARCH}-${_pyversion}/cmake-build
  # export DUNE_CONTROL_PATH="${srcdir}/tmp_install/usr"
  # CMAKE_PREFIX_PATH="${srcdir}/tmp_install/usr/lib/cmake/${pkgname}" LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${srcdir}/tmp_install/usr/lib/" ctest --verbose --output-on-failure --test-dir build-cmake
}

package() {
  cd ${pkgname}-${_tarver}
  PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python setup.py --skip-cmake install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
  install -Dm 644 LICENSE.md -t "${pkgdir}/usr/share/licenses/${pkgname}"
  # ls ${pkgdir}/usr/
  find "${pkgdir}" -type d -empty -delete
}
