## [`dune-geometry-git`](https://gitlab.com/dune-archiso/dune-core-git/-/raw/main/dune-geometry-git/PKGBUILD)

When the package `dune-geometry-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `bin`
- [x] `include`
- [x] `lib`
- [ ] `share`
  - [x] `bash-completion`
  - [ ] `doc`
  - [x] `dune`
  - [x] `dune-common`
  - [x] `licenses`
  - [ ] `man`

```console
-DPYTHON_INSTALL_LOCATION=system
-DADDITIONAL_PIP_PARAMS='-upgrade'
-DCMAKE_DISABLE_DOCUMENTATION=TRUE
```

```
-Ddune-common_DIR=/usr/lib/cmake/dune-common
```

### Remarks

Not found `doc/refinement/refinement.tex` for compile.

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doc_refinement_refinement_tex
doc_refinement_refinement_tex_clean
doxyfile
doxygen_dune-geometry
doxygen_install
env_install_python__tmp_makepkg_dune-geometry-git_src_dune-geometry_python_.
headercheck
install_python
metadata_env_install_python__tmp_makepkg_dune-geometry-git_src_dune-geometry_python_.
target_pygeometrytype
target_pyrefelement
target_pytestquad
test_python
update_images
_geometry
dunegeometry
headercheck__dune_geometry_affinegeometry.hh
headercheck__dune_geometry_axisalignedcubegeometry.hh
headercheck__dune_geometry_deprecated_topology.hh
headercheck__dune_geometry_dimension.hh
headercheck__dune_geometry_generalvertexorder.hh
headercheck__dune_geometry_multilineargeometry.hh
headercheck__dune_geometry_quadraturerules.hh
headercheck__dune_geometry_quadraturerules_compositequadraturerule.hh
headercheck__dune_geometry_referenceelement.hh
headercheck__dune_geometry_referenceelementimplementation.hh
headercheck__dune_geometry_referenceelements.hh
headercheck__dune_geometry_refinement.hh
headercheck__dune_geometry_test_checkgeometry.hh
headercheck__dune_geometry_topologyfactory.hh
headercheck__dune_geometry_type.hh
headercheck__dune_geometry_typeindex.hh
headercheck__dune_geometry_utility_typefromvertexcount.hh
headercheck__dune_geometry_virtualrefinement.hh
headercheck__dune_python_geometry_multilineargeometry.hh
headercheck__dune_python_geometry_quadraturerules.hh
headercheck__dune_python_geometry_referenceelements.hh
headercheck__dune_python_geometry_type.hh
test-affinegeometry
test-axisalignedcubegeometry
test-constexpr-geometrytype
test-cornerstoragerefwrap
test-fromvertexcount
test-geometrytype-id
test-multilineargeometry
test-nonetype
test-quadrature
test-referenceelements
test-refinement
headercheck/dune/geometry/affinegeometry.hh.o
headercheck/dune/geometry/affinegeometry.hh.i
headercheck/dune/geometry/affinegeometry.hh.s
headercheck/dune/geometry/axisalignedcubegeometry.hh.o
headercheck/dune/geometry/axisalignedcubegeometry.hh.i
headercheck/dune/geometry/axisalignedcubegeometry.hh.s
headercheck/dune/geometry/deprecated_topology.hh.o
headercheck/dune/geometry/deprecated_topology.hh.i
headercheck/dune/geometry/deprecated_topology.hh.s
headercheck/dune/geometry/dimension.hh.o
headercheck/dune/geometry/dimension.hh.i
headercheck/dune/geometry/dimension.hh.s
headercheck/dune/geometry/generalvertexorder.hh.o
headercheck/dune/geometry/generalvertexorder.hh.i
headercheck/dune/geometry/generalvertexorder.hh.s
headercheck/dune/geometry/multilineargeometry.hh.o
headercheck/dune/geometry/multilineargeometry.hh.i
headercheck/dune/geometry/multilineargeometry.hh.s
headercheck/dune/geometry/quadraturerules.hh.o
headercheck/dune/geometry/quadraturerules.hh.i
headercheck/dune/geometry/quadraturerules.hh.s
headercheck/dune/geometry/quadraturerules/compositequadraturerule.hh.o
headercheck/dune/geometry/quadraturerules/compositequadraturerule.hh.i
headercheck/dune/geometry/quadraturerules/compositequadraturerule.hh.s
headercheck/dune/geometry/referenceelement.hh.o
headercheck/dune/geometry/referenceelement.hh.i
headercheck/dune/geometry/referenceelement.hh.s
headercheck/dune/geometry/referenceelementimplementation.hh.o
headercheck/dune/geometry/referenceelementimplementation.hh.i
headercheck/dune/geometry/referenceelementimplementation.hh.s
headercheck/dune/geometry/referenceelements.hh.o
headercheck/dune/geometry/referenceelements.hh.i
headercheck/dune/geometry/referenceelements.hh.s
headercheck/dune/geometry/refinement.hh.o
headercheck/dune/geometry/refinement.hh.i
headercheck/dune/geometry/refinement.hh.s
headercheck/dune/geometry/test/checkgeometry.hh.o
headercheck/dune/geometry/test/checkgeometry.hh.i
headercheck/dune/geometry/test/checkgeometry.hh.s
headercheck/dune/geometry/topologyfactory.hh.o
headercheck/dune/geometry/topologyfactory.hh.i
headercheck/dune/geometry/topologyfactory.hh.s
headercheck/dune/geometry/type.hh.o
headercheck/dune/geometry/type.hh.i
headercheck/dune/geometry/type.hh.s
headercheck/dune/geometry/typeindex.hh.o
headercheck/dune/geometry/typeindex.hh.i
headercheck/dune/geometry/typeindex.hh.s
headercheck/dune/geometry/utility/typefromvertexcount.hh.o
headercheck/dune/geometry/utility/typefromvertexcount.hh.i
headercheck/dune/geometry/utility/typefromvertexcount.hh.s
headercheck/dune/geometry/virtualrefinement.hh.o
headercheck/dune/geometry/virtualrefinement.hh.i
headercheck/dune/geometry/virtualrefinement.hh.s
headercheck/dune/python/geometry/multilineargeometry.hh.o
headercheck/dune/python/geometry/multilineargeometry.hh.i
headercheck/dune/python/geometry/multilineargeometry.hh.s
headercheck/dune/python/geometry/quadraturerules.hh.o
headercheck/dune/python/geometry/quadraturerules.hh.i
headercheck/dune/python/geometry/quadraturerules.hh.s
headercheck/dune/python/geometry/referenceelements.hh.o
headercheck/dune/python/geometry/referenceelements.hh.i
headercheck/dune/python/geometry/referenceelements.hh.s
headercheck/dune/python/geometry/type.hh.o
headercheck/dune/python/geometry/type.hh.i
headercheck/dune/python/geometry/type.hh.s
```

### WIP 

In the future, we will have the module `python-quadpy` on Arch Linux. August 2021: there are already https://aur.archlinux.org/packages/python-quadpy working smootth.

```python
try:
    import quadpy as qp
    import numpy
```

```
sed -i 's/^                     SCRIPT/                     COMMAND ${PYTHON_EXECUTABLE}/' ${_name}/dune/python/test/CMakeLists.txt
```
