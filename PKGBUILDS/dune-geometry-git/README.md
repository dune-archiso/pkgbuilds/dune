## [`dune-geometry-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-geometry-git/PKGBUILD)

```console
-- The following OPTIONAL packages have been found:
 * LATEX
 * LatexMk
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * BLAS, fast linear algebra routines
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library, <https://gmplib.org>
 * QuadMath, GCC Quad-Precision Math Library, <https://gcc.gnu.org/onlinedocs/libquadmath>
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * Threads, Multi-threading library
 * TBB, Intel's Threading Building Blocks
 * PTScotch, Sequential and Parallel Graph Partitioning
 * METIS (required version >= 5.0), Serial Graph Partitioning
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * ParMETIS (required version >= 4.0), Parallel Graph Partitioning
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * Python3
-- The following REQUIRED packages have been found:
 * dune-common
-- The following OPTIONAL packages have not been found:
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
```

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-geometry
doxygen_install
dune_geometry_refinement_tex
dune_geometry_refinement_tex_clean
env_install_python__tmp_makepkg_dune-geometry-git_src_dune-geometry_python_.
headercheck
install_python
install_python__tmp_makepkg_dune-geometry-git_src_dune-geometry_python_.
metadata_env_install_python__tmp_makepkg_dune-geometry-git_src_dune-geometry_python_.
metadata_install_python__tmp_makepkg_dune-geometry-git_src_dune-geometry_python_.
pip_install_python__tmp_makepkg_dune-geometry-git_src_dune-geometry_python_.
referenceelement_images
refinement_images
target_pygeometrytype
target_pyrefelement
target_pytestquad
test_python
_geometry
dunegeometry
headercheck__dune_geometry_affinegeometry.hh
headercheck__dune_geometry_axisalignedcubegeometry.hh
headercheck__dune_geometry_deprecated_topology.hh
headercheck__dune_geometry_dimension.hh
headercheck__dune_geometry_generalvertexorder.hh
headercheck__dune_geometry_multilineargeometry.hh
headercheck__dune_geometry_quadraturerules.hh
headercheck__dune_geometry_quadraturerules_compositequadraturerule.hh
headercheck__dune_geometry_referenceelement.hh
headercheck__dune_geometry_referenceelementimplementation.hh
headercheck__dune_geometry_referenceelements.hh
headercheck__dune_geometry_refinement.hh
headercheck__dune_geometry_test_checkgeometry.hh
headercheck__dune_geometry_topologyfactory.hh
headercheck__dune_geometry_type.hh
headercheck__dune_geometry_typeindex.hh
headercheck__dune_geometry_utility_typefromvertexcount.hh
headercheck__dune_geometry_virtualrefinement.hh
headercheck__dune_python_geometry_multilineargeometry.hh
headercheck__dune_python_geometry_quadraturerules.hh
headercheck__dune_python_geometry_referenceelements.hh
headercheck__dune_python_geometry_type.hh
test-affinegeometry
test-axisalignedcubegeometry
test-constexpr-geometrytype
test-cornerstoragerefwrap
test-fromvertexcount
test-geometrytype-id
test-multilineargeometry
test-nonetype
test-quadrature
test-referenceelements
test-refinement
dune/geometry/quadraturerules/quadraturerules.o
dune/geometry/quadraturerules/quadraturerules.i
dune/geometry/quadraturerules/quadraturerules.s
dune/geometry/referenceelementimplementation.o
dune/geometry/referenceelementimplementation.i
dune/geometry/referenceelementimplementation.s
headercheck/dune/geometry/affinegeometry.hh.o
headercheck/dune/geometry/affinegeometry.hh.i
headercheck/dune/geometry/affinegeometry.hh.s
headercheck/dune/geometry/axisalignedcubegeometry.hh.o
headercheck/dune/geometry/axisalignedcubegeometry.hh.i
headercheck/dune/geometry/axisalignedcubegeometry.hh.s
headercheck/dune/geometry/deprecated_topology.hh.o
headercheck/dune/geometry/deprecated_topology.hh.i
headercheck/dune/geometry/deprecated_topology.hh.s
headercheck/dune/geometry/dimension.hh.o
headercheck/dune/geometry/dimension.hh.i
headercheck/dune/geometry/dimension.hh.s
headercheck/dune/geometry/generalvertexorder.hh.o
headercheck/dune/geometry/generalvertexorder.hh.i
headercheck/dune/geometry/generalvertexorder.hh.s
headercheck/dune/geometry/multilineargeometry.hh.o
headercheck/dune/geometry/multilineargeometry.hh.i
headercheck/dune/geometry/multilineargeometry.hh.s
headercheck/dune/geometry/quadraturerules.hh.o
headercheck/dune/geometry/quadraturerules.hh.i
headercheck/dune/geometry/quadraturerules.hh.s
headercheck/dune/geometry/quadraturerules/compositequadraturerule.hh.o
headercheck/dune/geometry/quadraturerules/compositequadraturerule.hh.i
headercheck/dune/geometry/quadraturerules/compositequadraturerule.hh.s
headercheck/dune/geometry/referenceelement.hh.o
headercheck/dune/geometry/referenceelement.hh.i
headercheck/dune/geometry/referenceelement.hh.s
headercheck/dune/geometry/referenceelementimplementation.hh.o
headercheck/dune/geometry/referenceelementimplementation.hh.i
headercheck/dune/geometry/referenceelementimplementation.hh.s
headercheck/dune/geometry/referenceelements.hh.o
headercheck/dune/geometry/referenceelements.hh.i
headercheck/dune/geometry/referenceelements.hh.s
headercheck/dune/geometry/refinement.hh.o
headercheck/dune/geometry/refinement.hh.i
headercheck/dune/geometry/refinement.hh.s
headercheck/dune/geometry/test/checkgeometry.hh.o
headercheck/dune/geometry/test/checkgeometry.hh.i
headercheck/dune/geometry/test/checkgeometry.hh.s
headercheck/dune/geometry/topologyfactory.hh.o
headercheck/dune/geometry/topologyfactory.hh.i
headercheck/dune/geometry/topologyfactory.hh.s
headercheck/dune/geometry/type.hh.o
headercheck/dune/geometry/type.hh.i
headercheck/dune/geometry/type.hh.s
headercheck/dune/geometry/typeindex.hh.o
headercheck/dune/geometry/typeindex.hh.i
headercheck/dune/geometry/typeindex.hh.s
headercheck/dune/geometry/utility/typefromvertexcount.hh.o
headercheck/dune/geometry/utility/typefromvertexcount.hh.i
headercheck/dune/geometry/utility/typefromvertexcount.hh.s
headercheck/dune/geometry/virtualrefinement.hh.o
headercheck/dune/geometry/virtualrefinement.hh.i
headercheck/dune/geometry/virtualrefinement.hh.s
headercheck/dune/python/geometry/multilineargeometry.hh.o
headercheck/dune/python/geometry/multilineargeometry.hh.i
headercheck/dune/python/geometry/multilineargeometry.hh.s
headercheck/dune/python/geometry/quadraturerules.hh.o
headercheck/dune/python/geometry/quadraturerules.hh.i
headercheck/dune/python/geometry/quadraturerules.hh.s
headercheck/dune/python/geometry/referenceelements.hh.o
headercheck/dune/python/geometry/referenceelements.hh.i
headercheck/dune/python/geometry/referenceelements.hh.s
headercheck/dune/python/geometry/type.hh.o
headercheck/dune/python/geometry/type.hh.i
headercheck/dune/python/geometry/type.hh.s
```
