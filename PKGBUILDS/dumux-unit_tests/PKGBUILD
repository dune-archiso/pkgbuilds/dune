# Maintainer: anon at sansorgan.es
pkgname=dumux
_tarver=3.7.0
_dunever=2.9.0
_tar="${_tarver}/${pkgname}-${_tarver}.tar.gz"
pkgver=${_tarver//-/_}
pkgrel=1
#epoch=
pkgdesc="An open-source simulator and research code in modern C++"
arch=('x86_64')
url="https://${pkgname}.org"
license=('GPL3')
# groups=()
depends=("dune-grid>=${_dunever}" "dune-istl>=${_dunever}" "dune-localfunctions>=${_dunever}")
# bash-completion
makedepends=('cjson' 'valgrind' 'python-setuptools')
# makedepends=('openmpi' 'texlive-core' 'biber' 'imagemagick' 'python-sphinx')
checkdepends=('gnuplot' 'dune-alugrid' 'dune-foamgrid' 'python-dune-grid')
# checkdepends=('superlu' 'dune-functions>=2.8.0' 'dune-spgrid' 'dune-foamgrid>=2.8.0' 'dune-alugrid>=2.8.0' 'opm-common>=2021.04' 'fmt' 'suitesparse') #'dune-subgrid' 'opm-grid>=2021.04' 'dune-mmesh' 'glpk' 'gnuplot' 'nlopt' 'gmsh' 'glfw-x11' 'paraview-opt' 'sionlib' dlmalloc gstat
# 'texlive-science' 'texlive-pictures' 'texlive-latexextra' 'texlive-bibtexextra'
optdepends=('valgrind: Tool to help find memory-management problems in programs')
# optdepends=('openmpi: for mpi support'
#   'texlive-core: Type setting system'
#   'biber: A Unicode-capable BibTeX replacement for biblatex users'
#   'imagemagick: image viewing/manipulation program'
#   'python-sphinx: Building Sphinx documentation'
#   'doxygen: Generate the class documentation from C++ sources'
#   'graphviz: Graph visualization software'
#   'inkscape: converts SVG images')
# provides=()
#conflicts=()
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=(https://git.iws.uni-stuttgart.de/${pkgname}-repositories/${pkgname}/-/archive/${_tar})
# https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/raw/8e81d6d6f1c66eb3209283efeafa47f6033f0455/cmake/modules/FindValgrind.cmake
#noextract=()
sha512sums=('b482041eb96ca76b47f3523d2e8f57cc55c63b7049bcba6d520adb1874d3f4422fa0877f899298aed02906d183793f6566ef9c161f957a7c370a9e438852aec1')

prepare() {
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L76
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
  export DUNE_PY_DIR="${BUILDDIR}/${pkgname}" # pkgbase if we will add dumux python bindings
  export PATH="${srcdir}/${pkgname}/bin:${PATH}" # /testing
  sed -i '9 a FindGmsh.cmake' ${pkgname}-${_tarver}/cmake/modules/CMakeLists.txt
  # mv FindValgrind.cmake ${pkgname}-${_tarver}/cmake/modules
}

build() {
  cmake \
    -S ${pkgname}-${_tarver} \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DDUNE_ENABLE_PYTHONBINDINGS=ON \
    -DDUNE_PYTHON_INSTALL_LOCATION='none' \
    -DCMAKE_DISABLE_FIND_PACKAGE_PTScotch=TRUE \
    -Wno-dev
  cmake --build build-cmake --target all
  cd build-cmake/python
  python setup.py build
  # cd "${pkgname}-${_tarver}/build-cmake/doc/handbook"
  # make doc_handbook_0_dumux-handbook_tex
}
# TODO: Look the required tests for unit tests https://git.iws.uni-stuttgart.de/search?search=stokes_donea_nocaching&group_id=9&project_id=31&scope=&search_code=true&snippets=false&repository_ref=&nav_source=navbar
check() {
  if [ -z "$(ldconfig -p | grep libcuda.so.1)" ]; then
    export OMPI_MCA_opal_warn_on_missing_libcuda=0
  fi
  DESTDIR="${PWD}/tmp_install" cmake --build build-cmake --target install
  cd build-cmake/python
  python setup.py install --root="${PWD}/tmp_install" --optimize=1 --skip-build
  cd "${srcdir}"
  cmake --build build-cmake --target build_unit_tests
  DUNE_CONTROL_PATH="${srcdir}/tmp_install/usr" PYTHONPATH="${srcdir}/build-cmake/python/tmp_install$(python -c "import site; print(site.getsitepackages()[0])"):${PYTHONPATH}" ctest -R "^test_python*" --verbose --output-on-failure --test-dir build-cmake
}

package() {
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  install -Dm644 ${pkgname}-${_tarver}/LICENSE.md "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}
