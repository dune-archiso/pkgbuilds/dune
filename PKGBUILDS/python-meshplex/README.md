```bash
depends=(python-meshio python-npx)
makedepends=(python-setuptools)
checkdepends=(python-pytest-codeblocks python-meshzoo python-matplotlib python-scipy) # unixodbc freetype2 glew libjpeg-turbo libtiff openmpi hdf5 netcdf jsoncpp gdal libtheora ffmpeg pdal qt5-x11extras
# optdepends=('vtk' 'python-transforms3d' 'unixodbc' 'freetype2' 'glew' 'libjpeg-turbo' 'libtiff' 'openmpi' 'hdf5' 'netcdf' 'jsoncpp' 'gdal' 'libtheora' 'ffmpeg' 'pdal' 'qt5-x11extras')

build() {
  cd "${_base}-${pkgver}"
  python -c "from setuptools import setup; setup();" build
}

check() {
  cd "${_base}-${pkgver}"
  python -c "from setuptools import setup; setup();" install --root="${PWD}/tmp_install" --optimize=1 --skip-build
  MPLBACKEND=Agg PYTHONPATH="${PWD}/tmp_install$(python -c "import site; print(site.getsitepackages()[0])"):${PYTHONPATH}" python -m pytest --codeblocks tests
}
```
