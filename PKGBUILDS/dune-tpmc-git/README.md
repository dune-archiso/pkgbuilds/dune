## [`dune-tpmc-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-tpmc-git/PKGBUILD)

```console
-- The following OPTIONAL packages have been found:
 * dune-grid
 * dune-alugrid
 * dune-uggrid
 * BLAS, fast linear algebra routines
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library, <https://gmplib.org>
 * QuadMath, GCC Quad-Precision Math Library, <https://gcc.gnu.org/onlinedocs/libquadmath>
 * Threads, Multi-threading library
 * TBB, Intel's Threading Building Blocks
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * Python3
 * Alberta (required version >= 3.0), An adaptive hierarchical finite element toolbox and grid manager
 * Psurface, Piecewise linear bijections between triangulated surfaces
 * ZLIB
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
-- The following REQUIRED packages have been found:
 * dune-common
 * dune-geometry
-- The following OPTIONAL packages have not been found:
 * LATEX
 * LatexMk
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * ParMETIS, Parallel Graph Partitioning
 * AmiraMesh
 * SIONlib
 * DLMalloc
 * PTScotch, Sequential and Parallel Graph Partitioning
 * ZOLTAN
 * METIS, Serial Graph Partitioning
 * wxWidgets
 * SFML
-- The following REQUIRED packages have not been found:
 * Tpmc
```
