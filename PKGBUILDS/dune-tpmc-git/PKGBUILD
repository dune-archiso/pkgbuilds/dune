# Maintainer: anon at sansorgan.es
_name=dune-tpmc
pkgname="${_name}-git"
_gitcommit=88614232530482d3f026aba5ec42a9a755321070
pkgver=0.1.r369.8861423
pkgrel=1
#epoch=
pkgdesc="Provides a topology preserving implementation of the marching cubes and marching simplex algorithm"
arch=(x86_64)
url="https://www.dune-project.org/modules/${_name}"
license=(GPL2)
# groups=('dune-extensions')
depends=(dune-grid-git python-tpmc-git)
makedepends=(doxygen graphviz superlu sfml) # 'wxgtk2' 'dune-alugrid-git'
#checkdepends=()
optdepends=(
  'texlive-core: Type setting system'
  'biber: A Unicode-capable BibTeX replacement for biblatex users'
  'imagemagick: image viewing/manipulation program'
  'python-sphinx: Building Sphinx documentation'
  'doxygen: Generate the class documentation from C++ sources'
  'inkscape: converts SVG images'
  'vc: C++ Vectorization library'
  'dune-alugrid: Adaptive, loadbalancing, unstructured implementation of the DUNE grid interface supporting either simplices or cubes')
provides=("${_name}=${pkgver%%.r*}")
conflicts=(${_name})
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=("git+https://gitlab.dune-project.org/extensions/${_name}#commit=${_gitcommit}") #${pkgname}.opts
#noextract=()
sha512sums=('SKIP')

pkgver() {
  cd "${_name}"
  printf "0.1.r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  sed -i 's/^set(modules "DuneMcMacros/set(modules "DuneTpmcMacros/' ${_name}/cmake/modules/CMakeLists.txt
  # Add something like https://gitlab.dune-project.org/extensions/dune-tpmc/-/blob/master/gui/CMakeLists.txt
  # sed -i 's/^  add_dune_alugrid_flags/#  add_dune_alugrid_flags/' ${_name}/gui/CMakeLists.txt
}

build() {
  # dunecontrol --opts=${pkgname}.opts cmake -Wno-dev : make
  cmake \
    -S "${_name}" \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE \
    -Wno-dev
  cmake --build build-cmake --target all # help
}

check() {
  # dunecontrol --opts=${pkgname}.opts make build_tests #test_python
  # export DUNE_CONTROL_PATH="${srcdir}/${_name}"
  # cd "${srcdir}/${_name}/build-cmake"
  # dune-ctest --verbose --output-on-failure
  cmake --build build-cmake --target build_tests
  ctest --verbose --output-on-failure --test-dir build-cmake
}

package() {
  # dunecontrol --only=${_name} make install DESTDIR="${pkgdir}"
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  # install -Dm644 ${_name}/COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}
