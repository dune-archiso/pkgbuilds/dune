_base=fpdf2
pkgname=python-${_base}
pkgdesc="Simple PDF generation for Python"
pkgver=2.5.4
pkgrel=1
arch=(x86_64)
url="https://github.com/PyFPDF/${_base}"
license=(LGPL3)
depends=(python-defusedxml python-pillow)
makedepends=(python-setuptools)
checkdepends=(python-tox qpdf)
source=(${url}/archive/${pkgver}.tar.gz)
sha512sums=('500b5fd20d923d5c02d511b79166a6cd222eec316554a9b56f96dbf279ed465c47e17602c2fc1834b8d031299dc232da0095c2e33f3628e84c564e09b99df33f')

build() {
  cd ${_base}-${pkgver}
  python setup.py build
}

check() {
  cd ${_base}-${pkgver}
  local _pyversion=$(python -c "import sys; print(f'{sys.version_info.major}.{sys.version_info.minor}'.replace('.', ''))")
  tox -e py${_pyversion}
}

package() {
  cd ${_base}-${pkgver}
  PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
