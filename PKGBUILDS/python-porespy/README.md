- [](https://pypi.org/project/porespy)

python-dask ok
python-edt wip
python-imagio (aur)
python-jupyterlab-widgets ok
python-loguru (aur)
python-matplotlib ok
python-numba (aur) no working
python-numpy ok
python-numpy-stl (aur)
python-openpnm wip
python-pandas ok
python-psutil ok
python-pyevtk (aur)
python-pyfastnoisesimd wip
python-scikit-fmm wip
pythonscikit-image (aur)
python-scipy ok
python-tqdm ok
python-trimesh ok
