# Maintainer: anon at sansorgan.es
pkgname=dumux-lecture
pkgver=3.8.0
_tar="${pkgver}/${pkgname}-${pkgver}.tar.gz"
pkgrel=1
#epoch=
pkgdesc="Examples for modeling flow and transport processes in porous media"
arch=(x86_64)
url="https://git.iws.uni-stuttgart.de/dumux-repositories/${pkgname}"
license=(GPL2)
# groups=('dumux-module')
depends=("dumux>=${_tarver}")
_dunever=2.9.0
makedepends=('eigen' 'petsc') # dune-codegen sionlib dlmalloc
# makedepends=('texlive-latexextra' 'texlive-pictures' 'texlive-science' 'texlive-bibtexextra' 'texlive-fontsextra' 'imagemagick' 'python-sphinx' 'doxygen' 'gnu-free-fonts' 'inkscape' 'superlu' 'arpackpp' 'suitesparse' 'zoltan' 'eigen' 'petsc') # dune-codegen sionlib dlmalloc
checkdepends=("dune-alugrid>=${_dunever}" "dune-foamgrid>=${_dunever}")
optdepends=('texlive-latexextra: Type setting system'
  'texlive-pictures: Packages for drawings graphics'
  'texlive-science: Typesetting for mathematics, natural and computer sciences'
  'texlive-bibtexextra: Additional BibTeX styles and bibliography databases'
  'texlive-fontsextra: all sorts of extra fonts'
  'imagemagick: image viewing/manipulation program'
  'python-sphinx: Building Sphinx documentation'
  'doxygen: Generate the class documentation from C++ sources'
  'eigen: Lightweight C++ template library for vector and matrix math'
  'petsc: Portable, extensible toolkit for scientific computation')
#provides=()
#conflicts=()
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=(${url}/-/archive/${_tar})
#noextract=()
sha512sums=('3bbe620ecf784014919b0b30a85ec166e4b7d6f501556b9a33f9931b912a107c7ce24320158b09543e09cc0c7a48d778e0e66fc2efa9696da33b7a78de560722')

prepare() {
  sed -i 's/^add_subdirectory(lecture EXCLUDE_FROM_ALL)/add_subdirectory(lecture)/' ${pkgname}-${pkgver}/CMakeLists.txt
  sed -i 's/^Version: '"${pkgver%.*}"'/Version: '"${pkgver}"'/' ${pkgname}-${pkgver}/dune.module
}

build() {
  cmake \
    -S ${pkgname}-${pkgver} \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_SKIP_RPATH=ON \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -Wno-dev
  cmake --build build-cmake --target all # help
}

check() {
  cmake --build build-cmake --target build_tests
  # LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${srcdir}/build-cmake/lib/"
  # ctest --verbose --output-on-failure --test-dir build-cmake
}

package() {
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  install -m644 -D ${pkgname}-${pkgver}/COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  rm -r "${pkgdir}"/usr/include/dumux/porousmediumflow/tracer
  # install -dm755 "${pkgdir}/usr/share/doc/${pkgname}/doxygen"
  find "${pkgdir}" -type d -empty -delete
}
