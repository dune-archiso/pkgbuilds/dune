## [`ampi-openmpi`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/ampi-openmpi/PKGBUILD) [![Packaging status](https://badgen.net/badge/AUR/Package/blue)](https://aur.archlinux.org/packages/ampi-openmpi)

[![ampi-openmpi](https://img.shields.io/aur/version/ampi-openmpi?color=1793d1&label=ampi-openmpi&logo=arch-linux&style=for-the-badge)](https://aur.archlinux.org/packages/ampi-openmpi)

[![Packaging status](https://repology.org/badge/vertical-allrepos/ampi-openmpi.svg)](https://repology.org/project/ampi-openmpi/versions)

```console
$ curl -s https://github.com/rbuch.gpg | gpg --import
```

```
commit 4c8494afb17174970baf94e9657a1812b176874e (HEAD)
gpg: Signature made Wed 21 Jul 2021 04:45:13 PM -05
gpg:                using RSA key 4AEE18F83AFDEB23
gpg: Can't check signature: No public key
Author: Ronak Buch <rabuch2@illinois.edu>
Date:   Wed Jul 21 17:45:13 2021 -0400

    fixup! CkArray: Change defaults for static insertion (#3418) (#3428)
```
