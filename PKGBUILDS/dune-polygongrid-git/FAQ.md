[](https://gitlab.dune-project.org/extensions/dune-polygongrid)

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxygen_install
headercheck
install_python
install_python__tmp_makepkg_dune-polygongrid-git_src_dune-polygongrid_python
test_python
dunepolygongrid
test-mesh
test-polygongrid
```

```console
   Site: runner-72989761-project-28300863-concurrent-0
   Build name: Linux-g++
Create new tag: 20210721-2233 - Experimental
Test project /tmp/makepkg/dune-polygongrid-git/src/dune-polygongrid/build-cmake
    Start 1: test-mesh
1/2 Test #1: test-mesh ........................   Passed    0.13 sec
    Start 2: test-polygongrid
2/2 Test #2: test-polygongrid .................Subprocess aborted***Exception:   0.13 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
Primal Structure:
node 0:   6 [1]  16 [1]  7 [2]  0 [1]
node 1:   7 [1]  17 [1]  8 [2]  1 [1]  0 [2]
node 2:   8 [1]  18 [1]  9 [2]  1 [2]
node 3:   10 [1]  20 [1]  6 [2]  0 [0]  2 [1]
node 4:   0 [3]  1 [0]  2 [2]
node 5:   1 [4]  3 [1]  2 [3]
node 6:   9 [1]  19 [1]  11 [2]  3 [2]  1 [3]
node 7:   12 [1]  22 [1]  10 [2]  2 [0]  4 [1]
node 8:   2 [4]  3 [0]  5 [1]
node 9:   11 [1]  21 [1]  14 [2]  5 [2]  3 [3]
node 10:   13 [1]  23 [1]  12 [2]  4 [0]
node 11:   15 [1]  25 [1]  13 [2]  4 [2]  2 [5]  5 [0]
node 12:   14 [1]  24 [1]  15 [2]  5 [3]
node 13:   6 [0]
node 14:   20 [0]
node 15:   7 [0]
node 16:   16 [0]
node 17:   8 [0]
node 18:   17 [0]
node 19:   9 [0]
node 20:   18 [0]
node 21:   10 [0]
node 22:   22 [0]
node 23:   11 [0]
node 24:   19 [0]
node 25:   12 [0]
node 26:   23 [0]
node 27:   13 [0]
node 28:   25 [0]
node 29:   14 [0]
node 30:   21 [0]
node 31:   15 [0]
node 32:   24 [0]
Dual Structure:
node 0:   0 [0]  1 [0]  4 [1]  3 [4]
node 1:   1 [4]  2 [0]  6 [0]  5 [1]  4 [2]
node 2:   3 [0]  4 [0]  5 [0]  8 [1]  11 [5]  7 [4]
node 3:   5 [2]  6 [4]  9 [0]  8 [2]
node 4:   7 [0]  11 [4]  10 [0]
node 5:   8 [0]  9 [4]  12 [0]  11 [0]
node 6:   0 [1]  3 [3]  14 [0]
node 7:   1 [1]  0 [3]  16 [0]
node 8:   2 [1]  1 [3]  18 [0]
node 9:   6 [1]  2 [3]  20 [0]
node 10:   3 [1]  7 [3]  22 [0]
node 11:   9 [1]  6 [3]  24 [0]
node 12:   7 [1]  10 [3]  26 [0]
node 13:   10 [1]  11 [3]  28 [0]
node 14:   12 [1]  9 [3]  30 [0]
node 15:   11 [1]  12 [3]  32 [0]
node 16:   0 [2]  13 [0]
node 17:   1 [2]  15 [0]
node 18:   2 [2]  17 [0]
node 19:   6 [2]  19 [0]
node 20:   3 [2]  21 [0]
node 21:   9 [2]  23 [0]
node 22:   7 [2]  25 [0]
node 23:   10 [2]  27 [0]
node 24:   12 [2]  29 [0]
node 25:   11 [2]  31 [0]
>>> Checking primal grid...
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
Error: Entity and geometry report different geometry types on codimension 1.
0
0
Lifetime / consistency check for entities, codim 2
WARNING! Requested check of first 32 entities, but grid view only contains 13 entities
Lifetime / consistency check for entities, codim 1
WARNING! Requested check of first 32 entities, but grid view only contains 18 entities
Lifetime / consistency check for entities, codim 0
WARNING! Requested check of first 32 entities, but grid view only contains 6 entities
Intersection Lifetime / consistency check
WARNING! Requested check of intersections for first 32 elements, but grid view only contains 6 elements
Lifetime / consistency check for entities, codim 2
WARNING! Requested check of first 32 entities, but grid view only contains 13 entities
Lifetime / consistency check for entities, codim 1
WARNING! Requested check of first 32 entities, but grid view only contains 18 entities
Lifetime / consistency check for entities, codim 0
WARNING! Requested check of first 32 entities, but grid view only contains 6 entities
Intersection Lifetime / consistency check
WARNING! Requested check of intersections for first 32 elements, but grid view only contains 6 elements
Checking iterators for higher codimension...
Checking iterators for interior partition...
Checking iterators for interior-border partition...
Checking iterators for overlap partition...
Checking iterators for overlap-front partition...
Checking iterators for ghost partition...
Checking communication for interior-border / interior-border interface...
WARNING: GridView::communicate of 'N4Dune13__PolygonGrid8GridViewIdEE' still returns void. Please update implementation to new interface returning a future object!
Checking communication for interior-border / all interface...
WARNING: GridView::communicate of 'N4Dune13__PolygonGrid8GridViewIdEE' still returns void. Please update implementation to new interface returning a future object!
Checking communication for overlap / overlap-front interface...
WARNING: GridView::communicate of 'N4Dune13__PolygonGrid8GridViewIdEE' still returns void. Please update implementation to new interface returning a future object!
Checking communication for overlap / all interface...
WARNING: GridView::communicate of 'N4Dune13__PolygonGrid8GridViewIdEE' still returns void. Please update implementation to new interface returning a future object!
Checking communication for all / all interface...
WARNING: GridView::communicate of 'N4Dune13__PolygonGrid8GridViewIdEE' still returns void. Please update implementation to new interface returning a future object!
<<< Checking intersection of primal grid...
Error: Wrong integration outer normal (-0.4 -0, should be 0 0).
       Intersection: 0 0.4, 0 0.
test-polygongrid: /usr/include/dune/grid/test/checkintersectionit.hh:353: void checkIntersection(const Intersection&, bool) [with Intersection = Dune::Intersection<const Dune::PolygonGrid<double>, Dune::__PolygonGrid::Intersection<const Dune::PolygonGrid<double> > >]: Assertion `false' failed.
[runner-72989761-project-28300863-concurrent-0:04305] *** Process received signal ***
[runner-72989761-project-28300863-concurrent-0:04305] Signal: Aborted (6)
[runner-72989761-project-28300863-concurrent-0:04305] Signal code:  (-6)
[runner-72989761-project-28300863-concurrent-0:04305] [ 0] /usr/lib64/libc.so.6(+0x3cda0)[0x7f1e6b2b1da0]
[runner-72989761-project-28300863-concurrent-0:04305] [ 1] /usr/lib64/libc.so.6(gsignal+0x142)[0x7f1e6b2b1d22]
[runner-72989761-project-28300863-concurrent-0:04305] [ 2] /usr/lib64/libc.so.6(abort+0x116)[0x7f1e6b29b862]
[runner-72989761-project-28300863-concurrent-0:04305] [ 3] /usr/lib64/libc.so.6(+0x26747)[0x7f1e6b29b747]
[runner-72989761-project-28300863-concurrent-0:04305] [ 4] /usr/lib64/libc.so.6(+0x35616)[0x7f1e6b2aa616]
[runner-72989761-project-28300863-concurrent-0:04305] [ 5] /tmp/makepkg/dune-polygongrid-git/src/dune-polygongrid/build-cmake/test/test-polygongrid(_Z17checkIntersectionIN4Dune12IntersectionIKNS0_11PolygonGridIdEENS0_13__PolygonGrid12IntersectionIS4_EEEEEvRKT_b+0x1b86)[0x558b7c947565]
[runner-72989761-project-28300863-concurrent-0:04305] [ 6] /tmp/makepkg/dune-polygongrid-git/src/dune-polygongrid/build-cmake/test/test-polygongrid(_Z25checkIntersectionIteratorIN4Dune8GridViewINS0_13__PolygonGrid14GridViewTraitsIdEEEE35CheckIntersectionIteratorErrorStateEvRKT_RKNS7_5CodimILi0EE8IteratorERT0_+0x33a)[0x558b7c938e7f]
[runner-72989761-project-28300863-concurrent-0:04305] [ 7] /tmp/makepkg/dune-polygongrid-git/src/dune-polygongrid/build-cmake/test/test-polygongrid(_Z29checkViewIntersectionIteratorIN4Dune8GridViewINS0_13__PolygonGrid14GridViewTraitsIdEEEEEvRKT_+0x68)[0x558b7c92ed2a]
[runner-72989761-project-28300863-concurrent-0:04305] [ 8] /tmp/makepkg/dune-polygongrid-git/src/dune-polygongrid/build-cmake/test/test-polygongrid(_Z25checkIntersectionIteratorIN4Dune11PolygonGridIdEEEvRKT_b+0x6c)[0x558b7c91fe42]
[runner-72989761-project-28300863-concurrent-0:04305] [ 9] /tmp/makepkg/dune-polygongrid-git/src/dune-polygongrid/build-cmake/test/test-polygongrid(_Z12performCheckRN4Dune11PolygonGridIdEE+0x12b)[0x558b7c914810]
[runner-72989761-project-28300863-concurrent-0:04305] [10] /tmp/makepkg/dune-polygongrid-git/src/dune-polygongrid/build-cmake/test/test-polygongrid(main+0x1d4)[0x558b7c914aed]
[runner-72989761-project-28300863-concurrent-0:04305] [11] /usr/lib64/libc.so.6(__libc_start_main+0xd5)[0x7f1e6b29cb25]
[runner-72989761-project-28300863-concurrent-0:04305] [12] /tmp/makepkg/dune-polygongrid-git/src/dune-polygongrid/build-cmake/test/test-polygongrid(_start+0x2e)[0x558b7c91389e]
[runner-72989761-project-28300863-concurrent-0:04305] *** End of error message ***
50% tests passed, 1 tests failed out of 2
Total Test time (real) =   0.26 sec
The following tests FAILED:
	  2 - test-polygongrid (Subprocess aborted)
Errors while running CTest
======================================================================
Name:      test-polygongrid
FullName:  ./test/test-polygongrid
Status:    FAILED
JUnit report for CTest results written to /builds/dune-archiso/repository/dune-extensions-git/junit/dune-extensions-git-cmake.xml
==> ERROR: A failure occurred in check().
```
