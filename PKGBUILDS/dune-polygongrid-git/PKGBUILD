# Maintainer: anon at sansorgan.es
_name=dune-polygongrid
pkgbase="${_name}-git"
pkgname=(${pkgbase} python-${pkgbase}) #${pkgbase}-docs
_gitcommit=b2a43e0b43fff90f0065d7497502c93b9c3306be
pkgver=2.9.r190.b2a43e0
pkgrel=1
#epoch=
pkgdesc="PolygonGrid implements a DUNE grid consisting of polygons"
arch=('x86_64')
url="https://www.dune-project.org/modules/${_name}"
# license=(GPL2)
groups=(dune-extensions-git)
makedepends=(python-dune-grid-git doxygen graphviz git) #python-setuptools
# 'biber'
# checkdepends=()
optdepends=('doxygen: Generate the class documentation from C++ sources'
  'graphviz: Graph visualization software'
  'parmetis: Parallel Graph Partitioning and Fill-reducing Matrix Ordering'
  'superlu: Set of subroutines to solve a sparse linear system'
  'arpackpp: C++ interface to ARPACK'
  'suitesparse: A collection of sparse matrix libraries')
# 'biber: A Unicode-capable BibTeX replacement for biblatex users'
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=("git+https://gitlab.dune-project.org/extensions/${_name}#commit=${_gitcommit}") #${pkgbase}.opts
#noextract=()
sha512sums=('SKIP')

pkgver() {
  cd "${_name}"
  printf "2.9.r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  sed -i 's/^Version: '"${pkgver%%.r*}"'-git/Version: '"${pkgver%%.r*}"'/' ${_name}/dune.module
}

build() {
  # dunecontrol --opts=${pkgbase}.opts cmake -Wno-dev : make #VERBOSE=1
  cmake \
    -S "${_name}" \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DDUNE_ENABLE_PYTHONBINDINGS=ON \
    -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE \
    -Wno-dev
  sed -i 's/^include-system-site-packages = false/include-system-site-packages = true/' build-cmake/dune-env/pyvenv.cfg
  cat build-cmake/dune-env/pyvenv.cfg
  cmake --build build-cmake --target all # help
  cd "build-cmake/python"
  export PYTHONHASHSEED=0
  python setup.py build
}

check() {
  # export DUNE_CONTROL_PATH="${srcdir}/${_name}"
  # dunecontrol --opts=${pkgbase}.opts make build_tests #test_python
  # dune-ctest -E test-polygongrid --verbose --output-on-failure
  cmake --build build-cmake --target build_tests
  ctest -E test-polygongrid --verbose --output-on-failure --test-dir build-cmake
}

package_dune-polygongrid-git() {
  depends=(dune-grid-git)
  provides=("${_name}=${pkgver%%.r*}")
  conflicts=(${_name})
  # dunecontrol --only=${_name} make install DESTDIR="${pkgdir}"
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  install -Dm644 ${_name}/LICENSE "${pkgdir}/usr/share/licenses/${_name}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}

package_python-dune-polygongrid-git() {
  depends=(dune-polygongrid-git python-dune-grid-git)
  pkgdesc+=" (python bindings)"
  conflicts+=("python-${_name}")
  # cd "${_name}/build-cmake/python"
  cd "build-cmake/python"
  PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
}
