## [`dune-functions-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-functions-git/PKGBUILD)

```console
-- The following OPTIONAL packages have been found:
 * dune-uggrid
 * LATEX
 * LatexMk
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * GMP, GNU multi-precision library, <https://gmplib.org>
 * QuadMath, GCC Quad-Precision Math Library, <https://gcc.gnu.org/onlinedocs/libquadmath>
 * TBB, Intel's Threading Building Blocks
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * Python3
 * SuperLU (required version >= 5.0), Supernodal LU
   Direct solver for linear system, based on LU decomposition
 * ARPACK, ARnoldi PACKage
   Solve large scale eigenvalue problems
 * ARPACKPP, ARPACK++
   C++ interface for ARPACK
 * Threads, Multi-threading library
 * SuiteSparse, A suite of sparse matrix software, <http://faculty.cse.tamu.edu/davis/suitesparse.html>
 * METIS (required version >= 5.0), Serial Graph Partitioning
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * ParMETIS, Parallel Graph Partitioning
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * Alberta (required version >= 3.0), An adaptive hierarchical finite element toolbox and grid manager
 * Psurface, Piecewise linear bijections between triangulated surfaces
-- The following REQUIRED packages have been found:
 * dune-common
 * dune-localfunctions
 * dune-grid
 * dune-istl
 * dune-typetree
 * dune-geometry
-- The following OPTIONAL packages have not been found:
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * PTScotch, Sequential and Parallel Graph Partitioning
 * AmiraMesh
```
