# Maintainer: anon at sansorgan.es
_name=dune-multidomaingrid
pkgname="${_name}-git"
_gitcommit=9f302e38ccf0c7c13f15e2833eb06682a1133a00
pkgver=2.8.r669.9f302e3
pkgrel=1
#epoch=
pkgdesc="Meta grid that allows creating multiple subdomains that span only part of the host grid"
arch=(x86_64)
url="https://dune-project.org/modules/dune-multidomaingrid"
license=(LGPL3)
# groups=('dune-extensions')
depends=(dune-grid-git) # 'dune-typetree-git'
makedepends=(doxygen graphviz git)
# 'biber'
#checkdepends=()
optdepends=('doxygen: Generate the class documentation from C++ sources'
  'graphviz: Graph visualization software'
  'parmetis: Parallel Graph Partitioning and Fill-reducing Matrix Ordering')
# 'biber: A Unicode-capable BibTeX replacement for biblatex users'
provides=("${_name}=${pkgver%%.r*}")
conflicts=(${_name})
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=("git+https://gitlab.dune-project.org/extensions/${_name}#commit=${_gitcommit}") # ${pkgname}.opts
#noextract=()
sha512sums=('SKIP')

pkgver() {
  cd "${_name}"
  printf "2.8.r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

prepare() {
  sed -i 's/^Version: '"${pkgver%%.r*}"'-git/Version: '"${pkgver%%.r*}"'/' ${_name}/dune.module
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L76
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
}

build() {
  # dunecontrol --opts=${pkgname}.opts cmake -Wno-dev : make
  cmake \
    -S "${_name}" \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DDUNE_ENABLE_PYTHONBINDINGS=ON \
    -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE \
    -Wno-dev
}

check() {
  if [ -z "$(ldconfig -p | grep libcuda.so.1)" ]; then
    export OMPI_MCA_opal_warn_on_missing_libcuda=0
  fi
  # dunecontrol --opts=${pkgname}.opts make build_tests #test_python
  # cd "${srcdir}/${_name}/build-cmake"
  # dune-ctest --verbose --output-on-failure
  cmake --build build-cmake --target build_tests
  ctest --verbose --output-on-failure --test-dir build-cmake
}

package() {
  # dunecontrol --only=${_name} make install DESTDIR="${pkgdir}"
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  install -Dm644 ${_name}/COPYING.md "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}
