## [`dune-localfunctions-git`]( https://gitlab.com/dune-archiso/dune-core-git/-/raw/main/dune-localfunctions-git/PKGBUILD)

When the package `dune-localfunctions-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `bin`
- [x] `include`
- [x] `lib`
- [ ] `share`
  - [x] `bash-completion`
  - [ ] `doc`
  - [x] `dune`
  - [x] `dune-common`
  - [x] `licenses`
  - [ ] `man`

```console
-DCMAKE_DISABLE_DOCUMENTATION=TRUE
```

### Remarks

Not found `doc/dune-localfunctions-manual.tex` for compile.

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doc_dune-localfunctions-manual_tex
doc_dune-localfunctions-manual_tex_clean
doxyfile
doxygen_dune-localfunctions
doxygen_install
headercheck
install_python
install_python__tmp_makepkg_dune-localfunctions-git_src_dune-localfunctions_python
test_python
_localfunctions
bdfmelementtest
brezzidouglasmarinielementtest
crouzeixraviartelementtest
dualmortarelementtest
globalmonomialfunctionstest
hierarchicalelementtest
lagrangeshapefunctiontest
monomialshapefunctiontest
nedelec1stkindelementtest
rannacherturekelementtest
raviartthomaselementtest
refinedelementtest
test-biorthogonality
test-edges0
test-finiteelementcache
test-lagrange1
test-lagrange2
test-lagrange3
test-lagrange4
test-orthonormal1
test-orthonormal2
test-orthonormal3
test-orthonormal4
test-pk2d
test-power-monomial
test-q1
test-q2
test-raviartthomassimplex1
test-raviartthomassimplex2
test-raviartthomassimplex3
test-raviartthomassimplex4
testgenericfem
virtualshapefunctiontest
```
