```bash
depends=(python-meshplex python-quadpy python-termplotlib)
makedepends=(python-setuptools)
checkdepends=(python-matplotx python-meshzoo python-pytest-codeblocks)
# optdepends=('python-matplotlib: for Matplotlib rendering')

# check() {
#   cd "${_base}"
#   python -c "from setuptools import setup; setup();" install --root="${PWD}/tmp_install" --optimize=1 --skip-build
#   PYTHONPATH="${PWD}/tmp_install$(python -c "import site; print(site.getsitepackages()[0])"):${PYTHONPATH}" python -m pytest --codeblocks
# }
```
