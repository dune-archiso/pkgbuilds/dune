[requests-cache](https://github.com/reclosedev/requests-cache)
[](https://github.com/reclosedev/requests-cache/blob/master/pyproject.toml)
```
prepare() {
  cd "${_base}-${pkgver}"
  dephell deps convert --from pyproject.toml --to setup.py
}

# python setup.py build
# poetry build --format wheel

# python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
```

```
optdepends=('python-boto3: Cache backend for Amazon DynamoDB database'
  'python-redis: Cache backend for Redis cache'
  'python-pymongo: Cache backend for MongoDB database') # python-botocore python-yaml python-sphinx-furo python-linkify-it-py python-myst-parser
checkdepends=(python-pytest python-requests-mock python-responses python-itsdangerous python-ujson python-timeout-decorator)
# python-pymongo python-redis redis python-boto3
```
