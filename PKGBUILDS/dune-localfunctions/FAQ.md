When the package `dune-localfunctions-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `include`
- [x] `lib`
- [x] `share`
  - [ ] `doc`
  - [x] `dune-localfunctions`
  - [x] `licenses`

```console
.
└── usr
    ├── include
    │   └── dune
    │       └── localfunctions
    ├── lib
    │   ├── cmake
    │   │   └── dune-localfunctions
    │   ├── dunecontrol
    │   │   └── dune-localfunctions
    │   │       └── dune.module
    │   └── pkgconfig
    │       └── dune-localfunctions.pc
    └── share
        ├── dune-localfunctions
        │   └── config.h.cmake
        └── licenses

59 directories, 185 files
```

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
auxclean
build_quick_tests
build_tests
clean_latex
doc
doc_dune-localfunctions-manual_tex
doc_dune-localfunctions-manual_tex_clean
doxyfile
doxygen_dune-localfunctions
doxygen_install
dvi
headercheck
html
install_python
pdf
ps
safepdf
test_python
bdfmelementtest
brezzidouglasmarinielementtest
crouzeixraviartelementtest
dualmortarelementtest
globalmonomialfunctionstest
hierarchicalelementtest
lagrangeshapefunctiontest
monomialshapefunctiontest
rannacherturekelementtest
raviartthomaselementtest
refinedelementtest
test-biorthogonality
test-edges0
test-finiteelementcache
test-lagrange1
test-lagrange2
test-lagrange3
test-lagrange4
test-orthonormal1
test-orthonormal2
test-orthonormal3
test-orthonormal4
test-pk2d
test-power-monomial
test-q1
test-q2
test-raviartthomassimplex1
test-raviartthomassimplex2
test-raviartthomassimplex3
test-raviartthomassimplex4
testgenericfem
virtualshapefunctiontest
```

[](https://fossies.org/diffs/dune-localfunctions)
[](https://dune-project.org/pdf/meetings/2013-08-globalfunctions/dune-localfunctions-manual.pdf)
