## [`dune-localfunctions`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-localfunctions/PKGBUILD)

#### debian

- [](https://tracker.debian.org/pkg/dune-localfunctions)
- [](https://salsa.debian.org/pjaap/dune-localfunctions)
- [](https://salsa.debian.org/lisajuliafog/dune-localfunctions)

```console
-- The following OPTIONAL packages have been found:

 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * UnixCommands, Some common Unix commands
   To generate the documentation with LaTeX
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * BLAS, fast linear algebra routines
 * Threads, Multi-threading library
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs

-- The following REQUIRED packages have been found:

 * dune-common
 * dune-geometry

-- The following OPTIONAL packages have not been found:

 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
```

```
usr/
usr/include/
usr/include/dune/
usr/include/dune/localfunctions/
usr/include/dune/localfunctions/brezzidouglasfortinmarini/
usr/include/dune/localfunctions/brezzidouglasfortinmarini/bdfmcube.hh
usr/include/dune/localfunctions/brezzidouglasfortinmarini/cube/
usr/include/dune/localfunctions/brezzidouglasfortinmarini/cube/localbasis.hh
usr/include/dune/localfunctions/brezzidouglasfortinmarini/cube/localcoefficients.hh
usr/include/dune/localfunctions/brezzidouglasfortinmarini/cube/localinterpolation.hh
usr/include/dune/localfunctions/brezzidouglasmarini/
usr/include/dune/localfunctions/brezzidouglasmarini.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube2d/
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube2d.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube2d/brezzidouglasmarini1cube2dlocalbasis.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube2d/brezzidouglasmarini1cube2dlocalcoefficients.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube2d/brezzidouglasmarini1cube2dlocalinterpolation.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube3d/
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube3d.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube3d/brezzidouglasmarini1cube3dlocalbasis.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube3d/brezzidouglasmarini1cube3dlocalcoefficients.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1cube3d/brezzidouglasmarini1cube3dlocalinterpolation.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1simplex2d/
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1simplex2d.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1simplex2d/brezzidouglasmarini1simplex2dlocalbasis.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1simplex2d/brezzidouglasmarini1simplex2dlocalcoefficients.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini1simplex2d/brezzidouglasmarini1simplex2dlocalinterpolation.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2cube2d/
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2cube2d.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2cube2d/brezzidouglasmarini2cube2dlocalbasis.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2cube2d/brezzidouglasmarini2cube2dlocalcoefficients.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2cube2d/brezzidouglasmarini2cube2dlocalinterpolation.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2simplex2d/
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2simplex2d.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2simplex2d/brezzidouglasmarini2simplex2dlocalbasis.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2simplex2d/brezzidouglasmarini2simplex2dlocalcoefficients.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarini2simplex2d/brezzidouglasmarini2simplex2dlocalinterpolation.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarinicube.hh
usr/include/dune/localfunctions/brezzidouglasmarini/brezzidouglasmarinisimplex.hh
usr/include/dune/localfunctions/common/
usr/include/dune/localfunctions/common/interface.hh
usr/include/dune/localfunctions/common/interfaceswitch.hh
usr/include/dune/localfunctions/common/localbasis.hh
usr/include/dune/localfunctions/common/localfiniteelementtraits.hh
usr/include/dune/localfunctions/common/localfiniteelementvariant.hh
usr/include/dune/localfunctions/common/localfiniteelementvariantcache.hh
usr/include/dune/localfunctions/common/localinterpolation.hh
usr/include/dune/localfunctions/common/localkey.hh
usr/include/dune/localfunctions/common/localtoglobaladaptors.hh
usr/include/dune/localfunctions/common/virtualinterface.hh
usr/include/dune/localfunctions/common/virtualwrappers.hh
usr/include/dune/localfunctions/crouzeixraviart.hh
usr/include/dune/localfunctions/dualmortarbasis/
usr/include/dune/localfunctions/dualmortarbasis.hh
usr/include/dune/localfunctions/dualmortarbasis/dualp1/
usr/include/dune/localfunctions/dualmortarbasis/dualp1.hh
usr/include/dune/localfunctions/dualmortarbasis/dualp1/dualp1localbasis.hh
usr/include/dune/localfunctions/dualmortarbasis/dualp1/dualp1localcoefficients.hh
usr/include/dune/localfunctions/dualmortarbasis/dualp1/dualp1localinterpolation.hh
usr/include/dune/localfunctions/dualmortarbasis/dualpq1factory.hh
usr/include/dune/localfunctions/dualmortarbasis/dualq1/
usr/include/dune/localfunctions/dualmortarbasis/dualq1.hh
usr/include/dune/localfunctions/dualmortarbasis/dualq1/dualq1localbasis.hh
usr/include/dune/localfunctions/dualmortarbasis/dualq1/dualq1localcoefficients.hh
usr/include/dune/localfunctions/dualmortarbasis/dualq1/dualq1localinterpolation.hh
usr/include/dune/localfunctions/hierarchical/
usr/include/dune/localfunctions/hierarchical.hh
usr/include/dune/localfunctions/hierarchical/hierarchicalp2/
usr/include/dune/localfunctions/hierarchical/hierarchicalp2.hh
usr/include/dune/localfunctions/hierarchical/hierarchicalp2/hierarchicalsimplexp2localbasis.hh
usr/include/dune/localfunctions/hierarchical/hierarchicalp2/hierarchicalsimplexp2localinterpolation.hh
usr/include/dune/localfunctions/hierarchical/hierarchicalp2withelementbubble/
usr/include/dune/localfunctions/hierarchical/hierarchicalp2withelementbubble.hh
usr/include/dune/localfunctions/hierarchical/hierarchicalp2withelementbubble/hierarchicalsimplexp2withelementbubble.hh
usr/include/dune/localfunctions/hierarchical/hierarchicalprismp2/
usr/include/dune/localfunctions/hierarchical/hierarchicalprismp2.hh
usr/include/dune/localfunctions/hierarchical/hierarchicalprismp2/hierarchicalprismp2localbasis.hh
usr/include/dune/localfunctions/hierarchical/hierarchicalprismp2/hierarchicalprismp2localinterpolation.hh
usr/include/dune/localfunctions/lagrange/
usr/include/dune/localfunctions/lagrange.hh
usr/include/dune/localfunctions/lagrange/emptypoints.hh
usr/include/dune/localfunctions/lagrange/equidistantpoints.hh
usr/include/dune/localfunctions/lagrange/interpolation.hh
usr/include/dune/localfunctions/lagrange/lagrangebasis.hh
usr/include/dune/localfunctions/lagrange/lagrangecoefficients.hh
usr/include/dune/localfunctions/lagrange/lagrangecube.hh
usr/include/dune/localfunctions/lagrange/lagrangelfecache.hh
usr/include/dune/localfunctions/lagrange/lagrangeprism.hh
usr/include/dune/localfunctions/lagrange/lagrangepyramid.hh
usr/include/dune/localfunctions/lagrange/lagrangesimplex.hh
usr/include/dune/localfunctions/lagrange/p0/
usr/include/dune/localfunctions/lagrange/p0.hh
usr/include/dune/localfunctions/lagrange/p0/p0localbasis.hh
usr/include/dune/localfunctions/lagrange/p0/p0localcoefficients.hh
usr/include/dune/localfunctions/lagrange/p0/p0localinterpolation.hh
usr/include/dune/localfunctions/lagrange/p1.hh
usr/include/dune/localfunctions/lagrange/p2.hh
usr/include/dune/localfunctions/lagrange/p23d.hh
usr/include/dune/localfunctions/lagrange/pk.hh
usr/include/dune/localfunctions/lagrange/pk1d.hh
usr/include/dune/localfunctions/lagrange/pk2d.hh
usr/include/dune/localfunctions/lagrange/pk3d.hh
usr/include/dune/localfunctions/lagrange/pq22d.hh
usr/include/dune/localfunctions/lagrange/pqkfactory.hh
usr/include/dune/localfunctions/lagrange/prismp1.hh
usr/include/dune/localfunctions/lagrange/prismp2.hh
usr/include/dune/localfunctions/lagrange/pyramidp1.hh
usr/include/dune/localfunctions/lagrange/pyramidp2.hh
usr/include/dune/localfunctions/lagrange/q1.hh
usr/include/dune/localfunctions/lagrange/q2.hh
usr/include/dune/localfunctions/lagrange/qk.hh
usr/include/dune/localfunctions/meta/
usr/include/dune/localfunctions/meta/power/
usr/include/dune/localfunctions/meta/power.hh
usr/include/dune/localfunctions/meta/power/basis.hh
usr/include/dune/localfunctions/meta/power/coefficients.hh
usr/include/dune/localfunctions/meta/power/interpolation.hh
usr/include/dune/localfunctions/mimetic/
usr/include/dune/localfunctions/mimetic.hh
usr/include/dune/localfunctions/mimetic/mimeticall.hh
usr/include/dune/localfunctions/monomial/
usr/include/dune/localfunctions/monomial.hh
usr/include/dune/localfunctions/monomial/monomiallocalbasis.hh
usr/include/dune/localfunctions/monomial/monomiallocalcoefficients.hh
usr/include/dune/localfunctions/monomial/monomiallocalinterpolation.hh
usr/include/dune/localfunctions/nedelec/
usr/include/dune/localfunctions/nedelec.hh
usr/include/dune/localfunctions/nedelec/nedelec1stkindcube.hh
usr/include/dune/localfunctions/nedelec/nedelec1stkindsimplex.hh
usr/include/dune/localfunctions/orthonormal/
usr/include/dune/localfunctions/orthonormal.hh
usr/include/dune/localfunctions/orthonormal/orthonormalbasis.hh
usr/include/dune/localfunctions/orthonormal/orthonormalcompute.hh
usr/include/dune/localfunctions/rannacherturek/
usr/include/dune/localfunctions/rannacherturek.hh
usr/include/dune/localfunctions/rannacherturek/rannacherturek.hh
usr/include/dune/localfunctions/rannacherturek/rannacherturek2d/
usr/include/dune/localfunctions/rannacherturek/rannacherturek2d/rannacherturek2dlocalbasis.hh
usr/include/dune/localfunctions/rannacherturek/rannacherturek3d/
usr/include/dune/localfunctions/rannacherturek/rannacherturek3d/rannacherturek3dlocalbasis.hh
usr/include/dune/localfunctions/rannacherturek/rannachertureklocalbasis.hh
usr/include/dune/localfunctions/rannacherturek/rannachertureklocalcoefficients.hh
usr/include/dune/localfunctions/rannacherturek/rannachertureklocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/
usr/include/dune/localfunctions/raviartthomas.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas02d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas02d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas02d/raviartthomas02dlocalbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas02d/raviartthomas02dlocalcoefficients.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas02d/raviartthomas02dlocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas03d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas03d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas03d/raviartthomas03dlocalbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas03d/raviartthomas03dlocalcoefficients.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas03d/raviartthomas03dlocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas0cube2d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas0cube2d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas0cube2d/raviartthomas0cube2dall.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas0cube3d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas0cube3d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas0cube3d/raviartthomas0cube3dall.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas12d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas12d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas12d/raviartthomas12dlocalbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas12d/raviartthomas12dlocalcoefficients.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas12d/raviartthomas12dlocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube2d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube2d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube2d/raviartthomas1cube2dlocalbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube2d/raviartthomas1cube2dlocalcoefficients.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube2d/raviartthomas1cube2dlocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube3d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube3d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube3d/raviartthomas1cube3dlocalbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube3d/raviartthomas1cube3dlocalcoefficients.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas1cube3d/raviartthomas1cube3dlocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas2cube2d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas2cube2d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas2cube2d/raviartthomas2cube2dlocalbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas2cube2d/raviartthomas2cube2dlocalcoefficients.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas2cube2d/raviartthomas2cube2dlocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas3cube2d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas3cube2d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas3cube2d/raviartthomas3cube2dlocalbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas3cube2d/raviartthomas3cube2dlocalcoefficients.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas3cube2d/raviartthomas3cube2dlocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas4cube2d/
usr/include/dune/localfunctions/raviartthomas/raviartthomas4cube2d.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas4cube2d/raviartthomas4cube2dlocalbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas4cube2d/raviartthomas4cube2dlocalcoefficients.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomas4cube2d/raviartthomas4cube2dlocalinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomascube.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomaslfecache.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomassimplex/
usr/include/dune/localfunctions/raviartthomas/raviartthomassimplex.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomassimplex/raviartthomassimplexbasis.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomassimplex/raviartthomassimplexinterpolation.hh
usr/include/dune/localfunctions/raviartthomas/raviartthomassimplex/raviartthomassimplexprebasis.hh
usr/include/dune/localfunctions/refined/
usr/include/dune/localfunctions/refined.hh
usr/include/dune/localfunctions/refined/common/
usr/include/dune/localfunctions/refined/common/refinedsimplexlocalbasis.hh
usr/include/dune/localfunctions/refined/refinedp0/
usr/include/dune/localfunctions/refined/refinedp0.hh
usr/include/dune/localfunctions/refined/refinedp0/refinedp0localbasis.hh
usr/include/dune/localfunctions/refined/refinedp0/refinedp0localcoefficients.hh
usr/include/dune/localfunctions/refined/refinedp0/refinedp0localinterpolation.hh
usr/include/dune/localfunctions/refined/refinedp1/
usr/include/dune/localfunctions/refined/refinedp1.hh
usr/include/dune/localfunctions/refined/refinedp1/refinedp1localbasis.hh
usr/include/dune/localfunctions/test/
usr/include/dune/localfunctions/test/geometries.hh
usr/include/dune/localfunctions/test/test-fe.hh
usr/include/dune/localfunctions/test/test-localfe.hh
usr/include/dune/localfunctions/utility/
usr/include/dune/localfunctions/utility/basisevaluator.hh
usr/include/dune/localfunctions/utility/basismatrix.hh
usr/include/dune/localfunctions/utility/basisprint.hh
usr/include/dune/localfunctions/utility/coeffmatrix.hh
usr/include/dune/localfunctions/utility/defaultbasisfactory.hh
usr/include/dune/localfunctions/utility/dglocalcoefficients.hh
usr/include/dune/localfunctions/utility/field.hh
usr/include/dune/localfunctions/utility/interpolationhelper.hh
usr/include/dune/localfunctions/utility/l2interpolation.hh
usr/include/dune/localfunctions/utility/lfematrix.hh
usr/include/dune/localfunctions/utility/localfiniteelement.hh
usr/include/dune/localfunctions/utility/monomialbasis.hh
usr/include/dune/localfunctions/utility/multiindex.hh
usr/include/dune/localfunctions/utility/polynomialbasis.hh
usr/include/dune/localfunctions/utility/tensor.hh
usr/include/dune/localfunctions/whitney/
usr/include/dune/localfunctions/whitney/edges0.5/
usr/include/dune/localfunctions/whitney/edges0.5.hh
usr/include/dune/localfunctions/whitney/edges0.5/basis.hh
usr/include/dune/localfunctions/whitney/edges0.5/coefficients.hh
usr/include/dune/localfunctions/whitney/edges0.5/common.hh
usr/include/dune/localfunctions/whitney/edges0.5/interpolation.hh
usr/include/dune/python/
usr/include/dune/python/localfunctions/
usr/include/dune/python/localfunctions/localfiniteelement.hh
usr/lib/
usr/lib/cmake/
usr/lib/cmake/dune-localfunctions/
usr/lib/cmake/dune-localfunctions/dune-localfunctions-config-version.cmake
usr/lib/cmake/dune-localfunctions/dune-localfunctions-config.cmake
usr/lib/dunecontrol/
usr/lib/dunecontrol/dune-localfunctions/
usr/lib/dunecontrol/dune-localfunctions/dune.module
usr/lib/pkgconfig/
usr/lib/pkgconfig/dune-localfunctions.pc
usr/share/
usr/share/doc/
usr/share/doc/dune-localfunctions/
usr/share/doc/dune-localfunctions/comm/
usr/share/doc/dune-localfunctions/comm/dune-localfunctions-manual.pdf
usr/share/doc/dune-localfunctions/doxygen/
usr/share/doc/dune-localfunctions/doxygen/index.html
usr/share/dune-localfunctions/
usr/share/dune-localfunctions/config.h.cmake
usr/share/licenses/
usr/share/licenses/dune-localfunctions/
usr/share/licenses/dune-localfunctions/LICENSE
```

```
usr/
usr/lib/
usr/lib/python3.9/
usr/lib/python3.9/site-packages/
usr/lib/python3.9/site-packages/dune/
usr/lib/python3.9/site-packages/dune/localfunctions/
usr/lib/python3.9/site-packages/dune/localfunctions/__init__.py
usr/lib/python3.9/site-packages/dune/localfunctions/_localfunctions.so
usr/lib/python3.9/site-packages/dune_localfunctions-2.8.0-py3.9-nspkg.pth
usr/lib/python3.9/site-packages/dune_localfunctions-2.8.0-py3.9.egg-info/
usr/lib/python3.9/site-packages/dune_localfunctions-2.8.0-py3.9.egg-info/PKG-INFO
usr/lib/python3.9/site-packages/dune_localfunctions-2.8.0-py3.9.egg-info/SOURCES.txt
usr/lib/python3.9/site-packages/dune_localfunctions-2.8.0-py3.9.egg-info/dependency_links.txt
usr/lib/python3.9/site-packages/dune_localfunctions-2.8.0-py3.9.egg-info/namespace_packages.txt
usr/lib/python3.9/site-packages/dune_localfunctions-2.8.0-py3.9.egg-info/not-zip-safe
usr/lib/python3.9/site-packages/dune_localfunctions-2.8.0-py3.9.egg-info/top_level.txt
```

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-localfunctions.svg)](https://repology.org/project/dune-localfunctions/versions)
