## [`dune-uggrid-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-uggrid-git/PKGBUILD)

```console
-- The following OPTIONAL packages have been found:
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * BLAS, fast linear algebra routines
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library, <https://gmplib.org>
 * QuadMath, GCC Quad-Precision Math Library, <https://gcc.gnu.org/onlinedocs/libquadmath>
 * Threads, Multi-threading library
 * TBB, Intel's Threading Building Blocks
 * PTScotch, Sequential and Parallel Graph Partitioning
 * METIS (required version >= 5.0), Serial Graph Partitioning
 * ParMETIS (required version >= 4.0), Parallel Graph Partitioning
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * Python3
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
-- The following REQUIRED packages have been found:
 * dune-common
-- The following OPTIONAL packages have not been found:
 * LATEX
 * LatexMk
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
```
