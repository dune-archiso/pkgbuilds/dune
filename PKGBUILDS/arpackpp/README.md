## [`arpackpp`](https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=arpackpp)

- Debian packages
  - [libparpack2-dev](https://packages.debian.org/sid/libparpack2-dev)
  - [libparpack2](https://packages.debian.org/sid/libparpack2)
  - [libarpack2-dbgsym](https://packages.debian.org/sid/libarpack2-dbgsym)
- Ubuntu packages
  - [arpack++](https://launchpad.net/ubuntu/+source/arpack++)
  - [libarpack++2-dev](https://packages.ubuntu.com/focal/libarpack++2-dev)
  - [Arpack++](https://help.ubuntu.com/community/Arpack%2B%2B)
- FreeBSD ports
  - [arpack++](https://www.freshports.org/math/arpack++)
- Arch Linux packages
  - [arpackpp](https://aur.archlinux.org/packages/arpackpp)
  - [arpack++](https://aur.archlinux.org/packages/arpack%2B%2B)

* [](https://unix.stackexchange.com/a/149361)
* [](https://people.bath.ac.uk/mamamf/talks/talk220405.pdf)
* [Arnoldi iteration](https://en.wikipedia.org/wiki/Arnoldi_iteration)
* [](https://repology.org/project/arpackpp/versions)
* [](https://repology.org/project/arpack++/versions)

[![arpackpp](https://img.shields.io/aur/version/arpackpp?color=1793d1&label=arpackpp&logo=arch-linux&style=for-the-badge)](https://aur.archlinux.org/packages/arpackpp)

[![Packaging status](https://repology.org/badge/vertical-allrepos/arpackpp.svg)](https://repology.org/project/arpackpp/versions)

[![Packaging status](https://repology.org/badge/vertical-allrepos/arpack++.svg)](https://repology.org/project/arpack++/versions)
