```console
# depends+=('cmake' 'gcc-fortran')
# makedepends+=('inkscape' 'imagemagick' 'python' 'doxygen' 'python-sphinx')
# ==> ERROR: makedepends can not be set inside a package function
```

```console
# source+=("https://gitlab.dune-project.org/staging/${_module}/-/archive/releases/${pkgver: :(3)}/${_module}-releases-${pkgver: :(3)}.tar.gz")
# 5c4165a4737df65adf9d821fb3722a01
# python-numpy python-mpi4py jupyter python-matplotlib
# export CMAKE_FLAGS="-DBUILD_SHARED_LIBS=TRUE \
#              -DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS=TRUE \
#              -DPYTHON_INSTALL_LOCATION="system"

# CMAKE_FLAGS='-DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=/usr/lib -fPIC -DBUILD_SHARED_LIBS:BOOL=OFF' \

# CMAKE_FLAGS='-DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=/usr/lib -fPIC -DBUILD_SHARED_LIBS:BOOL=OFF' \
# DUNE_CMAKE_FLAGS='-DDUNE_ENABLE_PYTHONBINDINGS=ON -DBUILD_SHARED_LIBS=TRUE -DDUNE_PYTHON_INSTALL_LOCATION=system' \
```

```console
makedepends=('superlu' 'suitesparse' 'parmetis' 'psurface' 'boost') # 'boost' 'openmpi' 'gmp'
```

```console
export LDFLAGS="-ldl -lm -ltirpc"
export CFLAGS="-fPIC ${CFLAGS}"
```

```console
# export CPPFLAGS="-I/usr/include/tirpc ${CPPFLAGS}"
# export CFLAGS="-fPIC ${CFLAGS}"
# export CXXFLAGS="-fPIC ${CFLAGS}"
# export LDFLAGS="-ldl -lm -ltirpc"
```

```console
# export CPPFLAGS="-I/usr/include/tirpc ${CPPFLAGS}"
# export CFLAGS="-fPIC ${CFLAGS}"
# export CXXFLAGS="-fPIC ${CFLAGS}"
# export LDFLAGS="-ldl -lm -ltirpc"

# export CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -DBUILD_SHARED_LIBS:BOOL=OFF ${CMAKE_FLAGS}"
```

```console
    # optdepends=(
    #     'lapack: Linear Algebra PACKage'
    #     'openmpi: xxxx'
    #     'gmp: xxxx'
    #     'texlive-core: xxx'
    #     'imagemagick: xxxx'
    #     'python-sphinx: Building Sphinx documentation'
    #     'doxygen: Generate the class documentation from C++ sources'
    #     'inkscape: xxx'
    #     'vc: For use of SIMD instructions'
    #     'python: xxx'
    #     'python-pip: xxx')
```

```
libdune-common-2.3.1 (2.3.1-1)
libdune-common-dbg (2.3.1-1)
libdune-common-dev (2.3.1-1)
libdune-common-doc (2.3.1-1)
libdune-geometry-2.3.1 (2.3.1-1)
libdune-geometry-dbg (2.3.1-1)
libdune-geometry-dev (2.3.1-1)
libdune-geometry-doc (2.3.1-1)
libdune-grid-2.3.1 (2.3.1-1)
libdune-grid-dbg (2.3.1-1)
libdune-grid-dev (2.3.1-1)
libdune-grid-doc (2.3.1-1)
libdune-grid-glue-2.3.0 (2.3.1-1)
libdune-grid-glue-dbg (2.3.1-1)
libdune-grid-glue-dev (2.3.1-1)
libdune-grid-glue-doc (2.3.1-1)
libdune-istl-dev (2.3.1-1)
libdune-istl-doc (2.3.1-1)
libdune-localfunctions-dev (2.3.1-1)
libdune-localfunctions-doc (2.3.1-1)
libdune-pdelab-2.0.0 (2.0.0-1)
libdune-pdelab-dev (2.0.0-1)
libdune-pdelab-doc (2.0.0-1)
libdune-typetree-2.3.1 (2.3.1-1+b1
libdune-typetree-dev (2.3.1-1+b1
libdune-typetree-doc (2.3.1-1)
```

```
# Maintainer: Daniel Moch <daniel AT danielmoch DOT com>
pkgbase=python-pytest-flakes
pkgname=('python-pytest-flakes' 'python2-pytest-flakes')
_name=${pkgbase#python-}
pkgver=4.0.0
pkgrel=1
pkgdesc='pytest plugin to check source code with pyflakes'
arch=('any')
url='https://github.com/fschulze/pytest-flakes'
license=('MIT')
makedepends=('python' 'python-setuptools' 'python2' 'python2-setuptools')
source=("${_name}-${pkgver}.tar.gz::https://files.pythonhosted.org/packages/source/${_name::1}/${_name}/${_name}-${pkgver}.tar.gz")
sha256sums=('341964bf5760ebbdde9619f68a17d5632c674c3f6903ef66daa0a4f540b3d143')

prepare() {
  cp -a ${_name}-$pkgver{,-py2}
}

build() {
  cd "${srcdir}/${_name}-${pkgver}"
  python setup.py build

  cd "${srcdir}/${_name}-${pkgver}-py2"
  python setup.py build
}

package_python-pytest-flakes() {
  depends=('python' 'python-pyflakes')
  cd "${srcdir}/${_name}-${pkgver}"
  python setup.py install --root="$pkgdir/" --optimize=1 --skip-build
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

package_python2-pytest-flakes() {
  depends=('python2' 'python2-pyflakes')
  cd "${srcdir}/${_name}-${pkgver}-py2"
  python2 setup.py install --root="$pkgdir/" --optimize=1 --skip-build
  install -Dm644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
```

```
# Contributor: Rafael Fontenelle <rafaelff@gnome.org>
# Contributor: Oliver Rümpelein <oli_r@fg4f.de>
# Based upon work of Jeff Parent <jecxjo@sdf.lonestar.org>
_name=pytest-pep8
pkgname=('python-pytest-pep8' 'python2-pytest-pep8')
pkgbase=python-$_name
pkgver=1.0.6
pkgrel=4
pkgdesc="pytest plugin to check PEP8 requirements"
arch=(any)
url="https://bitbucket.org/pytest-dev/pytest-pep8"
license=('MIT')
makedepends=(python-pycodestyle  python-pytest-cache
             python2-pycodestyle python2-pytest-cache) # setuptools included
source=("https://pypi.org/packages/source/${_name:0:1}/$_name/$_name-$pkgver.tar.gz")
sha256sums=('032ef7e5fa3ac30f4458c73e05bb67b0f036a8a5cb418a534b3170f89f120318')

prepare() {
  cp -a $_name-$pkgver{,-py2}
}

build() {
  cd "$srcdir/$_name-$pkgver"
  python setup.py build

  cd "$srcdir/$_name-$pkgver-py2"
  python2 setup.py build
}

package_python-pytest-pep8() {
  depends=(python-pycodestyle python-pytest-cache)
  cd "$srcdir/$_name-$pkgver"
  PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python setup.py install --root="$pkgdir/" --optimize=1 --skip-build
  install -D -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_python2-pytest-pep8() {
  depends=(python2-pycodestyle python2-pytest-cache)
  cd "$srcdir/$_name-$pkgver-py2"
  python2 setup.py install --root="$pkgdir/" --optimize=1 --skip-build
  install -D -m644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
```
