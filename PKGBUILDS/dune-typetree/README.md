## [`dune-typetree`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-typetree/PKGBUILD)

#### debian

- [](https://tracker.debian.org/pkg/dune-typetree)
- [](https://salsa.debian.org/pjaap/dune-typetree)
- [](https://salsa.debian.org/lisajuliafog/dune-typetree)

```console
-- The following OPTIONAL packages have been found:
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * BLAS, fast linear algebra routines
 * Threads, Multi-threading library
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
-- The following REQUIRED packages have been found:
 * dune-common
-- The following OPTIONAL packages have not been found:
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
```

```
usr/
usr/include/
usr/include/dune/
usr/include/dune/typetree/
usr/include/dune/typetree/accumulate_static.hh
usr/include/dune/typetree/childextraction.hh
usr/include/dune/typetree/compositenode.hh
usr/include/dune/typetree/dynamicpowernode.hh
usr/include/dune/typetree/exceptions.hh
usr/include/dune/typetree/filteredcompositenode.hh
usr/include/dune/typetree/filters.hh
usr/include/dune/typetree/fixedcapacitystack.hh
usr/include/dune/typetree/generictransformationdescriptors.hh
usr/include/dune/typetree/leafnode.hh
usr/include/dune/typetree/nodeinterface.hh
usr/include/dune/typetree/nodetags.hh
usr/include/dune/typetree/pairtraversal.hh
usr/include/dune/typetree/powercompositenodetransformationtemplates.hh
usr/include/dune/typetree/powernode.hh
usr/include/dune/typetree/proxynode.hh
usr/include/dune/typetree/simpletransformationdescriptors.hh
usr/include/dune/typetree/transformation.hh
usr/include/dune/typetree/transformationutilities.hh
usr/include/dune/typetree/traversal.hh
usr/include/dune/typetree/traversalutilities.hh
usr/include/dune/typetree/treecontainer.hh
usr/include/dune/typetree/treepath.hh
usr/include/dune/typetree/typetraits.hh
usr/include/dune/typetree/typetree.hh
usr/include/dune/typetree/utility.hh
usr/include/dune/typetree/visitor.hh
usr/lib/
usr/lib/cmake/
usr/lib/cmake/dune-typetree/
usr/lib/cmake/dune-typetree/dune-typetree-config-version.cmake
usr/lib/cmake/dune-typetree/dune-typetree-config.cmake
usr/lib/dunecontrol/
usr/lib/dunecontrol/dune-typetree/
usr/lib/dunecontrol/dune-typetree/dune.module
usr/lib/pkgconfig/
usr/lib/pkgconfig/dune-typetree.pc
usr/share/
usr/share/doc/
usr/share/doc/dune-typetree/
usr/share/doc/dune-typetree/doxygen/
usr/share/doc/dune-typetree/doxygen/index.html
usr/share/dune-typetree/
usr/share/dune-typetree/config.h.cmake
usr/share/licenses/
usr/share/licenses/dune-typetree/
usr/share/licenses/dune-typetree/LICENSE
```

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-typetree.svg)](https://repology.org/project/dune-typetree/versions)
