```
-- The C compiler identification is GNU 11.2.0
-- The CXX compiler identification is GNU 11.2.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/sbin/gcc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/sbin/g++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Performing Test HAVE_C99
-- Performing Test HAVE_C99 - Success
-- Found C99: -std=c99  
-- Found Boost: /usr/lib64/cmake/Boost-1.78.0/BoostConfig.cmake (found suitable version "1.78.0", minimum required is "1.44.0") found components: system unit_test_framework 
-- Performing Test HAVE_CJSON
-- Performing Test HAVE_CJSON - Success
-- Found cjson: /usr/include/cjson  
-- Looking for sgemm_
-- Looking for sgemm_ - not found
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Success
-- Found Threads: TRUE  
-- Looking for sgemm_
-- Looking for sgemm_ - found
-- Found BLAS: /usr/lib/libblas.so  
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - found
-- Found LAPACK: /usr/lib/liblapack.so;/usr/lib/libblas.so  
-- Performing Test HAVE_CXA_DEMANGLE
-- Performing Test HAVE_CXA_DEMANGLE - Success
-- Found MPI_C: /usr/lib/openmpi/libmpi.so (found version "3.1") 
-- Found MPI_CXX: /usr/lib/openmpi/libmpi_cxx.so (found version "3.1") 
-- Found MPI: TRUE (found version "3.1")  
-- Found GMP: /usr/lib/libgmpxx.so  
-- Found dune-common: /usr/include  
-- Looking for MPI_Finalized
-- Looking for MPI_Finalized - found
-- Version 2.8.0 of dune-common from /usr/lib64/cmake/dune-common
-- Found METIS: /usr/lib/libmetis.so  
-- Looking for parmetis.h
-- Looking for parmetis.h - found
-- Found ParMETIS: /usr/include  
-- Performing Test HAVE_UMFPACK_WITHOUT_CHOLMOD
-- Performing Test HAVE_UMFPACK_WITHOUT_CHOLMOD - Success
-- Creating target SuitSparse::umfpack
-- Found SuiteSparse: /usr/lib/libumfpack.so;/usr/lib/libamd.so  
-- Found dune-istl: /usr/include  
-- Version 2.8.0 of dune-istl from /usr/lib64/cmake/dune-istl
-- Found dune-geometry: /usr/include  
-- Version 2.8.0 of dune-geometry from /usr/lib64/cmake/dune-geometry
-- Found dune-uggrid: /usr/include  
-- Version 2.8.0 of dune-uggrid from /usr/lib64/cmake/dune-uggrid
-- Found dune-grid: /usr/include  
-- Version 2.8.0 of dune-grid from /usr/lib64/cmake/dune-grid
-- Found PTScotch: /usr/include/scotch  
-- Found ZOLTAN: /usr/lib/libzoltan.so  
-- Found opm-grid: /usr/lib/libopmgrid.so  
-- Performing Test HAVE_ATTRIBUTE_ALWAYS_INLINE
-- Performing Test HAVE_ATTRIBUTE_ALWAYS_INLINE - Success
-- CMake version: 3.23.0
-- Linux distribution: Arch Linux
-- Target architecture: x86_64
-- Found Git: /usr/sbin/git (found version "2.35.2") 
-- Source code repository: not found!
-- GNU C++ compiler version: 11.2.0
-- Linker: ld 2.38
Processing opm_defaults opm-upscaling
-- Checking to see if CXX compiler accepts flag -Wl,--enable-new-dtags
-- Checking to see if CXX compiler accepts flag -Wl,--enable-new-dtags - yes
-- Precompiled headers: disabled
-- Build type: None
-- Checking to see if CXX compiler accepts flag -mtune=native
-- Checking to see if CXX compiler accepts flag -mtune=native - yes
-- Found OpenMP_C: -fopenmp (found version "4.5") 
-- Found OpenMP_CXX: -fopenmp (found version "4.5") 
-- Found OpenMP: TRUE (found version "4.5")  
-- Checking to see if CXX compiler accepts flag -pthread
-- Checking to see if CXX compiler accepts flag -pthread - yes
-- Could NOT find CppCheck (missing: CPPCHECK_PROGRAM) 
-- Disabling clang-check as CMAKE_EXPORT_COMPILE_COMMANDS is not enabled
-- Generating debug symbols: -ggdb3
-- Looking for strip utility
-- Looking for strip utility - found
-- Performing Test HAVE_DYNAMIC_BOOST_TEST
-- Performing Test HAVE_DYNAMIC_BOOST_TEST - Success
-- Writing config file "/tmp/makepkg/opm-upscaling/src/build-cmake/config.h"...
-- The Fortran compiler identification is GNU 11.2.0
-- Detecting Fortran compiler ABI info
-- Detecting Fortran compiler ABI info - done
-- Check for working Fortran compiler: /usr/sbin/gfortran - skipped
-- Detecting Fortran/C Interface
-- Detecting Fortran/C Interface - Found GLOBAL and MODULE mangling
-- This build defaults to installing in /usr
-- Could NOT find Doxygen (missing: DOXYGEN_EXECUTABLE) 
-- Writing version information to local header project-version.h
-- Configuring done
-- Generating done
-- Build files have been written to: /tmp/makepkg/opm-upscaling/src/build-cmake
The following are some of the valid targets for this Makefile:
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
additionals
attic
benchmark20
benchmarks
benchmarks-static
check
check-commits
datafiles
distclean
dune-common-static
dune-geometry-static
dune-grid-static
dune-istl-static
ert-static
examples
opm-common-static
opm-core-static
opm-grid-static
opm-material-static
opm-output-static
opm-parser-static
opm-upscaling-static
stonefile
test-suite
tests
aniso_implicitcap_test
aniso_simulator_test
compareUpscaling
cpchop
cpchop_depthtrend
cpregularize
exp_variogram
grdecldips
implicitcap_test
known_answer_test
mimetic_aniso_solver_test
mimetic_periodic_test
mimetic_solver_test
opmupscaling
sim_steadystate_explicit
sim_steadystate_implicit
steadystate_test_implicit
test_boundaryconditions
test_gravitypressure
test_matrix
upscale_avg
upscale_cap
upscale_cond
upscale_elasticity
upscale_perm
upscale_relperm
upscale_relperm_benchmark
upscale_relpermvisc
upscale_steadystate_implicit
benchmarks/upscale_relperm_benchmark.o
benchmarks/upscale_relperm_benchmark.i
benchmarks/upscale_relperm_benchmark.s
examples/aniso_implicitcap_test.o
examples/aniso_implicitcap_test.i
examples/aniso_implicitcap_test.s
examples/aniso_simulator_test.o
examples/aniso_simulator_test.i
examples/aniso_simulator_test.s
examples/cpchop.o
examples/cpchop.i
examples/cpchop.s
examples/cpchop_depthtrend.o
examples/cpchop_depthtrend.i
examples/cpchop_depthtrend.s
examples/cpregularize.o
examples/cpregularize.i
examples/cpregularize.s
examples/exp_variogram.o
examples/exp_variogram.i
examples/exp_variogram.s
examples/grdecldips.o
examples/grdecldips.i
examples/grdecldips.s
examples/implicitcap_test.o
examples/implicitcap_test.i
examples/implicitcap_test.s
examples/known_answer_test.o
examples/known_answer_test.i
examples/known_answer_test.s
examples/mimetic_aniso_solver_test.o
examples/mimetic_aniso_solver_test.i
examples/mimetic_aniso_solver_test.s
examples/mimetic_periodic_test.o
examples/mimetic_periodic_test.i
examples/mimetic_periodic_test.s
examples/mimetic_solver_test.o
examples/mimetic_solver_test.i
examples/mimetic_solver_test.s
examples/sim_steadystate_explicit.o
examples/sim_steadystate_explicit.i
examples/sim_steadystate_explicit.s
examples/sim_steadystate_implicit.o
examples/sim_steadystate_implicit.i
examples/sim_steadystate_implicit.s
examples/steadystate_test_implicit.o
examples/steadystate_test_implicit.i
examples/steadystate_test_implicit.s
examples/upscale_avg.o
examples/upscale_avg.i
examples/upscale_avg.s
examples/upscale_cap.o
examples/upscale_cap.i
examples/upscale_cap.s
examples/upscale_cond.o
examples/upscale_cond.i
examples/upscale_cond.s
examples/upscale_elasticity.o
examples/upscale_elasticity.i
examples/upscale_elasticity.s
examples/upscale_perm.o
examples/upscale_perm.i
examples/upscale_perm.s
examples/upscale_relperm.o
examples/upscale_relperm.i
examples/upscale_relperm.s
examples/upscale_relpermvisc.o
examples/upscale_relpermvisc.i
examples/upscale_relpermvisc.s
examples/upscale_steadystate_implicit.o
examples/upscale_steadystate_implicit.i
examples/upscale_steadystate_implicit.s
opm/core/transport/implicit/transport_source.o
opm/core/transport/implicit/transport_source.i
opm/core/transport/implicit/transport_source.s
opm/elasticity/boundarygrid.o
opm/elasticity/boundarygrid.i
opm/elasticity/boundarygrid.s
opm/elasticity/elasticity_preconditioners.o
opm/elasticity/elasticity_preconditioners.i
opm/elasticity/elasticity_preconditioners.s
opm/elasticity/material.o
opm/elasticity/material.i
opm/elasticity/material.s
opm/elasticity/materials.o
opm/elasticity/materials.i
opm/elasticity/materials.s
opm/elasticity/matrixops.o
opm/elasticity/matrixops.i
opm/elasticity/matrixops.s
opm/elasticity/meshcolorizer.o
opm/elasticity/meshcolorizer.i
opm/elasticity/meshcolorizer.s
opm/elasticity/mpc.o
opm/elasticity/mpc.i
opm/elasticity/mpc.s
opm/porsol/common/BoundaryPeriodicity.o
opm/porsol/common/BoundaryPeriodicity.i
opm/porsol/common/BoundaryPeriodicity.s
opm/porsol/common/ImplicitTransportDefs.o
opm/porsol/common/ImplicitTransportDefs.i
opm/porsol/common/ImplicitTransportDefs.s
opm/porsol/common/blas_lapack.o
opm/porsol/common/blas_lapack.i
opm/porsol/common/blas_lapack.s
opm/porsol/common/setupGridAndProps.o
opm/porsol/common/setupGridAndProps.i
opm/porsol/common/setupGridAndProps.s
opm/porsol/euler/ImplicitCapillarity.o
opm/porsol/euler/ImplicitCapillarity.i
opm/porsol/euler/ImplicitCapillarity.s
opm/upscaling/ParserAdditions.o
opm/upscaling/ParserAdditions.i
opm/upscaling/ParserAdditions.s
opm/upscaling/RelPermUtils.o
opm/upscaling/RelPermUtils.i
opm/upscaling/RelPermUtils.s
opm/upscaling/initCPGrid.o
opm/upscaling/initCPGrid.i
opm/upscaling/initCPGrid.s
opm/upscaling/writeECLData.o
opm/upscaling/writeECLData.i
opm/upscaling/writeECLData.s
tests/common/test_boundaryconditions.o
tests/common/test_boundaryconditions.i
tests/common/test_boundaryconditions.s
tests/common/test_gravitypressure.o
tests/common/test_gravitypressure.i
tests/common/test_gravitypressure.s
tests/common/test_matrix.o
tests/common/test_matrix.i
tests/common/test_matrix.s
tests/compareUpscaling.o
tests/compareUpscaling.i
tests/compareUpscaling.s
```
