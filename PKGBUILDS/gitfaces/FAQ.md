- [GitHub](https://github.com/nschloe/gitfaces)
- [Pypi](https://pypi.org/project/gitfaces)

```console
===================================================================================== FAILURES =====================================================================================
__________________________________________________________________________________ test_gitfaces ___________________________________________________________________________________

    def test_gitfaces():
        repo_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
        temp_dir = tempfile.mkdtemp()
        # Don't check GitHub, their rate limits are too tight for frequent tests.
>       gitfaces.fetch(repo_dir, temp_dir, github=False)

test/test_gitfaces.py:12: 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
.tox/py39/lib/python3.9/site-packages/gitfaces/main.py:16: in fetch
    repo = git.Repo(local_repo)
.tox/py39/lib/python3.9/site-packages/git/repo/base.py:220: in __init__
    self.working_dir = self._working_tree_dir or self.common_dir  # type: Optional[PathLike]
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 

self = <git.repo.base.Repo ''>

    @property
    def common_dir(self) -> PathLike:
        """
        :return: The git dir that holds everything except possibly HEAD,
            FETCH_HEAD, ORIG_HEAD, COMMIT_EDITMSG, index, and logs/."""
        if self._common_dir:
            return self._common_dir
        elif self.git_dir:
            return self.git_dir
        else:
            # or could return ""
>           raise InvalidGitRepositoryError()
E           git.exc.InvalidGitRepositoryError

.tox/py39/lib/python3.9/site-packages/git/repo/base.py:303: InvalidGitRepositoryError
_____________________________________________________________________________________ test_cli _____________________________________________________________________________________

    def test_cli():
        repo_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)
        temp_dir = tempfile.mkdtemp()
>       gitfaces.cli.main([repo_dir, temp_dir])

test/test_gitfaces.py:20: 
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
.tox/py39/lib/python3.9/site-packages/gitfaces/cli.py:18: in main
    gitfaces.fetch(args.repo, args.outdir)
.tox/py39/lib/python3.9/site-packages/gitfaces/main.py:16: in fetch
    repo = git.Repo(local_repo)
.tox/py39/lib/python3.9/site-packages/git/repo/base.py:220: in __init__
    self.working_dir = self._working_tree_dir or self.common_dir  # type: Optional[PathLike]
_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 

self = <git.repo.base.Repo ''>

    @property
    def common_dir(self) -> PathLike:
        """
        :return: The git dir that holds everything except possibly HEAD,
            FETCH_HEAD, ORIG_HEAD, COMMIT_EDITMSG, index, and logs/."""
        if self._common_dir:
            return self._common_dir
        elif self.git_dir:
            return self.git_dir
        else:
            # or could return ""
>           raise InvalidGitRepositoryError()
E           git.exc.InvalidGitRepositoryError

.tox/py39/lib/python3.9/site-packages/git/repo/base.py:303: InvalidGitRepositoryError

----------- coverage: platform linux, python 3.9.6-final-0 -----------
Name                                                          Stmts   Miss  Cover
---------------------------------------------------------------------------------
.tox/py39/lib/python3.9/site-packages/gitfaces/__about__.py       8      4    50%
.tox/py39/lib/python3.9/site-packages/gitfaces/__init__.py        4      0   100%
.tox/py39/lib/python3.9/site-packages/gitfaces/cli.py            12      1    92%
.tox/py39/lib/python3.9/site-packages/gitfaces/main.py           98     82    16%
---------------------------------------------------------------------------------
TOTAL                                                           122     87    29%
Coverage XML written to file coverage.xml

============================================================================= short test summary info ==============================================================================
FAILED test/test_gitfaces.py::test_gitfaces - git.exc.InvalidGitRepositoryError
FAILED test/test_gitfaces.py::test_cli - git.exc.InvalidGitRepositoryError
================================================================================ 2 failed in 0.43s =================================================================================
ERROR: InvocationError for command /tmp/makepkg/gitfaces/src/gitfaces-0.3.0/.tox/py39/bin/pytest --cov .tox/py39/lib/python3.9/site-packages/gitfaces --cov-report xml --cov-report term (exited with code 1)
_____________________________________________________________________________________ summary ______________________________________________________________________________________
ERROR:   py39: commands failed
==> ERROR: A failure occurred in check().
```
