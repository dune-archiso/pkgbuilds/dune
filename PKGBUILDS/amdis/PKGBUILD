# Maintainer: anon at sansorgan.es
# pkgbase=amdis
pkgname=amdis # amdis-docs
_tarver=2.9
_tar="${_tarver}/${pkgname}-releases-${_tarver}.tar.gz"
pkgver="${_tarver}"
pkgrel=1
#epoch=
pkgdesc="Adaptive Multi-Dimensional Simulation Toolbox"
arch=('x86_64')
url="https://gitlab.com/${pkgname}/${pkgname}"
license=('MIT')
#groups=('')
depends=("dune-functions>=${pkgver}" "dune-typetree>=${pkgver}" "dune-alugrid>=${pkgver}" 'fmt')
makedepends=('doxygen' 'graphviz')
checkdepends=("dune-spgrid>=${pkgver}" "dune-foamgrid>=${pkgver}")
#optdepends=("dune-vtk>=${pkgver}" 'dune-gmsh4')
#provides=()
#conflicts=()
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=(${url}/-/archive/releases/${_tar}
  fmt-compatibility.patch::${url}/-/commit/6444e2a38b3ee17467cdee946031a15d83a13cf9.patch)
# source=("git+${url}.git?#branch=releases/${pkgver}")
# source=(${url}/-/archive/${_tar})
#noextract=()
sha512sums=('c06bb7bdccb5ff2db1a775e2dfff964f044d3ca520536872b99c6047004255f0227143394ac8e1c10d6d741cb47783ee4ad5966465d64bfb96d087ed6c42f21b'
  '099abae355ed3036b34b769c9da3e7d066b02dd8e239142e784f70918c686d6aee769352cad0fa371629515e46b6989ee1c3752db27c9957b1437365d9f1d6c6')
#validpgpkeys=()

prepare() {
  # https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/blob/master/bin/duneci-test#L93
  export OMPI_MCA_rmaps_base_oversubscribe=1
  export OMPI_MCA_mpi_yield_when_idle=1
  export OMPI_MCA_btl_base_warn_component_unused=0
}

prepare() {
  sed -i 's/^add_subdirectory("libs")/#add_subdirectory("libs")/' ${pkgname}-releases-${_tarver}/CMakeLists.txt
  cat ${pkgname}-releases-${_tarver}/CMakeLists.txt
  cd ${pkgname}-releases-${_tarver}
  patch -p1 -i ../fmt-compatibility.patch
}

build() {
  # git submodule init && git submodule update
  # cd "${srcdir}"
  cmake \
    -S ${pkgname}-releases-${_tarver} \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DCMAKE_DISABLE_FIND_PACKAGE_PTScotch=TRUE \
    -DBACKEND=ISTL \
    -Wno-dev
  cmake --build build-cmake --target all # help
}

check() {
  if [ -z "$(ldconfig -p | grep libcuda.so.1)" ]; then
    export OMPI_MCA_opal_warn_on_missing_libcuda=0
  fi
  cmake --build build-cmake --target build_tests
  ctest -E "(DirichletBCTest|GlobalIdSetTest-mpi-*|ISTLCommTest-mpi-*|MpiWrapperTest-mpi-*|ParallelIndexSetTest_*|UniqueBorderPartitionTest-mpi-*)" --verbose --output-on-failure --test-dir build-cmake
  cmake --build build-cmake --target examples
}

package() {
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  install -Dm644 ${pkgname}-releases-${_tarver}/LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
}
