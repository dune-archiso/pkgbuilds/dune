```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-vectorclass
doxygen_install
headercheck
install_python
test_python
multirhsperftest-AMG-loop-2
multirhsperftest-AMG-loop-4
multirhsperftest-AMG-loop-8
multirhsperftest-AMG-scalar
multirhsperftest-AMG-vectorclass-2
multirhsperftest-AMG-vectorclass-4
multirhsperftest-AMG-vectorclass-8
multirhsperftest-jacobi-loop-2
multirhsperftest-jacobi-loop-4
multirhsperftest-jacobi-loop-8
multirhsperftest-jacobi-scalar
multirhsperftest-jacobi-vectorclass-2
multirhsperftest-jacobi-vectorclass-4
multirhsperftest-jacobi-vectorclass-8
multirhstest-avx
multirhstest-avx2
multirhstest-avx512
multirhstest-sse2
vctest
vectorclasstest-avx
vectorclasstest-avx2
vectorclasstest-avx512
vectorclasstest-sse2
```

```console
==> Starting check()...
/usr/bin/cmake -S/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6 -B/tmp/makepkg/dune-vectorclass/src/build-cmake --check-build-system CMakeFiles/Makefile.cmake 0
/usr/sbin/make  -f CMakeFiles/Makefile2 build_tests
make[1]: Entering directory '/tmp/makepkg/dune-vectorclass/src/build-cmake'
/usr/bin/cmake -S/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6 -B/tmp/makepkg/dune-vectorclass/src/build-cmake --check-build-system CMakeFiles/Makefile.cmake 0
/usr/bin/cmake -E cmake_progress_start /tmp/makepkg/dune-vectorclass/src/build-cmake/CMakeFiles 100
/usr/sbin/make  -f CMakeFiles/Makefile2 CMakeFiles/build_tests.dir/all
make[2]: Entering directory '/tmp/makepkg/dune-vectorclass/src/build-cmake'
/usr/sbin/make  -f dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/build.make dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/depend
make[3]: Entering directory '/tmp/makepkg/dune-vectorclass/src/build-cmake'
cd /tmp/makepkg/dune-vectorclass/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6 /tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/test /tmp/makepkg/dune-vectorclass/src/build-cmake /tmp/makepkg/dune-vectorclass/src/build-cmake/dune/vectorclass/test /tmp/makepkg/dune-vectorclass/src/build-cmake/dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/DependInfo.cmake --color=
make[3]: Leaving directory '/tmp/makepkg/dune-vectorclass/src/build-cmake'
/usr/sbin/make  -f dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/build.make dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/build
make[3]: Entering directory '/tmp/makepkg/dune-vectorclass/src/build-cmake'
[  1%] Building CXX object dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/multirhsperftest-jacobi-vectorclass.cc.o
cd /tmp/makepkg/dune-vectorclass/src/build-cmake/dune/vectorclass/test && /tmp/makepkg/dune-vectorclass/src/build-cmake/CXX_compiler.sh -DENABLE_ARPACKPP=1 -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_PARMETIS=1 -DENABLE_SUITESPARSE=1 -DENABLE_SUPERLU=1 -DENABLE_TBB=1 -DHAVE_CONFIG_H -DMAX_VECTOR_SIZE=512 -DMPICH_SKIP_MPICXX -DMPIPP_H -DMPI_NO_CPPBIND -DRUNS=10 -DTYPE=Vec4d -D_TBB_CPP0X -I/tmp/makepkg/dune-vectorclass/src/build-cmake -I/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6 -I/usr/include/superlu -std=c++17  -fPIE -isystem/usr/include/arpack++ -Wabi -fabi-version=0 -fabi-compat-version=0 -ffp-contract=fast -DENABLE_VC=1 -mavx2 -mfma -std=gnu++17 -MD -MT dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/multirhsperftest-jacobi-vectorclass.cc.o -MF CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/multirhsperftest-jacobi-vectorclass.cc.o.d -o CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/multirhsperftest-jacobi-vectorclass.cc.o -c /tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/test/multirhsperftest-jacobi-vectorclass.cc
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -DENABLE_ARPACKPP=1 -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_PARMETIS=1 -DENABLE_SUITESPARSE=1 -DENABLE_SUPERLU=1 -DENABLE_TBB=1 -DHAVE_CONFIG_H -DMAX_VECTOR_SIZE=512 -DMPICH_SKIP_MPICXX -DMPIPP_H -DMPI_NO_CPPBIND -DRUNS=10 -DTYPE=Vec4d -D_TBB_CPP0X -I/tmp/makepkg/dune-vectorclass/src/build-cmake -I/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6 -I/usr/include/superlu -std=c++17 -fPIE -isystem/usr/include/arpack++ -Wabi -fabi-version=0 -fabi-compat-version=0 -ffp-contract=fast -DENABLE_VC=1 -mavx2 -mfma -std=gnu++17 -MD -MT dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/multirhsperftest-jacobi-vectorclass.cc.o -MF CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/multirhsperftest-jacobi-vectorclass.cc.o.d -o CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/multirhsperftest-jacobi-vectorclass.cc.o -c /tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/test/multirhsperftest-jacobi-vectorclass.cc
In file included from /tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/test/multirhsperftest-jacobi-vectorclass.cc:3:
/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/vectorclass.hh: In substitution of ‘template<class W, class> Dune::Simd::VectorclassImpl::Proxy<Vec4i>::operator W<W, <template-parameter-1-2> >() const [with W = Vec4i; <template-parameter-1-2> = <missing>]’:
/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/vectorclass.hh:157:9:   recursively required by substitution of ‘template<class T, class, class> decltype(auto) Dune::Simd::VectorclassImpl::operator%=(T&, Dune::Simd::VectorclassImpl::Proxy<Vec4i>&&) [with T = Dune::Simd::VectorclassImpl::Proxy<Vec4i>; <template-parameter-1-2> = <missing>; <template-parameter-1-3> = <missing>]’
/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/vectorclass.hh:157:9:   required by substitution of ‘template<class T, class, class> decltype(auto) Dune::Simd::VectorclassImpl::operator%=(T&, Dune::Simd::VectorclassImpl::Proxy<Vec4i>&&) [with T = Dune::Simd::VectorclassImpl::Proxy<Vec4i>; <template-parameter-1-2> = <missing>; <template-parameter-1-3> = <missing>]’
/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/vectorclass.hh:565:3:   required from here
/tmp/makepkg/dune-vectorclass/src/dune-vectorclass-releases-2.6/dune/vectorclass/vectorclass.hh:126:27: fatal error: template instantiation depth exceeds maximum of 900 (use ‘-ftemplate-depth=’ to increase the maximum)
  126 |         template<class W, class = std::enable_if_t<IsVector<W>::value> >
      |                           ^~~~~
compilation terminated.
make[3]: *** [dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/build.make:79: dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/multirhsperftest-jacobi-vectorclass.cc.o] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-vectorclass/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:1798: dune/vectorclass/test/CMakeFiles/multirhsperftest-jacobi-vectorclass-4.dir/all] Error 2
make[2]: Leaving directory '/tmp/makepkg/dune-vectorclass/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:1207: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-vectorclass/src/build-cmake'
make: *** [Makefile:670: build_tests] Error 2
==> ERROR: A failure occurred in check().
```
