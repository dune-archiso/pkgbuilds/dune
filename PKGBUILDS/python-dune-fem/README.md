

- [x] community/python-mpi4py
- [] aur/python-mpi4py-intel
- [] aur/python-ufl
- [] aur/python-ufl-git
- [] aur/python-scipy-git
- [] aur/python-scipy-mkl-bin
- [] aur/python-scipy-doc
- [] {aur,arch4edu}/python-scipy-mkl
- [x] community/python-scipy
- [x] python-matplotlib
- [] aur/petsc4py