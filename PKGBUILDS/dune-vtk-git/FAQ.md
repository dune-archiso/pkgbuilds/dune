```console
-- The CXX compiler identification is GNU 11.1.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/g++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- The C compiler identification is GNU 11.1.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/gcc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Dependencies for dune-vtk: dune-grid (>= 2.7);dune-localfunctions (>= 2.7)
-- Suggestions for dune-vtk: dune-functions;dune-spgrid;dune-polygongrid;dune-alugrid;dune-foamgrid;dune-uggrid
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-functions: dune-localfunctions (>= 2.8);dune-grid (>= 2.8);dune-istl (>= 2.8);dune-typetree (>= 2.8)
-- Dependencies for dune-functions: dune-localfunctions (>= 2.8);dune-grid (>= 2.8);dune-istl (>= 2.8);dune-typetree (>= 2.8)
-- Dependencies for dune-spgrid: dune-grid (>= 2.8)
-- Dependencies for dune-spgrid: dune-grid (>= 2.8)
-- Dependencies for dune-polygongrid: dune-grid (>= 2.8)
-- Dependencies for dune-polygongrid: dune-grid (>= 2.8)
-- Dependencies for dune-alugrid: dune-grid (>= 2.8)
-- Dependencies for dune-alugrid: dune-grid (>= 2.8)
-- Dependencies for dune-foamgrid: dune-common (>= 2.6);dune-geometry (>= 2.6);dune-grid (>= 2.6)
-- Suggestions for dune-foamgrid: dune-python
-- Dependencies for dune-foamgrid: dune-common (>= 2.6);dune-geometry (>= 2.6);dune-grid (>= 2.6)
-- Suggestions for dune-foamgrid: dune-python
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Could NOT find dune-python (missing: dune-python_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Found PkgConfig: /usr/sbin/pkg-config (found version "1.7.3") 
-- Checking for module 'dune-python '
--   Package 'dune-python', required by 'virtual:world', not found
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-istl: dune-common (>= 2.8)
-- Dependencies for dune-istl: dune-common (>= 2.8)
-- Dependencies for dune-typetree: dune-common (>= 2.8)
-- Dependencies for dune-typetree: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Performing Test cxx_std_flag_17
-- Performing Test cxx_std_flag_17 - Success
-- Performing Test compiler_supports_cxx17
-- Performing Test compiler_supports_cxx17 - Success
-- Performing Test HAS_ATTRIBUTE_UNUSED
-- Performing Test HAS_ATTRIBUTE_UNUSED - Success
-- Performing Test HAS_ATTRIBUTE_DEPRECATED
-- Performing Test HAS_ATTRIBUTE_DEPRECATED - Success
-- Performing Test HAS_ATTRIBUTE_DEPRECATED_MSG
-- Performing Test HAS_ATTRIBUTE_DEPRECATED_MSG - Success
-- Looking for std::experimental::make_array<int,int>
-- Looking for std::experimental::make_array<int,int> - found
-- Looking for std::move<std::experimental::detected_t<std::decay_t,int>>
-- Looking for std::move<std::experimental::detected_t<std::decay_t,int>> - found
-- Looking for std::identity
-- Looking for std::identity - not found
-- Found LATEX: /usr/sbin/latex   
-- Found LatexMk: /usr/sbin/latexmk (found version "Version 4.70b") 
-- Could NOT find Sphinx (missing: SPHINX_EXECUTABLE) 
-- Could NOT find Doxygen (missing: DOXYGEN_EXECUTABLE) 
-- Searching for macro file 'DuneCommonMacros' for module 'dune-common'.
-- Performing tests specific to dune-common from file /usr/share/dune/cmake/modules/DuneCommonMacros.cmake.
-- Set Minimal Debug Level to 4
-- Looking for sgemm_
-- Looking for sgemm_ - not found
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
-- Check if compiler accepts -pthread
-- Check if compiler accepts -pthread - yes
-- Found Threads: TRUE  
-- Looking for sgemm_
-- Looking for sgemm_ - not found
-- Looking for sgemm_
-- Looking for sgemm_ - found
-- Found BLAS: /usr/lib/libblas.so  
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - found
-- Found LAPACK: /usr/lib/liblapack.so;/usr/lib/libblas.so  
-- Looking for dsyev_
-- Looking for dsyev_ - found
-- Found GMP: /usr/lib/libgmpxx.so  
-- Performing Test QuadMath_COMPILES
-- Performing Test QuadMath_COMPILES - Success
-- Found QuadMath: (Supported by compiler)  
-- Found MPI_C: /usr/lib/openmpi/libmpi.so (found suitable version "3.1", minimum required is "3.0") 
-- Found MPI: TRUE (found suitable version "3.1", minimum required is "3.0") found components: C 
-- Found TBB: using configuration from TBB_DIR=/usr/lib64/cmake/TBB (found version "2020.3")
-- Found PTScotch: /usr/lib/libscotch.so (found version "6.1.0")  
-- Found METIS: /usr/lib/libmetis.so (found version "5.1") 
-- Found METIS: /usr/lib/libmetis.so (found suitable version "5.1", minimum required is "5.0") 
-- Found MPI_C: /usr/lib/openmpi/libmpi.so (found version "3.1") 
-- Found MPI: TRUE (found version "3.1") found components: C 
-- Found ParMETIS: /usr/lib/libparmetis.so (found suitable version "4.0", minimum required is "4.0") 
-- Detected Compiler: GCC 11.1.0
-- Performing Test Vc_HAVE_SSE_SINCOS
-- Performing Test Vc_HAVE_SSE_SINCOS - Success
-- Performing Test Vc_HAVE_AVX_SINCOS
-- Performing Test Vc_HAVE_AVX_SINCOS - Success
-- Performing Test check_cxx_compiler_flag__Wabi
-- Performing Test check_cxx_compiler_flag__Wabi - Success
-- Performing Test check_cxx_compiler_flag__fabi_version_0
-- Performing Test check_cxx_compiler_flag__fabi_version_0 - Success
-- Performing Test check_cxx_compiler_flag__fabi_compat_version_0
-- Performing Test check_cxx_compiler_flag__fabi_compat_version_0 - Success
-- Performing Test check_cxx_compiler_flag__ffp_contract_fast
-- Performing Test check_cxx_compiler_flag__ffp_contract_fast - Success
-- target changed from "" to "auto"
-- Detected CPU: haswell
-- Performing Test check_cxx_compiler_flag__march_haswell
-- Performing Test check_cxx_compiler_flag__march_haswell - Success
-- Performing Test check_cxx_compiler_flag__msse2
-- Performing Test check_cxx_compiler_flag__msse2 - Success
-- Performing Test check_cxx_compiler_flag__msse3
-- Performing Test check_cxx_compiler_flag__msse3 - Success
-- Looking for C++ include pmmintrin.h
-- Looking for C++ include pmmintrin.h - found
-- Performing Test check_cxx_compiler_flag__mssse3
-- Performing Test check_cxx_compiler_flag__mssse3 - Success
-- Looking for C++ include tmmintrin.h
-- Looking for C++ include tmmintrin.h - found
-- Performing Test check_cxx_compiler_flag__msse4_1
-- Performing Test check_cxx_compiler_flag__msse4_1 - Success
-- Looking for C++ include smmintrin.h
-- Looking for C++ include smmintrin.h - found
-- Performing Test check_cxx_compiler_flag__msse4_2
-- Performing Test check_cxx_compiler_flag__msse4_2 - Success
-- Performing Test check_cxx_compiler_flag__mavx
-- Performing Test check_cxx_compiler_flag__mavx - Success
-- Looking for C++ include immintrin.h
-- Looking for C++ include immintrin.h - found
-- Performing Test check_cxx_compiler_flag__mfma
-- Performing Test check_cxx_compiler_flag__mfma - Success
-- Performing Test check_cxx_compiler_flag__mbmi2
-- Performing Test check_cxx_compiler_flag__mbmi2 - Success
-- Performing Test check_cxx_compiler_flag__mavx2
-- Performing Test check_cxx_compiler_flag__mavx2 - Success
-- Performing Test check_cxx_compiler_flag__mno_sse4a
-- Performing Test check_cxx_compiler_flag__mno_sse4a - Success
-- Performing Test check_cxx_compiler_flag__mno_xop
-- Performing Test check_cxx_compiler_flag__mno_xop - Success
-- Performing Test check_cxx_compiler_flag__mno_fma4
-- Performing Test check_cxx_compiler_flag__mno_fma4 - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512f
-- Performing Test check_cxx_compiler_flag__mno_avx512f - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512vl
-- Performing Test check_cxx_compiler_flag__mno_avx512vl - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512pf
-- Performing Test check_cxx_compiler_flag__mno_avx512pf - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512er
-- Performing Test check_cxx_compiler_flag__mno_avx512er - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512cd
-- Performing Test check_cxx_compiler_flag__mno_avx512cd - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512dq
-- Performing Test check_cxx_compiler_flag__mno_avx512dq - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512bw
-- Performing Test check_cxx_compiler_flag__mno_avx512bw - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512ifma
-- Performing Test check_cxx_compiler_flag__mno_avx512ifma - Success
-- Performing Test check_cxx_compiler_flag__mno_avx512vbmi
-- Performing Test check_cxx_compiler_flag__mno_avx512vbmi - Success
-- Found Python3: /usr/sbin/python3 (found version "3.9.6") found components: Interpreter Development Development.Module Development.Embed 
-- Found pip_/usr/sbin/python3: TRUE  
-- Setting dune-common_INCLUDE_DIRS=/usr/include
-- Setting dune-common_LIBRARIES=dunecommon
-- Searching for macro file 'DuneUggridMacros' for module 'dune-uggrid'.
-- Performing tests specific to dune-uggrid from file /usr/share/dune/cmake/modules/DuneUggridMacros.cmake.
-- Setting dune-uggrid_INCLUDE_DIRS=/usr/include
-- Setting dune-uggrid_LIBRARIES=duneuggrid
-- Searching for macro file 'DuneGeometryMacros' for module 'dune-geometry'.
-- No module specific tests performed for module 'dune-geometry' because macro file 'DuneGeometryMacros.cmake' not found in /tmp/makepkg/dune-vtk-git/src/dune-vtk/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/lib/cmake/Vc.
-- Setting dune-geometry_INCLUDE_DIRS=/usr/include
-- Setting dune-geometry_LIBRARIES=dunegeometry
-- Searching for macro file 'DuneTypetreeMacros' for module 'dune-typetree'.
-- No module specific tests performed for module 'dune-typetree' because macro file 'DuneTypetreeMacros.cmake' not found in /tmp/makepkg/dune-vtk-git/src/dune-vtk/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/lib/cmake/Vc.
-- Setting dune-typetree_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneIstlMacros' for module 'dune-istl'.
-- Performing tests specific to dune-istl from file /usr/share/dune/cmake/modules/DuneIstlMacros.cmake.
-- Found METIS: /usr/lib/libmetis.so (found version "5.1") 
-- Found METIS: /usr/lib/libmetis.so (found suitable version "5.1", minimum required is "5.0") 
-- Found ParMETIS: /usr/lib/libparmetis.so (found version "4.0") 
-- Found SuperLU: /usr/lib/libsuperlu.so (found suitable version "5.2.2", minimum required is "5.0") 
-- Found ARPACK: /usr/lib/libarpack.so  
-- Found ARPACKPP: TRUE  
-- Found SuiteSparse: /usr/lib/libsuitesparseconfig.so (found version "5.10.1") found components: CHOLMOD LDL SPQR UMFPACK 
-- Setting dune-istl_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneLocalfunctionsMacros' for module 'dune-localfunctions'.
-- No module specific tests performed for module 'dune-localfunctions' because macro file 'DuneLocalfunctionsMacros.cmake' not found in /tmp/makepkg/dune-vtk-git/src/dune-vtk/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/lib/cmake/Vc.
-- Setting dune-localfunctions_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneGridMacros' for module 'dune-grid'.
-- Performing tests specific to dune-grid from file /usr/share/dune/cmake/modules/DuneGridMacros.cmake.
-- Looking for mkstemp
-- Looking for mkstemp - found
-- Found METIS: /usr/lib/libmetis.so (found version "5.1") 
-- Found METIS: /usr/lib/libmetis.so (found suitable version "5.1", minimum required is "5.0") 
-- Checking for module 'alberta-grid_1d>=3.0'
--   Found alberta-grid_1d, version 3.0.3
-- Checking for module 'alberta-grid_2d>=3.0'
--   Found alberta-grid_2d, version 3.0.3
-- Checking for module 'alberta-grid_3d>=3.0'
--   Found alberta-grid_3d, version 3.0.3
-- Checking for module 'alberta-grid_4d>=3.0'
--   Package 'alberta-grid_4d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_5d>=3.0'
--   Package 'alberta-grid_5d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_6d>=3.0'
--   Package 'alberta-grid_6d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_7d>=3.0'
--   Package 'alberta-grid_7d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_8d>=3.0'
--   Package 'alberta-grid_8d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_9d>=3.0'
--   Package 'alberta-grid_9d', required by 'virtual:world', not found
-- Found Alberta: /usr (found suitable version "3.0.3", minimum required is "3.0") 
-- Found Psurface: /usr/include/psurface  
-- Could NOT find AmiraMesh (missing: AMIRAMESH_INCLUDE_DIR AMIRAMESH_LIBRARY) 
-- Setting dune-grid_INCLUDE_DIRS=/usr/include
-- Setting dune-grid_LIBRARIES=dunegrid;dunealbertagrid1d;dunealbertagrid2d;dunealbertagrid3d
-- Searching for macro file 'DunePythonMacros' for module 'dune-python'.
-- Performing tests specific to dune-python from file /usr/share/dune/cmake/modules/DunePythonMacros.cmake.
-- Searching for macro file 'DuneFoamgridMacros' for module 'dune-foamgrid'.
-- Performing tests specific to dune-foamgrid from file /usr/share/dune/cmake/modules/DuneFoamgridMacros.cmake.
-- Setting dune-foamgrid_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneAlugridMacros' for module 'dune-alugrid'.
-- Performing tests specific to dune-alugrid from file /usr/share/dune/cmake/modules/DuneAlugridMacros.cmake.
-- Found ZLIB: /usr/lib/libz.so (found version "1.2.11") 
CMake Warning at /usr/share/dune/cmake/modules/DuneAlugridMacros.cmake:48 (find_package):
  By not providing "FindSIONlib.cmake" in CMAKE_MODULE_PATH this project has
  asked CMake to find a package configuration file provided by "SIONlib", but
  CMake did not find one.
  Could not find a package configuration file provided by "SIONlib" with any
  of the following names:
    SIONlibConfig.cmake
    sionlib-config.cmake
  Add the installation prefix of "SIONlib" to CMAKE_PREFIX_PATH or set
  "SIONlib_DIR" to a directory containing one of the above files.  If
  "SIONlib" provides a separate development package or SDK, be sure it has
  been installed.
Call Stack (most recent call first):
  /usr/share/dune/cmake/modules/DuneMacros.cmake:614 (include)
  /usr/share/dune/cmake/modules/DuneMacros.cmake:704 (dune_process_dependency_macros)
  CMakeLists.txt:21 (dune_project)
-- Could NOT find DLMalloc (missing: DLMALLOC_INCLUDE_DIR DLMALLOC_SOURCE_INCLUDE) 
Found /usr/include
Found /usr/lib/libzoltan.so
-- Found METIS: /usr/lib/libmetis.so (found version "5.1") 
CMake Error at /usr/share/dune/cmake/modules/DuneAlugridMacros.cmake:57 (include):
  include could not find requested file:
    FindPThreads
Call Stack (most recent call first):
  /usr/share/dune/cmake/modules/DuneMacros.cmake:614 (include)
  /usr/share/dune/cmake/modules/DuneMacros.cmake:704 (dune_process_dependency_macros)
  CMakeLists.txt:21 (dune_project)
-- Not enabling torture-tests
-- Setting dune-alugrid_INCLUDE_DIRS=/usr/include
-- Setting dune-alugrid_LIBRARIES=dunealugrid
-- Searching for macro file 'DunePolygongridMacros' for module 'dune-polygongrid'.
-- Performing tests specific to dune-polygongrid from file /usr/share/dune/cmake/modules/DunePolygongridMacros.cmake.
-- Setting dune-polygongrid_INCLUDE_DIRS=/usr/include
-- Setting dune-polygongrid_LIBRARIES=dunepolygongrid
-- Searching for macro file 'DuneSpgridMacros' for module 'dune-spgrid'.
-- Performing tests specific to dune-spgrid from file /usr/share/dune/cmake/modules/DuneSpgridMacros.cmake.
-- Setting dune-spgrid_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneFunctionsMacros' for module 'dune-functions'.
-- No module specific tests performed for module 'dune-functions' because macro file 'DuneFunctionsMacros.cmake' not found in /tmp/makepkg/dune-vtk-git/src/dune-vtk/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/lib/cmake/Vc.
-- Setting dune-functions_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneVtkMacros' for module 'dune-vtk'.
-- Performing tests specific to dune-vtk from file /tmp/makepkg/dune-vtk-git/src/dune-vtk/cmake/modules/DuneVtkMacros.cmake.
-- Using scripts from /usr/share/dune/cmake/scripts for creating doxygen stuff.
-- Adding custom target for config.h generation
-- The following OPTIONAL packages have been found:
 * dune-functions
 * dune-spgrid
 * dune-polygongrid
 * dune-alugrid
 * dune-foamgrid
 * dune-uggrid
 * dune-istl
 * dune-typetree
 * LATEX
 * LatexMk
 * GMP, GNU multi-precision library, <https://gmplib.org>
 * QuadMath, GCC Quad-Precision Math Library, <https://gcc.gnu.org/onlinedocs/libquadmath>
 * TBB, Intel's Threading Building Blocks
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * Python3
 * SuperLU (required version >= 5.0), Supernodal LU
   Direct solver for linear system, based on LU decomposition
 * ARPACK, ARnoldi PACKage
   Solve large scale eigenvalue problems
 * ARPACKPP, ARPACK++
   C++ interface for ARPACK
 * Threads, Multi-threading library
 * SuiteSparse, A suite of sparse matrix software, <http://faculty.cse.tamu.edu/davis/suitesparse.html>
 * ParMETIS, Parallel Graph Partitioning
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * Alberta (required version >= 3.0), An adaptive hierarchical finite element toolbox and grid manager
 * Psurface, Piecewise linear bijections between triangulated surfaces
 * PTScotch, Sequential and Parallel Graph Partitioning
 * ZOLTAN
 * METIS, Serial Graph Partitioning
 * ZLIB
-- The following REQUIRED packages have been found:
 * dune-common
 * dune-grid
 * dune-localfunctions
 * dune-geometry
-- The following OPTIONAL packages have not been found:
 * dune-python
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * AmiraMesh
 * SIONlib
 * DLMalloc
-- Configuring incomplete, errors occurred!
See also "/tmp/makepkg/dune-vtk-git/src/dune-vtk/build-cmake/CMakeFiles/CMakeOutput.log".
See also "/tmp/makepkg/dune-vtk-git/src/dune-vtk/build-cmake/CMakeFiles/CMakeError.log".
```
