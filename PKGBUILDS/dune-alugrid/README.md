## [`dune-alugrid`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-alugrid/PKGBUILD)

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-alugrid.svg)](https://repology.org/project/dune-alugrid/versions)

```console
-- The following OPTIONAL packages have been found:
 * dune-python
 * dune-istl
 * dune-uggrid
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
 * Alberta
 * Psurface
 * ParMETIS
 * SuperLU, Supernodal LU
   Direct solver for linear system, based on LU decomposition
 * ARPACK, ARnoldi PACKage
   Solve large scale eigenvalue problems
 * ARPACKPP, ARPACK++
   C++ interface for ARPACK
 * SuiteSparse
 * ZLIB
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
   Parallel programming on multiple processors
 * Threads, Multi-threading library
 * PTScotch
 * ZOLTAN
 * METIS
-- The following REQUIRED packages have been found:
 * dune-common
 * dune-grid
 * dune-geometry
-- The following OPTIONAL packages have not been found:
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * AmiraMesh
 * SIONlib
 * DLMalloc
```

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-alugrid.svg)](https://repology.org/project/dune-alugrid/versions)
