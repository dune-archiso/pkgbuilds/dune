When the package `dune-common-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `bin`
- [x] `include`
- [x] `lib`
- [x] `share`
  - [x] `bash-completion`
  - [x] `doc`
  - [x] `dune`
  - [x] `dune-common`
  - [x] `licenses`
  - [x] `man`

```console
usr/
├── bin
│   ├── dunecontrol
│   ├── dune-ctest
│   ├── dune-git-whitespace-hook
│   └── duneproject
├── include
│   └── dune
│       └── common
├── lib
│   ├── cmake
│   │   └── dune-common
│   ├── dunecontrol
│   │   └── dune-common
│   │       └── dune.module
│   ├── dunemodules.lib
│   ├── libdunecommon.a
│   └── pkgconfig
│       └── dune-common.pc
└── share
    ├── bash-completion
    │   └── completions
    │       └── dunecontrol
    ├── doc
    │   └── dune-common
    ├── dune
    │   └── cmake
    │       ├── modules
    │       └── scripts
    ├── dune-common
    │   ├── config.h.cmake
    │   └── doc
    │       └── doxygen
    ├── licenses
    │   └── dune-common
    └── man
        └── man1

31 directories, 1492 files

`superlu` is necessary?

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-alugrid
doxygen_install
headercheck
install_python
test_python
convert
dunealugrid
estclo
gmsh2dgf
main_ball
main_ball_cb
main_ball_comm
main_ball_conform_2d
main_ball_conform_3d
main_ball_cube_2d
main_ball_cube_3d
main_ball_eff
main_ball_simplex_2d
main_ball_simplex_3d
main_br
main_euler
main_euler_cb
main_euler_comm
main_euler_conform_2d
main_euler_conform_3d
main_euler_cube_2d
main_euler_cube_3d
main_euler_eff
main_euler_simplex_2d
main_euler_simplex_3d
main_internal
main_quality
main_simple
main_transport
main_transport_cb
main_transport_comm
main_transport_conform_2d
main_transport_conform_3d
main_transport_cube_2d
main_transport_cube_3d
main_transport_eff
main_transport_simplex_2d
main_transport_simplex_3d
main_weights
main_zoltan
test-alugrid
test-backup-restore
test-dgf
test-structuredgridfactory
```

```console
   Site: runner-72989761-project-25568825-concurrent-0
   Build name: Linux-g++
Create new tag: 20210720-2226 - Experimental
Test project /tmp/makepkg/dune-alugrid/src/build-cmake
      Start  1: test-alugrid
 1/16 Test  #1: test-alugrid .......................   Passed   50.89 sec
      Start  2: test-alugrid-mpi-2
 2/16 Test  #2: test-alugrid-mpi-2 .................***Failed    0.14 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-alugrid
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start  3: test-alugrid-mpi-3
 3/16 Test  #3: test-alugrid-mpi-3 .................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 3
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-alugrid
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start  4: test-alugrid-mpi-4
 4/16 Test  #4: test-alugrid-mpi-4 .................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 4
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-alugrid
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start  5: test-structuredgridfactory
 5/16 Test  #5: test-structuredgridfactory .........   Passed    0.15 sec
      Start  6: test-structuredgridfactory-mpi-2
 6/16 Test  #6: test-structuredgridfactory-mpi-2 ...***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-structuredgridfactory
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start  7: test-structuredgridfactory-mpi-3
 7/16 Test  #7: test-structuredgridfactory-mpi-3 ...***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 3
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-structuredgridfactory
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start  8: test-structuredgridfactory-mpi-4
 8/16 Test  #8: test-structuredgridfactory-mpi-4 ...***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 4
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-structuredgridfactory
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start  9: test-backup-restore
 9/16 Test  #9: test-backup-restore ................   Passed    1.89 sec
      Start 10: test-backup-restore-mpi-2
10/16 Test #10: test-backup-restore-mpi-2 ..........***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-backup-restore
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start 11: test-backup-restore-mpi-3
11/16 Test #11: test-backup-restore-mpi-3 ..........***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 3
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-backup-restore
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start 12: test-backup-restore-mpi-4
12/16 Test #12: test-backup-restore-mpi-4 ..........***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 4
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-backup-restore
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start 13: test-dgf
13/16 Test #13: test-dgf ...........................   Passed    0.14 sec
      Start 14: test-dgf-mpi-2
14/16 Test #14: test-dgf-mpi-2 .....................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 2
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-dgf
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start 15: test-dgf-mpi-3
15/16 Test #15: test-dgf-mpi-3 .....................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 3
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-dgf
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
      Start 16: test-dgf-mpi-4
16/16 Test #16: test-dgf-mpi-4 .....................***Failed    0.03 sec
--------------------------------------------------------------------------
There are not enough slots available in the system to satisfy the 4
slots that were requested by the application:
  /tmp/makepkg/dune-alugrid/src/build-cmake/dune/alugrid/test/test-dgf
Either request fewer slots for your application, or make more slots
available for use.
A "slot" is the Open MPI term for an allocatable unit where we can
launch a process.  The number of slots available are defined by the
environment in which Open MPI processes are run:
  1. Hostfile, via "slots=N" clauses (N defaults to number of
     processor cores if not provided)
  2. The --host command line parameter, via a ":N" suffix on the
     hostname (N defaults to 1 if not provided)
  3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
  4. If none of a hostfile, the --host command line parameter, or an
     RM is present, Open MPI defaults to the number of processor cores
In all the above cases, if you want Open MPI to default to the number
of hardware threads instead of the number of processor cores, use the
--use-hwthread-cpus option.
Alternatively, you can use the --oversubscribe option to ignore the
number of available slots when deciding the number of processes to
launch.
--------------------------------------------------------------------------
25% tests passed, 12 tests failed out of 16
Total Test time (real) =  53.68 sec
The following tests FAILED:
	  2 - test-alugrid-mpi-2 (Failed)
	  3 - test-alugrid-mpi-3 (Failed)
	  4 - test-alugrid-mpi-4 (Failed)
	  6 - test-structuredgridfactory-mpi-2 (Failed)
	  7 - test-structuredgridfactory-mpi-3 (Failed)
	  8 - test-structuredgridfactory-mpi-4 (Failed)
	 10 - test-backup-restore-mpi-2 (Failed)
	 11 - test-backup-restore-mpi-3 (Failed)
	 12 - test-backup-restore-mpi-4 (Failed)
	 14 - test-dgf-mpi-2 (Failed)
	 15 - test-dgf-mpi-3 (Failed)
	 16 - test-dgf-mpi-4 (Failed)
Errors while running CTest
======================================================================
Name:      test-alugrid-mpi-2
FullName:  ./dune/alugrid/test/test-alugrid-mpi-2
Status:    FAILED
======================================================================
Name:      test-alugrid-mpi-3
FullName:  ./dune/alugrid/test/test-alugrid-mpi-3
Status:    FAILED
======================================================================
Name:      test-alugrid-mpi-4
FullName:  ./dune/alugrid/test/test-alugrid-mpi-4
Status:    FAILED
======================================================================
Name:      test-structuredgridfactory-mpi-2
FullName:  ./dune/alugrid/test/test-structuredgridfactory-mpi-2
Status:    FAILED
======================================================================
Name:      test-structuredgridfactory-mpi-3
FullName:  ./dune/alugrid/test/test-structuredgridfactory-mpi-3
Status:    FAILED
======================================================================
Name:      test-structuredgridfactory-mpi-4
FullName:  ./dune/alugrid/test/test-structuredgridfactory-mpi-4
Status:    FAILED
======================================================================
Name:      test-backup-restore-mpi-2
FullName:  ./dune/alugrid/test/test-backup-restore-mpi-2
Status:    FAILED
======================================================================
Name:      test-backup-restore-mpi-3
FullName:  ./dune/alugrid/test/test-backup-restore-mpi-3
Status:    FAILED
======================================================================
Name:      test-backup-restore-mpi-4
FullName:  ./dune/alugrid/test/test-backup-restore-mpi-4
Status:    FAILED
======================================================================
Name:      test-dgf-mpi-2
FullName:  ./dune/alugrid/test/test-dgf-mpi-2
Status:    FAILED
======================================================================
Name:      test-dgf-mpi-3
FullName:  ./dune/alugrid/test/test-dgf-mpi-3
Status:    FAILED
======================================================================
Name:      test-dgf-mpi-4
FullName:  ./dune/alugrid/test/test-dgf-mpi-4
Status:    FAILED
JUnit report for CTest results written to /builds/dune-archiso/repository/dune-extensions/junit/dune-extensions-cmake.xml
==> ERROR: A failure occurred in check().
```
