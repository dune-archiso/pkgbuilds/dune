[](https://resinsight.org)
[](https://github.com/OPM/ResInsight)
```
-- Build files have been written to: /tmp/makepkg/resinsight/src/build-cmake
The following are some of the valid targets for this Makefile:
... all (the default if no target is provided)
... clean
... depend
... edit_cache
... install
... install/local
... install/strip
... list_install_components
... package
... package_source
... rebuild_cache
... octave_plugins
... ApplicationLibCode
... Commands
... CommonCode
... LibCore
... LibGeometry
... LibGuiQt
... LibRender
... LibViewing
... NRLib
... ResInsight
... ResultStatisticsCache
... RigGeoMechDataModel
... cafAnimControl
... cafCommand
... cafCommandFeatures
... cafPdmCore
... cafPdmCvf
... cafPdmScripting
... cafPdmUiCore
... cafPdmXml
... cafProjectDataModel
... cafTensor
... cafUserInterface
... cafViewer
... cafVizExtensions
... catch2
... clipper
... custom-opm-common
... custom-opm-flowdiag-app
... custom-opm-flowdiagnostics
... ecl
... expressionparser
... nightcharts
... opm-parser-tests
... qwt
```

```
CMake Warning:
  Manually-specified variables were not used by the project:
    RESINSIGHT_GRPC_INSTALL_PREFIX
    RESINSIGHT_QT5_BUNDLE_LIBRARIES
-- Build files have been written to: /tmp/makepkg/resinsight/src/build-cmake
/tmp/makepkg/resinsight/src/build-cmake /tmp/makepkg/resinsight/src
CMake Warning:
  No source or binary directory provided.  Both will be assumed to be the
  same as the current working directory, but note that this warning will
  become a fatal error in future CMake releases.
CMake Error: The source directory "/tmp/makepkg/resinsight/src/build-cmake" does not appear to contain CMakeLists.txt.
Specify --help for usage, or press the help button on the CMake GUI.
-- Cache values
// Path to a file.
BOOST_SPIRIT_INCLUDE_DIRS:PATH=/usr/include
// Should we build small utility applications
BUILD_APPLICATIONS:BOOL=OFF
// Build the commandline application ecl_summary
BUILD_ECL_SUMMARY:BOOL=OFF
// Build the examples
BUILD_EXAMPLES:BOOL=OFF
// Builds the googlemock subproject
BUILD_GMOCK:BOOL=OFF
// 
BUILD_GTEST:BOOL=ON
// Run py_compile on the python wrappers
BUILD_PYTHON:BOOL=OFF
// Build shared libraries
BUILD_SHARED_LIBS:BOOL=1
// Build the static library
BUILD_STATIC:BOOL=ON
// Should the tests be built
BUILD_TESTS:BOOL=OFF
// The directory containing a CMake configuration file for Boost.
Boost_DIR:PATH=/usr/lib/cmake/Boost-1.81.0
// 
Boost_FILESYSTEM_LIBRARY_RELEASE:STRING=/usr/lib/libboost_filesystem.so.1.81.0
// Path to a file.
Boost_INCLUDE_DIR:PATH=/usr/include
// 
Boost_SYSTEM_LIBRARY_RELEASE:STRING=/usr/lib/libboost_system.so.1.81.0
// Enable CVF-data support in Scripting
CAF_CVF_SCRIPTING:BOOL=ON
// Path to a program.
CMAKE_ADDR2LINE:FILEPATH=/usr/sbin/addr2line
// Path to a program.
CMAKE_AR:FILEPATH=/usr/sbin/ar
// Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel ...
CMAKE_BUILD_TYPE:STRING=None
// Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON
// CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/usr/sbin/c++
// A wrapper around 'ar' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_CXX_COMPILER_AR:FILEPATH=/usr/sbin/gcc-ar
// A wrapper around 'ranlib' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_CXX_COMPILER_RANLIB:FILEPATH=/usr/sbin/gcc-ranlib
// Flags used by the CXX compiler during all build types.
CMAKE_CXX_FLAGS:STRING=     -std=c++17     -O2 -g     -Wall     -fdiagnostics-color=always
// Flags used by the CXX compiler during DEBUG builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g
// Flags used by the CXX compiler during MINSIZEREL builds.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG
// Flags used by the CXX compiler during NONE builds.
CMAKE_CXX_FLAGS_NONE:STRING=
// Flags used by the CXX compiler during RELEASE builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=-O3 -DNDEBUG
// Flags used by the CXX compiler during RELWITHDEBINFO builds.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG
// C compiler
CMAKE_C_COMPILER:FILEPATH=/usr/sbin/cc
// A wrapper around 'ar' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_C_COMPILER_AR:FILEPATH=/usr/sbin/gcc-ar
// A wrapper around 'ranlib' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_C_COMPILER_RANLIB:FILEPATH=/usr/sbin/gcc-ranlib
// Flags used by the C compiler during all build types.
CMAKE_C_FLAGS:STRING=-march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection
// Flags used by the C compiler during DEBUG builds.
CMAKE_C_FLAGS_DEBUG:STRING=-g
// Flags used by the C compiler during MINSIZEREL builds.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG
// Flags used by the C compiler during NONE builds.
CMAKE_C_FLAGS_NONE:STRING=
// Flags used by the C compiler during RELEASE builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG
// Flags used by the C compiler during RELWITHDEBINFO builds.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG
// Path to a program.
CMAKE_DLLTOOL:FILEPATH=CMAKE_DLLTOOL-NOTFOUND
// Flags used by the linker during all build types.
CMAKE_EXE_LINKER_FLAGS:STRING=-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
// Flags used by the linker during DEBUG builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=
// Flags used by the linker during MINSIZEREL builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=
// Flags used by the linker during NONE builds.
CMAKE_EXE_LINKER_FLAGS_NONE:STRING=
// Flags used by the linker during RELEASE builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=
// Flags used by the linker during RELWITHDEBINFO builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=
// Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=
// User executables (bin)
CMAKE_INSTALL_BINDIR:PATH=bin
// Read-only architecture-independent data (DATAROOTDIR)
CMAKE_INSTALL_DATADIR:PATH=
// Read-only architecture-independent data root (share)
CMAKE_INSTALL_DATAROOTDIR:PATH=share
// Documentation root (DATAROOTDIR/doc/PROJECT_NAME)
CMAKE_INSTALL_DOCDIR:PATH=
// C header files (include)
CMAKE_INSTALL_INCLUDEDIR:PATH=include
// Info documentation (DATAROOTDIR/info)
CMAKE_INSTALL_INFODIR:PATH=
// Object code libraries (lib)
CMAKE_INSTALL_LIBDIR:PATH=lib
// Program executables (libexec)
CMAKE_INSTALL_LIBEXECDIR:PATH=libexec
// Locale-dependent data (DATAROOTDIR/locale)
CMAKE_INSTALL_LOCALEDIR:PATH=
// Modifiable single-machine data (var)
CMAKE_INSTALL_LOCALSTATEDIR:PATH=var
// Man documentation (DATAROOTDIR/man)
CMAKE_INSTALL_MANDIR:PATH=
// C header files for non-gcc (/usr/include)
CMAKE_INSTALL_OLDINCLUDEDIR:PATH=/usr/include
// Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/usr
// Run-time variable data (LOCALSTATEDIR/run)
CMAKE_INSTALL_RUNSTATEDIR:PATH=
// System admin executables (sbin)
CMAKE_INSTALL_SBINDIR:PATH=sbin
// Modifiable architecture-independent data (com)
CMAKE_INSTALL_SHAREDSTATEDIR:PATH=com
// Read-only single-machine data (etc)
CMAKE_INSTALL_SYSCONFDIR:PATH=etc
// Path to a program.
CMAKE_LINKER:FILEPATH=/usr/sbin/ld
// Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/sbin/make
// Flags used by the linker during the creation of modules during all build types.
CMAKE_MODULE_LINKER_FLAGS:STRING=-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
// Flags used by the linker during the creation of modules during DEBUG builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=
// Flags used by the linker during the creation of modules during MINSIZEREL builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=
// Flags used by the linker during the creation of modules during NONE builds.
CMAKE_MODULE_LINKER_FLAGS_NONE:STRING=
// Flags used by the linker during the creation of modules during RELEASE builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=
// Flags used by the linker during the creation of modules during RELWITHDEBINFO builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=
// Path to a program.
CMAKE_NM:FILEPATH=/usr/sbin/nm
// Path to a program.
CMAKE_OBJCOPY:FILEPATH=/usr/sbin/objcopy
// Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/sbin/objdump
// Path to a program.
CMAKE_RANLIB:FILEPATH=/usr/sbin/ranlib
// Path to a program.
CMAKE_READELF:FILEPATH=/usr/sbin/readelf
// Flags used by the linker during the creation of shared libraries during all build types.
CMAKE_SHARED_LINKER_FLAGS:STRING=-Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now
// Flags used by the linker during the creation of shared libraries during DEBUG builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=
// Flags used by the linker during the creation of shared libraries during MINSIZEREL builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=
// Flags used by the linker during the creation of shared libraries during NONE builds.
CMAKE_SHARED_LINKER_FLAGS_NONE:STRING=
// Flags used by the linker during the creation of shared libraries during RELEASE builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=
// Flags used by the linker during the creation of shared libraries during RELWITHDEBINFO builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=
// If set, runtime paths are not added when installing shared libraries, but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO
// If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO
// Flags used by the linker during the creation of static libraries during all build types.
CMAKE_STATIC_LINKER_FLAGS:STRING=
// Flags used by the linker during the creation of static libraries during DEBUG builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=
// Flags used by the linker during the creation of static libraries during MINSIZEREL builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=
// Flags used by the linker during the creation of static libraries during NONE builds.
CMAKE_STATIC_LINKER_FLAGS_NONE:STRING=
// Flags used by the linker during the creation of static libraries during RELEASE builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=
// Flags used by the linker during the creation of static libraries during RELWITHDEBINFO builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=
// Path to a program.
CMAKE_STRIP:FILEPATH=/usr/sbin/strip
// If this value is on, makefiles will be generated without the .SILENT directive, and all commands will be echoed to the console during the make.  This is useful for debugging only. With Visual Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE
// Enable to build RPM source packages
CPACK_SOURCE_RPM:BOOL=OFF
// Enable to build TBZ2 source packages
CPACK_SOURCE_TBZ2:BOOL=ON
// Enable to build TGZ source packages
CPACK_SOURCE_TGZ:BOOL=ON
// Enable to build TXZ source packages
CPACK_SOURCE_TXZ:BOOL=ON
// Enable to build TZ source packages
CPACK_SOURCE_TZ:BOOL=ON
// Enable to build ZIP source packages
CPACK_SOURCE_ZIP:BOOL=OFF
// Dot tool for use with Doxygen
DOXYGEN_DOT_EXECUTABLE:FILEPATH=/usr/sbin/dot
// Doxygen documentation generation tool (https://www.doxygen.nl)
DOXYGEN_EXECUTABLE:FILEPATH=/usr/sbin/doxygen
// Build and install the Python wrappers
ENABLE_PYTHON:BOOL=OFF
// Root to Equinor internal testdata
EQUINOR_TESTDATA_ROOT:PATH=
// Use OpenMP
ERT_USE_OPENMP:BOOL=TRUE
// The directory containing a CMake configuration file for Eigen3.
Eigen3_DIR:PATH=/usr/share/eigen3/cmake
// Directory under which to collect all populated content
FETCHCONTENT_BASE_DIR:PATH=/tmp/makepkg/resinsight/src/build-cmake/_deps
// Disables all attempts to download or update content and assumes source dirs already exist
FETCHCONTENT_FULLY_DISCONNECTED:BOOL=OFF
// Enables QUIET option for all content population
FETCHCONTENT_QUIET:BOOL=ON
// When not empty, overrides where to find pre-populated content for googletest
FETCHCONTENT_SOURCE_DIR_GOOGLETEST:PATH=
// Enables UPDATE_DISCONNECTED behavior for all content population
FETCHCONTENT_UPDATES_DISCONNECTED:BOOL=OFF
// Enables UPDATE_DISCONNECTED behavior just for population of googletest
FETCHCONTENT_UPDATES_DISCONNECTED_GOOGLETEST:BOOL=OFF
// Git command line client
GIT_EXECUTABLE:FILEPATH=/usr/sbin/git
// Check submodules during build
GIT_SUBMODULE:BOOL=ON
// HDF5 CXX Wrapper compiler.  Used only to detect HDF5 compile flags.
HDF5_CXX_COMPILER_EXECUTABLE:FILEPATH=/usr/sbin/h5c++
// Path to a library.
HDF5_CXX_LIBRARY_dl:FILEPATH=/usr/lib/libdl.a
// Path to a library.
HDF5_CXX_LIBRARY_hdf5:FILEPATH=/usr/lib/libhdf5.so
// Path to a library.
HDF5_CXX_LIBRARY_hdf5_cpp:FILEPATH=/usr/lib/libhdf5_cpp.so
// Path to a library.
HDF5_CXX_LIBRARY_m:FILEPATH=/usr/lib/libm.so
// Path to a library.
HDF5_CXX_LIBRARY_sz:FILEPATH=/usr/lib/libsz.so
// Path to a library.
HDF5_CXX_LIBRARY_z:FILEPATH=/usr/lib/libz.so
// HDF5 file differencing tool.
HDF5_DIFF_EXECUTABLE:FILEPATH=/usr/sbin/h5diff
// The directory containing a CMake configuration file for HDF5.
HDF5_DIR:PATH=HDF5_DIR-NOTFOUND
// ERT: Install library
INSTALL_ERT:BOOL=OFF
// Add ert legacy wrappers
INSTALL_ERT_LEGACY:BOOL=OFF
// Group to install as - blank to install as current group
INSTALL_GROUP:STRING=
// Enable installation of googletest. (Projects embedding googletest may want to turn this OFF.)
INSTALL_GTEST:BOOL=OFF
// Path to a library.
M_LIBRARY:FILEPATH=/usr/lib/libm.so
// Path to Octave component and library information retrieval
OCTAVE_CONFIG_COMMAND:FILEPATH=/usr/sbin/octave-config
// Path to a program.
OCTAVE_CONFIG_EXECUTABLE:FILEPATH=/usr/sbin/octave-config
// Path to a library.
OCTAVE_CRUFT_LIBRARY:FILEPATH=OCTAVE_CRUFT_LIBRARY-NOTFOUND
// Path to a program.
OCTAVE_EXECUTABLE:FILEPATH=/usr/bin/octave
// Path to a file.
OCTAVE_INCLUDE_DIR:PATH=/usr/include/octave-8.2.0
// Path to a program.
OCTAVE_MKOCTFILE:FILEPATH=/usr/bin/mkoctfile
// Path to a library.
OCTAVE_OCTAVE_LIBRARY:FILEPATH=/usr/lib/octave/8.2.0/liboctave.so
// Path to a library.
OCTAVE_OCTINTERP_LIBRARY:FILEPATH=/usr/lib/octave/8.2.0/liboctinterp.so
// Octave plugin directory
OCTAVE_SITE_OCT_DIR:STRING=/usr/lib/octave/8.2.0/site/oct/x86_64-pc-linux-gnu
// Path to a file.
OPENGL_EGL_INCLUDE_DIR:PATH=/usr/include
// Path to a file.
OPENGL_GLX_INCLUDE_DIR:PATH=/usr/include
// Path to a file.
OPENGL_INCLUDE_DIR:PATH=/usr/include
// Path to a library.
OPENGL_egl_LIBRARY:FILEPATH=/usr/lib/libEGL.so
// Path to a library.
OPENGL_gl_LIBRARY:FILEPATH=/usr/lib/libGL.so
// Path to a library.
OPENGL_glu_LIBRARY:FILEPATH=/usr/lib/libGLU.so
// Path to a library.
OPENGL_glx_LIBRARY:FILEPATH=/usr/lib/libGLX.so
// Path to a library.
OPENGL_opengl_LIBRARY:FILEPATH=/usr/lib/libOpenGL.so
// Path to a file.
OPENGL_xmesa_INCLUDE_DIR:PATH=OPENGL_xmesa_INCLUDE_DIR-NOTFOUND
// Path to a library.
OPENSSL_CRYPTO_LIBRARY:FILEPATH=/usr/lib/libcrypto.so
// Path to a file.
OPENSSL_INCLUDE_DIR:PATH=/usr/include
// Path to a library.
OPENSSL_SSL_LIBRARY:FILEPATH=/usr/lib/libssl.so
// CXX compiler flags for OpenMP parallelization
OpenMP_CXX_FLAGS:STRING=-fopenmp
// CXX compiler libraries for OpenMP parallelization
OpenMP_CXX_LIB_NAMES:STRING=gomp;pthread
// C compiler flags for OpenMP parallelization
OpenMP_C_FLAGS:STRING=-fopenmp
// C compiler libraries for OpenMP parallelization
OpenMP_C_LIB_NAMES:STRING=gomp;pthread
// Path to the gomp library for OpenMP
OpenMP_gomp_LIBRARY:FILEPATH=/usr/lib/libgomp.so
// Path to the pthread library for OpenMP
OpenMP_pthread_LIBRARY:FILEPATH=/usr/lib/libpthread.a
// Arguments to supply to pkg-config
PKG_CONFIG_ARGN:STRING=
// pkg-config executable
PKG_CONFIG_EXECUTABLE:FILEPATH=/usr/sbin/pkg-config
// The directory containing a CMake configuration file for Protobuf.
Protobuf_DIR:PATH=/usr/lib/cmake/protobuf
// The directory containing a CMake configuration file for QT.
QT_DIR:PATH=/usr/lib/cmake/Qt5
// The directory containing a CMake configuration file for Qt5Charts.
Qt5Charts_DIR:PATH=/usr/lib/cmake/Qt5Charts
// The directory containing a CMake configuration file for Qt5Concurrent.
Qt5Concurrent_DIR:PATH=/usr/lib/cmake/Qt5Concurrent
// The directory containing a CMake configuration file for Qt5Core.
Qt5Core_DIR:PATH=/usr/lib/cmake/Qt5Core
// The directory containing a CMake configuration file for Qt5Gui.
Qt5Gui_DIR:PATH=/usr/lib/cmake/Qt5Gui
// The directory containing a CMake configuration file for Qt5Network.
Qt5Network_DIR:PATH=/usr/lib/cmake/Qt5Network
// The directory containing a CMake configuration file for Qt5OpenGL.
Qt5OpenGL_DIR:PATH=/usr/lib/cmake/Qt5OpenGL
// The directory containing a CMake configuration file for Qt5PrintSupport.
Qt5PrintSupport_DIR:PATH=/usr/lib/cmake/Qt5PrintSupport
// The directory containing a CMake configuration file for Qt5Svg.
Qt5Svg_DIR:PATH=/usr/lib/cmake/Qt5Svg
// The directory containing a CMake configuration file for Qt5Widgets.
Qt5Widgets_DIR:PATH=/usr/lib/cmake/Qt5Widgets
// The directory containing a CMake configuration file for Qt5Xml.
Qt5Xml_DIR:PATH=/usr/lib/cmake/Qt5Xml
// The directory containing a CMake configuration file for Qt5.
Qt5_DIR:PATH=/usr/lib/cmake/Qt5
// Use Doxygen to create the HTML based API documentation
RESINSIGHT_BUILD_DOCUMENTATION:BOOL=ON
// Bundle the OpenSSL binary library files
RESINSIGHT_BUNDLE_OPENSSL:BOOL=ON
// Copy TestModels into the installation
RESINSIGHT_BUNDLE_TESTMODELS:BOOL=OFF
// Enable the gRPC scripting framework
RESINSIGHT_ENABLE_GRPC:BOOL=ON
// Use Precompiled Headers
RESINSIGHT_ENABLE_PRECOMPILED_HEADERS:BOOL=OFF
// Enable static analysis
RESINSIGHT_ENABLE_STATIC_ANALYSIS:BOOL=OFF
// Experimental speedup of compilation using CMake Unity Build
RESINSIGHT_ENABLE_UNITY_BUILD:BOOL=OFF
// Path to installed ERT includes
RESINSIGHT_ERT_EXTERNAL_INCLUDE_ROOT:PATH=
// Path to installed ERT libraries
RESINSIGHT_ERT_EXTERNAL_LIB_ROOT:PATH=
// Path to ERT CMakeList.txt (source path)
RESINSIGHT_ERT_EXTERNAL_SOURCE_ROOT:STRING=
// Bundle the gRPC python modules into the install folder
RESINSIGHT_GRPC_BUNDLE_PYTHON_MODULE:BOOL=OFF
// Download the gRPC python modules to enable generation of Python interface
RESINSIGHT_GRPC_DOWNLOAD_PYTHON_MODULE:BOOL=OFF
// gRPC : Path to Python 3 executable, required to build the Python client library
RESINSIGHT_GRPC_PYTHON_EXECUTABLE:FILEPATH=/usr/bin/python
// Bundle HDF5 libraries
RESINSIGHT_HDF5_BUNDLE_LIBRARIES:BOOL=OFF
// Enable AppFwk Tests
RESINSIGHT_INCLUDE_APPFWK_TESTS:BOOL=OFF
// Include ApplicationCode Unit Tests
RESINSIGHT_INCLUDE_APPLICATION_UNIT_TESTS:BOOL=OFF
// Optional path to the ABAQUS ODB API from Simulia. Needed for support of geomechanical models
RESINSIGHT_ODB_API_DIR:PATH=
// Link with Legacy OpenGL libraries. This may be necessary in some virtualization environments
RESINSIGHT_PREFER_LEGACY_OPENGL:BOOL=ON
// Linux only: Install the libecl shared libraries along the executable
RESINSIGHT_PRIVATE_INSTALL:BOOL=OFF
// Treat warnings as errors (stops build)
RESINSIGHT_TREAT_WARNINGS_AS_ERRORS:BOOL=OFF
// Enable OpenMP parallellization in the code
RESINSIGHT_USE_OPENMP:BOOL=ON
// Enable QtCharts in the code
RESINSIGHT_USE_QT_CHARTS:BOOL=ON
// Build RST documentation
RST_DOC:BOOL=OFF
// Path to a library.
SHLWAPI_LIBRARY:FILEPATH=SHLWAPI_LIBRARY-NOTFOUND
// Don't strip RPATH from libraries and binaries
USE_RPATH:BOOL=OFF
// Automatically download build pre-requisites with VCPKG
VCPKG_AUTO_INSTALL:BOOL=OFF
// Path to a file.
ZLIB_INCLUDE_DIR:PATH=/usr/include
// Path to a library.
ZLIB_LIBRARY_DEBUG:FILEPATH=ZLIB_LIBRARY_DEBUG-NOTFOUND
// Path to a library.
ZLIB_LIBRARY_RELEASE:FILEPATH=/usr/lib/libz.so
// The directory containing a CMake configuration file for absl.
absl_DIR:PATH=/usr/lib/cmake/absl
// The directory containing a CMake configuration file for boost_atomic.
boost_atomic_DIR:PATH=/usr/lib/cmake/boost_atomic-1.81.0
// The directory containing a CMake configuration file for boost_filesystem.
boost_filesystem_DIR:PATH=/usr/lib/cmake/boost_filesystem-1.81.0
// The directory containing a CMake configuration file for boost_headers.
boost_headers_DIR:PATH=/usr/lib/cmake/boost_headers-1.81.0
// The directory containing a CMake configuration file for boost_system.
boost_system_DIR:PATH=/usr/lib/cmake/boost_system-1.81.0
// Path to a program.
buildcache_program:FILEPATH=buildcache_program-NOTFOUND
// The directory containing a CMake configuration file for c-ares.
c-ares_DIR:PATH=/usr/lib/cmake/c-ares
// Path to a program.
ccache_exe:FILEPATH=ccache_exe-NOTFOUND
// The directory containing a CMake configuration file for gRPC.
gRPC_DIR:PATH=/usr/lib/cmake/grpc
// Build gtest's sample programs.
gtest_build_samples:BOOL=OFF
// Build all of gtest's own tests.
gtest_build_tests:BOOL=OFF
// Disable uses of pthreads in gtest.
gtest_disable_pthreads:BOOL=OFF
// Use shared (DLL) run-time lib even when Google Test is built as static lib.
gtest_force_shared_crt:BOOL=ON
// Build gtest with internal symbols hidden in shared libraries.
gtest_hide_internal_symbols:BOOL=OFF
// Path to a library.
pkgcfg_lib_RE2_re2:FILEPATH=/usr/lib/libre2.so
// Path to a library.
pkgcfg_lib__OPENSSL_crypto:FILEPATH=/usr/lib/libcrypto.so
// Path to a library.
pkgcfg_lib__OPENSSL_ssl:FILEPATH=/usr/lib/libssl.so
// Enable for verbose output
protobuf_VERBOSE:BOOL=OFF
// The directory containing a CMake configuration file for re2.
re2_DIR:PATH=re2_DIR-NOTFOUND
```
