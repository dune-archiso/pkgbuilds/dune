When the package `dune-common-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `bin`
- [x] `include`
- [x] `lib`
- [x] `share`
  - [x] `bash-completion`
  - [x] `doc`
  - [x] `dune`
  - [x] `dune-common`
  - [x] `licenses`
  - [x] `man`

```console
usr/
├── bin
│   ├── dunecontrol
│   ├── dune-ctest
│   ├── dune-git-whitespace-hook
│   └── duneproject
├── include
│   └── dune
│       └── common
├── lib
│   ├── cmake
│   │   └── dune-common
│   ├── dunecontrol
│   │   └── dune-common
│   │       └── dune.module
│   ├── dunemodules.lib
│   ├── libdunecommon.a
│   └── pkgconfig
│       └── dune-common.pc
└── share
    ├── bash-completion
    │   └── completions
    │       └── dunecontrol
    ├── doc
    │   └── dune-common
    ├── dune
    │   └── cmake
    │       ├── modules
    │       └── scripts
    ├── dune-common
    │   ├── config.h.cmake
    │   └── doc
    │       └── doxygen
    ├── licenses
    │   └── dune-common
    └── man
        └── man1

31 directories, 1492 files
```

## Tests

OK

Some flags founded in gitlab.dune-project.org:

```console
"CC=gcc-9 CXX=g++-9 -DCMAKE_CXX_FLAGS='-std=c++17 -O2 -g -Wall -fdiagnostics-color=always' -DDUNE_ENABLE_PYTHONBINDINGS=ON -DDUNE_MAX_TEST_CORES=4 -DBUILD_SHARED_LIBS=TRUE -DDUNE_PYTHON_INSTALL_LOCATION=none -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_Alberta=TRUE -DCMAKE_DISABLE_FIND_PACKAGE_Vc=TRUE -DCMAKE_DISABLE_DOCUMENTATION=TRUE"
```

```console
CC=clang-9
CXX=clang++-9
FC=gfortran-10

CFLAGS="-Wall -fdiagnostics-color=always"
FFLAGS="-Wall -fdiagnostics-color=always"
CXXFLAGS="-Wall -fdiagnostics-color=always -std=c++17 -stdlib=libc++"

REL_CFLAGS="-O3 -g -UNDEBUG"
REL_CXXFLAGS="-O3 -g -UNDEBUG"
REL_FFLAGS="-O3 -g"
TOOLCHAIN_CMAKE_FLAGS="-DCMAKE_DISABLE_FIND_PACKAGE_GMP=1 -DCXX_MAX_STANDARD=17"
TOOLCHAIN_CMAKE_FLAGS+=" -DTARGET_ARCHITECTURE=none"
```

DUNE_CONTROL_PATH: /duneci/modules:$CI_PROJECT_DIR
DUNE_PY_DIR: /duneci/modules/dune-py
PYTHONPATH: $CI_PROJECT_DIR/build-cmake/python

```console
# export CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr ${CMAKE_FLAGS}"
```

Missing `BUILD_ON_INSTALL` on release 2.7.

### Remarks

Not using python

imagemagick will not be necesary in DuneDock.cmake be removed on 2.8.

### Docs

LaTeX: yes
Doxygen: yes
Sphinx: no

[What does “Performing Test CMAKE_HAVE_LIBC_PTHREAD” failed actually mean?](https://stackoverflow.com/a/64515538/9302545)

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
auxclean
build_quick_tests
build_tests
clean_latex
doc
doc_comm_communication_tex
doc_comm_communication_tex_clean
doxyfile
doxygen_dune-common
doxygen_install
dvi
headercheck
html
install_python
pdf
ps
safepdf
sphinx_html
test_python
alignedallocatortest
arithmetictestsuitetest
arraylisttest
arraytest
assertandreturntest
assertandreturntest_compiletime_fail
assertandreturntest_ndebug
autocopytest
bigunsignedinttest
bitsetvectortest
boundscheckingmvtest
boundscheckingoptest
boundscheckingtest
calloncetest
check_fvector_size
check_fvector_size_fail1
check_fvector_size_fail2
classnametest-demangled
classnametest-fallback
concept
constexprifelsetest
debugalignsimdtest
debugaligntest
densematrixassignmenttest
densematrixassignmenttest_fail0
densematrixassignmenttest_fail1
densematrixassignmenttest_fail2
densematrixassignmenttest_fail3
densematrixassignmenttest_fail4
densematrixassignmenttest_fail5
densematrixassignmenttest_fail6
densevectorassignmenttest
densevectortest
diagonalmatrixtest
dunecommon
dynmatrixtest
dynvectortest
eigenvaluestest
enumsettest
filledarraytest
fmatrixtest
functiontest
fvectorconversion1d
fvectortest
gcdlcmtest
genericiterator_compile_fail
hybridutilitiestest
indexset
indexsettest
indicestest
integersequence
iteratorfacadetest
iteratorfacadetest2
looptest
lrutest
mathclassifierstest
mathtest
mpi_collective_benchmark
mpicommunicationtest
mpidatatest
mpifuturetest
mpigatherscattertest
mpiguardtest
mpihelpertest
mpihelpertest2
mpipacktest
optionaltest
overloadsettest
parameterizedobjecttest
parametertreelocaletest
parametertreetest
pathtest
poolallocatortest
poosc08
poosc08_test
powertest
quadmathtest
rangeutilitiestest
remoteindicestest
reservedvectortest
selectiontest
shared_ptrtest
singletontest
sllisttest
standardtest
stdapplytest
stdidentity
stdtypetraitstest
streamoperatorstest
streamtest
stringutilitytest
syncertest
testdebugallocator
testdebugallocator_fail1
testdebugallocator_fail2
testdebugallocator_fail3
testdebugallocator_fail4
testdebugallocator_fail5
testfloatcmp
to_unique_ptrtest
tupleutilitytest
typelisttest
typeutilitytest
utilitytest
variablesizecommunicatortest
varianttest
vcarraytest
vcexpectedimpltest
vcvectortest
versiontest
```

https://cmake.org/cmake/help/v3.0/module/GNUInstallDirs.html
https://stackoverflow.com/questions/41024820/how-do-you-control-the-location-of-header-files-in-cmake
https://cmake.org/cmake/help/v2.8.8/cmake.html
https://stackoverflow.com/questions/54464929/cmake-install-rpath-use-link-path-without-any-effect
https://stackoverflow.com/questions/49138195/whats-the-difference-between-rpath-link-and-l

```
# DUNE_CONTROL_PATH=${srcdir}/${pkgname}-${pkgver}

# -DBUILD_TESTING=OFF

# local _dunecontrol=$(echo ${srcdir}/${pkgname}-${pkgver}/bin/dunecontrol)
# $_dunecontrol --current --opts=${pkgname}.opts cmake

# $_dunecontrol --current make install sphinx_html DESTDIR="${pkgdir}"
# $_dunecontrol --only=${pkgname} make install sphinx_html DESTDIR="${pkgdir}"
# mv "${srcdir}/${pkgname}-${pkgver}/build-cmake/doc/buildsystem/html" "${pkgdir}/usr/share/doc/${pkgname}/buildsystem"
# install -dm755 "${pkgdir}/usr/share/doc/${pkgname}/doxygen"

# install -Dt "${pkgdir}/usr/share/doc/${pkgname}/buildsystem" -m644 build-cmake/doc/buildsystem/html
```

https://gitlab.dune-project.org/core/dune-common/-/blob/master/python/dune/packagemetadata.py
https://gitlab.dune-project.org/core/dune-common/-/issues/225#note_73598

```
./bin/dunecontrol --current --opts=${pkgname}.opts cmake
```

```
# -DCMAKE_C_FLAGS="" \
# -DCMAKE_CXX_FLAGS="" \
```

```
# CMAKE_FLAGS='-DDUNE_ENABLE_PYTHONBINDINGS=ON \
# -DPYTHON_INSTALL_LOCATION=system \
# -DADDITIONAL_PIP_PARAMS="-upgrade"'
```

https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/pipelines/33895
https://gitlab.dune-project.org/infrastructure/dune-nightly-test/-/pipelines/35375
https://github.com/archlinux/svntogit-packages/blob/packages/python-appdirs/trunk/PKGBUILD

```
-DPYTHON_INSTALL_LOCATION=system
-DADDITIONAL_PIP_PARAMS='-upgrade'
```

[](https://stackoverflow.com/a/2456882)
[](https://cmake.org/cmake/help/v3.1/search.html?q=shared+library)

[MPI with C bindings and the MPI_FOUND variable](https://gitlab.dune-project.org/core/dune-common/-/merge_requests/804)

[MPI implementation is not thread-safe: compile the parallel versions without SCOTCH_PTHREAD](https://github.com/arch4edu/arch4edu/blob/master/scotch/PKGBUILD#L48)

```
[ 45%] Building CXX object dune/common/test/CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o
cd /tmp/makepkg/dune-common/src/build-cmake/dune/common/test && /tmp/makepkg/dune-common/src/build-cmake/CXX_compiler.sh -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_QUADMATH=1 -DHAVE_CONFIG_H -DMETIS_API_VERSION=5 -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX -D_GLIBCXX_USE_FLOAT128 -I/tmp/makepkg/dune-common/src/build-cmake -I/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1 -I/usr/include/python3.9 -isystem /usr/include/scotch -std=c++17  -fPIE -pthread -fext-numeric-literals -std=gnu++17 -MD -MT dune/common/test/CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o -MF CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o.d -o CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o -c /tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/common/test/testfloatcmp.cc
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_QUADMATH=1 -DHAVE_CONFIG_H -DMETIS_API_VERSION=5 -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX -D_GLIBCXX_USE_FLOAT128 -I/tmp/makepkg/dune-common/src/build-cmake -I/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1 -I/usr/include/python3.9 -isystem /usr/include/scotch -std=c++17 -fPIE -pthread -fext-numeric-literals -std=gnu++17 -MD -MT dune/common/test/CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o -MF CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o.d -o CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o -c /tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/common/test/testfloatcmp.cc
[ 45%] Linking CXX executable testfloatcmp
cd /tmp/makepkg/dune-common/src/build-cmake/dune/common/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/testfloatcmp.dir/link.txt --verbose=1
/tmp/makepkg/dune-common/src/build-cmake/CXX_compiler.sh -std=c++17  -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o -o testfloatcmp  /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libptscotch.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so -pthread -lm /usr/lib/openmpi/libmpi.so -lptscotcherr /usr/lib/libscotch.so -lscotcherr -lquadmath /usr/lib/libgmp.so
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -std=c++17 -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/testfloatcmp.dir/testfloatcmp.cc.o -o testfloatcmp /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libptscotch.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so -pthread -lm /usr/lib/openmpi/libmpi.so -lptscotcherr /usr/lib/libscotch.so -lscotcherr -lquadmath /usr/lib/libgmp.so
make[3]: Leaving directory '/tmp/makepkg/dune-common/src/build-cmake'
[ 45%] Built target testfloatcmp
/usr/sbin/make  -f dune/common/test/CMakeFiles/scotchtest.dir/build.make dune/common/test/CMakeFiles/scotchtest.dir/depend
make[3]: Entering directory '/tmp/makepkg/dune-common/src/build-cmake'
cd /tmp/makepkg/dune-common/src/build-cmake && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /tmp/makepkg/dune-common/src/dune-common-2.8.0rc1 /tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/common/test /tmp/makepkg/dune-common/src/build-cmake /tmp/makepkg/dune-common/src/build-cmake/dune/common/test /tmp/makepkg/dune-common/src/build-cmake/dune/common/test/CMakeFiles/scotchtest.dir/DependInfo.cmake --color=
make[3]: Leaving directory '/tmp/makepkg/dune-common/src/build-cmake'
/usr/sbin/make  -f dune/common/test/CMakeFiles/scotchtest.dir/build.make dune/common/test/CMakeFiles/scotchtest.dir/build
make[3]: Entering directory '/tmp/makepkg/dune-common/src/build-cmake'
[ 47%] Building CXX object dune/common/test/CMakeFiles/scotchtest.dir/scotchtest.cc.o
cd /tmp/makepkg/dune-common/src/build-cmake/dune/common/test && /tmp/makepkg/dune-common/src/build-cmake/CXX_compiler.sh -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_QUADMATH=1 -DHAVE_CONFIG_H -DMETIS_API_VERSION=5 -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX -D_GLIBCXX_USE_FLOAT128 -I/tmp/makepkg/dune-common/src/build-cmake -I/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1 -I/usr/include/python3.9 -isystem /usr/include/scotch -std=c++17  -fPIE -pthread -fext-numeric-literals -std=gnu++17 -MD -MT dune/common/test/CMakeFiles/scotchtest.dir/scotchtest.cc.o -MF CMakeFiles/scotchtest.dir/scotchtest.cc.o.d -o CMakeFiles/scotchtest.dir/scotchtest.cc.o -c /tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/common/test/scotchtest.cc
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_QUADMATH=1 -DHAVE_CONFIG_H -DMETIS_API_VERSION=5 -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX -D_GLIBCXX_USE_FLOAT128 -I/tmp/makepkg/dune-common/src/build-cmake -I/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1 -I/usr/include/python3.9 -isystem /usr/include/scotch -std=c++17 -fPIE -pthread -fext-numeric-literals -std=gnu++17 -MD -MT dune/common/test/CMakeFiles/scotchtest.dir/scotchtest.cc.o -MF CMakeFiles/scotchtest.dir/scotchtest.cc.o.d -o CMakeFiles/scotchtest.dir/scotchtest.cc.o -c /tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/common/test/scotchtest.cc
[ 47%] Linking CXX executable scotchtest
cd /tmp/makepkg/dune-common/src/build-cmake/dune/common/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/scotchtest.dir/link.txt --verbose=1
/tmp/makepkg/dune-common/src/build-cmake/CXX_compiler.sh -std=c++17  -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/scotchtest.dir/scotchtest.cc.o -o scotchtest  /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libptscotch.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunecommon.so -lm /usr/lib/openmpi/libmpi.so -lptscotcherr /usr/lib/libscotch.so -lscotcherr -lquadmath /usr/lib/libgmp.so -pthread /usr/lib/liblapack.so /usr/lib/libblas.so
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -std=c++17 -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/scotchtest.dir/scotchtest.cc.o -o scotchtest /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libptscotch.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunecommon.so -lm /usr/lib/openmpi/libmpi.so -lptscotcherr /usr/lib/libscotch.so -lscotcherr -lquadmath /usr/lib/libgmp.so -pthread /usr/lib/liblapack.so /usr/lib/libblas.so
/usr/sbin/ld: /usr/lib/libptscotcherr.so: undefined reference to `ompi_mpi_comm_world'
/usr/sbin/ld: /usr/lib/libptscotcherr.so: undefined reference to `MPI_Initialized'
/usr/sbin/ld: /usr/lib/libptscotcherr.so: undefined reference to `MPI_Comm_rank'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/common/test/CMakeFiles/scotchtest.dir/build.make:114: dune/common/test/scotchtest] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-common/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:6784: dune/common/test/CMakeFiles/scotchtest.dir/all] Error 2
make[2]: Leaving directory '/tmp/makepkg/dune-common/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2335: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-common/src/build-cmake'
make: *** [Makefile:767: build_tests] Error 2
==> ERROR: A failure occurred in check().
```

[](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2014/n4184.pdf)

Side note: [vc-dev](https://packages.debian.org/experimental/vc-dev) for Debian Experimental is the same version of the [package](https://archlinux.org/packages/extra/x86_64/vc) in Arch Linux. Under Debian 11, the [currently version is 1.3.3-4](https://packages.debian.org/sid/vc-dev).

[](https://stackoverflow.com/a/5757256/9302545)
[](https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html)
[](https://www.foonathan.net/2018/10/cmake-warnings)
[](https://www.foonathan.net/2021/07/concepts-structural-nominal/#content)

```console
102/113 Test #102: testdebugallocator_fail5 ...............Subprocess aborted***Exception:   0.00 sec
NEW 64 -> 0x7f670ba95fc0
NEW 512 -> 0x7f670ba93e00
NEW 16 -> 0x7f670ba91ff0
NEW 64 -> 0x7f670ba8ffc0
NEW 512 -> 0x7f670aef8e00
NEW 16 -> 0x7f670aef6ff0
NEW 64 -> 0x7f670aef4fc0
NEW 512 -> 0x7f670aef2e00
NEW 16 -> 0x7f670aef0ff0
NEW 64 -> 0x7f670aeeefc0
NEW 512 -> 0x7f670aeece00
NEW 16 -> 0x7f670aeeaff0
NEW 64 -> 0x7f670aee8fc0
NEW 512 -> 0x7f670aee6e00
NEW 16 -> 0x7f670aee4ff0
NEW 64 -> 0x7f670aee2fc0
NEW 512 -> 0x7f670aee0e00
NEW 16 -> 0x7f670aedeff0

99% tests passed, 1 tests failed out of 113

Label Time Summary:
quick    =  24.53 sec*proc (105 tests)

Total Test time (real) =  23.05 sec

The following tests did not run:
         18 - vcarraytest (Skipped)
         19 - vcvectortest (Skipped)
         80 - parametertreelocaletest (Skipped)
         88 - scotchtest (Skipped)
        112 - vcexpectedimpltest (Skipped)

The following tests FAILED:
        102 - testdebugallocator_fail5 (Subprocess aborted)
```

```
113: DUNE-INFO: Creating new dune-py module in /home/aur/.cache/dune-py
113: DUNE-INFO: Configuring dune-py module in /home/aur/.cache/dune-py
113: Traceback (most recent call last):
113:   File "/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/python/test/pythontests.py", line 35, in <module>
113:     test_class_export()
113:   File "/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/python/test/pythontests.py", line 22, in test_class_export
113:     from dune.generator.importclass import load
113:   File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/generator/__init__.py", line 12, in <module>
113:     builder = Builder( env_force in ('1', 'TRUE'), env_save )
113:   File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 31, in __init__
113:     dune.common.module.build_dune_py_module(self.dune_py_dir)
113:   File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/common/module.py", line 418, in build_dune_py_module
113:     output = configure_module(dune_py_dir, dune_py_dir, {d: prefix[d] for d in deps}, cmake_args)
113:   File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/common/module.py", line 296, in configure_module
113:     raise RuntimeError(buffer_to_str(stderr))
113: RuntimeError: CMake Error at CMakeLists.txt:10 (find_package):
113:   By not providing "Finddune-common.cmake" in CMAKE_MODULE_PATH this project
113:   has asked CMake to find a package configuration file provided by
113:   "dune-common", but CMake did not find one.
113:
113:   Could not find a package configuration file provided by "dune-common" with
113:   any of the following names:
113:
113:     dune-commonConfig.cmake
113:     dune-common-config.cmake
113:
113:   Add the installation prefix of "dune-common" to CMAKE_PREFIX_PATH or set
113:   "dune-common_DIR" to a directory containing one of the above files.  If
113:   "dune-common" provides a separate development package or SDK, be sure it
113:   has been installed.
113:
113:
113:
113/114 Test #113: pythontests ............................***Failed    1.12 sec
```

```console
# PATH="${srcdir}/tmp_install/usr/bin:$PATH"
# dune-common_DIR="${srcdir}/tmp_install"
```

```console
test 113
        Start 113: pythontests

113: Test command: /usr/sbin/python3 "pythontests.py"
113: Test timeout computed to be: 1500
113: --------------------------------------------------------------------------
113: The library attempted to open the following supporting CUDA libraries,
113: but each of them failed.  CUDA-aware support is disabled.
113: libcuda.so.1: cannot open shared object file: No such file or directory
113: libcuda.dylib: cannot open shared object file: No such file or directory
113: /usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
113: /usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
113: If you are not interested in CUDA-aware support, then run with
113: --mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
113: in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
113: of libcuda.so.1 to get passed this issue.
113: --------------------------------------------------------------------------
113: DUNE-INFO: Configuring dune-py module in /home/aur/.cache/dune-py
113: Traceback (most recent call last):
113:   File "/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/python/test/pythontests.py", line 35, in <module>
113:     test_class_export()
113:   File "/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/python/test/pythontests.py", line 22, in test_class_export
113:     from dune.generator.importclass import load
113:   File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/generator/__init__.py", line 12, in <module>
113:     builder = Builder( env_force in ('1', 'TRUE'), env_save )
113:   File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 31, in __init__
113:     dune.common.module.build_dune_py_module(self.dune_py_dir)
113:   File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/common/module.py", line 418, in build_dune_py_module
113:     output = configure_module(dune_py_dir, dune_py_dir, {d: prefix[d] for d in deps}, cmake_args)
113:   File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/common/module.py", line 296, in configure_module
113:     raise RuntimeError(buffer_to_str(stderr))
113: RuntimeError: CMake Error at /tmp/makepkg/dune-common/src/tmp_install/usr/share/dune/cmake/modules/DuneMacros.cmake:686 (include):
113:   include could not find requested file:
113:
113:     CheckCXXFeatures
113: Call Stack (most recent call first):
113:   CMakeLists.txt:14 (dune_project)
113:
113:
113: CMake Error at python/dune/generated/CMakeLists.txt:2 (add_dune_mpi_flags):
113:   Unknown CMake command "add_dune_mpi_flags".
113:
113:
113:
113/114 Test #113: pythontests ............................***Failed    0.81 sec
--------------------------------------------------------------------------
The library attempted to open the following supporting CUDA libraries,
but each of them failed.  CUDA-aware support is disabled.
libcuda.so.1: cannot open shared object file: No such file or directory
libcuda.dylib: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.so.1: cannot open shared object file: No such file or directory
/usr/lib64/libcuda.dylib: cannot open shared object file: No such file or directory
If you are not interested in CUDA-aware support, then run with
--mca opal_warn_on_missing_libcuda 0 to suppress this message.  If you are interested
in CUDA-aware support, then try setting LD_LIBRARY_PATH to the location
of libcuda.so.1 to get passed this issue.
--------------------------------------------------------------------------
DUNE-INFO: Configuring dune-py module in /home/aur/.cache/dune-py
Traceback (most recent call last):
  File "/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/python/test/pythontests.py", line 35, in <module>
    test_class_export()
  File "/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/python/test/pythontests.py", line 22, in test_class_export
    from dune.generator.importclass import load
  File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/generator/__init__.py", line 12, in <module>
    builder = Builder( env_force in ('1', 'TRUE'), env_save )
  File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/generator/builder.py", line 31, in __init__
    dune.common.module.build_dune_py_module(self.dune_py_dir)
  File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/common/module.py", line 418, in build_dune_py_module
    output = configure_module(dune_py_dir, dune_py_dir, {d: prefix[d] for d in deps}, cmake_args)
  File "/tmp/makepkg/dune-common/src/build-cmake/python/tmp_install/usr/lib/python3.9/site-packages/dune/common/module.py", line 296, in configure_module
    raise RuntimeError(buffer_to_str(stderr))
RuntimeError: CMake Error at /tmp/makepkg/dune-common/src/tmp_install/usr/share/dune/cmake/modules/DuneMacros.cmake:686 (include):
  include could not find requested file:

    CheckCXXFeatures
Call Stack (most recent call first):
  CMakeLists.txt:14 (dune_project)


CMake Error at python/dune/generated/CMakeLists.txt:2 (add_dune_mpi_flags):
  Unknown CMake command "add_dune_mpi_flags".
```

```
# -DCMAKE_CXX_FLAGS='-std=c++17 -march=native -mavx' \
```

[](https://github.com/OPM/opm-grid/issues/345)
[](https://gitlab.dune-project.org/dune-fem/dune-fempy/-/issues/38)
[](https://www.google.com/search?q=Add+the+installation+prefix+of+dune-common+to+CMAKE_PREFIX_PATH+or+set+dune-common_DIR+to+a+directory+containing+one+of+the+above+files)

[](https://gitlab.dune-project.org/core/dune-common/-/jobs/260112)
[](https://gitlab.dune-project.org/core/dune-common/-/jobs/260111)
[](https://gitlab.com/dune-project/core/dune-common)

```
[ 48%] Building CXX object dune/common/test/CMakeFiles/scotchtest.dir/scotchtest.cc.o
cd /tmp/makepkg/dune-common/src/build-cmake/dune/common/test && /tmp/makepkg/dune-common/src/build-cmake/CXX_compiler.sh -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_QUADMATH=1 -DHAVE_CONFIG_H -DMETIS_API_VERSION=5 -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX -D_GLIBCXX_USE_FLOAT128 -I/tmp/makepkg/dune-common/src/build-cmake -I/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1 -I/usr/include/python3.9 -isystem /usr/include/scotch -std=c++17  -fPIE -pthread -fext-numeric-literals -std=gnu++17 -MD -MT dune/common/test/CMakeFiles/scotchtest.dir/scotchtest.cc.o -MF CMakeFiles/scotchtest.dir/scotchtest.cc.o.d -o CMakeFiles/scotchtest.dir/scotchtest.cc.o -c /tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/common/test/scotchtest.cc
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -DENABLE_GMP=1 -DENABLE_MPI=1 -DENABLE_QUADMATH=1 -DHAVE_CONFIG_H -DMETIS_API_VERSION=5 -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX -D_GLIBCXX_USE_FLOAT128 -I/tmp/makepkg/dune-common/src/build-cmake -I/tmp/makepkg/dune-common/src/dune-common-2.8.0rc1 -I/usr/include/python3.9 -isystem /usr/include/scotch -std=c++17 -fPIE -pthread -fext-numeric-literals -std=gnu++17 -MD -MT dune/common/test/CMakeFiles/scotchtest.dir/scotchtest.cc.o -MF CMakeFiles/scotchtest.dir/scotchtest.cc.o.d -o CMakeFiles/scotchtest.dir/scotchtest.cc.o -c /tmp/makepkg/dune-common/src/dune-common-2.8.0rc1/dune/common/test/scotchtest.cc
[ 48%] Linking CXX executable scotchtest
cd /tmp/makepkg/dune-common/src/build-cmake/dune/common/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/scotchtest.dir/link.txt --verbose=1
/tmp/makepkg/dune-common/src/build-cmake/CXX_compiler.sh -std=c++17  -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/scotchtest.dir/scotchtest.cc.o -o scotchtest  /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libptscotch.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunecommon.so -lm /usr/lib/openmpi/libmpi.so -lptscotcherr /usr/lib/libscotch.so -lscotcherr -lquadmath /usr/lib/libgmp.so -pthread /usr/lib/liblapack.so /usr/lib/libblas.so
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -std=c++17 -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/scotchtest.dir/scotchtest.cc.o -o scotchtest /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libptscotch.so /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so ../../../lib/libdunecommon.so -lm /usr/lib/openmpi/libmpi.so -lptscotcherr /usr/lib/libscotch.so -lscotcherr -lquadmath /usr/lib/libgmp.so -pthread /usr/lib/liblapack.so /usr/lib/libblas.so
/usr/sbin/ld: /usr/lib/libptscotcherr.so: undefined reference to `ompi_mpi_comm_world'
/usr/sbin/ld: /usr/lib/libptscotcherr.so: undefined reference to `MPI_Initialized'
/usr/sbin/ld: /usr/lib/libptscotcherr.so: undefined reference to `MPI_Comm_rank'
collect2: error: ld returned 1 exit status
make[3]: *** [dune/common/test/CMakeFiles/scotchtest.dir/build.make:114: dune/common/test/scotchtest] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-common/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:6811: dune/common/test/CMakeFiles/scotchtest.dir/all] Error 2
make[2]: Leaving directory '/tmp/makepkg/dune-common/src/build-cmake'
make[1]: *** [CMakeFiles/Makefile2:2172: CMakeFiles/build_tests.dir/rule] Error 2
make[1]: Leaving directory '/tmp/makepkg/dune-common/src/build-cmake'
make: *** [Makefile:754: build_tests] Error 2
==> ERROR: A failure occurred in check().
```

[](https://gitlab.dune-project.org/core/dune-common/-/blob/master/dune/common/test/scotchtest.cc)
[](https://gitlab.dune-project.org/docker/ci/-/blob/master/base-common/dune.opts#L50)

[](https://fossies.org/diffs/dune-common)

[](https://gitlab.dune-project.org/core/dune-common/-/issues/229)
[](https://cgit.freebsd.org/ports/tree/math/dune-common/Makefile)
[](https://www.open-mpi.org/faq/?category=rsh)
[](https://en.wikipedia.org/wiki/Remote_Shell)

```
'python-ufl: for dunepackaging.py'
'python-requests: for dunepackaging.py'
'python-pip: for dunepackaging.py'
'python-scipy: for dunepackaging.py'
'python-scikit-build: for dunepackaging.py'
```

```
OMPI_ALLOW_RUN_AS_ROOT=1
OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
```

```
dune-common W: Package was 88% docs by size; maybe you should split out a docs package
dune-common W: Referenced library 'skbuild' is an uninstalled dependency
dune-common W: Referenced library 'packagemetadata' is an uninstalled dependency
dune-common W: Referenced library 'pkg_resources' is an uninstalled dependency
dune-common W: Referenced library 'dune' is an uninstalled dependency
dune-common W: Referenced library 'docutils' is an uninstalled dependency
dune-common W: Referenced library 'python' is an uninstalled dependency
dune-common W: Referenced library 'bash' is an uninstalled dependency
dune-common W: Referenced library 'python3' is an uninstalled dependency
dune-common W: Referenced library 'sh' is an uninstalled dependency
dune-common W: Referenced library 'liblapack.so.3' is an uninstalled dependency
dune-common E: Dependency python detected and not included (libraries ['glob', '__future__', 'argparse', 'email', 're', 'datetime', 'getopt', 'shutil', 'io', 'itertools', 'sys', 'importlib', 'os', 'subprocess', 'warnings', 'errno', 'hashlib'] needed in files ['usr/share/dune/cmake/scripts/extract_cmake_data.py', 'usr/share/dune/cmake/scripts/sphinx_cmake_dune.py', 'usr/bin/rmgenerated.py', 'usr/share/dune/cmake/scripts/checkvenvconf.py', 'usr/bin/dunepackaging.py', 'usr/share/dune/cmake/scripts/pyversion.py', 'usr/share/dune/cmake/scripts/venvpath.py'])
dune-common W: Dependency included and not needed ('cmake')
dune-common W: Dependency included and not needed ('python-docutils')
dune-common W: Dependency included and not needed ('git')
dune-common W: Dependency included and not needed ('lapack')
```

```
/tmp/makepkg/dune-common/src/dune-common-2.9.0/dune/common/simd/test.hh:831:44: error: ambiguous overload for ‘operator*’ (operand types are ‘Dune::Simd::VcImpl::Proxy<Vc_1::Vector<short int, Vc_1::VectorAbi::Sse> >’ and ‘Vc_1::Vector<short int, Vc_1::VectorAbi::Sse>’)
  831 |       DUNE_SIMD_INFIX_OP(Mul,              *  );
```

```
# -DCMAKE_INSTALL_LIBDIR=/usr/lib \
# -DCMAKE_SKIP_RPATH=ON \
```
