### Dependency 🟥, Make dependency 🟦, Check dependency 🟧, Optional dependency 🟩, No (yet) packaged over Arch Linux ⬛

| Package description                                |     Package name      | Source path                                     | 🟥 🟦 🟧 🟩 ⬛ |
| :------------------------------------------------- | :-------------------: | :---------------------------------------------- | :-------: |
| Threading Building Blocks                          |       `onetbb`        | `dune/common/CMakeLists.txt`                    |    🟦 🟩    |
| SIMD Vector Classes for C++                        |         `vc`          | `dune/common/vc.hh`                             |    🟧 🟩    |
| Serial Graph Partitioning                          |        `metis`        | `dune/common/test/metistest.cc`                 |    🟧 🟩    |
| Graph, mesh/hypergraph partitioning                |       `scotch`        | `dune/common/test/metistest.cc`                 |   🟧 🟩 ⬛   |
| High performance message passing library           |       `openmpi`       | `dune/common/parallel/mpihelper.hh`             |    🟧 🟩    |
| The GNU Compiler Collection - C and C++ frontends  |         `gcc`         | `dune/common/classname.hh`                      |    🟥 🟦    |
| Linear Algebra PACKage                             |       `lapack`        | `dune/common/fmatrixev.cc`                      |    🟥 🟧    |
| Template library for operating on statically trees |    `dune-typetree`    | `dune/python/common/dimrange.hh`                |     🟩     |
| Parallel graph partitioning library                |      `parmetis`       | `dune/common/test/parmetistest.cc`              |    🟧 🟩    |
| Implementations {Alberta, Geometry, OneD,Yasp}Grid |      `dune-grid`      | `dune/python/common/numpycommdatahandle.hh`     |     🟩     |
| GNU C Library                                      |        `glibc`        | `dune/common/debugallocator.hh`                 |     🟥     |
| Free library for arbitrary precision arithmetic    |         `gmp`         | `dune/common/gmpfield.hh`                       |     🟥     |
| Documentation system for C++, C, Java, IDL and PHP |       `doxygen`       | `doc/doxygen/Doxylocal`                         |    🟦 🟩    |
| Easy, portable file locking API                    | `python-portalocker`  | `python/dune/common/locking.py`                 |     🟥     |
| Easily build, install, upgrade Python packages     |  `python-setuptools`  | `python/dune/packagemetadata.py`                |     🟥     |
| Python plotting library, making publication plots  |  `python-matplotlib`  | `python/dune/plotting.py`                       |     🟥     |
| Tools for processing plaintext docs into formats   |   `python-docutils`   | `cmake/scripts/sphinx_cmake_dune.py`            |     🟥     |
| Cross-platform open-source make system             |        `cmake`        | `CMakeLists.txt`                                |    🟥 🟦    |
| Python bindings for the Message Passing Interface  |    `python-mpi4py`    | `python/dune/common/__init__.py`                |     🟥     |
| PyPA tool for installing Python packages           |     `python-pip`      | `bin/dunepackaging.py`                          |     🟥     |
| Improved build system for C{P}ython, C{++}         | `python-scikit-build` | `setup.py`                                      |     🟦     |
| Package compiler and linker metadata toolkit       |       `pkgconf`       | `bin/duneproject`                               |     🟥     |
| Scientific tools for Python                        |    `python-numpy`     | `python/dune/generator/algorithm.py`            |     🟥     |
| Programmable completion for the bash shell         |   `bash-completion`   | `share/bash-completion/completions/dunecontrol` |     🟩     |
| The GNU Bourne Again shell                         |        `bash`         | `bin/dunecontrol`                               |    🟥 🟦    |
| A utility for reading man pages                    |       `man-db`        | `doc/dunecontrol.1`                             |     🟩     |
| TeX Live core distribution                         |    `texlive-core`     | `doc/comm/communication.tex`                    |    🟦 🟩    |
| Python documentation generator                     |    `python-sphinx`    | `doc/buildsystem/dune-common.rst`               |    🟦 🟩    |
| A collection of sparse matrix libraries            |     `suitesparse`     | `cmake/modules/FindSuiteSparse.cmake`           |           |
| Set of serial programs for partitioning graphs     |        `metis`        | `cmake/modules/FindMETIS.cmake`                 |    🟧 🟩    |
| Professional vector graphics editor                |      `inkscape`       | `cmake/modules/FindInkscape.cmake`              |           |
| Next generation of the python high-level language  |       `python`        | `bin/dune-ctest`                                |     🟥     |
| the fast distributed version control system        |         `git`         | `bin/dune-git-whitespace-hook`                  |     🟥     |
| C++ template library for vector and matrix math    |        `eigen`        | `dune/python/pybind11/eigen.h`                  |     🟩     |

### Support AUR packages / Increase votes for move to official repositories

- [python-portalocker](https://aur.archlinux.org/packages/python-portalocker) 1
- [parmetis](https://aur.archlinux.org/packages/parmetis) 30
- [scotch](https://aur.archlinux.org/packages/scotch) 34
