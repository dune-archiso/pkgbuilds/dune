## [`dune-common`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-common/PKGBUILD)


[](https://sources.debian.org/patches/dune-common/2.8.0-3)

[](https://sources.debian.org/src/dune-common/2.8.0-3)
#### binaries

- [Deepin](https://community-packages.deepin.com/deepin/pool/main/d/dune-common)
- [Devuan](https://pkginfo.devuan.org/cgi-bin/package-query.html?c=package&q=libdune-common-dev=2.7.1-2)
- [DragonFlyBSD](https://github.com/DragonFlyBSD/DPorts/tree/master/math/dune-common)
- [Guix](https://guix.gnu.org/packages/dune-common-openmpi-2.7.0)
- [Gentoo](https://github.com/easior/gentoo-wonderland/tree/master/sci-libs)

#### building recipes

- [openSUSE](https://build.opensuse.org/package/show/science/dune-common)
- [FreeBSD](https://github.com/freebsd/freebsd-ports/tree/main/math/dune-common)
- [Debian](https://packages.debian.org/sid/libdune-common-dev)

#### debian

- [](https://tracker.debian.org/pkg/dune-common)
- [](https://salsa.debian.org/pjaap/dune-common)
- [](https://salsa.debian.org/lisajuliafog/dune-common)
- [](https://packages.debian.org/experimental/amd64/libdune-common-dev/filelist)

#### openSUSE

- [](https://build.opensuse.org/users/mathletic?search_text=dune)
- [](https://build.opensuse.org/users/polyconvex?search_text=dune)

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-common.svg)](https://repology.org/project/dune-common/versions)

- [Salsa Debian Vc repository](https://salsa.debian.org/qt-kde-team/extras/vc).
[](https://github.com/ruslo/leathers)
[](https://github.com/ruslo/sugar)

```console
-- The following OPTIONAL packages have been found:
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * UnixCommands, Some common Unix commands
   To generate the documentation with LaTeX
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * BLAS, fast linear algebra routines
 * Threads, Multi-threading library
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * QuadMath, GCC Quad-Precision library
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
-- The following OPTIONAL packages have not been found:
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
```

```
usr/
usr/bin/
usr/bin/dune-ctest
usr/bin/dune-git-whitespace-hook
usr/bin/dunecontrol
usr/bin/dunepackaging.py
usr/bin/duneproject
usr/bin/rmgenerated.py
usr/bin/setup-dunepy.py
usr/include/
usr/include/dune/
usr/include/dune/common/
usr/include/dune/common/alignedallocator.hh
usr/include/dune/common/arraylist.hh
usr/include/dune/common/assertandreturn.hh
usr/include/dune/common/bartonnackmanifcheck.hh
usr/include/dune/common/bigunsignedint.hh
usr/include/dune/common/binaryfunctions.hh
usr/include/dune/common/bitsetvector.hh
usr/include/dune/common/boundschecking.hh
usr/include/dune/common/classname.hh
usr/include/dune/common/concept.hh
usr/include/dune/common/conditional.hh
usr/include/dune/common/debugalign.hh
usr/include/dune/common/debugallocator.hh
usr/include/dune/common/debugstream.hh
usr/include/dune/common/densematrix.hh
usr/include/dune/common/densevector.hh
usr/include/dune/common/deprecated.hh
usr/include/dune/common/diagonalmatrix.hh
usr/include/dune/common/documentation.hh
usr/include/dune/common/dotproduct.hh
usr/include/dune/common/dynmatrix.hh
usr/include/dune/common/dynmatrixev.hh
usr/include/dune/common/dynvector.hh
usr/include/dune/common/enumset.hh
usr/include/dune/common/exceptions.hh
usr/include/dune/common/filledarray.hh
usr/include/dune/common/float_cmp.cc
usr/include/dune/common/float_cmp.hh
usr/include/dune/common/fmatrix.hh
usr/include/dune/common/fmatrixev.hh
usr/include/dune/common/ftraits.hh
usr/include/dune/common/function.hh
usr/include/dune/common/fvector.hh
usr/include/dune/common/gcd.hh
usr/include/dune/common/genericiterator.hh
usr/include/dune/common/gmpfield.hh
usr/include/dune/common/hash.hh
usr/include/dune/common/hybridutilities.hh
usr/include/dune/common/indent.hh
usr/include/dune/common/indices.hh
usr/include/dune/common/interfaces.hh
usr/include/dune/common/ios_state.hh
usr/include/dune/common/iteratorfacades.hh
usr/include/dune/common/iteratorrange.hh
usr/include/dune/common/keywords.hh
usr/include/dune/common/lcm.hh
usr/include/dune/common/lru.hh
usr/include/dune/common/mallocallocator.hh
usr/include/dune/common/math.hh
usr/include/dune/common/matvectraits.hh
usr/include/dune/common/overloadset.hh
usr/include/dune/common/parallel/
usr/include/dune/common/parallel/collectivecommunication.hh
usr/include/dune/common/parallel/communication.hh
usr/include/dune/common/parallel/communicator.hh
usr/include/dune/common/parallel/future.hh
usr/include/dune/common/parallel/indexset.hh
usr/include/dune/common/parallel/indicessyncer.hh
usr/include/dune/common/parallel/interface.hh
usr/include/dune/common/parallel/localindex.hh
usr/include/dune/common/parallel/mpicollectivecommunication.hh
usr/include/dune/common/parallel/mpicommunication.hh
usr/include/dune/common/parallel/mpidata.hh
usr/include/dune/common/parallel/mpifuture.hh
usr/include/dune/common/parallel/mpiguard.hh
usr/include/dune/common/parallel/mpihelper.hh
usr/include/dune/common/parallel/mpipack.hh
usr/include/dune/common/parallel/mpitraits.hh
usr/include/dune/common/parallel/plocalindex.hh
usr/include/dune/common/parallel/remoteindices.hh
usr/include/dune/common/parallel/selection.hh
usr/include/dune/common/parallel/variablesizecommunicator.hh
usr/include/dune/common/parameterizedobject.hh
usr/include/dune/common/parametertree.hh
usr/include/dune/common/parametertreeparser.hh
usr/include/dune/common/path.hh
usr/include/dune/common/poolallocator.hh
usr/include/dune/common/power.hh
usr/include/dune/common/precision.hh
usr/include/dune/common/promotiontraits.hh
usr/include/dune/common/propertymap.hh
usr/include/dune/common/proxymemberaccess.hh
usr/include/dune/common/quadmath.hh
usr/include/dune/common/rangeutilities.hh
usr/include/dune/common/reservedvector.hh
usr/include/dune/common/scalarmatrixview.hh
usr/include/dune/common/scalarvectorview.hh
usr/include/dune/common/shared_ptr.hh
usr/include/dune/common/simd/
usr/include/dune/common/simd.hh
usr/include/dune/common/simd/base.hh
usr/include/dune/common/simd/defaults.hh
usr/include/dune/common/simd/interface.hh
usr/include/dune/common/simd/io.hh
usr/include/dune/common/simd/loop.hh
usr/include/dune/common/simd/simd.hh
usr/include/dune/common/simd/standard.hh
usr/include/dune/common/simd/test.hh
usr/include/dune/common/simd/vc.hh
usr/include/dune/common/singleton.hh
usr/include/dune/common/sllist.hh
usr/include/dune/common/std/
usr/include/dune/common/std/apply.hh
usr/include/dune/common/std/functional.hh
usr/include/dune/common/std/make_array.hh
usr/include/dune/common/std/optional.hh
usr/include/dune/common/std/type_traits.hh
usr/include/dune/common/std/utility.hh
usr/include/dune/common/std/variant.hh
usr/include/dune/common/stdstreams.hh
usr/include/dune/common/stdthread.hh
usr/include/dune/common/streamoperators.hh
usr/include/dune/common/stringutility.hh
usr/include/dune/common/test/
usr/include/dune/common/test/arithmetictestsuite.hh
usr/include/dune/common/test/checkmatrixinterface.hh
usr/include/dune/common/test/collectorstream.hh
usr/include/dune/common/test/iteratortest.hh
usr/include/dune/common/test/testsuite.hh
usr/include/dune/common/timer.hh
usr/include/dune/common/to_unique_ptr.hh
usr/include/dune/common/transpose.hh
usr/include/dune/common/tupleutility.hh
usr/include/dune/common/tuplevector.hh
usr/include/dune/common/typelist.hh
usr/include/dune/common/typetraits.hh
usr/include/dune/common/typeutilities.hh
usr/include/dune/common/unused.hh
usr/include/dune/common/vc.hh
usr/include/dune/common/version.hh
usr/include/dune/common/visibility.hh
usr/include/dune/python/
usr/include/dune/python/common/
usr/include/dune/python/common/densematrix.hh
usr/include/dune/python/common/densevector.hh
usr/include/dune/python/common/dimrange.hh
usr/include/dune/python/common/dynmatrix.hh
usr/include/dune/python/common/dynvector.hh
usr/include/dune/python/common/fmatrix.hh
usr/include/dune/python/common/fvecmatregistry.hh
usr/include/dune/python/common/fvector.hh
usr/include/dune/python/common/getdimension.hh
usr/include/dune/python/common/logger.hh
usr/include/dune/python/common/mpihelper.hh
usr/include/dune/python/common/numpycommdatahandle.hh
usr/include/dune/python/common/numpyvector.hh
usr/include/dune/python/common/pythonvector.hh
usr/include/dune/python/common/string.hh
usr/include/dune/python/common/typeregistry.hh
usr/include/dune/python/common/vector.hh
usr/include/dune/python/pybind11/
usr/include/dune/python/pybind11/attr.h
usr/include/dune/python/pybind11/buffer_info.h
usr/include/dune/python/pybind11/cast.h
usr/include/dune/python/pybind11/chrono.h
usr/include/dune/python/pybind11/common.h
usr/include/dune/python/pybind11/complex.h
usr/include/dune/python/pybind11/detail/
usr/include/dune/python/pybind11/detail/class.h
usr/include/dune/python/pybind11/detail/common.h
usr/include/dune/python/pybind11/detail/descr.h
usr/include/dune/python/pybind11/detail/init.h
usr/include/dune/python/pybind11/detail/internals.h
usr/include/dune/python/pybind11/detail/typeid.h
usr/include/dune/python/pybind11/eigen.h
usr/include/dune/python/pybind11/embed.h
usr/include/dune/python/pybind11/eval.h
usr/include/dune/python/pybind11/extensions.h
usr/include/dune/python/pybind11/functional.h
usr/include/dune/python/pybind11/iostream.h
usr/include/dune/python/pybind11/numpy.h
usr/include/dune/python/pybind11/operators.h
usr/include/dune/python/pybind11/options.h
usr/include/dune/python/pybind11/pybind11.h
usr/include/dune/python/pybind11/pytypes.h
usr/include/dune/python/pybind11/stl.h
usr/include/dune/python/pybind11/stl_bind.h
usr/lib/
usr/lib/cmake/
usr/lib/cmake/dune-common/
usr/lib/cmake/dune-common/dune-common-config-version.cmake
usr/lib/cmake/dune-common/dune-common-config.cmake
usr/lib/cmake/dune-common/dune-common-targets-none.cmake
usr/lib/cmake/dune-common/dune-common-targets.cmake
usr/lib/dunecontrol/
usr/lib/dunecontrol/dune-common/
usr/lib/dunecontrol/dune-common/dune.module
usr/lib/dunemodules.lib
usr/lib/libdunecommon.so
usr/lib/pkgconfig/
usr/lib/pkgconfig/dune-common.pc
usr/python/
usr/python/dune/
usr/python/dune/common/
usr/python/dune/common/_common.so
usr/python/dune/typeregistry/
usr/python/dune/typeregistry/_typeregistry.so
usr/share/
usr/share/bash-completion/
usr/share/bash-completion/completions/
usr/share/bash-completion/completions/dunecontrol
usr/share/doc/
usr/share/doc/dune-common/
usr/share/doc/dune-common/buildsystem/
usr/share/doc/dune-common/buildsystem/index.html
usr/share/doc/dune-common/comm/
usr/share/doc/dune-common/comm/communication.pdf
usr/share/doc/dune-common/doxygen/
usr/share/doc/dune-common/doxygen/index.html
usr/share/doc/dune-common/dune-common.rst
usr/share/dune/
usr/share/dune-common/
usr/share/dune-common/config.h.cmake
usr/share/dune-common/doc/
usr/share/dune-common/doc/doxygen/
usr/share/dune-common/doc/doxygen/Doxystyle
usr/share/dune-common/doc/doxygen/doxygen-macros
usr/share/dune/cmake/
usr/share/dune/cmake/modules/
usr/share/dune/cmake/modules/AddBLASLapackFlags.cmake
usr/share/dune/cmake/modules/AddGMPFlags.cmake
usr/share/dune/cmake/modules/AddMETISFlags.cmake
usr/share/dune/cmake/modules/AddMPIFlags.cmake
usr/share/dune/cmake/modules/AddPTScotchFlags.cmake
usr/share/dune/cmake/modules/AddParMETISFlags.cmake
usr/share/dune/cmake/modules/AddQuadMathFlags.cmake
usr/share/dune/cmake/modules/AddSuiteSparseFlags.cmake
usr/share/dune/cmake/modules/AddTBBFlags.cmake
usr/share/dune/cmake/modules/AddThreadsFlags.cmake
usr/share/dune/cmake/modules/AddVcFlags.cmake
usr/share/dune/cmake/modules/CMakeBuiltinFunctionsDocumentation.cmake
usr/share/dune/cmake/modules/CheckCXXFeatures.cmake
usr/share/dune/cmake/modules/DuneAddPybind11Module.cmake
usr/share/dune/cmake/modules/DuneCMakeCompat.cmake
usr/share/dune/cmake/modules/DuneCommonMacros.cmake
usr/share/dune/cmake/modules/DuneDoc.cmake
usr/share/dune/cmake/modules/DuneDoxygen.cmake
usr/share/dune/cmake/modules/DuneEnableAllPackages.cmake
usr/share/dune/cmake/modules/DuneExecuteProcess.cmake
usr/share/dune/cmake/modules/DuneInstance.cmake
usr/share/dune/cmake/modules/DuneMPI.cmake
usr/share/dune/cmake/modules/DuneMacros.cmake
usr/share/dune/cmake/modules/DunePathHelper.cmake
usr/share/dune/cmake/modules/DunePkgConfig.cmake
usr/share/dune/cmake/modules/DunePythonCommonMacros.cmake
usr/share/dune/cmake/modules/DunePythonFindPackage.cmake
usr/share/dune/cmake/modules/DunePythonInstallPackage.cmake
usr/share/dune/cmake/modules/DunePythonMacros.cmake
usr/share/dune/cmake/modules/DunePythonTestCommand.cmake
usr/share/dune/cmake/modules/DunePythonVirtualenv.cmake
usr/share/dune/cmake/modules/DuneSphinxCMakeDoc.cmake
usr/share/dune/cmake/modules/DuneSphinxDoc.cmake
usr/share/dune/cmake/modules/DuneStreams.cmake
usr/share/dune/cmake/modules/DuneSymlinkOrCopy.cmake
usr/share/dune/cmake/modules/DuneTestMacros.cmake
usr/share/dune/cmake/modules/FindGMP.cmake
usr/share/dune/cmake/modules/FindInkscape.cmake
usr/share/dune/cmake/modules/FindLatexMk.cmake
usr/share/dune/cmake/modules/FindMETIS.cmake
usr/share/dune/cmake/modules/FindPTScotch.cmake
usr/share/dune/cmake/modules/FindParMETIS.cmake
usr/share/dune/cmake/modules/FindPkgConfig/
usr/share/dune/cmake/modules/FindPkgConfig/FindPkgConfig.cmake
usr/share/dune/cmake/modules/FindPython3/
usr/share/dune/cmake/modules/FindPython3/FindPython3.cmake
usr/share/dune/cmake/modules/FindPython3/Support.cmake
usr/share/dune/cmake/modules/FindQuadMath.cmake
usr/share/dune/cmake/modules/FindSphinx.cmake
usr/share/dune/cmake/modules/FindSuiteSparse.cmake
usr/share/dune/cmake/modules/FindTBB.cmake
usr/share/dune/cmake/modules/Headercheck.cmake
usr/share/dune/cmake/modules/OverloadCompilerFlags.cmake
usr/share/dune/cmake/modules/UseInkscape.cmake
usr/share/dune/cmake/modules/UseLatexMk.cmake
usr/share/dune/cmake/modules/latexmkrc.cmake
usr/share/dune/cmake/scripts/
usr/share/dune/cmake/scripts/CreateDoxyFile.cmake
usr/share/dune/cmake/scripts/FinalizeHeadercheck.cmake
usr/share/dune/cmake/scripts/InstallFile.cmake
usr/share/dune/cmake/scripts/RunDoxygen.cmake
usr/share/dune/cmake/scripts/conf.py.in
usr/share/dune/cmake/scripts/envdetect.py
usr/share/dune/cmake/scripts/extract_cmake_data.py
usr/share/dune/cmake/scripts/index.rst.in
usr/share/dune/cmake/scripts/main77.cc.in
usr/share/dune/cmake/scripts/module_library.cc.in
usr/share/dune/cmake/scripts/pyversion.py
usr/share/dune/cmake/scripts/run-in-dune-env.sh.in
usr/share/dune/cmake/scripts/sphinx_cmake_dune.py
usr/share/licenses/
usr/share/licenses/dune-common/
usr/share/licenses/dune-common/LICENSE
usr/share/man/
usr/share/man/man1/
usr/share/man/man1/dunecontrol.1.gz
```

```
usr/
usr/lib/
usr/lib/python3.9/
usr/lib/python3.9/site-packages/
usr/lib/python3.9/site-packages/dune/
usr/lib/python3.9/site-packages/dune/__main__.py
usr/lib/python3.9/site-packages/dune/common/
usr/lib/python3.9/site-packages/dune/common/__init__.py
usr/lib/python3.9/site-packages/dune/common/_common.so
usr/lib/python3.9/site-packages/dune/common/checkconfiguration.py
usr/lib/python3.9/site-packages/dune/common/compatibility.py
usr/lib/python3.9/site-packages/dune/common/deprecated.py
usr/lib/python3.9/site-packages/dune/common/hashit.py
usr/lib/python3.9/site-packages/dune/common/locking.py
usr/lib/python3.9/site-packages/dune/common/module.py
usr/lib/python3.9/site-packages/dune/common/pickle.py
usr/lib/python3.9/site-packages/dune/common/project.py
usr/lib/python3.9/site-packages/dune/common/utility.py
usr/lib/python3.9/site-packages/dune/create.py
usr/lib/python3.9/site-packages/dune/deprecate.py
usr/lib/python3.9/site-packages/dune/generator/
usr/lib/python3.9/site-packages/dune/generator/__init__.py
usr/lib/python3.9/site-packages/dune/generator/algorithm.py
usr/lib/python3.9/site-packages/dune/generator/builder.py
usr/lib/python3.9/site-packages/dune/generator/exceptions.py
usr/lib/python3.9/site-packages/dune/generator/generator.py
usr/lib/python3.9/site-packages/dune/generator/importclass.py
usr/lib/python3.9/site-packages/dune/packagemetadata.py
usr/lib/python3.9/site-packages/dune/plotting.py
usr/lib/python3.9/site-packages/dune/typeregistry/
usr/lib/python3.9/site-packages/dune/typeregistry/__init__.py
usr/lib/python3.9/site-packages/dune/typeregistry/_typeregistry.so
usr/lib/python3.9/site-packages/dune/utility.py
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9-nspkg.pth
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9.egg-info/
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9.egg-info/PKG-INFO
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9.egg-info/SOURCES.txt
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9.egg-info/dependency_links.txt
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9.egg-info/namespace_packages.txt
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9.egg-info/not-zip-safe
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9.egg-info/requires.txt
usr/lib/python3.9/site-packages/dune_common-2.8.0-py3.9.egg-info/top_level.txt
```

```
$ gpg --recv-keys 5006B177FD52742F
gpg: key 3F71FE0770D47FFB: public key "Markus Blatt (applied mathematician and DUNE core developer) <markus@dr-blatt.de>" imported
gpg: Total number processed: 1
gpg:               imported: 1
```

[](http://allanmcrae.com/2015/01/two-pgp-keyrings-for-package-management-in-arch-linux)
