## [`psurface`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/psurface/PKGBUILD)

[![Packaging status](
https://repology.org/badge/vertical-allrepos/psurface.svg
)](https://repology.org/project/psurface/versions)

[](https://cgit.freebsd.org/ports/tree/math/psurface)
[](https://www.freshports.org/math/psurface)
