## [`psurface`](https://github.com/m-reuter/arpackpp)

- Debian packages:
  - [libpsurface-dev](https://packages.debian.org/sid/libpsurface-dev)
  - [libpsurface-dbg](https://packages.debian.org/sid/libpsurface-dbg)
  - [psurface](https://packages.debian.org/sid/psurface)
- Ubuntu packages:
  - [psurface](https://packages.ubuntu.com/hirsute/psurface)
  - [psurface](https://launchpad.net/ubuntu/+source/psurface)
- OpenSUSE packages:
  - [psurface](https://build.opensuse.org/package/show/science/psurface)
- Arch Linux packages:
  - [psurface](https://aur.archlinux.org/packages/psurface)

[GSoC psurface-paraview](https://duneirisxiaoxue.blogspot.com)
[The PSurface library](https://link.springer.com/article/10.1007/s00791-013-0193-4)

[salsa.debian](https://salsa.debian.org/science-team/psurface.git)
[](https://assets.thermofisher.com/TFS-Assets/MSD/Product-Guides/users-guide-amira-software-2019.pdf)

```bash
prepare() {
  patch -d "${pkgname}-${pkgname}-${pkgver}" -p1 -i ../hdf5.patch
}
```

[Amira and Avizo](https://www.zib.de/software/amira)
[Amira-Avizo Software](https://www.fei.com/software/avizo3d/%C2%A0#gsc.tab=0)
[](https://www.researchgate.net/publication/334373689_High-Throughput_Segmentation_of_Tiled_Biological_Structures_using_Random_Walk_Distance_Transforms)
[](https://www.sciencedirect.com/science/article/pii/S2215016120301242)
[](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6907396)
[](https://elib.uni-stuttgart.de/handle/11682/10855)
