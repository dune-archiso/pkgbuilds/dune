- [GitHub](https://github.com/meowtec/Imagine)


```console
➜  prueba /tmp/prueba/Imagine-0.6.1.AppImage --appimage-extract
squashfs-root/.DirIcon
squashfs-root/AppRun
squashfs-root/LICENSE.electron.txt
squashfs-root/LICENSES.chromium.html
squashfs-root/chrome-sandbox
squashfs-root/chrome_100_percent.pak
squashfs-root/chrome_200_percent.pak
squashfs-root/icudtl.dat
squashfs-root/imagine
squashfs-root/imagine.desktop
squashfs-root/imagine.png
squashfs-root/libEGL.so
squashfs-root/libGLESv2.so
squashfs-root/libffmpeg.so
squashfs-root/libvk_swiftshader.so
squashfs-root/libvulkan.so
squashfs-root/locales
squashfs-root/locales/am.pak
squashfs-root/locales/ar.pak
squashfs-root/locales/bg.pak
squashfs-root/locales/bn.pak
squashfs-root/locales/ca.pak
squashfs-root/locales/cs.pak
squashfs-root/locales/da.pak
squashfs-root/locales/de.pak
squashfs-root/locales/el.pak
squashfs-root/locales/en-GB.pak
squashfs-root/locales/en-US.pak
squashfs-root/locales/es-419.pak
squashfs-root/locales/es.pak
squashfs-root/locales/et.pak
squashfs-root/locales/fa.pak
squashfs-root/locales/fi.pak
squashfs-root/locales/fil.pak
squashfs-root/locales/fr.pak
squashfs-root/locales/gu.pak
squashfs-root/locales/he.pak
squashfs-root/locales/hi.pak
squashfs-root/locales/hr.pak
squashfs-root/locales/hu.pak
squashfs-root/locales/id.pak
squashfs-root/locales/it.pak
squashfs-root/locales/ja.pak
squashfs-root/locales/kn.pak
squashfs-root/locales/ko.pak
squashfs-root/locales/lt.pak
squashfs-root/locales/lv.pak
squashfs-root/locales/ml.pak
squashfs-root/locales/mr.pak
squashfs-root/locales/ms.pak
squashfs-root/locales/nb.pak
squashfs-root/locales/nl.pak
squashfs-root/locales/pl.pak
squashfs-root/locales/pt-BR.pak
squashfs-root/locales/pt-PT.pak
squashfs-root/locales/ro.pak
squashfs-root/locales/ru.pak
squashfs-root/locales/sk.pak
squashfs-root/locales/sl.pak
squashfs-root/locales/sr.pak
squashfs-root/locales/sv.pak
squashfs-root/locales/sw.pak
squashfs-root/locales/ta.pak
squashfs-root/locales/te.pak
squashfs-root/locales/th.pak
squashfs-root/locales/tr.pak
squashfs-root/locales/uk.pak
squashfs-root/locales/vi.pak
squashfs-root/locales/zh-CN.pak
squashfs-root/locales/zh-TW.pak
squashfs-root/resources
squashfs-root/resources/app-update.yml
squashfs-root/resources/app.asar
squashfs-root/resources/app.asar.unpacked
squashfs-root/resources/app.asar.unpacked/bin
squashfs-root/resources/app.asar.unpacked/bin/linux
squashfs-root/resources/app.asar.unpacked/bin/linux/cwebp
squashfs-root/resources/app.asar.unpacked/bin/linux/libpng12.so.0
squashfs-root/resources/app.asar.unpacked/bin/linux/moz-cjpeg
squashfs-root/resources/app.asar.unpacked/bin/linux/pngquant
squashfs-root/resources.pak
squashfs-root/snapshot_blob.bin
squashfs-root/swiftshader
squashfs-root/swiftshader/libEGL.so
squashfs-root/swiftshader/libGLESv2.so
squashfs-root/usr
squashfs-root/usr/lib
squashfs-root/usr/lib/libXss.so.1
squashfs-root/usr/lib/libXtst.so.6
squashfs-root/usr/lib/libappindicator.so.1
squashfs-root/usr/lib/libgconf-2.so.4
squashfs-root/usr/lib/libindicator.so.7
squashfs-root/usr/lib/libnotify.so.4
squashfs-root/usr/share
squashfs-root/usr/share/icons
squashfs-root/usr/share/icons/hicolor
squashfs-root/usr/share/icons/hicolor/0x0
squashfs-root/usr/share/icons/hicolor/0x0/apps
squashfs-root/usr/share/icons/hicolor/0x0/apps/imagine.png
squashfs-root/v8_context_snapshot.bin
squashfs-root/vk_swiftshader_icd.json
```

```console
imagine-git /opt/
imagine-git /opt/imagine-git/
imagine-git /opt/imagine-git/LICENSE.electron.txt
imagine-git /opt/imagine-git/LICENSES.chromium.html
imagine-git /opt/imagine-git/chrome-sandbox
imagine-git /opt/imagine-git/chrome_100_percent.pak
imagine-git /opt/imagine-git/chrome_200_percent.pak
imagine-git /opt/imagine-git/icudtl.dat
imagine-git /opt/imagine-git/imagine
imagine-git /opt/imagine-git/libEGL.so
imagine-git /opt/imagine-git/libGLESv2.so
imagine-git /opt/imagine-git/libffmpeg.so
imagine-git /opt/imagine-git/locales/
imagine-git /opt/imagine-git/locales/am.pak
imagine-git /opt/imagine-git/locales/ar.pak
imagine-git /opt/imagine-git/locales/bg.pak
imagine-git /opt/imagine-git/locales/bn.pak
imagine-git /opt/imagine-git/locales/ca.pak
imagine-git /opt/imagine-git/locales/cs.pak
imagine-git /opt/imagine-git/locales/da.pak
imagine-git /opt/imagine-git/locales/de.pak
imagine-git /opt/imagine-git/locales/el.pak
imagine-git /opt/imagine-git/locales/en-GB.pak
imagine-git /opt/imagine-git/locales/en-US.pak
imagine-git /opt/imagine-git/locales/es-419.pak
imagine-git /opt/imagine-git/locales/es.pak
imagine-git /opt/imagine-git/locales/et.pak
imagine-git /opt/imagine-git/locales/fa.pak
imagine-git /opt/imagine-git/locales/fi.pak
imagine-git /opt/imagine-git/locales/fil.pak
imagine-git /opt/imagine-git/locales/fr.pak
imagine-git /opt/imagine-git/locales/gu.pak
imagine-git /opt/imagine-git/locales/he.pak
imagine-git /opt/imagine-git/locales/hi.pak
imagine-git /opt/imagine-git/locales/hr.pak
imagine-git /opt/imagine-git/locales/hu.pak
imagine-git /opt/imagine-git/locales/id.pak
imagine-git /opt/imagine-git/locales/it.pak
imagine-git /opt/imagine-git/locales/ja.pak
imagine-git /opt/imagine-git/locales/kn.pak
imagine-git /opt/imagine-git/locales/ko.pak
imagine-git /opt/imagine-git/locales/lt.pak
imagine-git /opt/imagine-git/locales/lv.pak
imagine-git /opt/imagine-git/locales/ml.pak
imagine-git /opt/imagine-git/locales/mr.pak
imagine-git /opt/imagine-git/locales/ms.pak
imagine-git /opt/imagine-git/locales/nb.pak
imagine-git /opt/imagine-git/locales/nl.pak
imagine-git /opt/imagine-git/locales/pl.pak
imagine-git /opt/imagine-git/locales/pt-BR.pak
imagine-git /opt/imagine-git/locales/pt-PT.pak
imagine-git /opt/imagine-git/locales/ro.pak
imagine-git /opt/imagine-git/locales/ru.pak
imagine-git /opt/imagine-git/locales/sk.pak
imagine-git /opt/imagine-git/locales/sl.pak
imagine-git /opt/imagine-git/locales/sr.pak
imagine-git /opt/imagine-git/locales/sv.pak
imagine-git /opt/imagine-git/locales/sw.pak
imagine-git /opt/imagine-git/locales/ta.pak
imagine-git /opt/imagine-git/locales/te.pak
imagine-git /opt/imagine-git/locales/th.pak
imagine-git /opt/imagine-git/locales/tr.pak
imagine-git /opt/imagine-git/locales/uk.pak
imagine-git /opt/imagine-git/locales/vi.pak
imagine-git /opt/imagine-git/locales/zh-CN.pak
imagine-git /opt/imagine-git/locales/zh-TW.pak
imagine-git /opt/imagine-git/natives_blob.bin
imagine-git /opt/imagine-git/resources.pak
imagine-git /opt/imagine-git/resources/
imagine-git /opt/imagine-git/resources/app.asar
imagine-git /opt/imagine-git/resources/app.asar.unpacked/
imagine-git /opt/imagine-git/resources/app.asar.unpacked/bin/
imagine-git /opt/imagine-git/resources/app.asar.unpacked/bin/linux/
imagine-git /opt/imagine-git/resources/app.asar.unpacked/bin/linux/cwebp
imagine-git /opt/imagine-git/resources/app.asar.unpacked/bin/linux/libpng12.so.0
imagine-git /opt/imagine-git/resources/app.asar.unpacked/bin/linux/moz-cjpeg
imagine-git /opt/imagine-git/resources/app.asar.unpacked/bin/linux/pngquant
imagine-git /opt/imagine-git/snapshot_blob.bin
imagine-git /opt/imagine-git/swiftshader/
imagine-git /opt/imagine-git/swiftshader/libEGL.so
imagine-git /opt/imagine-git/swiftshader/libGLESv2.so
imagine-git /opt/imagine-git/swiftshader/libvk_swiftshader.so
imagine-git /opt/imagine-git/v8_context_snapshot.bin
imagine-git /usr/
imagine-git /usr/bin/
imagine-git /usr/bin/imagine-git
imagine-git /usr/share/
imagine-git /usr/share/applications/
imagine-git /usr/share/applications/imagine-git.desktop
imagine-git /usr/share/icons/
imagine-git /usr/share/icons/hicolor/
imagine-git /usr/share/icons/hicolor/128x128/
imagine-git /usr/share/icons/hicolor/128x128/apps/
imagine-git /usr/share/icons/hicolor/128x128/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/16x16/
imagine-git /usr/share/icons/hicolor/16x16/apps/
imagine-git /usr/share/icons/hicolor/16x16/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/192x192/
imagine-git /usr/share/icons/hicolor/192x192/apps/
imagine-git /usr/share/icons/hicolor/192x192/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/20x20/
imagine-git /usr/share/icons/hicolor/20x20/apps/
imagine-git /usr/share/icons/hicolor/20x20/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/22x22/
imagine-git /usr/share/icons/hicolor/22x22/apps/
imagine-git /usr/share/icons/hicolor/22x22/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/24x24/
imagine-git /usr/share/icons/hicolor/24x24/apps/
imagine-git /usr/share/icons/hicolor/24x24/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/32x32/
imagine-git /usr/share/icons/hicolor/32x32/apps/
imagine-git /usr/share/icons/hicolor/32x32/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/48x48/
imagine-git /usr/share/icons/hicolor/48x48/apps/
imagine-git /usr/share/icons/hicolor/48x48/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/64x64/
imagine-git /usr/share/icons/hicolor/64x64/apps/
imagine-git /usr/share/icons/hicolor/64x64/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/8x8/
imagine-git /usr/share/icons/hicolor/8x8/apps/
imagine-git /usr/share/icons/hicolor/8x8/apps/imagine-git.png
imagine-git /usr/share/icons/hicolor/96x96/
imagine-git /usr/share/icons/hicolor/96x96/apps/
imagine-git /usr/share/icons/hicolor/96x96/apps/imagine-git.png
imagine-git /usr/share/licenses/
imagine-git /usr/share/licenses/imagine-git/
imagine-git /usr/share/licenses/imagine-git/LICENSE
```
