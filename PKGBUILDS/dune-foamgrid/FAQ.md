## [`dune-common`]( https://gitlab.com/dune-archiso/dune-core/-/raw/main/dune-common/PKGBUILD)

When the package `dune-common-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `bin`
- [x] `include`
- [x] `lib`
- [x] `share`
  - [x] `bash-completion`
  - [x] `doc`
  - [x] `dune`
  - [x] `dune-common`
  - [x] `licenses`
  - [x] `man`

```console
usr/
├── bin
│   ├── dunecontrol
│   ├── dune-ctest
│   ├── dune-git-whitespace-hook
│   └── duneproject
├── include
│   └── dune
│       └── common
├── lib
│   ├── cmake
│   │   └── dune-common
│   ├── dunecontrol
│   │   └── dune-common
│   │       └── dune.module
│   ├── dunemodules.lib
│   ├── libdunecommon.a
│   └── pkgconfig
│       └── dune-common.pc
└── share
    ├── bash-completion
    │   └── completions
    │       └── dunecontrol
    ├── doc
    │   └── dune-common
    ├── dune
    │   └── cmake
    │       ├── modules
    │       └── scripts
    ├── dune-common
    │   ├── config.h.cmake
    │   └── doc
    │       └── doxygen
    ├── licenses
    │   └── dune-common
    └── man
        └── man1

31 directories, 1492 files
```
https://journals.ub.uni-heidelberg.de/index.php/ans/article/view/28490

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doxyfile
doxygen_dune-foamgrid
doxygen_install
headercheck
install_python
test_python
boundary-segment-test
elementparametrization
foamgrid-test
global-refine-test
growth-test-1d
growth-test-2d
local-refine-test
parametrized-refinement
setposition-test
```

```console
/usr/lib/python3.9/site-packages/setuptools/dist.py:498: UserWarning: The version specified ('2.7-git') is an invalid version, this may not work as expected with newer versions of setuptools, pip, and PyPI. Please see PEP 440 for more details.
```

[](https://stackoverflow.com/questions/584894/environment-variable-substitution-in-sed)
