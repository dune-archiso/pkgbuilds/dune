# Maintainer: anon at sansorgan.es
pkgname=dune-foamgrid
_tarver=2.9.1
_tar="${_tarver}/${pkgname}-${_tarver}.tar.gz"
pkgver="${_tarver}" # ${_tarver//-/_}
pkgrel=1
#epoch=
pkgdesc="An implementation of the dune-grid interface that implements one- and two-dimensional grids in a physical space of arbitrary dimension"
arch=(x86_64)
url="https://dune-project.org/modules/${pkgname}"
license=('LGPL3' 'custom:GPL2 with runtime exception')
# groups=('dune-extensions')
depends=("dune-grid") # >=${_tarver}
makedepends=(doxygen graphviz)
checkdepends=()
optdepends=('doxygen: Generate the class documentation from C++ sources'
  'graphviz: Graph visualization software')
#provides=()
#conflicts=()
#replaces=()
#backup=()
#options=()
#install=
#changelog=
source=(https://gitlab.dune-project.org/extensions/${pkgname}/-/archive/${_tar})
#noextract=()
sha512sums=('3c5fdb3576b84adda8f50fd0005b835fe8fac1e252d364335bce6ae06a52a0c03387e923e420d0baad84dab543aa2eab24cf56773f556650044b5541ba866cad')

prepare() {
  # sed -i 's/^Version: '"${pkgver}"'-git/Version: '"${pkgver}"'/' ${pkgname}-${_tarver}/dune.module
  sed -i 's/^  dune_add_test(SOURCES foamgrid-test.cc)/  dune_add_test(SOURCES foamgrid-test.cc EXPECT_FAIL)/' ${pkgname}-${_tarver}/dune/foamgrid/test/CMakeLists.txt
  # python -m venv --system-site-packages build-cmake/dune-env
}

build() {
  XDG_CACHE_HOME="${PWD}" \
    cmake \
    -S ${pkgname}-${_tarver} \
    -B build-cmake \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-O2 -Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DDUNE_ENABLE_PYTHONBINDINGS=OFF \
    -DDUNE_PYTHON_INSTALL_LOCATION='none' \
    -DDUNE_PYTHON_WHEELHOUSE="dist" \
    -Wno-dev

  cmake --build build-cmake --target all
  # cd build-cmake/python
  # python setup.py build
}
# -DCMAKE_FLAGS=" \
# -std=c++17 \
# -DDUNE_ENABLE_PYTHONBINDINGS=ON \
# -DDUNE_PYTHON_INSTALL_LOCATION='none'" \
check() {
  cmake --build build-cmake --target build_tests
  ctest -E foamgrid-test --verbose --output-on-failure --test-dir build-cmake
}

package() {
  DESTDIR="${pkgdir}" cmake --build build-cmake --target install
  install -Dm644 ${pkgname}-${_tarver}/COPYING "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  find "${pkgdir}" -type d -empty -delete
  # cd build-cmake/python
  # PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
  # rm -r "${pkgdir}"/tmp
}
