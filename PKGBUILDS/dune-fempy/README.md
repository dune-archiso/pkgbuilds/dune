[](https://gitlab.dune-project.org/dune-fem/dune-fempy/-/tags/v2.8.0.0)

e4ec6cb9b9cee1403da47adc85d7cc572cc876b7f5faa072a46cb7af2c834ae2a4e86bfc659d3c9ba7aac84255b8daae1bee961eabbd126c4b3dd3b23eeeb88d

Version: 2.8-git

Depends: dune-common (>= 2.8)

<!-- [![Packaging status](
https://repology.org/badge/vertical-allrepos/dune-fempy.svg
)](https://repology.org/project/dune-fempy/versions) -->
