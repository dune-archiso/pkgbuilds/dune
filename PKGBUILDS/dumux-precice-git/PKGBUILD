# Maintainer: anon at sansorgan.es
_base=dumux-precice
pkgname=${_base}-git
pkgver=0.1.r161.gdc1ff9d
pkgrel=1
pkgdesc="DuMuX-preCICE adapter"
arch=('x86_64')
url="https://github.com/precice/${_base/precice/adapter}"
license=('GPL3')
depends=(dumux precice)
makedepends=(git)
# checkdepends=(dune-subgrid dune-spgrid dune-mmesh opm-grid)
# optdepends=() # arpackpp onetbb
source=(${_base}::git+${url}.git#branch=main)
sha512sums=('SKIP')
provides=("${_base}=${pkgver%%.r*}")
conflicts=(${_base})

pkgver() {
  cd ${_base}
  git describe --long --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
  sed -i 's/DumuxPreciceTestMacros.cmake/#DumuxPreciceTestMacros.cmake/' ${_base}/cmake/modules/CMakeLists.txt
  sed -i '2 a 	dumuxpreciceindexmapper.hh' ${_base}/${_base}/CMakeLists.txt
  # sed -i 's/min-iteration-convergence-measure min-iterations/min-iterations value/' ${_base}/examples/dummysolver/precice-dummy-solver-config.xml
  # sed -i 's/mesh="MeshOne"\/>/mesh="MeshOne"\/>-->/' ${_base}/examples/dummysolver/precice-dummy-solver-config.xml
  # https://stackoverflow.com/a/50949315/9302545
  sed -i 's/return/exit/' ${_base}/test/return-test-passed.sh
  # sed -i '3 a 	dune_symlink_to_source_files(FILES params-solver-a.input)' ${_base}/examples/dummysolver/CMakeLists.txt
  export DUNE_LOG_LEVEL=DEBUG
  export DUNE_SAVE_BUILD='console'
}

build() {
  cmake \
    -S ${_base} \
    -B build \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_SHARED_LIBS=TRUE \
    -DCMAKE_CXX_STANDARD=17 \
    -DCMAKE_C_COMPILER=gcc \
    -DCMAKE_CXX_COMPILER=g++ \
    -DCMAKE_C_FLAGS='-Wall -fdiagnostics-color=always' \
    -DCMAKE_CXX_FLAGS="-Wall -fdiagnostics-color=always -mavx" \
    -DCMAKE_VERBOSE_MAKEFILE=ON \
    -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \
    -DENABLE_HEADERCHECK=ON \
    -DDUNE_ENABLE_PYTHONBINDINGS=ON \
    -DDUNE_PYTHON_INSTALL_LOCATION='none' \
    -DCMAKE_DISABLE_FIND_PACKAGE_PTScotch=TRUE \
    -Wno-dev
  # sed -i 's/^include-system-site-packages = false/include-system-site-packages = true/' build/dune-env/pyvenv.cfg
  # cat build/dune-env/pyvenv.cfg
  cmake --build build --target all
  # cd build/python
  # python setup.py build
}

check() {
  cmake --build build --target build_tests
  ctest --verbose --output-on-failure --test-dir build
}

package() {
  DESTDIR="${pkgdir}" cmake --build build --target install
  install -Dm 644 ${_base}/LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
  find "${pkgdir}" -type d -empty -delete
}
