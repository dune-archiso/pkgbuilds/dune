```
-- The CXX compiler identification is GNU 12.1.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/sbin/g++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Looking for C++ include pthread.h
-- Looking for C++ include pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Success
-- Found Threads: TRUE  
-- The C compiler identification is GNU 12.1.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/sbin/gcc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Dependencies for dumux-precice: dumux (>=3.2)
-- Suggestions for dumux-precice: dune-subgrid
-- Dependencies for dumux: dune-common (>=2.8);dune-grid (>=2.8);dune-localfunctions (>=2.8);dune-istl (>=2.8)
-- Suggestions for dumux: dune-alugrid (>=2.8);dune-foamgrid (>=2.8);dune-uggrid (>=2.8);dune-functions (>=2.8);opm-common;opm-grid;dune-subgrid (>=2.8);dune-spgrid (>=2.8);dune-mmesh (>=1.2)
-- Dependencies for dumux: dune-common (>=2.8);dune-grid (>=2.8);dune-localfunctions (>=2.8);dune-istl (>=2.8)
-- Suggestions for dumux: dune-alugrid (>=2.8);dune-foamgrid (>=2.8);dune-uggrid (>=2.8);dune-functions (>=2.8);opm-common;opm-grid;dune-subgrid (>=2.8);dune-spgrid (>=2.8);dune-mmesh (>=1.2)
-- Could NOT find dune-subgrid (missing: dune-subgrid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Found PkgConfig: /usr/sbin/pkg-config (found version "1.8.0") 
-- Checking for module 'dune-subgrid '
--   Package 'dune-subgrid', required by 'virtual:world', not found
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-istl: dune-common (>= 2.8)
-- Dependencies for dune-istl: dune-common (>= 2.8)
-- Dependencies for dune-alugrid: dune-grid (>= 2.8)
-- Dependencies for dune-alugrid: dune-grid (>= 2.8)
-- Dependencies for dune-foamgrid: dune-common (>= 2.6);dune-geometry (>= 2.6);dune-grid (>= 2.6)
-- Suggestions for dune-foamgrid: dune-python
-- Dependencies for dune-foamgrid: dune-common (>= 2.6);dune-geometry (>= 2.6);dune-grid (>= 2.6)
-- Suggestions for dune-foamgrid: dune-python
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-functions: dune-localfunctions (>= 2.8);dune-grid (>= 2.8);dune-istl (>= 2.8);dune-typetree (>= 2.8)
-- Dependencies for dune-functions: dune-localfunctions (>= 2.8);dune-grid (>= 2.8);dune-istl (>= 2.8);dune-typetree (>= 2.8)
-- Could NOT find opm-common (missing: opm-common_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'opm-common '
--   Package 'opm-common', required by 'virtual:world', not found
-- Could NOT find opm-grid (missing: opm-grid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'opm-grid '
--   Package 'opm-grid', required by 'virtual:world', not found
-- Could NOT find dune-subgrid (missing: dune-subgrid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-subgrid>=2.8'
--   Package 'dune-subgrid', required by 'virtual:world', not found
-- Could NOT find dune-spgrid (missing: dune-spgrid_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-spgrid>=2.8'
--   Package 'dune-spgrid', required by 'virtual:world', not found
-- Could NOT find dune-mmesh (missing: dune-mmesh_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-mmesh>=1.2'
--   Package 'dune-mmesh', required by 'virtual:world', not found
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-localfunctions: dune-geometry (>= 2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-istl: dune-common (>= 2.8)
-- Dependencies for dune-istl: dune-common (>= 2.8)
-- Dependencies for dune-typetree: dune-common (>= 2.8)
-- Dependencies for dune-typetree: dune-common (>= 2.8)
-- Could NOT find dune-python (missing: dune-python_DIR)
-- No full CMake package configuration support available. Falling back to pkg-config.
-- Checking for module 'dune-python '
--   Package 'dune-python', required by 'virtual:world', not found
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-grid: dune-geometry (>= 2.8)
-- Suggestions for dune-grid: dune-uggrid (>=2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-uggrid: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Dependencies for dune-geometry: dune-common (>= 2.8)
-- Performing Test cxx_std_flag_17
-- Performing Test cxx_std_flag_17 - Success
-- Performing Test compiler_supports_cxx17
-- Performing Test compiler_supports_cxx17 - Success
-- Performing Test HAS_ATTRIBUTE_UNUSED
-- Performing Test HAS_ATTRIBUTE_UNUSED - Success
-- Performing Test HAS_ATTRIBUTE_DEPRECATED
-- Performing Test HAS_ATTRIBUTE_DEPRECATED - Success
-- Performing Test HAS_ATTRIBUTE_DEPRECATED_MSG
-- Performing Test HAS_ATTRIBUTE_DEPRECATED_MSG - Success
-- Looking for std::experimental::make_array<int,int>
-- Looking for std::experimental::make_array<int,int> - found
-- Looking for std::move<std::experimental::detected_t<std::decay_t,int>>
-- Looking for std::move<std::experimental::detected_t<std::decay_t,int>> - found
-- Looking for std::identity
-- Looking for std::identity - not found
-- Could NOT find LATEX (missing: LATEX_COMPILER) 
-- Could NOT find LatexMk (missing: LATEXMK_EXECUTABLE) 
-- Could NOT find Sphinx (missing: SPHINX_EXECUTABLE) 
-- Could NOT find Doxygen (missing: DOXYGEN_EXECUTABLE) 
-- Searching for macro file 'DuneCommonMacros' for module 'dune-common'.
-- Performing tests specific to dune-common from file /usr/share/dune/cmake/modules/DuneCommonMacros.cmake.
-- Set Minimal Debug Level to 4
-- Looking for sgemm_
-- Looking for sgemm_ - not found
-- Looking for sgemm_
-- Looking for sgemm_ - found
-- Found BLAS: /usr/lib/libblas.so  
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - not found
-- Looking for cheev_
-- Looking for cheev_ - found
-- Found LAPACK: /usr/lib/liblapack.so;/usr/lib/libblas.so  
-- Looking for dsyev_
-- Looking for dsyev_ - found
-- Found GMP: /usr/lib/libgmpxx.so  
-- Performing Test QuadMath_COMPILES
-- Performing Test QuadMath_COMPILES - Success
-- Found QuadMath: (Supported by compiler)  
-- Found MPI_C: /usr/lib/openmpi/libmpi.so (found suitable version "3.1", minimum required is "3.0") 
-- Found MPI: TRUE (found suitable version "3.1", minimum required is "3.0") found components: C 
-- Could NOT find TBB (set TBB_DIR to path containing TBBConfig.cmake or set PKG_CONFIG_PATH to include the location of the tbb.pc file) (missing: PkgConfigTBB_LINK_LIBRARIES PkgConfigTBB_FOUND) (found version "")
-- Found METIS: /usr/lib/libmetis.so (found version "5.1") 
-- Found METIS: /usr/lib/libmetis.so (found suitable version "5.1", minimum required is "5.0") 
-- Found MPI_C: /usr/lib/openmpi/libmpi.so (found version "3.1") 
-- Found MPI: TRUE (found version "3.1") found components: C 
-- Found ParMETIS: /usr/lib/libparmetis.so (found suitable version "4.0", minimum required is "4.0") 
-- Found Python3: /usr/sbin/python3 (found version "3.10.4") found components: Interpreter Development Development.Module Development.Embed 
-- Failed to find the python package pip with interpreter /usr/sbin/python3. (missing: DUNE_PYTHON_pip_FOUND) 
-- Setting dune-common_INCLUDE_DIRS=/usr/include
-- Setting dune-common_LIBRARIES=dunecommon
-- Searching for macro file 'DuneGeometryMacros' for module 'dune-geometry'.
-- No module specific tests performed for module 'dune-geometry' because macro file 'DuneGeometryMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Setting dune-geometry_INCLUDE_DIRS=/usr/include
-- Setting dune-geometry_LIBRARIES=dunegeometry
-- Searching for macro file 'DuneUggridMacros' for module 'dune-uggrid'.
-- Performing tests specific to dune-uggrid from file /usr/share/dune/cmake/modules/DuneUggridMacros.cmake.
-- Setting dune-uggrid_INCLUDE_DIRS=/usr/include
-- Setting dune-uggrid_LIBRARIES=duneuggrid
-- Searching for macro file 'DuneGridMacros' for module 'dune-grid'.
-- Performing tests specific to dune-grid from file /usr/share/dune/cmake/modules/DuneGridMacros.cmake.
-- Looking for mkstemp
-- Looking for mkstemp - found
-- Found METIS: /usr/lib/libmetis.so (found version "5.1") 
-- Found METIS: /usr/lib/libmetis.so (found suitable version "5.1", minimum required is "5.0") 
-- Found ParMETIS: /usr/lib/libparmetis.so (found version "4.0") 
-- Checking for module 'alberta-grid_1d>=3.0'
--   Package 'alberta-grid_1d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_2d>=3.0'
--   Package 'alberta-grid_2d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_3d>=3.0'
--   Package 'alberta-grid_3d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_4d>=3.0'
--   Package 'alberta-grid_4d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_5d>=3.0'
--   Package 'alberta-grid_5d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_6d>=3.0'
--   Package 'alberta-grid_6d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_7d>=3.0'
--   Package 'alberta-grid_7d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_8d>=3.0'
--   Package 'alberta-grid_8d', required by 'virtual:world', not found
-- Checking for module 'alberta-grid_9d>=3.0'
--   Package 'alberta-grid_9d', required by 'virtual:world', not found
-- Could NOT find Alberta (set PKG_CONFIG_PATH to include the location of the alberta-grid_[n]d.pc files) (missing: ALBERTA_GRID_PREFIX) (Required is at least version "3.0")
-- Checking for one of the modules 'psurface'
-- Checking for module 'psurface'
--   Package 'psurface', required by 'virtual:world', not found
-- Could NOT find Psurface (missing: PSURFACE_INCLUDE_DIR PSURFACE_LIBRARY) 
-- Could NOT find AmiraMesh (missing: AMIRAMESH_INCLUDE_DIR AMIRAMESH_LIBRARY) 
-- Setting dune-grid_INCLUDE_DIRS=/usr/include
-- Setting dune-grid_LIBRARIES=dunegrid
-- Searching for macro file 'DunePythonMacros' for module 'dune-python'.
-- Performing tests specific to dune-python from file /usr/share/dune/cmake/modules/DunePythonMacros.cmake.
-- Searching for macro file 'DuneTypetreeMacros' for module 'dune-typetree'.
-- No module specific tests performed for module 'dune-typetree' because macro file 'DuneTypetreeMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Setting dune-typetree_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneIstlMacros' for module 'dune-istl'.
-- Performing tests specific to dune-istl from file /usr/share/dune/cmake/modules/DuneIstlMacros.cmake.
-- Found METIS: /usr/lib/libmetis.so (found version "5.1") 
-- Found METIS: /usr/lib/libmetis.so (found suitable version "5.1", minimum required is "5.0") 
-- Found SuperLU: /usr/lib/libsuperlu.so (found suitable version "5.3.0", minimum required is "5.0") 
-- Found ARPACK: /usr/lib/libarpack.so  
-- Could NOT find ARPACKPP (missing: ARPACKPP_INCLUDE_DIR) 
-- Found SuiteSparse: /usr/lib/libsuitesparseconfig.so (found version "5.12.0") found components: CHOLMOD LDL SPQR UMFPACK 
-- Setting dune-istl_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneLocalfunctionsMacros' for module 'dune-localfunctions'.
-- No module specific tests performed for module 'dune-localfunctions' because macro file 'DuneLocalfunctionsMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Setting dune-localfunctions_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneMmeshMacros' for module 'dune-mmesh'.
-- No module specific tests performed for module 'dune-mmesh' because macro file 'DuneMmeshMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneSpgridMacros' for module 'dune-spgrid'.
-- No module specific tests performed for module 'dune-spgrid' because macro file 'DuneSpgridMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneSubgridMacros' for module 'dune-subgrid'.
-- No module specific tests performed for module 'dune-subgrid' because macro file 'DuneSubgridMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'OpmGridMacros' for module 'opm-grid'.
-- No module specific tests performed for module 'opm-grid' because macro file 'OpmGridMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'OpmCommonMacros' for module 'opm-common'.
-- No module specific tests performed for module 'opm-common' because macro file 'OpmCommonMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Searching for macro file 'DuneFunctionsMacros' for module 'dune-functions'.
-- No module specific tests performed for module 'dune-functions' because macro file 'DuneFunctionsMacros.cmake' not found in /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules;/usr/share/dune/cmake/modules.
-- Setting dune-functions_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneFoamgridMacros' for module 'dune-foamgrid'.
-- Performing tests specific to dune-foamgrid from file /usr/share/dune/cmake/modules/DuneFoamgridMacros.cmake.
-- Setting dune-foamgrid_INCLUDE_DIRS=/usr/include
-- Searching for macro file 'DuneAlugridMacros' for module 'dune-alugrid'.
-- Performing tests specific to dune-alugrid from file /usr/share/dune/cmake/modules/DuneAlugridMacros.cmake.
-- Found ZLIB: /usr/lib/libz.so (found version "1.2.12") 
-- Looking for include file sion.h
-- Looking for include file sion.h - not found
-- Could NOT find SIONlib (missing: SIONLIB_INCLUDE_DIR SIONLIB_LIBRARY SIONLIB_HEADER_USABLE SIONLIB_LIB_WORKS SIONLIB_LIB_SIONSER_WORKS) 
-- Could NOT find DLMalloc (missing: DLMALLOC_INCLUDE_DIR DLMALLOC_SOURCE_INCLUDE) 
Found /usr/include
Found /usr/lib/libzoltan.so
-- Found METIS: /usr/lib/libmetis.so (found version "5.1") 
-- Performing Test DUNE_CV_PTHREAD_TLS_COMPILES
-- Performing Test DUNE_CV_PTHREAD_TLS_COMPILES - Failed
-- Not enabling torture-tests
-- Setting dune-alugrid_INCLUDE_DIRS=/usr/include
-- Setting dune-alugrid_LIBRARIES=dunealugrid
-- Searching for macro file 'DumuxMacros' for module 'dumux'.
-- Performing tests specific to dumux from file /usr/share/dune/cmake/modules/DumuxMacros.cmake.
-- Could NOT find TBB (set TBB_DIR to path containing TBBConfig.cmake or set PKG_CONFIG_PATH to include the location of the tbb.pc file) (missing: PkgConfigTBB_LINK_LIBRARIES PkgConfigTBB_FOUND) (found version "")
-- Looking for std::execution::par_unseq
-- Looking for std::execution::par_unseq - found
-- Dumux multithreading backed: OpenMP
-- Setting dumux_INCLUDE_DIRS=/usr/include
-- Setting dumux_LIBRARIES=dumux_fmt;dumux_parameters
-- Searching for macro file 'DumuxPreciceMacros' for module 'dumux-precice'.
-- Performing tests specific to dumux-precice from file /tmp/makepkg/dumux-precice-git/src/dumux-precice/cmake/modules/DumuxPreciceMacros.cmake.
-- Using scripts from /usr/share/dune/cmake/scripts for creating doxygen stuff.
-- LIBRARY_DIRS: 
-- Found Boost: /usr/lib64/cmake/Boost-1.78.0/BoostConfig.cmake (found suitable version "1.78.0", minimum required is "1.65.1") found components: log system 
-- LIBRARY_DIRS: 
CMake Warning at /usr/share/dune/cmake/modules/DuneTestMacros.cmake:264 (message):
  Unrecognized arguments ('test_pm_flow_over_square_2d') for dune_add_test!
Call Stack (most recent call first):
  /usr/share/dune/cmake/modules/DumuxTestMacros.cmake:207 (dune_add_test)
  test/partitioned/flow-over-square-2d/CMakeLists.txt:75 (dumux_add_test)


CMake Warning at /usr/share/dune/cmake/modules/DuneTestMacros.cmake:264 (message):
  Unrecognized arguments ('test_pm_flow_over_square_2d') for dune_add_test!
Call Stack (most recent call first):
  /usr/share/dune/cmake/modules/DumuxTestMacros.cmake:207 (dune_add_test)
  test/partitioned/flow-over-square-2d/CMakeLists.txt:106 (dumux_add_test)


CMake Warning at /usr/share/dune/cmake/modules/DuneTestMacros.cmake:264 (message):
  Unrecognized arguments ('test_pm_flow_over_square_2d') for dune_add_test!
Call Stack (most recent call first):
  /usr/share/dune/cmake/modules/DumuxTestMacros.cmake:207 (dune_add_test)
  test/partitioned/flow-over-square-2d/CMakeLists.txt:127 (dumux_add_test)


CMake Warning at /usr/share/dune/cmake/modules/DuneTestMacros.cmake:264 (message):
  Unrecognized arguments ('test_pm_flow_over_square_2d') for dune_add_test!
Call Stack (most recent call first):
  /usr/share/dune/cmake/modules/DumuxTestMacros.cmake:207 (dune_add_test)
  test/partitioned/flow-over-square-2d/CMakeLists.txt:148 (dumux_add_test)


-- Adding custom target for config.h generation
-- The following OPTIONAL packages have been found:

 * dune-alugrid
 * dune-foamgrid
 * dune-uggrid
 * dune-functions
 * dune-typetree
 * GMP, GNU multi-precision library, <https://gmplib.org>
 * QuadMath, GCC Quad-Precision Math Library, <https://gcc.gnu.org/onlinedocs/libquadmath>
 * Python3
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * ParMETIS, Parallel Graph Partitioning
 * SuperLU (required version >= 5.0), Supernodal LU
   Direct solver for linear system, based on LU decomposition
 * ARPACK, ARnoldi PACKage
   Solve large scale eigenvalue problems
 * SuiteSparse, A suite of sparse matrix software, <http://faculty.cse.tamu.edu/davis/suitesparse.html>
 * ZLIB
 * ZOLTAN
 * METIS, Serial Graph Partitioning
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies

-- The following REQUIRED packages have been found:

 * dune-common
 * precice (required version >= 2)
 * dumux
 * dune-grid
 * dune-localfunctions
 * dune-istl
 * dune-geometry
 * boost_chrono (required version == 1.78.0)
 * boost_atomic (required version == 1.78.0)
 * boost_filesystem (required version == 1.78.0)
 * boost_regex (required version == 1.78.0)
 * Threads, Multi-threading library
 * boost_thread (required version == 1.78.0)
 * boost_headers (required version == 1.78.0)
 * boost_log (required version == 1.78.0)
 * boost_system (required version == 1.78.0)
 * Boost (required version >= 1.65.1)

-- The following OPTIONAL packages have not been found:

 * opm-common
 * opm-grid
 * dune-subgrid
 * dune-spgrid
 * dune-mmesh
 * dune-python
 * LATEX
 * LatexMk
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
 * Alberta (required version >= 3.0), An adaptive hierarchical finite element toolbox and grid manager
 * Psurface, Piecewise linear bijections between triangulated surfaces
 * AmiraMesh
 * ARPACKPP, ARPACK++
   C++ interface for ARPACK
 * SIONlib
 * DLMalloc
 * TBB, Intel's Threading Building Blocks
```

```
The following are some of the valid targets for this Makefile:
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_darcy_tests
build_ff-pm_tests
build_freeflow_tests
build_monolithic_tests
build_navierstokes_tests
build_naviewstokes_tests
build_partitioned_tests
build_precice_tests
build_quick_tests
build_stokes_tests
build_tests
clean_latex
doc
doxygen_install
headercheck
install_python
test_python
dumux-precice
ff-pm-3d
headercheck__dumux-precice_couplingadapter.hh
headercheck__dumux-precice_dumux-addon_multidomain_boundary_freeflowsolidenergy_couplingdata.hh
headercheck__dumux-precice_dumux-addon_multidomain_boundary_freeflowsolidenergy_couplingmanager.hh
headercheck__dumux-precice_dumux-addon_multidomain_boundary_stokesdarcy_couplingdata.hh
headercheck__dumux-precice_dumux-addon_multidomain_boundary_stokesdarcy_couplingmanager.hh
headercheck__dumux-precice_dumux-addon_multidomain_boundary_stokesdarcy_couplingmapper.hh
headercheck__dumux-precice_dumuxpreciceindexmapper.hh
headercheck__examples_ff-pm_monolithic_flow-over-step-3d_1pspatialparams.hh
headercheck__examples_ff-pm_monolithic_flow-over-step-3d_freeflowsubproblem.hh
headercheck__examples_ff-pm_monolithic_flow-over-step-3d_porousmediumsubproblem.hh
headercheck__examples_ff-pm_monolithic_flow-over-step-3d_properties.hh
headercheck__examples_ff-pm_monolithic_td-ff-pm-2d-chaabani_problem_darcy.hh
headercheck__examples_ff-pm_monolithic_td-ff-pm-2d-chaabani_problem_stokes.hh
headercheck__examples_ff-pm_monolithic_td-ff-pm-2d-chaabani_spatialparams.hh
headercheck__examples_ff-pm_partitioned_ff-pm-3d_1pspatialparams.hh
headercheck__examples_ff-pm_partitioned_ff-pm-3d_ffproblem-reversed.hh
headercheck__examples_ff-pm_partitioned_ff-pm-3d_pmproblem-reversed.hh
headercheck__examples_legacy_cht_common_dumuxpreciceindexwrapper.hh
headercheck__examples_legacy_cht_common_helperfunctions.hh
headercheck__examples_legacy_cht_common_preciceadapter.hh
headercheck__examples_legacy_cht_monolithic_problem_freeflow.hh
headercheck__examples_legacy_cht_monolithic_problem_heat.hh
headercheck__examples_legacy_cht_monolithic_spatialparams.hh
headercheck__test_monolithic_ff-pm-3d_problem_darcy_3d.hh
headercheck__test_monolithic_ff-pm-3d_problem_stokes_3d.hh
headercheck__test_monolithic_ff-pm-3d_spatialparams.hh
headercheck__test_monolithic_flow-over-square-2d_problem_darcy.hh
headercheck__test_monolithic_flow-over-square-2d_problem_stokes.hh
headercheck__test_monolithic_flow-over-square-2d_spatialparams.hh
headercheck__test_monolithic_flow-over-step-2d_1pspatialparams.hh
headercheck__test_monolithic_flow-over-step-2d_freeflowsubproblem.hh
headercheck__test_monolithic_flow-over-step-2d_porousmediumsubproblem.hh
headercheck__test_monolithic_flow-over-step-2d_properties.hh
headercheck__test_partitioned_flow-over-square-2d_1pspatialparams.hh
headercheck__test_partitioned_flow-over-square-2d_ffproblem-reversed.hh
headercheck__test_partitioned_flow-over-square-2d_pmproblem-reversed.hh
td-ff-pm-2d-chaabani
test_conjugate_heat_monolithic
test_conjugate_heat_monolithic_const_dt
test_ff_flow_over_square_2d
test_ff_pm_flow_over_square_2d
test_ff_reversed-3d
test_freeflow
test_pm_flow_over_square_2d
test_pm_reversed-3d
test_solidenergy
headercheck/dumux-precice/couplingadapter.hh.o
headercheck/dumux-precice/couplingadapter.hh.i
headercheck/dumux-precice/couplingadapter.hh.s
headercheck/dumux-precice/dumux-addon/multidomain/boundary/freeflowsolidenergy/couplingdata.hh.o
headercheck/dumux-precice/dumux-addon/multidomain/boundary/freeflowsolidenergy/couplingdata.hh.i
headercheck/dumux-precice/dumux-addon/multidomain/boundary/freeflowsolidenergy/couplingdata.hh.s
headercheck/dumux-precice/dumux-addon/multidomain/boundary/freeflowsolidenergy/couplingmanager.hh.o
headercheck/dumux-precice/dumux-addon/multidomain/boundary/freeflowsolidenergy/couplingmanager.hh.i
headercheck/dumux-precice/dumux-addon/multidomain/boundary/freeflowsolidenergy/couplingmanager.hh.s
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingdata.hh.o
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingdata.hh.i
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingdata.hh.s
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingmanager.hh.o
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingmanager.hh.i
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingmanager.hh.s
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingmapper.hh.o
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingmapper.hh.i
headercheck/dumux-precice/dumux-addon/multidomain/boundary/stokesdarcy/couplingmapper.hh.s
headercheck/dumux-precice/dumuxpreciceindexmapper.hh.o
headercheck/dumux-precice/dumuxpreciceindexmapper.hh.i
headercheck/dumux-precice/dumuxpreciceindexmapper.hh.s
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/1pspatialparams.hh.o
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/1pspatialparams.hh.i
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/1pspatialparams.hh.s
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/freeflowsubproblem.hh.o
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/freeflowsubproblem.hh.i
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/freeflowsubproblem.hh.s
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/porousmediumsubproblem.hh.o
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/porousmediumsubproblem.hh.i
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/porousmediumsubproblem.hh.s
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/properties.hh.o
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/properties.hh.i
headercheck/examples/ff-pm/monolithic/flow-over-step-3d/properties.hh.s
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/problem_darcy.hh.o
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/problem_darcy.hh.i
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/problem_darcy.hh.s
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/problem_stokes.hh.o
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/problem_stokes.hh.i
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/problem_stokes.hh.s
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/spatialparams.hh.o
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/spatialparams.hh.i
headercheck/examples/ff-pm/monolithic/td-ff-pm-2d-chaabani/spatialparams.hh.s
headercheck/examples/ff-pm/partitioned/ff-pm-3d/1pspatialparams.hh.o
headercheck/examples/ff-pm/partitioned/ff-pm-3d/1pspatialparams.hh.i
headercheck/examples/ff-pm/partitioned/ff-pm-3d/1pspatialparams.hh.s
headercheck/examples/ff-pm/partitioned/ff-pm-3d/ffproblem-reversed.hh.o
headercheck/examples/ff-pm/partitioned/ff-pm-3d/ffproblem-reversed.hh.i
headercheck/examples/ff-pm/partitioned/ff-pm-3d/ffproblem-reversed.hh.s
headercheck/examples/ff-pm/partitioned/ff-pm-3d/pmproblem-reversed.hh.o
headercheck/examples/ff-pm/partitioned/ff-pm-3d/pmproblem-reversed.hh.i
headercheck/examples/ff-pm/partitioned/ff-pm-3d/pmproblem-reversed.hh.s
headercheck/examples/legacy/cht/common/dumuxpreciceindexwrapper.hh.o
headercheck/examples/legacy/cht/common/dumuxpreciceindexwrapper.hh.i
headercheck/examples/legacy/cht/common/dumuxpreciceindexwrapper.hh.s
headercheck/examples/legacy/cht/common/helperfunctions.hh.o
headercheck/examples/legacy/cht/common/helperfunctions.hh.i
headercheck/examples/legacy/cht/common/helperfunctions.hh.s
headercheck/examples/legacy/cht/common/preciceadapter.hh.o
headercheck/examples/legacy/cht/common/preciceadapter.hh.i
headercheck/examples/legacy/cht/common/preciceadapter.hh.s
headercheck/examples/legacy/cht/monolithic/problem_freeflow.hh.o
headercheck/examples/legacy/cht/monolithic/problem_freeflow.hh.i
headercheck/examples/legacy/cht/monolithic/problem_freeflow.hh.s
headercheck/examples/legacy/cht/monolithic/problem_heat.hh.o
headercheck/examples/legacy/cht/monolithic/problem_heat.hh.i
headercheck/examples/legacy/cht/monolithic/problem_heat.hh.s
headercheck/examples/legacy/cht/monolithic/spatialparams.hh.o
headercheck/examples/legacy/cht/monolithic/spatialparams.hh.i
headercheck/examples/legacy/cht/monolithic/spatialparams.hh.s
headercheck/test/monolithic/ff-pm-3d/problem_darcy_3d.hh.o
headercheck/test/monolithic/ff-pm-3d/problem_darcy_3d.hh.i
headercheck/test/monolithic/ff-pm-3d/problem_darcy_3d.hh.s
headercheck/test/monolithic/ff-pm-3d/problem_stokes_3d.hh.o
headercheck/test/monolithic/ff-pm-3d/problem_stokes_3d.hh.i
headercheck/test/monolithic/ff-pm-3d/problem_stokes_3d.hh.s
headercheck/test/monolithic/ff-pm-3d/spatialparams.hh.o
headercheck/test/monolithic/ff-pm-3d/spatialparams.hh.i
headercheck/test/monolithic/ff-pm-3d/spatialparams.hh.s
headercheck/test/monolithic/flow-over-square-2d/problem_darcy.hh.o
headercheck/test/monolithic/flow-over-square-2d/problem_darcy.hh.i
headercheck/test/monolithic/flow-over-square-2d/problem_darcy.hh.s
headercheck/test/monolithic/flow-over-square-2d/problem_stokes.hh.o
headercheck/test/monolithic/flow-over-square-2d/problem_stokes.hh.i
headercheck/test/monolithic/flow-over-square-2d/problem_stokes.hh.s
headercheck/test/monolithic/flow-over-square-2d/spatialparams.hh.o
headercheck/test/monolithic/flow-over-square-2d/spatialparams.hh.i
headercheck/test/monolithic/flow-over-square-2d/spatialparams.hh.s
headercheck/test/monolithic/flow-over-step-2d/1pspatialparams.hh.o
headercheck/test/monolithic/flow-over-step-2d/1pspatialparams.hh.i
headercheck/test/monolithic/flow-over-step-2d/1pspatialparams.hh.s
headercheck/test/monolithic/flow-over-step-2d/freeflowsubproblem.hh.o
headercheck/test/monolithic/flow-over-step-2d/freeflowsubproblem.hh.i
headercheck/test/monolithic/flow-over-step-2d/freeflowsubproblem.hh.s
headercheck/test/monolithic/flow-over-step-2d/porousmediumsubproblem.hh.o
headercheck/test/monolithic/flow-over-step-2d/porousmediumsubproblem.hh.i
headercheck/test/monolithic/flow-over-step-2d/porousmediumsubproblem.hh.s
headercheck/test/monolithic/flow-over-step-2d/properties.hh.o
headercheck/test/monolithic/flow-over-step-2d/properties.hh.i
headercheck/test/monolithic/flow-over-step-2d/properties.hh.s
headercheck/test/partitioned/flow-over-square-2d/1pspatialparams.hh.o
headercheck/test/partitioned/flow-over-square-2d/1pspatialparams.hh.i
headercheck/test/partitioned/flow-over-square-2d/1pspatialparams.hh.s
headercheck/test/partitioned/flow-over-square-2d/ffproblem-reversed.hh.o
headercheck/test/partitioned/flow-over-square-2d/ffproblem-reversed.hh.i
headercheck/test/partitioned/flow-over-square-2d/ffproblem-reversed.hh.s
headercheck/test/partitioned/flow-over-square-2d/pmproblem-reversed.hh.o
headercheck/test/partitioned/flow-over-square-2d/pmproblem-reversed.hh.i
headercheck/test/partitioned/flow-over-square-2d/pmproblem-reversed.hh.s
```
