```
_base=multiprocess
pkgname=python-${_base}
pkgdesc="better multiprocessing and multithreading in python"
pkgver=0.70.12.2
pkgrel=1
url="https://github.com/uqfoundation/${_base}"
arch=('any')
license=(BSD)
depends=(python-dill)
makedepends=(python-setuptools)
# checkdepends=(python-pytest)
source=(${url}/archive/${_base}-${pkgver}.tar.gz)
sha512sums=('16b8dc6b34d4f48db2cce697c31f591815f34bc71560225d8461b15cb63bff7b35011d3075505f7c26053eba7b281fc57a3237260a20e2984e548e80400c3cdc')

export PYTHONPYCACHEPREFIX="${BUILDDIR}/${pkgname}/.cache/cpython/"

build() {
  cd "${_base}-${_base}-${pkgver}"
  python setup.py build
}

# check() {
#   cd "${_base}-${_base}-${pkgver}"
#   python setup.py install --root="${PWD}/tmp_install" --optimize=1 --skip-build
#   local _pyversion=$(python -c "import sys; print(sys.version[:3])")
#   PYTHONPATH="${PWD}/tmp_install$(python -c "import site; print(site.getsitepackages()[0])"):${PYTHONPATH}" python -m pytest "py${_pyversion}"
#   # PYTHONPATH="$PWD/build/lib/${_base}/" python -m pytest "py${_pyversion}"
# }

package() {
  cd "${_base}-${_base}-${pkgver}"
  export PYTHONHASHSEED=0
  python setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
```
