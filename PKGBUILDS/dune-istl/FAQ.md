When the package `dune-istl-2.7.1-1-x86_64.pkg.tar.zst` is already installed we have:

- [x] `include`
- [x] `lib`
- [x] `share`
  - [x] `doc`
  - [x] `dune-istl`
  - [x] `licenses`

```console
.
└── usr
    ├── include
    │   └── dune
    │       └── istl
    ├── lib
    │   ├── cmake
    │   │   └── dune-istl
    │   ├── dunecontrol
    │   │   └── dune-istl
    │   └── pkgconfig
    └── share
        ├── doc
        │   └── dune-istl
        ├── dune
        │   └── cmake
        ├── dune-istl
        └── licenses

24 directories, 1290 files
```

## Tests

OK

```console
all (the default if no target is provided)
clean
depend
edit_cache
install
install/local
install/strip
list_install_components
package
package_source
rebuild_cache
test
Continuous
ContinuousBuild
ContinuousConfigure
ContinuousCoverage
ContinuousMemCheck
ContinuousStart
ContinuousSubmit
ContinuousTest
ContinuousUpdate
Experimental
ExperimentalBuild
ExperimentalConfigure
ExperimentalCoverage
ExperimentalMemCheck
ExperimentalStart
ExperimentalSubmit
ExperimentalTest
ExperimentalUpdate
Nightly
NightlyBuild
NightlyConfigure
NightlyCoverage
NightlyMemCheck
NightlyMemoryCheck
NightlyStart
NightlySubmit
NightlyTest
NightlyUpdate
OUTPUT
build_quick_tests
build_tests
clean_latex
doc
doc_istl_tex
doc_istl_tex_clean
doxygen_install
headercheck
install_python
test_python
amgtest
arpackppsuperlutest
arpackpptest
bcrsassigntest
bcrsbuild
bcrsimplicitbuild
bcrsmatrixtest
bcrsnormtest
bvectortest
cgconditiontest
cholmodtest
complexmatrixtest
complexrhstest
dotproducttest
fastamg
fieldvectortest
galerkintest
graphtest
hierarchytest
iluildltest
inverseoperator2prectest
iotest
kamgtest
ldltest
matrixiteratortest
matrixmarkettest
matrixnormtest
matrixredisttest
matrixtest
matrixutilstest
mmtest
multirhstest
multitypeblockmatrixtest
multitypeblockvectortest
mv
overlappingschwarztest
pamg_comm_repart_test
pamgmmtest
pamgtest
poweriterationsuperlutest
poweriterationtest
preconditionerstest
pthreadamgtest
pthreaddirectamgtest
pthreaddirectfastamgtest
pthreaddirecttwoleveltest
pthreadfastamgtest
pthreadtwoleveltest
registrytest
scalarproductstest
scaledidmatrixtest
solveraborttest
solverfactorytest_DuneFieldVectorDuneLoopSIMDdouble41
solverfactorytest_DuneFieldVectorDuneLoopSIMDdouble42
solverfactorytest_DuneFieldVectorDuneLoopSIMDdouble43
solverfactorytest_DuneFieldVectordouble1
solverfactorytest_DuneFieldVectordouble2
solverfactorytest_DuneFieldVectordouble3
solverfactorytest_DuneFieldVectorfloat1
solverfactorytest_DuneFieldVectorfloat2
solverfactorytest_DuneFieldVectorfloat3
solverfactorytest_DuneFieldVectorstdcomplexdouble1
solverfactorytest_DuneFieldVectorstdcomplexdouble2
solverfactorytest_DuneFieldVectorstdcomplexdouble3
solverfactorytest_DuneFieldVectorstdcomplexfloat1
solverfactorytest_DuneFieldVectorstdcomplexfloat2
solverfactorytest_DuneFieldVectorstdcomplexfloat3
solverfactorytest_DuneLoopSIMDdouble4
solverfactorytest_double
solverfactorytest_float
solverfactorytest_stdcomplexdouble
solverfactorytest_stdcomplexfloat
solvertest
spqrtest
superluamgtest
superluctest
superlufastamgtest
superlustest
superlutest
superluztest
transfertest
twolevelmethodschwarztest
twolevelmethodtest
umfpackamgtest
umfpackfastamgtest
umfpacktest
vbvectortest
vectorcommtest
```

[](https://git.imp.fu-berlin.de/podlesny/dune-faultnetworks/-/blob/master/CMakeLists.txt)

```
[ 71%] Linking CXX executable solverfactorytest_float_direct
cd /tmp/makepkg/dune-istl/src/build-cmake/dune/istl/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/solverfactorytest_float_direct.dir/link.txt --verbose=1
/tmp/makepkg/dune-istl/src/build-cmake/CXX_compiler.sh -std=c++17  -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/solverfactorytest_float_direct.dir/solverfactorytest_float_direct.cc.o -o solverfactorytest_float_direct  /usr/lib/libdunecommon.so /usr/lib/libarpack.so /usr/lib/libsuperlu.so /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libtbb.so.2 /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so -pthread /usr/lib/libldl.so /usr/lib/libspqr.so /usr/lib/libumfpack.so /usr/lib/libcholmod.so /usr/lib/libamd.so /usr/lib/libcolamd.so /usr/lib/libcamd.so /usr/lib/libccolamd.so /usr/lib/libsuitesparseconfig.so /usr/lib/liblapack.so /usr/lib/libblas.so -lm /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -std=c++17 -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/solverfactorytest_float_direct.dir/solverfactorytest_float_direct.cc.o -o solverfactorytest_float_direct /usr/lib/libdunecommon.so /usr/lib/libarpack.so /usr/lib/libsuperlu.so /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libtbb.so.2 /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so -pthread /usr/lib/libldl.so /usr/lib/libspqr.so /usr/lib/libumfpack.so /usr/lib/libcholmod.so /usr/lib/libamd.so /usr/lib/libcolamd.so /usr/lib/libcamd.so /usr/lib/libccolamd.so /usr/lib/libsuitesparseconfig.so /usr/lib/liblapack.so /usr/lib/libblas.so -lm /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so
{standard input}: Assembler messages:
{standard input}:225823: Warning: end of file not at end of a line; newline inserted
{standard input}:226854: Error: unknown pseudo-op: `.uleb'
{standard input}:226730: Error: invalid operands (*UND* and .gcc_except_table sections) for `-'
g++: fatal error: Killed signal terminated program cc1plus
compilation terminated.
make[3]: *** [dune/istl/test/CMakeFiles/solverfactorytest_DuneFieldVectordouble2.dir/build.make:79: dune/istl/test/CMakeFiles/solverfactorytest_DuneFieldVectordouble2.dir/solverfactorytest_DuneFieldVectordouble2.cc.o] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-istl/src/build-cmake'
make[2]: *** [CMakeFiles/Makefile2:5292: dune/istl/test/CMakeFiles/solverfactorytest_DuneFieldVectordouble2.dir/all] Error 2
make[2]: *** Waiting for unfinished jobs....
```

```
[ 73%] Linking CXX executable multirhstest
cd /tmp/makepkg/dune-istl/src/build-cmake/dune/istl/test && /usr/bin/cmake -E cmake_link_script CMakeFiles/multirhstest.dir/link.txt --verbose=1
/tmp/makepkg/dune-istl/src/build-cmake/CXX_compiler.sh -std=c++17  -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/multirhstest.dir/multirhstest.cc.o -o multirhstest  /usr/lib/libdunecommon.so /usr/lib/libarpack.so /usr/lib/libsuperlu.so /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libtbb.so.2 /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so -pthread /usr/lib/libldl.so /usr/lib/libspqr.so /usr/lib/libumfpack.so /usr/lib/libcholmod.so /usr/lib/libamd.so /usr/lib/libcolamd.so /usr/lib/libcamd.so /usr/lib/libccolamd.so /usr/lib/libsuitesparseconfig.so /usr/lib/liblapack.so /usr/lib/libblas.so -lm /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so 
/usr/sbin/g++ -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions         -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security         -fstack-clash-protection -fcf-protection -Wp,-D_GLIBCXX_ASSERTIONS  -std=c++17 -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -Wl,-rpath -Wl,/usr/lib64 -Wl,-rpath -Wl,/usr/lib/openmpi -Wl,--enable-new-dtags -L/usr/lib -pthread CMakeFiles/multirhstest.dir/multirhstest.cc.o -o multirhstest /usr/lib/libdunecommon.so /usr/lib/libarpack.so /usr/lib/libsuperlu.so /usr/lib/libparmetis.so /usr/lib/libmetis.so /usr/lib/libtbb.so.2 /usr/lib/libgmpxx.so /usr/lib/liblapack.so /usr/lib/libblas.so -pthread /usr/lib/libldl.so /usr/lib/libspqr.so /usr/lib/libumfpack.so /usr/lib/libcholmod.so /usr/lib/libamd.so /usr/lib/libcolamd.so /usr/lib/libcamd.so /usr/lib/libccolamd.so /usr/lib/libsuitesparseconfig.so /usr/lib/liblapack.so /usr/lib/libblas.so -lm /usr/lib/openmpi/libmpi.so -lquadmath /usr/lib/libgmp.so
make[3]: Leaving directory '/tmp/makepkg/dune-istl/src/build-cmake'
[ 73%] Built target multirhstest
{standard input}: Assembler messages:
{standard input}:205112: Warning: end of file not at end of a line; newline inserted
{standard input}:206121: Error: expected comma after name `_ZN4Dune3Amg15MatrixHierarchyINS_13MatrixAdapterINS_10BCRSMatrixINS_11FieldMatrixIdLi1ELi1EEESaIS5_EEENS_11BlockVectorINS_11FieldVectorIdLi1EEESaISA_EEESC_EENS0_21SequentialInfor' in .size directive
g++: fatal error: Killed signal terminated program cc1plus
compilation terminated.
make[3]: *** [dune/istl/test/CMakeFiles/solverfactorytest_DuneFieldVectordouble1_direct.dir/build.make:79: dune/istl/test/CMakeFiles/solverfactorytest_DuneFieldVectordouble1_direct.dir/solverfactorytest_DuneFieldVectordouble1_direct.cc.o] Error 1
make[3]: Leaving directory '/tmp/makepkg/dune-istl/src/build-cmake'
```

[](https://fossies.org/diffs/dune-istl)
[](https://elib.uni-stuttgart.de/bitstream/11682/2605/1/TR_2006_08.pdf)
