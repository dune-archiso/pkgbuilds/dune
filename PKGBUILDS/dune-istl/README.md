## [`dune-istl`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-istl/PKGBUILD)

#### debian

- [](https://tracker.debian.org/pkg/dune-istl)
- [](https://salsa.debian.org/pjaap/dune-istl)
- [](https://salsa.debian.org/lisajuliafog/dune-istl)

```console
-- The following OPTIONAL packages have been found:
 * MPI, Message Passing Interface library
   Parallel programming on multiple processors
 * LatexMk
 * LATEX, Type setting system
   To generate the documentation
 * UnixCommands, Some common Unix commands
   To generate the documentation with LaTeX
 * Doxygen, Class documentation generator, <www.doxygen.org>
   To generate the class documentation from C++ sources
 * PkgConfig, Unified interface for querying installed libraries
   To find Dune module dependencies
 * LAPACK, fast linear algebra routines
 * GMP, GNU multi-precision library including the C++ bindings GMPxx
   Multi-precision quadrature rules, basis function evaluation etc.
 * TBB, Threading Building Blocks library
   Parallel programming on multi-core processors
 * Vc, C++ Vectorization library, <https://github.com/VcDevel/Vc>
   For use of SIMD instructions
 * PythonInterp (required version >= 3)
 * PythonLibs
 * ParMETIS
 * SuperLU, Supernodal LU
   Direct solver for linear system, based on LU decomposition
 * ARPACK, ARnoldi PACKage
   Solve large scale eigenvalue problems
 * ARPACKPP, ARPACK++
   C++ interface for ARPACK
 * SuiteSparse
 * Threads, Multi-threading library
-- The following REQUIRED packages have been found:
 * dune-common
-- The following OPTIONAL packages have not been found:
 * Sphinx, Documentation generator, <www.sphinx-doc.org>
   To generate the documentation from CMake and Python sources
 * QuadMath
 * Inkscape, converts SVG images, <www.inkscape.org>
   To generate the documentation with LaTeX
```

```
.BUILDINFO
.MTREE
.PKGINFO
usr/
usr/include/
usr/include/dune/
usr/include/dune/istl/
usr/include/dune/istl/allocator.hh
usr/include/dune/istl/basearray.hh
usr/include/dune/istl/bccsmatrix.hh
usr/include/dune/istl/bccsmatrixinitializer.hh
usr/include/dune/istl/bcrsmatrix.hh
usr/include/dune/istl/bdmatrix.hh
usr/include/dune/istl/blocklevel.hh
usr/include/dune/istl/btdmatrix.hh
usr/include/dune/istl/bvector.hh
usr/include/dune/istl/cholmod.hh
usr/include/dune/istl/colcompmatrix.hh
usr/include/dune/istl/common/
usr/include/dune/istl/common/counter.hh
usr/include/dune/istl/common/registry.hh
usr/include/dune/istl/eigenvalue/
usr/include/dune/istl/eigenvalue/arpackpp.hh
usr/include/dune/istl/eigenvalue/poweriteration.hh
usr/include/dune/istl/foreach.hh
usr/include/dune/istl/gsetc.hh
usr/include/dune/istl/ildl.hh
usr/include/dune/istl/ilu.hh
usr/include/dune/istl/ilusubdomainsolver.hh
usr/include/dune/istl/io.hh
usr/include/dune/istl/istlexception.hh
usr/include/dune/istl/ldl.hh
usr/include/dune/istl/matrix.hh
usr/include/dune/istl/matrixindexset.hh
usr/include/dune/istl/matrixmarket.hh
usr/include/dune/istl/matrixmatrix.hh
usr/include/dune/istl/matrixredistribute.hh
usr/include/dune/istl/matrixutils.hh
usr/include/dune/istl/multitypeblockmatrix.hh
usr/include/dune/istl/multitypeblockvector.hh
usr/include/dune/istl/novlpschwarz.hh
usr/include/dune/istl/operators.hh
usr/include/dune/istl/overlappingschwarz.hh
usr/include/dune/istl/owneroverlapcopy.hh
usr/include/dune/istl/paamg/
usr/include/dune/istl/paamg/aggregates.hh
usr/include/dune/istl/paamg/amg.hh
usr/include/dune/istl/paamg/combinedfunctor.hh
usr/include/dune/istl/paamg/construction.hh
usr/include/dune/istl/paamg/dependency.hh
usr/include/dune/istl/paamg/fastamg.hh
usr/include/dune/istl/paamg/fastamgsmoother.hh
usr/include/dune/istl/paamg/galerkin.hh
usr/include/dune/istl/paamg/globalaggregates.hh
usr/include/dune/istl/paamg/graph.hh
usr/include/dune/istl/paamg/graphcreator.hh
usr/include/dune/istl/paamg/hierarchy.hh
usr/include/dune/istl/paamg/indicescoarsener.hh
usr/include/dune/istl/paamg/kamg.hh
usr/include/dune/istl/paamg/matrixhierarchy.hh
usr/include/dune/istl/paamg/parameters.hh
usr/include/dune/istl/paamg/pinfo.hh
usr/include/dune/istl/paamg/properties.hh
usr/include/dune/istl/paamg/renumberer.hh
usr/include/dune/istl/paamg/smoother.hh
usr/include/dune/istl/paamg/test/
usr/include/dune/istl/paamg/test/anisotropic.hh
usr/include/dune/istl/paamg/transfer.hh
usr/include/dune/istl/paamg/twolevelmethod.hh
usr/include/dune/istl/preconditioner.hh
usr/include/dune/istl/preconditioners.hh
usr/include/dune/istl/repartition.hh
usr/include/dune/istl/scalarproducts.hh
usr/include/dune/istl/scaledidmatrix.hh
usr/include/dune/istl/schwarz.hh
usr/include/dune/istl/solver.hh
usr/include/dune/istl/solvercategory.hh
usr/include/dune/istl/solverfactory.hh
usr/include/dune/istl/solverregistry.hh
usr/include/dune/istl/solvers.hh
usr/include/dune/istl/solvertype.hh
usr/include/dune/istl/spqr.hh
usr/include/dune/istl/superlu.hh
usr/include/dune/istl/superlufunctions.hh
usr/include/dune/istl/supermatrix.hh
usr/include/dune/istl/test/
usr/include/dune/istl/test/laplacian.hh
usr/include/dune/istl/test/matrixtest.hh
usr/include/dune/istl/test/multirhstest.hh
usr/include/dune/istl/test/vectortest.hh
usr/include/dune/istl/umfpack.hh
usr/include/dune/istl/vbvector.hh
usr/include/dune/python/
usr/include/dune/python/istl/
usr/include/dune/python/istl/bcrsmatrix.hh
usr/include/dune/python/istl/bvector.hh
usr/include/dune/python/istl/iterator.hh
usr/include/dune/python/istl/matrixindexset.hh
usr/include/dune/python/istl/operators.hh
usr/include/dune/python/istl/preconditioners.hh
usr/include/dune/python/istl/slice.hh
usr/include/dune/python/istl/solvers.hh
usr/lib/
usr/lib/cmake/
usr/lib/cmake/dune-istl/
usr/lib/cmake/dune-istl/dune-istl-config-version.cmake
usr/lib/cmake/dune-istl/dune-istl-config.cmake
usr/lib/dunecontrol/
usr/lib/dunecontrol/dune-istl/
usr/lib/dunecontrol/dune-istl/dune.module
usr/lib/pkgconfig/
usr/lib/pkgconfig/dune-istl.pc
usr/share/
usr/share/doc/
usr/share/doc/dune-istl/
usr/share/doc/dune-istl/doxygen/
usr/share/doc/dune-istl/doxygen/index.html
usr/share/doc/dune-istl/istl.pdf
usr/share/dune/
usr/share/dune-istl/
usr/share/dune-istl/config.h.cmake
usr/share/dune/cmake/
usr/share/dune/cmake/modules/
usr/share/dune/cmake/modules/AddARPACKPPFlags.cmake
usr/share/dune/cmake/modules/AddSuperLUFlags.cmake
usr/share/dune/cmake/modules/DuneIstlMacros.cmake
usr/share/dune/cmake/modules/FindARPACK.cmake
usr/share/dune/cmake/modules/FindARPACKPP.cmake
usr/share/dune/cmake/modules/FindSuperLU.cmake
usr/share/licenses/
usr/share/licenses/dune-istl/
usr/share/licenses/dune-istl/LICENSE
```

```
usr/
usr/lib/
usr/lib/python3.9/
usr/lib/python3.9/site-packages/
usr/lib/python3.9/site-packages/dune/
usr/lib/python3.9/site-packages/dune/istl/
usr/lib/python3.9/site-packages/dune/istl/__init__.py
usr/lib/python3.9/site-packages/dune/istl/_istl.so
usr/lib/python3.9/site-packages/dune_istl-2.8.0-py3.9-nspkg.pth
usr/lib/python3.9/site-packages/dune_istl-2.8.0-py3.9.egg-info/
usr/lib/python3.9/site-packages/dune_istl-2.8.0-py3.9.egg-info/PKG-INFO
usr/lib/python3.9/site-packages/dune_istl-2.8.0-py3.9.egg-info/SOURCES.txt
usr/lib/python3.9/site-packages/dune_istl-2.8.0-py3.9.egg-info/dependency_links.txt
usr/lib/python3.9/site-packages/dune_istl-2.8.0-py3.9.egg-info/namespace_packages.txt
usr/lib/python3.9/site-packages/dune_istl-2.8.0-py3.9.egg-info/not-zip-safe
usr/lib/python3.9/site-packages/dune_istl-2.8.0-py3.9.egg-info/top_level.txt
```

[![Packaging status](https://repology.org/badge/vertical-allrepos/dune-istl.svg)](https://repology.org/project/dune-istl/versions)
