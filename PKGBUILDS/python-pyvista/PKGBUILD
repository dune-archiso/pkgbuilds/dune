_base=pyvista
pkgname=python-${_base}
pkgdesc="Easier Pythonic interface to VTK"
pkgver=0.43.2
pkgrel=1
arch=(x86_64)
url="https://github.com/${_base}/${_base}"
license=(MIT)
depends=(python-matplotlib python-pooch python-scooby vtk fmt openmpi verdict libxcursor
  glew jsoncpp ospray qt5-base opencascade openxr openvr ffmpeg hdf5-openmpi
  postgresql-libs netcdf pdal mariadb-libs liblas cgns adios2 libharu gl2ps)
makedepends=(python-build python-installer python-setuptools python-wheel)
checkdepends=(python-pytest xorg-server-xvfb python-meshio python-hypothesis
  python-trimesh ipython python-tqdm python-sympy python-imageio)
# python-ipywidgets python-trame python-trame-vtk python-trame-vuetify python-sphinx-gallery
# python-jupyter-server-proxy python-nest-asyncio | python-imageio-ffmpeg ttf-dejavu | pyembree
optdepends=('python-cmocean: colormaps for oceanography'
  'python-colorcet: for 256-color colormaps'
  'python-trame: for client and server-side rendering in Jupyter'
  'python-imageio: for '
  'python-meshio: for input/output for many mesh formats')
source=(${_base}-${pkgver}.tar.gz::${url}/archive/v${pkgver}.tar.gz)
sha512sums=('a220a6dd768cf0b62edf8fbb84206600344dc5203cdf87bc2d0cd75d4a99facf999b0058535837f83a0fc806c80d9fe457444a4c12be10ee33d39dd30a6858f7')

build() {
  cd ${_base}-${pkgver}
  python -m build --wheel --skip-dependency-check --no-isolation
}

check() {
  cd ${_base}-${pkgver}
  python -m venv --system-site-packages test-env
  test-env/bin/python -m installer dist/*.whl
  PYVISTA_OFF_SCREEN=True DISPLAY=:99.0 PYTHONPATH="${PWD}/build/lib/${_base}:${PYTHONPATH}" \
    xvfb-run test-env/bin/python -m pytest \
    --ignore=tests/plotting \
    -k 'not sample_over_line and not sample_over_multiple_lines and not nrrd_reader and not vtk_error_catche' \
    -vv
}

package() {
  cd ${_base}-${pkgver}
  PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python -m installer --destdir="${pkgdir}" dist/*.whl
  install -Dm 644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}"
}
