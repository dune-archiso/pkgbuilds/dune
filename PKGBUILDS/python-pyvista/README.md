- [GitHub](https://github.com/pyvista/pyvista)
- [Pypi](https://pypi.org/project/pyvista)

[Dependencies from Python bindings of VTK (Visualization Toolkit)](https://bugs.archlinux.org/task/64511)
[[vtk] support for python 3](https://bugs.archlinux.org/task/48113)
[Building VTK with python bindings in linux (arch)](https://ghoshbishakh.github.io/blogpost/2016/03/05/buid-vtk.html)
#### TODO

[Test examples](https://docs.pyvista.org)