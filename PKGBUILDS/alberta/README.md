## [`alberta`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/alberta/PKGBUILD) [![Packaging status](https://badgen.net/badge/AUR/Package/blue)](https://aur.archlinux.org/packages/alberta)

[![alberta](https://img.shields.io/aur/version/alberta?color=1793d1&label=alberta&logo=arch-linux&style=for-the-badge)](https://aur.archlinux.org/packages/alberta)

[![AUR version](https://img.shields.io/aur/version/alberta?logo=Arch%20Linux&color=brightgreen)](https://aur.archlinux.org/packages/alberta)

[![Packaging status](https://repology.org/badge/vertical-allrepos/alberta-fem.svg)](https://repology.org/project/alberta-fem/versions)

[![Packaging status](https://repology.org/badge/vertical-allrepos/alberta.svg)](https://repology.org/project/alberta/versions)
