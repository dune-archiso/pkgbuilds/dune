# Maintainer: Carlos Aznarán <caznaranl@uni.pe>
_base=py-modelrunner
pkgname=python-${_base}
pkgdesc="Python classes for organizing (HPC) simulations"
pkgver=0.15.0
pkgrel=1
arch=(any)
url="https://github.com/zwicker-group/${_base}"
license=(MIT)
depends=(python-jinja python-numpy python-tqdm python-numcodecs)
makedepends=(python-build python-installer python-setuptools-scm python-wheel)
checkdepends=(python-pytest)
optdepends=('python-h5py: storing data in the hierarchical file format'
  'python-mpi4py: parallel processing using MPI'
  'python-pandas: handling tabular data'
  'python-yaml: '
  'python-zarr: ')
source=(${_base}-${pkgver}.tar.gz::${url}/archive/${pkgver}.tar.gz)
sha512sums=('668e7c481e1c65179c36c1e9d9397432e84abeeb4b58c50fd7bb0a85bd1a4ba3bd969ebab1279a54799893c5bd4a1f76e5c6eedbc84ffac8312327f73fd7162a')

build() {
  cd ${_base}-${pkgver}
  export SETUPTOOLS_SCM_PRETEND_VERSION=${pkgver}
  python -m build --wheel --skip-dependency-check --no-isolation
}

check() {
  cd ${_base}-${pkgver}
  python -m venv --system-site-packages test-env
  test-env/bin/python -m installer dist/*.whl
  test-env/bin/python -m pytest
}

package() {
  cd ${_base}-${pkgver}
  PYTHONPYCACHEPREFIX="${PWD}/.cache/cpython/" python -m installer --destdir="${pkgdir}" dist/*.whl

  # Symlink license file
  local site_packages=$(python -c "import site; print(site.getsitepackages()[0])")
  install -d ${pkgdir}/usr/share/licenses/${pkgname}
  ln -s "${site_packages}/${_base/-/_}-${pkgver}.dist-info/LICENSE" \
    "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}
