## [PKGBUILDS](https://gitlab.com/dune-archiso/pkgbuilds)

Collection of PKGBUILDs based on [existing](https://aur.archlinux.org/packages/?O=0&SeB=nd&K=dune-&outdated=&SB=n&SO=a&PP=50&do_Search=Go) ones from the [Arch User Repository](https://wiki.archlinux.org/title/Arch_User_Repository).

## Packages

- `alberta`
- `arpack++`
- `dune-core`
- `dune-disc`
- `dune-extension`
- `dune-grid`
- `hdnum-git`
- `psurface`
- `sionlib`
- `dune-tutorial`
